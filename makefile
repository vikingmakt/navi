default_target: help

.PHONY: default_target

build: init
	cd build && make

clean:
	@rm -rf build

help:
	@echo "Usage:"
	@echo "build        - Build all targets"
	@echo "clean        - Delete all build files"
	@echo "localinstall - Install Navi at User's bin"

init:
	@doas pkg_add git cmake sdl2 sdl2-gfx sdl2-image sdl2-mixer sdl2-net sdl2-ttf 
	@git submodule update --init
	@mkdir -p build
	@cmake . -DCMAKE_CXX_COMPILER=clang++ -B build

localinstall:
	@mkdir -p ~/bin
	@cd build/bin && cp Player ~/bin/navi

.PHONY: build help
