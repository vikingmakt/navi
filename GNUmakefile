SHELL = /bin/bash
NAVI_MODULES  = Async Data Encode Lua Memory Network Ui
ROOT_DIR = $(shell pwd)

default_target: help

.PHONY: default_target

build: init
	cd build && $(MAKE)

help:
	@echo "Usage:"
	@echo "make init         - Init submodules and run cmake"
	@echo "make build        - Build all targets"
	@echo "make editor       - Build and run Editor"
	@echo "make vmmsg_tester - Build and run the VM message tester"
	@echo "make test         - Run all tests"
	@echo "make retest       - Run last failed test"

init:
	@git submodule update --init
	@mkdir -p build
	@cmake . -DCMAKE_CXX_COMPILER=${CXX} -B build

editor: player
	build/src/Player/Player editor/Main.scene.lua

player:
	cd build && $(MAKE) Player

retest: build
	@for mod in $(NAVI_MODULES); do \
		if [ -f build/src/Navi/$$mod/Test/Testing/Temporary/LastTestsFailed.log ]; then \
			cd $(ROOT_DIR); \
			echo "RERUNNING TESTS IN MODULE $$mod"; \
			cd build/src/Navi/$$mod/Test && ctest -I Testing/Temporary/LastTestsFailed.log -V && rm -rf Testing/Temporary || true; \
		fi \
	done

test: build
	@for mod in $(NAVI_MODULES); do \
		cd $(ROOT_DIR); \
		echo "RUNNING TESTS IN MODULE $$mod"; \
		cd build/src/Navi/$$mod/Test && rm -rf Testing/Temporary && ctest -V || true; \
	done

tests: test

vmmsg_tester: player
	build/src/Player/Player vmmsg_tester/Main.scene.lua

.PHONY: build player editor test tests retest help
