import os

def get_filenames(path, files):
    exc = "src/"
    for fn in os.listdir(path):
        fullfn = '/'.join([path, fn])
        if os.path.isdir(fullfn):
            get_filenames(fullfn, files)
        else:
            files.append(fullfn[len(exc):])

def include_file(filename):
    return (
        '.cpp' in filename and 'unitybuild' not in filename and 'Lua' not in filename and 'Test' not in filename
    )

if __name__ == '__main__':
    fh = open("src/Navi/unitybuild.cpp", "w")
    fh.write("// Auto-generated file by unitybuild.py, do not edit!\n")

    filenames = []
    get_filenames("src/Navi", filenames)
    for filename in filenames:
        if include_file(filename):
            fh.write('#include "%s"\n' % filename)
    fh.close()