local console = {}

function console.debug(...)
    local args = table.pack(...)
    _console_debug(args)
end

function console.info(...)
    local args = table.pack(...)
    _console_info(args)
end

function console.error(...)
    local args = table.pack(...)
    _console_error(args)
end

function console.exec(cmd)
    _console_exec(cmd)
end

function console.get_var(name)
    return _console_get_var(name)
end

function console.set_var(name, value)
    _console_set_var(name, value)
end

function console.register_command()

end

return console