local func = {}

function func.bind(...)
    local args = table.pack(...)
    local fn = table.remove(args, 1)
    return function(...)
        local rargs = table.pack(...)
        return fn(table.unpack(table.concat(args, rargs)))
    end
end