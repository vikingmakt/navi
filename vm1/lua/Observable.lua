local Observable = {}

local function get_observable(t, k)
    return rawget(t, 'O_'..k)
end

local function set_observable(t, k, obs)
    rawset(t, 'O_'..k, obs)
end

local function create_observable(v)
    return ObservableBind(v)
end

local _meta = {}
local meta = {
    __index = function(self, k)
        local obs = get_observable(self, k)
        if not obs then
            obs = create_observable(nil)
            set_observable(self, k, obs)
        end
        return obs
    end,

    __newindex = function(self, k, v)
        local obs = get_observable(self, k)
        if not obs then
            obs = create_observable(v)
            set_observable(self, k, obs)
        end

        if type(v) == 'userdata' and v.value_changed then
            void(v >> function(vv) obs:set_value(vv) end)
        elseif type(v) == 'userdata' and v.is_reference then
            obs:bind_reference(v)
        else
            obs:set_value(v)
        end
    end,

    __pairs = function(self)
        -- Iterator function takes the table and an index and returns the next index and associated value
        -- or nil to end iteration

        local function stateless_iter(self, k)
            local v
            -- Implement your own key,value selection logic in place of next

            local key = k
            if key then
                key = 'O_' .. key
            end

            k, v = next(self, key)
            if k and k:sub(1,2) == 'O_' and v then
                return k:sub(3), v
            end

            return nil
        end

        -- Return an iterator function, the table, starting point
        return stateless_iter, self, nil
    end,
}
_meta = meta

function Observable.table(data, parent)
    local cpy = {}

    setmetatable(cpy, meta)
    for k,v in pairs(data) do
        cpy[k] = v
    end

    return cpy
end

function Observable.assign(obsTable, newTable)
    for k,v in pairs(newTable) do
        obsTable[k] = v
    end
end

function Observable.new(v)
    return ObservableBind(v)
end

function Observable.proxy(org)
    local prx = {
        __proxy = true,
        org = org,
        obs = Observable.table(org),
    }

    for k,v in pairs(prx.obs) do
        void(v >> function(vv)
            print('proxy set', k, vv)
            prx.org[k] = vv
        end)
    end

    return prx.obs
end

function Observable.set_proxy_target(proxy, target)
    proxy.org = target
end

function Observable.clear_bindings(obs)
    if type(obs) == 'table' then
        if obs.__proxy then
            Observable.clear_bindings(obs.obs)
        else
            rawset(obs, '__table_listeners', nil)
            for k,v in pairs(obs) do
                _clear_observable_bindings(v)
            end
        end
    else
        _clear_observable_bindings(obs)
    end
end

function Observable.reflect(obsa, obsb)
    _reflect_observable(obsa, obsb)
end

function Observable.when_any(obs)
    local values = {}
    for _,o in pairs(obs) do
        table.insert(values, ~o)
    end

    local shared = ObservableBind(values)
    for i,o in ipairs(obs) do
        void(o >> function(v)
            local values = ~shared
            values[i] = v
            shared:set_value(values)
        end)
    end
    return shared
end

-- Global ObservableReference constructor, usage: _& myObservable
_ = {}
setmetatable(_, {
    __band = function(self, obs)
        return obs:reference()
    end,
})

return Observable