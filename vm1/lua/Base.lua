console = require('vm1/lua/console')
scene = require('vm1/lua/scene')
observable = require('vm1/lua/observable')
lens = require('vm1/lua/lens')
animation = require('vm1/lua/animation')

Button              = scene.widget_node('Button')
CollapsingHeader    = scene.widget_node('CollapsingHeader')
Combo               = scene.widget_node('Combo')
HorizontalLayout    = scene.widget_node('HorizontalLayout')
Image               = scene.widget_node('Image')
InputText           = scene.widget_node('InputText')
Label               = scene.widget_node('Label')
ListView            = scene.widget_node('LuaListView')
Menu                = scene.widget_node('Menu')
MenuBar             = scene.widget_node('MenuBar')
MenuItem            = scene.widget_node('MenuItem')
SceneFactory        = scene.widget_node('sceneFactory')
SceneInstance       = scene.widget_node('sceneInstance')
Separator           = scene.widget_node('Separator')
SliderFloat         = scene.widget_node('SliderFloat')
SliderInt           = scene.widget_node('SliderInt')
StyleLoader         = scene.widget_node('StyleLoader')
TreeView            = scene.widget_node('TreeView')
VerticalLayout      = scene.widget_node('VerticalLayout')
Window              = scene.widget_node('Window')
Widget              = scene.widget_node('Widget')

Window = scene.node_builder{
    native_type = 'UserWidget',
    props = {
        prop.list(),
        prop.flags(),
        prop.widget_id('login_screen'),
        prop.callback_list('on_click'),
    },
    methods = {
        translate = _window_translate
    },
}

function login_screen_msg_received(self, sender, event_type, event_data)
    if sender.id == 'login_btn' and event_type == 'pressed' then
        
    end
end

function login_screen(self, props)
    client.msg_received:push(self)
end

return {
    props = props,
    setup = login_screen,
}

Template{id='userlist_item', Label{id='userlist_name'}},
ListView{id='list', template_id='', on_bind={'#login_screen.bind_userlist_item'}}
LoginScreen{id='login_screen'},
UtosClient{id='utos_client', host='', port=64, msg_received='#login_screen.msg_received'}
Button{on_click={'#login_screen.login_btn_pressed'}}
Button{on_click={'#login_screen.login_btn_pressed'}}

function void(...)
end