local lens = {}

function lens.identity(v)
    return v
end

--- The `get` function returns a function that lookup a member of the table received from the Observable
function lens.get(prop)
    return function(v)
        return v[prop]
    end
end

--- The `suffix` function takes a string and returns a function that suffixes the string received from the Observable
function lens.suffix(str)
    return function(v)
        return v .. str
    end
end

--- The `suffix` function takes a string and returns a function that prefixes the string received from the Observable
function lens.prefix(str)
    return function(v)
        return str .. v
    end
end

--- The `head` function the first item in a table, or nil if the table is empty
function lens.head(ls)
    return ls[1]
end

--- The `maybe` function takes a default value, and a function. If the value received from the Observable is non-truthy,
--  the function returns the default value. Otherwise, it applies the function to the value inside the Observable and returns the result.
function lens.maybe(defval, func)
    return function(v)
        if v then
            return func(v)
        end

        return defval
    end
end

function lens.unpack(func)
    return function(values)
        return func(table.unpack(values))
    end
end

return lens