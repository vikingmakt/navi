local Scene = {}

local function parse_widget_properties(widget, props)
    for k,v in pairs(props) do
        if type(k) == 'string' then
            widget[k] = v
        end
    end
end

local function eval_widget(data, parent)
    if type(data) == 'string' then
        local widget = parent:make_child(hash('Label'))
        parse_widget_properties(widget, {text = data})
        return
    end

    local widget = parent:make_child(data.type)
    parse_widget_properties(widget, data.props)
    for k,v in ipairs(data.props) do
        eval_widget(v, widget)
    end

    if data.props.children then
        for k,v in ipairs(data.props.children) do
            eval_widget(v, widget)
        end
    end
end

function Scene.eval_scene_tree(tree)
    local scene = SceneBind()
    eval_widget(tree, scene.root)
    return scene
end

function Scene.widget_node(nodeType)
    local typeHash = hash(nodeType)
    return function(props)
        return {
            type = typeHash,
            typeName = nodeType,
            props = props,
        }
    end
end

function Scene.widget_set_prop(widget, prop, value)
    _widget_set_prop(widget, prop, value)
end

function Widget(def)
    return function(props)
        local w = {bind = _widget_make(def.typename, props)}
        local meta = {
            __index = function(self, k)
                return _widget_get_prop(self.bind, def.prop_indices[k])
            end,

            __setindex = function(self, k, v)
                _widget_set_prop(self.bind, def.prop_indices[k], v)
            end,
        }
        setmetatable(w, meta)
        return w
    end
end

Window = Widget{
    classname = 'Window',
    native = widget_native(0),
    props = {
        prop.vector2('position', Vector2(0, 0)),
        prop.vector2('size', Vector2(0, 0)),
    },
}

return Scene