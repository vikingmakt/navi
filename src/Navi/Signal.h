#ifndef Navi_SignalEmitter_h
#define Navi_SignalEmitter_h

#include <queue>
#include <Corrade/Interconnect/Emitter.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Data/Tuple.h"
#include "Navi/Navi.h"

namespace Navi {

template<class Arg, bool Deferred = true> class SignalDetail;

namespace Implementation {

template<class Arg> class SignalImpl : public Interconnect::Emitter {
    public:
        /// Default constructor
        SignalImpl() : Interconnect::Emitter{} {}
        /// Copying is not allowed
        SignalImpl(const SignalImpl&) = delete;
        /// Moving is not allowed
        SignalImpl(SignalImpl&& other) = delete;

        /// Copying is not allowed
        SignalImpl& operator =(const SignalImpl&) = delete;
        /// Moving is not allowed
        SignalImpl& operator =(SignalImpl&& other) = delete;

        template<class Receiver> Interconnect::Connection connect(Receiver& r, void (Receiver::*handler)(Arg)) {
            using SelfType = SignalImpl<Arg>;
            return Interconnect::connect(
                *this, &SelfType::emitSignal, static_cast<Receiver&>(r), handler
            );
        }
        template<class Functor> Interconnect::Connection connect(Functor&& slot) {
            return Interconnect::connect(*this, &SignalImpl<Arg>::emitSignal, slot);
        }

        void disconnect(const Interconnect::Connection& conn) {
            Interconnect::disconnect(*this, conn);
        }

        Interconnect::Emitter::Signal emitSignal(Arg arg) {
            return emit(&SignalImpl<Arg>::emitSignal, arg);
        }
};

template<class Arg, bool Deferred> class SignalEmitter {
    public:
        void emit(SignalDetail<Arg, false>& sig, Arg arg) {
        }

        void dispatch(SignalDetail<Arg, false>& sig) {
        }
};

}

template<class Arg, bool Deferred> class SignalDetail : public Interconnect::Receiver {
    public:
        /// Default constructor
        SignalDetail() : _impl{} {}
        /// Copying is not allowed
        SignalDetail(const SignalDetail&) = delete;
        /// Moving is not allowed
        SignalDetail(SignalDetail&& other) = delete;

        /// Copying is not allowed
        SignalDetail& operator =(const SignalDetail&) = delete;
        /// Moving is not allowed
        SignalDetail& operator =(SignalDetail&& other) = delete;

        template<class Receiver> Interconnect::Connection connect(Receiver& r, void (Receiver::*handler)(Arg)) {
            return _impl.connect(r, handler);
        }
        template<class Functor> Interconnect::Connection connect(Functor&& slot) {
            return _impl.connect(slot);
        }

        void disconnectAll() {
            _impl.disconnectAllSignals();
        }

        void disconnect(const Interconnect::Connection& conn) {
            _impl.disconnect(conn);
        }

        void emit(Arg arg) {
            _emitter.emit(*this, arg);
        }

        void dispatch() {
            _emitter.dispatch(*this);
        }

    private:
        template<class A, bool D> friend class Navi::Implementation::SignalEmitter;

        Navi::Implementation::SignalImpl<Arg> _impl;
        Navi::Implementation::SignalEmitter<Arg, Deferred> _emitter;
};

template<class Arg>
using Signal = SignalDetail<Arg, false>;

template<class Arg>
using DeferredSignal = SignalDetail<Arg, true>;

namespace Implementation {
template<class Arg> class SignalEmitter<Arg, false> {
    public:
        void emit(SignalDetail<Arg, false>& sig, Arg arg) {
            sig._impl.emitSignal(arg);
        }

        void dispatch(SignalDetail<Arg, false>& sig) {
        }
};
template<class Arg> class SignalEmitter<Arg, true> {
    public:
        void emit(SignalDetail<Arg, true>& sig, Arg arg) {
            _calls.push(arg);
        }

        void dispatch(SignalDetail<Arg, true>& sig) {
            while (!_calls.empty()) {
                auto arg = _calls.front();
                _calls.pop();

                sig._impl.emitSignal(arg);
            }
        }

    private:
        std::queue<Arg> _calls;
};
}

}

#endif
