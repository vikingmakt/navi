#ifndef Navi_Resource_PakFile_h
#define Navi_Resource_PakFile_h

#include <unordered_map>
#include <vector>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Resource/MemoryFile.h"

namespace Navi::Resource {

struct PakHeader {
    unsigned int version;
    unsigned int tocSize;
};

struct PakFileEntry {
    char name[256];
    char checksum[32];
    unsigned int offset;
    unsigned int size;
};

class PakFile {
    public:
        /// PakFile constructor for writing
        explicit PakFile();

        /// PakFile constructor for reading
        explicit PakFile(Containers::Pointer<AbstractFile>&& file);

        /// Destructor
        ~PakFile();

        /// Returns whether the pak contains the file with the specified name
        bool hasFile(const std::string& name) const;

        /// Reads a file contained in the pak
        Containers::Pointer<AbstractFile> openFile(const std::string& name);

        /// Adds a file to the pak
        void addFile(const std::string& name, Containers::Pointer<AbstractFile>&& file);

        /// Write the pak to the disk
        bool write();

        /// Sets the path of the pak file
        void setPath(const std::string& dest);

        /// Clears all files in this pak file
        void clear();

        /// Pak file path
        const std::string& path() const { return _file->path(); }

        /// File entries in this pak
        std::vector<PakFileEntry>& files() { return _files; }

        /// File objects entries in this pak
        std::vector<Containers::Pointer<AbstractFile>>& realFiles() { return _realFiles; }

    private:
        Containers::Pointer<AbstractFile> _file;
        std::vector<PakFileEntry> _files;
        std::vector<Containers::Pointer<AbstractFile>> _realFiles;
        std::unordered_map<std::string, unsigned int> _fileMap;
        PakHeader _header;
        bool _read;
};

}

#endif
