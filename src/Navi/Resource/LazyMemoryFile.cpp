#include <cstring>
#include <sstream>
#include <Corrade/Containers/Tags.h>
#include "Navi/Resource/LazyMemoryFile.h"

namespace Navi::Resource {

LazyMemoryFile::LazyMemoryFile(AbstractFile& realFile, unsigned int offset, unsigned int size, const std::string& path)
    : AbstractFile{ path }
    , _realFile{ &realFile }
    , _offset{ offset }
    , _size{ size } {
}

Containers::Array<unsigned char> LazyMemoryFile::doReadBytes() {
    Containers::Array<unsigned char> res{ Containers::NoInit, _size };
    
    auto handle = _realFile->open(FileOpenMode::Read);
    handle->seek(_offset);
    handle->read(_size, res.data());
    return res;
}

Containers::Pointer<std::istream> LazyMemoryFile::doIstream() {
    auto data = readBytes();
    return Containers::pointer<std::istream>(
        static_cast<std::istream*>(new std::istringstream{ std::string{ (const char*)data.data(), data.size() } })
    );
}

}
