#include <cstring>
#include "Navi/Resource/PakFile.h"
#include "Navi/Resource/FileSystemFile.h"
#include "Navi/Resource/LazyMemoryFile.h"
#include "Navi/Data/Serializer.hpp"

namespace Navi::Resource {

namespace {
constexpr unsigned int kHeaderSize = (
    6 + sizeof(unsigned int) + sizeof(unsigned int)
);

constexpr unsigned int kFileEntrySize = (
    256 + 32 + sizeof(unsigned int) + sizeof(unsigned int)
);
}

PakFile::PakFile() {
    _file = Containers::pointer<FileSystemFile>("");
    _read = false;
}

PakFile::PakFile(Containers::Pointer<AbstractFile>&& file) : _file{ std::move(file) } {
    _read = false;
    if (!_file->exists()) return;

    auto handle = _file->open(FileOpenMode::ReadBinary);
    auto se = Data::serializer(*handle);

    {
        char idData[6];
        se.read(6, idData);
        if (std::memcmp(idData, "VMPACK", 6))
            return;
    }

    _header.version = se.readUInt();
    _header.tocSize = se.readUInt();

    std::size_t numFiles = _header.tocSize / kFileEntrySize;
    for (std::size_t i = 0; i < numFiles; i++) {
        _files.emplace_back();
        _realFiles.emplace_back();

        PakFileEntry& file = _files.back();
        se.read(sizeof(file.name), file.name);
        se.read(sizeof(file.checksum), file.checksum);
        file.offset = se.readUInt();
        file.size = se.readUInt();

        _realFiles.back() = Containers::pointer<LazyMemoryFile>(*_file, file.offset, file.size, std::string{ file.name });
        //file.dataToWrite = nullptr;
        
        _fileMap[file.name] = i;
    }

    _read = true;
}

PakFile::~PakFile() {
}

bool PakFile::hasFile(const std::string& name) const {
    return _fileMap.find(name) != _fileMap.end();
}

Containers::Pointer<AbstractFile> PakFile::openFile(const std::string& name) {
    auto it = _fileMap.find(name);
    if (it == _fileMap.end())
        return nullptr;

    const PakFileEntry& entry = _files[it->second];
    return Containers::pointer<LazyMemoryFile>(*_file, entry.offset, entry.size, std::string{ entry.name });
}

void PakFile::addFile(const std::string& name, Containers::Pointer<AbstractFile>&& file) {
    // TODO: checksum
    PakFileEntry entry = {};
    std::memcpy((void*)entry.name, name.c_str(), name.size()+1);

    auto it = _fileMap.find(name);
    if (it != _fileMap.end()) {
        _files[it->second] = entry;
        _realFiles[it->second] = std::move(file);
        return;
    }
    
    _files.push_back(entry);
    _realFiles.push_back(std::move(file));
    _fileMap[name] = _files.size() - 1;
}

void PakFile::setPath(const std::string& dest) {
    _file->setPath(dest);
}

bool PakFile::write() {
    auto handle = _file->open(FileOpenMode::WriteBinary);
    if (!handle)
        return false;

    _header.version = 0;
    _header.tocSize = _files.size() * kFileEntrySize;

    auto serializer = Data::serializer(*handle);
    serializer.write(6, "VMPACK");
    serializer.writeUInt(_header.version);
    serializer.writeUInt(_header.tocSize);

    {
        std::vector<Containers::Array<unsigned char>> data;
        for (std::size_t i = 0; i < _files.size(); i++) {
            data.push_back(_realFiles[i]->readBytes());
        }

        unsigned int offset = kHeaderSize + _header.tocSize;
        for (std::size_t i = 0; i < _files.size(); i++) {
            auto& entry = _files[i];
            entry.offset = offset;
            entry.size = data[i].size();

            serializer.write(sizeof(entry.name), entry.name);
            serializer.write(sizeof(entry.checksum), entry.checksum);
            serializer.writeUInt(entry.offset);
            serializer.writeUInt(entry.size);
            offset += entry.size;
        }

        for (std::size_t i = 0; i < _files.size(); i++) {
            serializer.write(data[i].size(), (const char*)data[i].data());
        }
    }

    return true;
}

void PakFile::clear() {
    _files.clear();
    _realFiles.clear();
    _fileMap.clear();
}

}
