#include "Navi/Resource/FileWatcher.h"
#include "Navi/Application.h"

namespace Navi::Resource {

FileWatcher::FileWatcher(Containers::Pointer<AbstractFile>&& file, float interval, Application& app)
    : _file{ std::move(file) }, _interval{ interval } {

    app.tick().connect(*this, &FileWatcher::tick);

    _prevModifiedTime = _file->modifiedTime();
}

FileWatcher::~FileWatcher() {
}

std::string FileWatcher::path() {
    return _file->path();
}

void FileWatcher::setPath(const std::string& path) {
    _file->setPath(path);
    _prevModifiedTime = _file->modifiedTime();
}

void FileWatcher::tick(float dt) {
    if (!_enabled) return;

    if (_timer <= 0.0f) {
        auto modTime = _file->modifiedTime();
        if (modTime != _prevModifiedTime) {
            _changed.emit(modTime);
            _prevModifiedTime = modTime;
        }
        _timer = _interval;
    }

    _timer -= dt;
}

}