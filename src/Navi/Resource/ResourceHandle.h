#ifndef Navi_Resource_ResourceHandle_h
#define Navi_Resource_ResourceHandle_h

#include <Corrade/Containers/EnumSet.h>
#include "Navi/Resource/AbstractResource.h"

namespace Navi::Data { class Variant; }

namespace Navi::Resource {

class ResourceManager;

enum class ResourceHandleFlag : unsigned int {
    None = 0,
    Cached = 1,
};
typedef Containers::EnumSet<ResourceHandleFlag> ResourceHandleFlags;
CORRADE_ENUMSET_OPERATORS(ResourceHandleFlags);

template<class T>
class ResourceHandle {
    public:
        explicit ResourceHandle() : _resource{ nullptr }, _flags{ ResourceHandleFlag::None } {}

        explicit ResourceHandle(T& res, ResourceHandleFlags flags = ResourceHandleFlag::None) : _resource{ &res }, _flags{ flags } {
            _resource->incRef();
        }

        ResourceHandle(const ResourceHandle& other) { *this = other; }

        ResourceHandle(ResourceHandle&& other) { *this = std::move(other); }

        ResourceHandle& operator =(const ResourceHandle& other) {
            if (_resource)
                _resource->decRef();
            _resource = other._resource;
            _flags = other._flags;
            _resource->incRef();
            return *this;
        }

        ResourceHandle& operator =(ResourceHandle&& other) {
            if (_resource)
                _resource->decRef();
            _resource = other._resource;
            _flags = other._flags;
            other._resource = nullptr;
            other._flags = ResourceHandleFlag::None;
            return *this;
        }

        ~ResourceHandle() {
            if (_resource)
                _resource->decRef();
        }

        bool valid() const { return _resource != nullptr; }

        bool loaded() const { return _resource->state() == ResourceState::Loaded; }

        bool cached() const { return _flags & ResourceHandleFlag::Cached; }

        operator bool() const { return valid(); }

        T& resource() { return *static_cast<T*>(_resource); }

        void release() { 
            if (_resource)
                _resource->decRef();
            
            _resource = nullptr;
        }

    private:
        friend class Data::Variant;

        AbstractResource* _resource = nullptr;
        ResourceHandleFlags _flags = ResourceHandleFlag::None;
};

}

#endif