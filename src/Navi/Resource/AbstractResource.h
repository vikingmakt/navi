#ifndef Navi_Resource_AbstractResource_h
#define Navi_Resource_AbstractResource_h

#include <vector>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Signal.h"
#include "Navi/Resource/AbstractFile.h"

namespace Navi::Resource {

class ResourceManager;

enum class ResourceState {
    Unknown,
    Ok,
    Loading,
    Loaded,
    Error,
};

class AbstractResource {
    public:
        explicit AbstractResource(const std::string& id) : _id{id}, _refs{0} {}

        void addFile(Containers::Pointer<AbstractFile>&& file) { _files.push_back(std::move(file)); }

        AbstractFile& file(std::size_t i) { return *_files[i]; }

        std::size_t numFiles() const { return _files.size(); }

        const std::string& id() const { return _id; }

        void incRef() { _refs++; }
        void decRef() { _refs--; }

        int numRefs() { return _refs; }

        StringHash typeHash() { return doTypeHash(); }

        Signal<AbstractResource*>& finishedLoading() { return _finishedLoading; }

        ResourceState state() const { return _state; }

    protected:
        virtual StringHash doTypeHash() { return "AbstractResource"; }

        std::string _id;
        ResourceState _state;

    private:
        friend class ResourceManager;
        
        Signal<AbstractResource*> _finishedLoading;
        std::vector<Containers::Pointer<AbstractFile>> _files;
        int _refs;
};

class BaseResourceLoader {
    public:
        void begin(AbstractResource& res, ResourceManager& mgr) { doBeginAbstract(res, mgr); }
        void reload(AbstractResource& res, ResourceManager& mgr) { doReloadAbstract(res, mgr); }
        void loadAsyncPart(AbstractResource& res, ResourceManager& mgr) { doLoadAsyncPartAbstract(res, mgr); }
        void loadSyncPart(AbstractResource& res, ResourceManager& mgr) { doLoadSyncPartAbstract(res, mgr); }
        void end(AbstractResource& res, ResourceManager& mgr) { doEndAbstract(res, mgr); }

    protected:
        virtual void doBeginAbstract(AbstractResource& res, ResourceManager& mgr) = 0;
        virtual void doReloadAbstract(AbstractResource& res, ResourceManager& mgr) = 0;
        virtual void doLoadAsyncPartAbstract(AbstractResource& res, ResourceManager& mgr) = 0;
        virtual void doLoadSyncPartAbstract(AbstractResource& res, ResourceManager& mgr) = 0;
        virtual void doEndAbstract(AbstractResource& res, ResourceManager& mgr) = 0;
};

template<class T>
class AbstractResourceLoader : public BaseResourceLoader {
    public:
        void begin(T& res, ResourceManager& mgr) { doBegin(res, mgr); }
        void reload(T& res, ResourceManager& mgr) { doReload(res, mgr); }
        void loadAsyncPart(T& res, ResourceManager& mgr) { doLoadAsyncPart(res, mgr); }
        void loadSyncPart(T& res, ResourceManager& mgr) { doLoadSyncPart(res, mgr); }
        void end(T& res, ResourceManager& mgr) { doEnd(res, mgr); }

    protected:
        virtual void doBeginAbstract(AbstractResource& res, ResourceManager& mgr) { doBegin(*static_cast<T*>(&res), mgr); }
        virtual void doReloadAbstract(AbstractResource& res, ResourceManager& mgr) { doReload(*static_cast<T*>(&res), mgr); }
        virtual void doLoadAsyncPartAbstract(AbstractResource& res, ResourceManager& mgr) { doLoadAsyncPart(*static_cast<T*>(&res), mgr); }
        virtual void doLoadSyncPartAbstract(AbstractResource& res, ResourceManager& mgr) { doLoadSyncPart(*static_cast<T*>(&res), mgr); }
        virtual void doEndAbstract(AbstractResource& res, ResourceManager& mgr) { doEnd(*static_cast<T*>(&res), mgr); }

        virtual void doBegin(T& res, ResourceManager& mgr) {}
        virtual void doReload(T& res, ResourceManager& mgr) {}
        virtual void doLoadAsyncPart(T& res, ResourceManager& mgr) = 0;
        virtual void doLoadSyncPart(T& res, ResourceManager& mgr) = 0;
        virtual void doEnd(T& res, ResourceManager& mgr) {}
};

}

#endif
