#ifndef Navi_Resource_ResourceManager_hpp
#define Navi_Resource_ResourceManager_hpp

#include "Navi/Resource/ResourceManager.h"
#include "Navi/WorkQueue.h"

namespace Navi::Resource {

template<class T>
T& ResourceManager::createOrGetResource(const std::string& id) {
    StringHash hash = id;
    auto it = _resources.find(hash);
    if (it == _resources.end()) {
        auto res = new T{id};
        _resources[hash] = Containers::Pointer<T>{ res };
        setState(id, ResourceState::Ok);
        return *static_cast<T*>(_resources[hash].get());
    }

    return *static_cast<T*>(it->second.get());
}

template<class T>
ResourceHandle<T> ResourceManager::loadSync(const std::string& id) {
    auto& resource = createOrGetResource<T>(id);
    if (state(id) != ResourceState::Ok) {
        // Resource is already loaded or loading, just return it
        return ResourceHandle<T>{ resource, ResourceHandleFlag::Cached };
    }

    auto loader = static_cast<AbstractResourceLoader<T>*>(_loaders[T::resourceTypeHash()].get());
    loader->begin(resource, *this);
    setState(id, ResourceState::Loading);
    loader->loadAsyncPart(resource, *this);
    loader->loadSyncPart(resource, *this);
    if (state(id) != ResourceState::Error) {
        setState(id, ResourceState::Loaded);
    }
    loader->end(resource, *this);
    resource.finishedLoading().emit(&resource);
    return ResourceHandle<T>{ resource };
}

template<class T>
ResourceHandle<T> ResourceManager::loadSyncWithParam(const std::string& id, const Data::Variant& param) {
    setParam(id, param);
    return loadSync<T>(id);
}

template<class T>
ResourceHandle<T> ResourceManager::loadSyncWithFile(const std::string& id, const std::string& file) {
    setParamFilenameMaybe(id, file);
    return loadSync<T>(id);
}

template<class T>
ResourceHandle<T> ResourceManager::loadAsync(const std::string& id) {
    auto& resource = createOrGetResource<T>(id);
    if (state(id) != ResourceState::Ok) {
        // Resource is already loaded or loading, just return it
        return ResourceHandle<T>{ resource, ResourceHandleFlag::Cached };
    }

    auto loader = static_cast<AbstractResourceLoader<T>*>(_loaders[T::resourceTypeHash()].get());
    loader->begin(resource, *this);
    setState(id, ResourceState::Loading);

    auto workFunc = [loader, &resource, id, this]() {
        loader->loadAsyncPart(resource, *this);
    };

    auto callbackFunc = [loader, &resource, id, this]() {
        loader->loadSyncPart(resource, *this);
        if (state(resource.id()) != ResourceState::Error) {
            setState(resource.id(), ResourceState::Loaded);
        }
        loader->end(resource, *this);
        resource.finishedLoading().emit(&resource);
    };

    _loadQueue->addWorkWithCallback(workFunc, callbackFunc);
    return ResourceHandle<T>{ resource };
}

template<class T>
ResourceHandle<T> ResourceManager::loadAsyncWithParam(const std::string& id, const Data::Variant& param) {
    setParam(id, param);
    return loadAsync<T>(id);
}

template<class T>
ResourceHandle<T> ResourceManager::loadAsyncWithFile(const std::string& id, const std::string& file) {
    setParamFilenameMaybe(id, file);
    return loadAsync<T>(id);
}

template<class T>
ResourceHandle<T> ResourceManager::get(const std::string& id) {
    auto it = _resources.find(id);
    if (it == _resources.end()) {
        return ResourceHandle<T>{ };
    }

    return ResourceHandle<T>{ *static_cast<T*>(it->second.get()), ResourceHandleFlag::Cached };
}

}

#endif
