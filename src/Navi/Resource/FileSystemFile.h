#ifndef Navi_Resource_FileSystemFile_h
#define Navi_Resource_FileSystemFile_h

#include "Navi/Resource/AbstractFile.h"

namespace Navi::Resource {

class FileSystemFile : public AbstractFile {
    public:
        explicit FileSystemFile(const std::string& path) : AbstractFile{ path } {}

        const std::string& path() const { return _path; }

    protected:
        Containers::Array<unsigned char> doReadBytes() override;

        Containers::Pointer<AbstractFileHandle> doOpen(FileOpenMode mode) override;

        Containers::Pointer<std::istream> doIstream() override;

        bool doExists() override;

        void doCache() override;

        void doInvalidateCache() override;

        unsigned long long doModifiedTime() override;

    private:
        Containers::Array<unsigned char> _cache;
};

class FileSystemFileHandle : public AbstractFileHandle {
    public:
        explicit FileSystemFileHandle(FileSystemFile& file, FileOpenMode mode);
        ~FileSystemFileHandle();

    protected:
        void doSeek(std::size_t n) override;

        std::size_t doRead(std::size_t n, void* buf) override;

        std::size_t doWrite(std::size_t n, const void* buf) override;

    private:
        FILE* _handle;
};

}

#endif
