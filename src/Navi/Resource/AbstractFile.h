#ifndef Navi_Resource_AbstractFile_h
#define Navi_Resource_AbstractFile_h

#include <iostream>
#include <string>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Navi.h"

namespace Navi::Resource {

enum class FileOpenMode {
    Read,
    ReadBinary,
    Write,
    WriteBinary,
    Append,
    ReadWrite,
};

class AbstractFileHandle;

class AbstractFile {
    public:
        explicit AbstractFile(const std::string& path) : _path{ path } {}

        virtual ~AbstractFile() {}

        std::string readString() {
            auto bytes = readBytes();
            return std::string{ (const char*)bytes.data(), bytes.size() };
        }

        Containers::Array<unsigned char> readBytes() { return doReadBytes(); }

        Containers::Pointer<AbstractFileHandle> open(FileOpenMode mode) { return doOpen(mode); }

        const std::string& path() const { return _path; }

        void setPath(const std::string& path) { _path = path; }

        bool exists() { return doExists(); }

        void cache() { doCache(); }

        void invalidateCache() { doInvalidateCache(); }

        unsigned long long modifiedTime() { return doModifiedTime(); }

        Containers::Pointer<std::istream> istream() { return doIstream(); }

    protected:
        virtual Containers::Array<unsigned char> doReadBytes() = 0;
        virtual Containers::Pointer<AbstractFileHandle> doOpen(FileOpenMode mode) = 0;
        virtual bool doExists() = 0;
        virtual Containers::Pointer<std::istream> doIstream() = 0;
        virtual void doCache() { }
        virtual void doInvalidateCache() { }
        virtual unsigned long long doModifiedTime() { return 0; }

        std::string _path;
};

class AbstractFileHandle {
    public:
        virtual ~AbstractFileHandle() {}

        void seek(std::size_t n) { doSeek(n); }

        std::size_t read(std::size_t n, void* buf) { return doRead(n, buf); }

        std::size_t write(std::size_t n, const void* buf) { return doWrite(n, buf); }

    protected:
        virtual void doSeek(std::size_t) = 0;

        virtual std::size_t doRead(std::size_t n, void* buf) = 0;

        virtual std::size_t doWrite(std::size_t n, const void* buf) { return 0; }
};

}

#endif
