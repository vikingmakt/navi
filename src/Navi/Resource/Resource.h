#ifndef Navi_Resource_Resource_h
#define Navi_Resource_Resource_h

namespace Navi::Resource {

class ResourceManager;
class AbstractResource;
class Scene;
class Style;
class Font;
class FontLoader;
class Image;
class ImageLoader;
class AbstractFile;
class FileSystemFile;
class MemoryFile;
class PakReader;

}

#endif