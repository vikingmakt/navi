#include <iostream>
#include <Corrade/Utility/Directory.h>
#include "Navi/Resource/ResourceManager.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Resource/FileSystemFile.h"
#include "Navi/Resource/MemoryFile.h"
#include "Navi/Ui/Image.h"
#include "Navi/Ui/Font.h"
#include "Navi/Application.h"
#include "Navi/WorkQueue.h"
#include "Navi/Console.hpp"

using namespace Corrade::Utility;

namespace Navi::Resource {

namespace {
void splitProtocolAndPath(const std::string& url, std::string& proto, std::string& path) {
    std::size_t index = url.find_first_of(':');
    if (index == std::string::npos)
        return;

    proto = url.substr(0, index);
    path = url.substr(index + 3, url.size() - index - 3); // skip the 2 forward slashes from the protocol
}
}

ResourceManager::ResourceManager(Application& app) : _app{ &app }, _loadQueue{ new WorkQueue{ app, 1, 128 } }, _pakEdit{} {
    app.tick().connect(*this, &ResourceManager::tick);

    // Resource file handler (must exist in a resource directory or pak)
    setProtocolHandler("res", [this](const std::string& path, const ResourceManager& mgr) -> Containers::Pointer<AbstractFile> {
        // First check the resource directories
        for (auto it = _resourceDirs.rbegin(); it != _resourceDirs.rend(); ++it) {
            const auto& dir = *it;
            auto filename = Directory::join(dir, path);
            Console::debug() << "Looking for resource file" << path << "in dir" << dir;

            if (Directory::exists(filename)) {
                Console::debug() << "Found:" << filename;
                return Containers::pointer<FileSystemFile>(filename);
            }
        }

        // Then check the resource PAKs
        for (auto it = _resourcePaks.rbegin(); it != _resourcePaks.rend(); ++it) {
            auto& pak = *it;
            Console::debug() << "Looking for resource file" << path << "in pak" << pak->path();

            if (pak->hasFile(path)) {
                Console::debug() << "Found:" << path;
                return pak->openFile(path);
            }
        }

        Console::error() << "Resource file doesn't exist:" << path;
        return nullptr;
    });

    // Raw filesystem file handler
    setProtocolHandler("fs", [this](const std::string& path, const ResourceManager& mgr) -> Containers::Pointer<AbstractFile> {
        if (Directory::exists(path))
            return Containers::pointer<FileSystemFile>(path);

        Console::error() << "File doesn't exist:" << path;
        return nullptr;
    });

    // Set the loaders for built-in resource types
    setLoader<Ui::Image>(Containers::pointer<Ui::ImageLoader>());
    setLoader<Ui::Font>(Containers::pointer<Ui::FontLoader>());

    // Register the resource manager commands
    auto resListCmd = [this](const ConsoleCommandCall& call) {
        Console::info() << "Loaded resources: ";
        Console::info() << " ";
        for (auto& pair : _resources) {
            Console::info() << pair.second->id() << "-" << pair.second->numRefs() << "refs" << "- files related:";
            for (std::size_t i = 0; i < pair.second->numFiles(); i++) {
                auto& file = pair.second->file(i);
                Console::info() << "\t" << file.path();
            }
        }
        Console::info() << " ";
    };
    auto pathCmd = [this](const ConsoleCommandCall& call) {
        addDirectoryOrPak(call.args[0]);
    };
    auto pathListCmd = [this](const ConsoleCommandCall& call) {
        for (auto it = _resourceDirs.rbegin(); it != _resourceDirs.rend(); ++it) {
            const auto& dir = *it;
            Console::info() << "dir:" << dir;
        }

        for (auto it = _resourcePaks.rbegin(); it != _resourcePaks.rend(); ++it) {
            Console::info() << "pak:" << (*it)->path();
        }
    };
    auto pakEditor = [this](const ConsoleCommandCall& call) {
        std::string cmd = call.args[0];
        if ("help" == cmd) {
            Console::info() << "pak open <file> - Opens an existing or new PAK file for editing";
            Console::info() << "pak cd <path> - Sets the current dir, from where the files will be added";
            Console::info() << "pak add [path] - Adds the file or directory to the PAK file";
            Console::info() << "pak list - List the files in the current PAK";
            Console::info() << "pak write - Write the pak to disk";
            Console::info() << "pak clear - Remove all files";
            Console::info() << "pak close - Write and close the current file";
            return;
        }

        if ("open" == cmd) {
            if (_pakEdit.get()) {
                Console::info() << "Close the current file first";
            }
            _pakEdit = Containers::Pointer<PakFile>{ new PakFile{ Containers::pointer<FileSystemFile>(call.args[1].cast<std::string>()) } };
            _pakEditDir = ".";
            return;
        }

        if (!_pakEdit) {
            Console::error() << "Open a file first";
            return;
        }

        if ("cd" == cmd) {
            _pakEditDir = call.args[1].cast<std::string>();
        }
        else if ("add" == cmd || "radd" == cmd) {
            bool recur = "radd" == cmd;

            auto addFile = [this](const std::string& name, const std::string& fn) {
                Console::info() << "Adding file:" << name << "=" << fn;
                _pakEdit->addFile(name, Containers::pointer<FileSystemFile>(fn));
            };

            std::function<void(const std::string&, const std::string&)> addDir = [this, addFile, &addDir, recur](const std::string& prefix, const std::string& dirName) {
                for (const auto& fn : Directory::list(dirName)) {
                    if (fn == "." || fn == "..") continue;

                    auto fullfn = Directory::join(dirName, fn);
                    bool isdir = Directory::isDirectory(fullfn);
                    if (isdir) {
                        if (!recur) continue;

                        if (prefix.empty()) {
                            addDir(fn, fullfn);
                        }
                        else {
                            addDir(prefix + "/" + fn, fullfn);
                        }
                    }
                    else {
                        auto entryname = prefix;
                        if (!prefix.empty()) {
                            entryname.append("/");
                        }
                        entryname.append(fn);
                        addFile(entryname, fullfn);
                    }
                }
            };

            if (call.args.size() > 1) {
                auto fullfn = Directory::join(_pakEditDir, call.args[1]);
                if (Directory::isDirectory(fullfn)) {
                    addDir(call.args[1], fullfn);
                }
                else {
                    addFile(call.args[1], fullfn);
                }
            }
            else {
                addDir("", _pakEditDir);
            }
        }
        else if ("list" == cmd) {
            for (const auto& entry : _pakEdit->files()) {
                Console::info() << std::string{ entry.name };
            }
        }
        else if ("write" == cmd) {
            Console::info() << "Wrote:" << _pakEdit->path();
            _pakEdit->write();
        }
        else if ("clear" == cmd) {
            _pakEdit->clear();
        }
        else if ("close" == cmd) {
            Console::info() << "Wrote:" << _pakEdit->path();
            _pakEdit->write();
            _pakEdit = {};
        }
    };

    Console::registerCommands(Containers::Array<ConsoleCommand>{Containers::InPlaceInit, {
        {"resList", "", "List all loaded resources and their references", 0, resListCmd},
        {"path", "<dir_or_pak>", "Adds a directory or PAK file to the resource search path", 1, pathCmd},
        {"pathList", "", "List all resource paths in search order", 0, pathListCmd},
        {"pak", "<cmd> [args...]", "PAK editor command, see `pak help`", 1, pakEditor}
    }});
}

void ResourceManager::setState(const std::string& id, ResourceState state) {
    std::unique_lock<std::mutex> lock{ _statesMutex };
    _resources[id]->_state = state;
}

ResourceState ResourceManager::state(const std::string& id) {
    std::unique_lock<std::mutex> lock{ _statesMutex };
    auto it = _resources.find(id);
    if (it == _resources.end()) {
        return ResourceState::Unknown;
    }
    return it->second->_state;
}

Containers::Optional<Data::Variant> ResourceManager::param(const std::string& id) const {
    auto it = _params.find(id);
    if (it == _params.end()) {
        return {};
    }
    return { it->second };
}

Containers::Pointer<AbstractFile> ResourceManager::findFile(const std::string& url) {
    std::string proto, path;
    splitProtocolAndPath(url, proto, path);
    if (proto.empty()) {
        proto = "fs";
        path = url;
    }

    auto it = _protoHandlers.find(proto);
    if (it == _protoHandlers.end()) {
        Console::error() << "could not handle the given file protocol '" << proto << "'";
        return nullptr;
    }

    auto handler = it->second;
    auto file = handler(path, *this);
    if (file) {
        _fileModifiedTimes[file->path()] = file->modifiedTime();
    }
    return std::move(file);
}

void ResourceManager::setParamFilenameMaybe(const std::string& id, const std::string& filename) {
    std::string proto, path;
    splitProtocolAndPath(filename, proto, path);
    bool isFilename = !path.empty();

    auto it = _params.find(id);
    if (it == _params.end() && isFilename) {
        auto param = Data::Variant::makeMap();
        param.map()["filename"] = Data::Variant{filename};
        setParam(id, param);
    }
}

void ResourceManager::addDirectoryOrPak(const std::string& path, bool isMain) {
    if (Directory::isDirectory(path)) {
        // TODO: remove trailing slash
        _resourceDirs.push_back(path);
        _resourceDirectoryAdded.emit({ path, isMain });
        Console::info() << "Added" << path << "resource dir";
        return;
    }
    else {
        auto file = findFile(path);
        if (file) {
            _resourcePaks.push_back(Containers::pointer<PakFile>( std::move(file) ));
            _resourcePakAdded.emit({ _resourcePaks.back().get(), isMain });
            Console::info() << "Added" << _resourcePaks.back()->path() << "resource pak";
            return;
        }
    }

    Console::error() << "could not find directory or pak file: " << path;
}

void ResourceManager::tick(float dt) {
    _loadQueue->tick(dt);

#if 1
    static float hotReloadTime = 0.0f;
    if (hotReloadTime >= 1.0f) {
        hotReloadTime = 0.0f;

        for (auto& pair : _resources) {
            auto& res = pair.second;
            bool changed = false;

            for (std::size_t i = 0; i < res->numFiles(); i++) {
                auto& file = res->file(i);
                unsigned long long mtime = file.modifiedTime();
                if (mtime != _fileModifiedTimes[file.path()]) {
                    _fileModifiedTimes[file.path()] = mtime;
                    file.invalidateCache();
                    changed = true;
                }
            }

            if (changed) {
                Console::debug() << "Resource"  << res->id() << "has changed, reloading now";
                reloadResource(*res);
            }
        }
    }

    hotReloadTime += dt;
#endif
}

void ResourceManager::reloadResource(AbstractResource& resource) {
    auto& loader = _loaders[resource.typeHash()];
    loader->reload(resource, *this);
    setState(resource.id(), ResourceState::Loading);

    auto workFunc = [&loader, &resource, this]() {
        loader->loadAsyncPart(resource, *this);
    };

    auto callbackFunc = [&loader, &resource, this]() {
        loader->loadSyncPart(resource, *this);
        if (state(resource.id()) != ResourceState::Error) {
            setState(resource.id(), ResourceState::Loaded);
        }
        loader->end(resource, *this);
        resource.finishedLoading().emit(&resource);
    };

    _loadQueue->addWorkWithCallback(workFunc, callbackFunc);
}

}
