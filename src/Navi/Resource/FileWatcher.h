#ifndef Navi_Resource_FileWatcher_h
#define Navi_Resource_FileWatcher_h

#include <iostream>
#include <string>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Navi.h"
#include "Navi/Signal.h"
#include "Navi/Resource/AbstractFile.h"
#include "Navi/Resource/ResourceManager.h"

namespace Navi::Resource {

/// Given a file and an interval, polls the file modified time
/// and fires a signal when the file is modified
class FileWatcher : public Interconnect::Receiver {
    public:
        explicit FileWatcher(Containers::Pointer<AbstractFile>&& file, float interval, Application& app);

        ~FileWatcher();

        /// The path of the file to be watched
        std::string path();

        /// Sets the path of the file to be watched
        void setPath(const std::string& path);

        /// Sets whether to watch the file or not
        void setEnabled(bool enabled) { _enabled = enabled; }

        /// The signal fired when the file is modified
        Signal<unsigned long long>& changed() { return _changed; }

    protected:
        void tick(float);

        Containers::Pointer<AbstractFile> _file;
        Signal<unsigned long long> _changed;
        unsigned long long _prevModifiedTime;
        float _interval;
        float _timer;
        bool _enabled;
};

}

#endif
