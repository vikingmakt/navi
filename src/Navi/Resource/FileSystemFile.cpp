#include <cstring>
#ifdef _WIN32
#include <Windows.h>
#endif
#include <Corrade/Utility/Directory.h>
#include "Navi/Resource/FileSystemFile.h"

namespace Navi::Resource {

namespace {
#ifdef _WIN32
unsigned long long FileTimeToUint64(FILETIME *FileTime) {
	ULARGE_INTEGER LargeInteger;
	LargeInteger.LowPart = FileTime->dwLowDateTime;
	LargeInteger.HighPart = FileTime->dwHighDateTime;
	return (unsigned long long)LargeInteger.QuadPart;
}

unsigned long long fileModifiedTime(const char* filename) {
    FILETIME LastWriteTime = {};
	WIN32_FILE_ATTRIBUTE_DATA Data;
	if (GetFileAttributesEx(filename, GetFileExInfoStandard, &Data))
	{
		LastWriteTime = Data.ftLastWriteTime;
	}
	return FileTimeToUint64(&LastWriteTime);
}
#else
unsigned long long fileModifiedTime(const char* filename) {
    return 0;
}
#endif

const char* fileOpenModeToChars(FileOpenMode mode) {
    switch (mode) {
    case FileOpenMode::Read:
        return "r";
    case FileOpenMode::ReadBinary:
        return "rb";
    case FileOpenMode::Write:
        return "w";
    case FileOpenMode::WriteBinary:
        return "wb";
    case FileOpenMode::Append:
        return "a";
    case FileOpenMode::ReadWrite:
        return "w+";
    default:
        return "";
    }
}
}

void FileSystemFile::doCache() {
    if (_cache.empty()) {
        auto data = Utility::Directory::read(_path);
        
        _cache = Containers::Array<unsigned char>{ Containers::NoInit, data.size() };
        std::memcpy(_cache.data(), data.data(), data.size());
    }
}

void FileSystemFile::doInvalidateCache() {
    _cache = {};
}

Containers::Array<unsigned char> FileSystemFile::doReadBytes() {
    doCache();

    Containers::Array<unsigned char> res{ Containers::NoInit, _cache.size() };
    std::memcpy(res.begin(), _cache.data(), _cache.size());
    return res;
}

Containers::Pointer<AbstractFileHandle> FileSystemFile::doOpen(FileOpenMode mode) {
    return Containers::Pointer<AbstractFileHandle>{ new FileSystemFileHandle{ *this, mode } };
}

Containers::Pointer<std::istream> FileSystemFile::doIstream() {
    doCache();

    return Containers::pointer<std::istream>(
        static_cast<std::istream*>(new std::istringstream{ std::string{ (const char*)_cache.data(), _cache.size() } })
    );
}

bool FileSystemFile::doExists() {
    return Utility::Directory::exists(_path);
}

unsigned long long FileSystemFile::doModifiedTime() {
    return fileModifiedTime(_path.c_str());
}


FileSystemFileHandle::FileSystemFileHandle(FileSystemFile& file, FileOpenMode mode) {
    _handle = fopen(file.path().c_str(), fileOpenModeToChars(mode));
}

FileSystemFileHandle::~FileSystemFileHandle() {
    fclose(_handle);
}

void FileSystemFileHandle::doSeek(std::size_t n) {
    fseek(_handle, n, SEEK_SET);
}

std::size_t FileSystemFileHandle::doRead(std::size_t n, void* buf) {
    return fread(buf, sizeof(char), n, _handle);
}

std::size_t FileSystemFileHandle::doWrite(std::size_t n, const void* buf) {
    return fwrite(buf, sizeof(char), n, _handle);
}

}
