#include <cstring>
#include <sstream>
#include <Corrade/Containers/Tags.h>
#include "Navi/Resource/MemoryFile.h"

namespace Navi::Resource {

MemoryFile::MemoryFile(const Containers::ArrayView<unsigned char>& data, const std::string& path) : AbstractFile{ path } {
    _data = Containers::Array<unsigned char>{ Containers::NoInit, data.size() };
    std::memcpy(_data.begin(), data.data(), data.size());
}

Containers::Array<unsigned char> MemoryFile::doReadBytes() {
    Containers::Array<unsigned char> res{ Containers::NoInit, _data.size() };
    std::memcpy(res.begin(), _data.data(), _data.size());
    return res;
}

Containers::Pointer<std::istream> MemoryFile::doIstream() {
    return Containers::pointer<std::istream>(
        static_cast<std::istream*>(new std::istringstream{ std::string{ (const char*)_data.data(), _data.size() } })
    );
}

}
