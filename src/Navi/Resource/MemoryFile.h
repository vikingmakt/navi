#ifndef Navi_Resource_MemoryFile_h
#define Navi_Resource_MemoryFile_h

#include <string>
#include <Corrade/Containers/Array.h>
#include "Navi/Navi.h"
#include "Navi/Resource/AbstractFile.h"

namespace Navi::Resource {

class MemoryFile : public AbstractFile {
    public:
        explicit MemoryFile(const Containers::ArrayView<unsigned char>& data, const std::string& path);

    protected:
        Containers::Array<unsigned char> doReadBytes() override;

        Containers::Pointer<std::istream> doIstream() override;

        bool doExists() override { return true; }

        Containers::Pointer<AbstractFileHandle> doOpen(FileOpenMode mode) override { return nullptr; }

    private:
        Containers::Array<unsigned char> _data;
};

}

#endif
