#ifndef Navi_Resource_LazyMemoryFile_h
#define Navi_Resource_LazyMemoryFile_h

#include <string>
#include <Corrade/Containers/Array.h>
#include "Navi/Navi.h"
#include "Navi/Resource/AbstractFile.h"

namespace Navi::Resource {

class LazyMemoryFile : public AbstractFile {
    public:
        explicit LazyMemoryFile(AbstractFile& realFile, unsigned int offset, unsigned int size, const std::string& path);

    protected:
        Containers::Array<unsigned char> doReadBytes() override;

        Containers::Pointer<std::istream> doIstream() override;

        bool doExists() override { return true; }

        Containers::Pointer<AbstractFileHandle> doOpen(FileOpenMode mode) override { return nullptr; }

    private:
        AbstractFile* _realFile;
        unsigned int _offset;
        unsigned int _size;
};

}

#endif
