#ifndef Navi_Resource_ResourceManager_h
#define Navi_Resource_ResourceManager_h

#include <unordered_map>
#include <functional>
#include <mutex>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Interconnect/Receiver.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/Trade/AbstractImporter.h>
#include "Navi/Navi.h"
#include "Navi/Signal.h"
#include "Navi/StringHash.h"
#include "Navi/Data/Variant.h"
#include "Navi/Resource/Resource.h"
#include "Navi/Resource/ResourceHandle.h"
#include "Navi/Resource/PakFile.h"

namespace Navi { class WorkQueue; }

namespace Navi::Resource {

class ResourceManager : public Interconnect::Receiver {
    public:
        typedef std::function<Containers::Pointer<AbstractFile>(const std::string& path, const ResourceManager& mgr)> ProtocolHandler;

        explicit ResourceManager(Application& app);

        template<class T>
        void setLoader(Containers::Pointer<AbstractResourceLoader<T>>&& loader) { _loaders[T::resourceTypeHash()] = std::move(loader); }

        template<class T>
        ResourceHandle<T> loadSync(const std::string& id);

        template<class T>
        ResourceHandle<T> loadSyncWithParam(const std::string& id, const Data::Variant& param);

        template<class T>
        ResourceHandle<T> loadSyncWithFile(const std::string& id, const std::string& file);

        template<class T>
        ResourceHandle<T> loadAsync(const std::string& id);

        template<class T>
        ResourceHandle<T> loadAsyncWithParam(const std::string& id, const Data::Variant& param);

        template<class T>
        ResourceHandle<T> loadAsyncWithFile(const std::string& id, const std::string& file);

        template<class T>
        ResourceHandle<T> get(const std::string& id);

        PluginManager::Manager<Magnum::Trade::AbstractImporter>& imageImporterManager() { return _manager; }

        void setParam(const std::string& id, const Data::Variant& param) { _params[id] = param; }

        void setProtocolHandler(const std::string& proto, ProtocolHandler handler) { _protoHandlers[proto] = handler; }

        Signal<std::pair<std::string, bool>>& resourceDirectoryAdded() { return _resourceDirectoryAdded; }

        Signal<std::pair<PakFile*, bool>>& resourcePakAdded() { return _resourcePakAdded; }

        Application& app() { return *_app; }

        void setState(const std::string& id, ResourceState);

        ResourceState state(const std::string& id);

        void setParamFilenameMaybe(const std::string& id, const std::string& filename);

        Containers::Optional<Data::Variant> param(const std::string& id) const;

        Containers::Pointer<AbstractFile> findFile(const std::string& url);

        void addDirectoryOrPak(const std::string& path, bool isMain = false);

        void tick(float dt);

    private:
        template<class T>
        T& createOrGetResource(const std::string& id);

        void reloadResource(AbstractResource& res);

        std::unordered_map<StringHash, Containers::Pointer<AbstractResource>> _resources;
        std::unordered_map<StringHash, Data::Variant> _params;
        std::unordered_map<StringHash, ProtocolHandler> _protoHandlers;
        std::unordered_map<StringHash, Containers::Pointer<BaseResourceLoader>> _loaders;
        std::unordered_map<std::string, unsigned long long> _fileModifiedTimes;
        std::vector<std::string> _resourceDirs;
        std::vector<Containers::Pointer<PakFile>> _resourcePaks;
        std::mutex _statesMutex;
        PluginManager::Manager<Magnum::Trade::AbstractImporter> _manager;
        Containers::Pointer<WorkQueue> _loadQueue;
        Signal<std::pair<std::string, bool>> _resourceDirectoryAdded;
        Signal<std::pair<PakFile*, bool>> _resourcePakAdded;
        Containers::Pointer<PakFile> _pakEdit;
        std::string _pakEditDir;
        Application* _app;
};

}

#endif
