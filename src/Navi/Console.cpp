#include <fstream>
#include <Corrade/Utility/Directory.h>
#include <Corrade/Utility/FileWatcher.h>
#include "Navi/Console.hpp"
#include "Navi/Application.h"
#include "Navi/Resource/ResourceManager.hpp"

namespace Navi {

using namespace Utility;

namespace {
class ConsoleCommandParser {
    public:
        explicit ConsoleCommandParser(const std::string& str) : _str{ &str }, _offs{ 0 } {
        }

        Containers::Optional<ParsedCommand> parseCommandAndArgs() {
            if (ch() == '$') return parseVarGetOrSet();

            auto cmd = parseCommand();
            if (!cmd) return {};

            std::vector<Data::Variant> args;
            while (isspace(ch())) {
                _offs++;

                auto arg = parseArg();
                if (!arg) break;

                args.push_back(*arg);
            }
            return { std::make_pair(*cmd, args) };
        }

        Containers::Optional<ParsedCommand> parseVarGetOrSet() {
            _offs++;

            auto var = parseArg();
            if (var->type() != Data::VariantType::String) return {};

            if (isspace(ch())) {
                _offs++;

                auto value = parseArg();
                if (value) {
                    return { std::make_pair("set", std::vector<Data::Variant>{{ *var, *value }}) };
                }
            }

            return { std::make_pair("show", std::vector<Data::Variant>{{ *var }}) };
        }

        Containers::Optional<std::string> parseCommand() {
            while (isspace(ch())) { _offs++; }

            if (!validIdentifierChar(ch())) return {};

            std::string cmd;
            while (validIdentifierChar(ch())) {
                cmd.append(1, ch());
                _offs++;
            }

            return { cmd };
        }

        Containers::Optional<Data::Variant> parseArg() {
            if (isdigit(ch()) || ch() == '.') return parseNumber();
            if (ch() == '$') return parseVar();
            if (ch() == '"') return parseString('"');
            if (ch() == '\'') return parseString('\'');
            return parseString();
        }

        Containers::Optional<Data::Variant> parseNumber() {
            bool isFloat = false;
            std::string str;
            while (isdigit(ch()) || ch() == '.') {
                if (ch() == '.') {
                    isFloat = true;
                }
                str.append(1, ch());
                _offs++;
            }

            if (isFloat) {
                return { Data::Variant{ static_cast<float>(std::atof(str.c_str())) } };
            }
            return { Data::Variant{ std::atoi(str.c_str()) } };
        }

        Containers::Optional<Data::Variant> parseVar() {
            _offs++;

            std::string name;
            while (validIdentifierChar(ch())) {
                name.append(1, ch());
                _offs++;
            }

            if (name.empty()) return {};

            return Console::var(name);
        }

        Containers::Optional<Data::Variant> parseString(char delim = 0) {
            std::string str;
            if (delim && ch() == delim) _offs++;

            while (true) {
                if (!delim && (isspace(ch()) || !ch())) break;
                else if (delim && ((ch() == delim) || !ch())) break;

                str.append(1, ch());
                _offs++;
            }

            // eat last delimiter
            if (delim && ch() == delim) {
                _offs++;
            }

            if (str[0] == 't' && str == "true") {
                return { Data::Variant{ true } };
            }
            else if (str[0] == 'f' && str == "false") {
                return { Data::Variant{ false } };
            }
            return { Data::Variant{ str } };
        }

        char ch() {
            if (_offs < _str->size()) {
                return (*_str)[_offs];
            }
            return 0;
        }

        bool validIdentifierChar(char c) {
            return isalnum(c) || c == '_';
        }

    private:
        const std::string* _str;
        std::size_t _offs;
};

void writeUserConfig(const ConsoleState& state, const std::string& path, const std::string& filename) {
    if (!Utility::Directory::exists(path)) {
        Utility::Directory::mkpath(path);
    }
    
    std::ofstream fh{ Utility::Directory::join(path, filename) };
    for (const auto& pair : state.vars) {
        if (pair.second->flags & ConsoleVarFlag::UserConfig) {
            fh << '$' << pair.first << ' ';
            fh << pair.second->value.toString();
            fh << '\n';
        }
    }
}

void readUserConfig(ConsoleState& state, const std::string& path, const std::string& filename) {
    if (!Utility::Directory::exists(path)) {
        return;
    }

    std::ifstream fh{ Utility::Directory::join(path, filename) };
    if (!fh.good()) {
        return;
    }

    std::string line;
    while (std::getline(fh, line)) {
        if (line.empty()) continue;

        Console::exec(line, false);
    }
}
}

ConsoleState Console::_state = ConsoleState{};

const Data::Variant* ConsoleCommandCall::findOption(const std::string& optname) const {
    std::size_t size = args.size();
    for (std::size_t i = 0; i < size; i++) {
        if (args[i].type() == Data::VariantType::String && args[i].cast<std::string>() == optname) {
            if (i+1 < size) {
                return &args[i + 1];
            }
            return nullptr;
        }
    }
    return nullptr;
}

ConsoleState::ConsoleState() {
    auto helpCmd = [this](const ConsoleCommandCall& call) {
        if (call.args.size()) {
            auto& arg = call.args[0].cast<std::string>();
            auto cmd = commands.find(arg);
            if (cmd != commands.end()) {
                Console::info() << cmd->second.name << cmd->second.usage << "-" << cmd->second.description;
                return;
            }

            auto var = vars.find(arg);
            if (var != vars.end()) {
                Console::info() << var->second->name << "-" << var->second->description;
                return;
            }
        }

        Console::info() << "Available commands: ";
        Console::info() << " ";
        for (const auto& pair : commands) {
            Console::info() << pair.first << pair.second.usage << "-" << pair.second.description;
        }
        Console::info() << " ";
        Console::info() << "Available variables: ";
        Console::info() << " ";
        for (const auto& pair : vars) {
            Console::info() << pair.first << "-" << pair.second->description;
        }
        Console::info() << " ";
    };
    auto clearCmd = [this](const ConsoleCommandCall& call) {
        entries.clear();
    };
    auto setCmd = [this](const ConsoleCommandCall& call) {
        Console::setVar(call.args[0].cast<std::string>(), call.args[1]);
    };
    auto getCmd = [this](const ConsoleCommandCall& call) {
        auto name = call.args[0].cast<std::string>();
        auto value = Console::findVar(name);
        if (value) {
            Console::info() << name << '=' << value->toString();
        }
    };
    auto cvarAdd = [this](const ConsoleCommandCall& call) {
        std::string desc;
        if (call.args.size() > 2) {
            desc = call.args[2].cast<std::string>();
        }
        Console::registerVar(call.args[0], desc, call.args[1]);
    };
    auto cvarBind = [this](const ConsoleCommandCall& call) {
        std::string dest = call.args[0];
        std::string cmd = call.args[1];
        Console::varChanged().connect([this, dest=std::move(dest), cmd=std::move(cmd)](std::pair<std::string, Data::Variant> pair) {
            Console::beginCapture();
            Console::exec(cmd, false);
            vars[dest]->value = Console::endCapture();
        });
    };
    auto cvarDump = [this](const ConsoleCommandCall& call) {
        for (const auto& pair : vars) {
            Console::info() << pair.first << "=" << pair.second->value.toString();
        }
        Console::info() << " ";
    };
    auto fsPath = [this](const ConsoleCommandCall& call) {
        Console::info() << Directory::path(Directory::fromNativeSeparators(call.args[0]));
    };
    auto fsFromNative = [this](const ConsoleCommandCall& call) {
        Console::info() << Directory::fromNativeSeparators(call.args[0]);
    };
    auto toggle = [this](const ConsoleCommandCall& call) {
        auto& var = Console::var(call.args[0]);
        var = !var;
    };
    auto strCat = [this](const ConsoleCommandCall& call) {
        auto out = Console::info(LogEntryFlag::None);
        for (const auto& str : call.args) {
            out << str.cast<std::string>();
        }
    };
    auto userConfigAddVar = [this](const ConsoleCommandCall& call) {
        Console::registerVar(call.args[0].cast<std::string>(), "", call.args[1], ConsoleVarFlag::UserConfig);
    };
    auto userConfigLoad = [this](const ConsoleCommandCall& call) {
        if (call.args.size() > 0) {
            Console::loadUserConfig(call.args[0]);
        }
        else {
            Console::loadUserConfig();
        }
    };
    auto userConfigSave = [this](const ConsoleCommandCall& call) {
        if (call.args.size() > 0) {
            Console::saveUserConfig(call.args[0]);
        }
        else {
            Console::saveUserConfig();
        }
    };
    auto execFile = [this](const ConsoleCommandCall& call) {
        Console::execFile(call.args[0]);
    };
    auto shell = [this](const ConsoleCommandCall& call) {
        std::string* output = nullptr;
        std::string cmd = call.args[0].cast<std::string>();
        auto pwd = call.findOption("-pwd");
        auto cvar = call.findOption("-cvar");
        if (pwd) {
            std::string newcmd = "cd ";
            newcmd.append(pwd->cast<std::string>());
            newcmd.append(" && ");
            newcmd.append(cmd);
            cmd = newcmd;
        }
        if (cvar) {
            output = &Console::var(*cvar).cast<std::string>();
        }

        ioQueue().addWork([this, output, cmd=std::move(cmd)]() {
            Containers::StaticArray<128, char> buffer;
            
            std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd.c_str(), "r"), _pclose);
            if (!pipe) {
                throw std::runtime_error("popen() failed!");
            }

            auto log = Console::info(LogEntryFlag::None);
            while (!feof(pipe.get())) {
                char c = fgetc(pipe.get());
                if (c == '\r')
                    continue;

                log << std::string{ &c, 1 };
            }

            Console::info() << " ";
        });
    };
    auto copy = [this](const ConsoleCommandCall& call) {
        std::string from = call.args[0];
        std::string to = call.args[1];

        ioQueue().addWork([this, from = std::move(from), to = std::move(to)]() {
            if (!Utility::Directory::copy(from, to)) {
                Console::error() << "An error ocurred when copying the file";
            }
        });
    };
    auto io = [this](const ConsoleCommandCall& call) {
        std::string cmd = call.args[0];
        auto scene = call.scene;
        std::vector<Data::Variant> args;

        for (std::size_t i = 1; i < call.args.size(); i++) {
            args.push_back(call.args[i]);
        }

        ioQueue().addWork([this, scene, cmd=std::move(cmd), args=std::move(args)]() {
            Console::call(cmd, args, scene);
        });
    };
    auto watchFile = [this](const ConsoleCommandCall& call) {
        auto file = Application::instance().resourceManager().findFile(call.args[0]);
        auto watch = Containers::pointer<Resource::FileWatcher>(std::move(file), 1.0f, Application::instance());
        std::string cmd = call.args[1];
        watch->changed().connect([cmd=std::move(cmd)](unsigned long long t) {
            Console::exec(cmd, false);
        });
        watchingFiles[call.args[0].cast<std::string>()] = std::move(watch);
    };
    auto watchStop = [this](const ConsoleCommandCall& call) {
        watchingFiles.erase(call.args[0].cast<std::string>());
    };
    auto watchList = [this](const ConsoleCommandCall& call) {
        for (const auto& it : watchingFiles) {
            Console::info() << it.first;
        }
    };
    auto watchClear = [this](const ConsoleCommandCall& call) {
        watchingFiles.clear();
    };
    commands["help"] = ConsoleCommand{ "help", "", "Help", 0, helpCmd };
    commands["clear"] = ConsoleCommand{ "clear", "", "Clear the console logs", 0, clearCmd };
    commands["set"] = ConsoleCommand{ "set", "<var> <value>", "Sets a variable", 2, setCmd };
    commands["show"] = ConsoleCommand{ "show", "<var>", "Displays a variable", 1, getCmd };
    commands["cvarDump"] = ConsoleCommand{ "cvarDump", "", "Show all cvars", 0, cvarDump };
    commands["cvarAdd"] = ConsoleCommand{ "cvarAdd", "<name> <value> [description]", "Create a new cvar (if doesn't exist already)", 2, cvarAdd };
    commands["cvarBind"] = ConsoleCommand{ "cvarBind", "<name> <cmd> [args...]", "Binds a cvar to the output of the specified command", 2, cvarBind };
    commands["fsPath"] = ConsoleCommand{ "fsPath", "<path>", "Get the directory portion of a file path", 1, fsPath };
    commands["fsFromNative"] = ConsoleCommand{ "fsFromNative", "<path>", "Replace native path separators to cross-platform", 1, fsFromNative };
    commands["toggle"] = ConsoleCommand{ "toggle", "<var>", "Toggle a cvar", 1, toggle };
    commands["strCat"] = ConsoleCommand{ "strCat", "[strings...]", "Concatenate strings", 0, strCat };
    commands["userConfigAddVar"] = ConsoleCommand{ "userConfigAddVar", "<var> <value>", "Adds an user config var", 1, userConfigAddVar };
    commands["userConfigLoad"] = ConsoleCommand{ "userConfigLoad", "[file]", "Loads the user config", 0, userConfigLoad };
    commands["userConfigSave"] = ConsoleCommand{ "userConfigSave", "[file]", "Saves the user config", 0, userConfigSave };
    commands["execFile"] = ConsoleCommand{ "execFile", "<file>", "Execute commands from a file", 1, execFile };
    commands["shell"] = ConsoleCommand{"shell", "<cmd> [-pwd <pwd>] [-cvar <output_cvar>]", "Execute shell commands in the IO queue and optionally send the output to a cvar", 1, shell};
    commands["copy"] = ConsoleCommand{ "copy", "<from> <to>", "Copy a file from one path to another, runs in the IO queue", 2, copy };
    commands["io"] = ConsoleCommand{ "io", "<cmd> [args...]", "Runs an arbitrary command in the IO queue", 1, io };
    commands["watchFile"] = ConsoleCommand{ "watchFile", "<path> <cmd>", "Watches a file and runs a command when changed", 2, watchFile };
    commands["watchStop"] = ConsoleCommand{ "watchStop", "<path>", "Stop watching a file", 1, watchStop };
    commands["watchList"] = ConsoleCommand{ "watchList", "", "List files being watched", 0, watchList };
    commands["watchClear"] = ConsoleCommand{ "watchClear", "", "Stop watching all files", 0, watchClear };

    vars["developer"] = Containers::Pointer<ConsoleVar>(new ConsoleVar{
        "developer", "Enable/disable developer mode", false, ConsoleVarFlag::None
    });
    vars["console_autoScroll"] = Containers::Pointer<ConsoleVar>(new ConsoleVar{
        "console_autoScroll", "Enable/disable console automatic scrolling to bottom", true, ConsoleVarFlag::None
    });
    vars["console_lockScroll"] = Containers::Pointer<ConsoleVar>(new ConsoleVar{
        "console_lockScroll", "Locks the console scroll at the bottom", false, ConsoleVarFlag::None
    });

    deferredCall.connect([](const auto& call) {
        Console::call(call.name, call.args, call.scene);
    });
}

WorkQueue& ConsoleState::ioQueue() {
    if (!_ioQueue) {
        _ioQueue = Containers::pointer<WorkQueue>(Application::instance(), std::size_t{ 1 }, std::size_t{ 128 });
    }
    return *_ioQueue;
}

ConsoleLogEntries Console::entries() {
    return std::move(ConsoleLogEntries{
        &_state.entries,
        Containers::pointer<std::lock_guard<std::mutex>>(_state.entriesMutex)
    });
}

Logger<LogDebug> Console::debug(LogEntryFlags flags) {
    std::lock_guard<std::mutex> lock{ _state.entriesMutex };
    auto* ent = &_state.entries;
    if (_state.capturing) ent = &_state.capturedEntries;

    ent->push_back(Containers::pointer<LogEntry<LogDebug>>( _state ));
    return Logger<LogDebug>{ *static_cast<LogEntry<LogDebug>*>(ent->back().get()), flags };
}

Logger<LogInfo> Console::info(LogEntryFlags flags) {
    std::lock_guard<std::mutex> lock{ _state.entriesMutex };
    auto* ent = &_state.entries;
    if (_state.capturing) ent = &_state.capturedEntries;

    ent->push_back(Containers::pointer<LogEntry<LogInfo>>( _state ));
    return Logger<LogInfo>{ *static_cast<LogEntry<LogInfo>*>(ent->back().get()), flags };
}

Logger<LogError> Console::error(LogEntryFlags flags) {
    std::lock_guard<std::mutex> lock{ _state.entriesMutex };
    if (_state.vars["developer"]->value)
        _state.wantToShow = true;

    auto* ent = &_state.entries;
    if (_state.capturing) ent = &_state.capturedEntries;

    ent->push_back(Containers::pointer<LogEntry<LogError>>( _state ));
    return Logger<LogError>{ *static_cast<LogEntry<LogError>*>(ent->back().get()), flags };
}

void Console::registerCommands(const Containers::Array<ConsoleCommand>& cmds) {
    for (const auto& cmd : cmds) {
        _state.commands[cmd.name] = cmd;
    }
}

void Console::registerVars(const Containers::Array<ConsoleVar>& vars) {
    for (const auto& var : vars) {
        _state.vars[var.name] = Containers::pointer<ConsoleVar>(var);
    }
}

void Console::registerVar(const std::string& name, const std::string& desc, const Data::Variant& defValue, ConsoleVarFlags flags) {
    _state.vars[name] = Containers::Pointer<ConsoleVar>(new ConsoleVar{name, desc, defValue, flags});
}

void Console::setVar(const std::string& name, const Data::Variant& value) {
    auto it = _state.vars.find(name);
    if (it == _state.vars.end()) {
        Console::error() << "Unknown var:" << name;
        return;
    }

    it->second->value = value;
    _state.varChanged.emit(std::make_pair(
        Containers::Reference<const std::string>(name),
        Containers::Reference<const Data::Variant>(value)
    ));
}

Data::Variant& Console::var(const std::string& name) {
    return _state.vars[name]->value;
}

Data::Variant* Console::findVar(const std::string& name) {
    auto it = _state.vars.find(name);
    if (it == _state.vars.end()) {
        Console::error() << "Unknown var:" << name;
        return nullptr;
    }

    return &it->second->value;
}

void Console::exec(const std::string& str, bool addToHistory, Ui::SceneInstance* scene) {
    if (addToHistory) {
        _state.history.push_back(str);
        Console::info() << ">" << str;
    }

    ConsoleCommandParser parser{ str };
    auto result = parser.parseCommandAndArgs();
    if (!result) {
        Console::error() << "Could not parse input:" << str;
        if (Console::var("developer")) {
            Console::exec("help", false);
        }
        return;
    }

    std::string cmd;
    std::vector<Data::Variant> args;
    std::tie(cmd, args) = *result;

    call(cmd, args, scene);
}

void Console::execDeferred(const std::string& str, bool addToHistory, Ui::SceneInstance* scene) {
    if (addToHistory) {
        _state.history.push_back(str);
        Console::info() << ">" << str;
    }

    ConsoleCommandParser parser{ str };
    auto result = parser.parseCommandAndArgs();
    if (!result) {
        Console::error() << "Could not parse input:" << str;
        if (Console::var("developer")) {
            Console::exec("help", false);
        }
        return;
    }

    std::string cmd;
    std::vector<Data::Variant> args;
    std::tie(cmd, args) = *result;

    _state.deferredCall.emit({ cmd, args, scene });
}

void Console::execFile(const std::string& filename) {
    auto file = Application::instance().resourceManager().findFile(filename);
    if (!file) {
        Console::error() << "File not found:" << filename;
        return;
    }

    auto stream = file->istream();
    std::string line;
    while (std::getline(*stream, line)) {
        if (line.empty()) continue;
        if (line[0] == '#') continue;
        
        Console::exec(line, false);
    }
}

void Console::call(const std::string& cmdstr, const std::vector<Data::Variant>& args, Ui::SceneInstance* scene) {
    auto it = _state.commands.find(cmdstr);
    if (it != _state.commands.end()) {
        auto& cmd = it->second;
        if (cmd.requiredArgs > args.size()) {
            Console::error() << "Not enough arguments," << cmd.requiredArgs << "required but" << args.size() << "provided";
            Console::info() << "Usage:" << cmd.name << cmd.usage;
            return;
        }

        cmd.func({ cmdstr, args, scene });
    }
    else {
        Console::error() << "Unknown command:" << cmdstr;
        if (Console::var("developer")) {
            Console::exec("help", false);
        }
        return;
    }
}

Containers::Optional<ParsedCommand> Console::parse(const std::string& cmd) {
    ConsoleCommandParser parser{ cmd };
    return parser.parseCommandAndArgs();
}

void Console::saveUserConfig() {
    writeUserConfig(_state, Utility::Directory::configurationDir(Console::var("app_name")), "config.cfg");
}

void Console::loadUserConfig() {
    readUserConfig(_state, Utility::Directory::configurationDir(Console::var("app_name")), "config.cfg");
}

void Console::saveUserConfig(const std::string& filename) {
    writeUserConfig(_state, Utility::Directory::path(filename), Utility::Directory::filename(filename));
}

void Console::loadUserConfig(const std::string& filename) {
    readUserConfig(_state, Utility::Directory::path(filename), Utility::Directory::filename(filename));
}

void Console::update() {
    _state.wantToShow = false;
}

void Console::beginCapture() {
    _state.capturedEntries.clear();
    _state.capturing = true;
    _state.capture = Containers::pointer<std::ostringstream>();
}

std::string Console::endCapture() {
    _state.capturing = false;
    return _state.capture->str();
}

}
