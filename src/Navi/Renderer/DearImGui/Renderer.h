#ifndef Navi_Renderer_DearImGui_Renderer_h
#define Navi_Renderer_DearImGui_Renderer_h

#include <unordered_map>
#include <stack>
#include <Corrade/Containers/StaticArray.h>
#include <Magnum/ImGuiIntegration/Context.hpp>
#include "Navi/Navi.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Ui/Ui.h"

namespace Navi::Renderer::DearImGui {

using namespace Magnum;

class Renderer {
    public:
        explicit Renderer(NoCreateT)
            : _imgui{NoCreate}, _app{nullptr} {}
        explicit Renderer(Application* app, Vector2f, Vector2i, Vector2i);

        template<class ViewportEvent>
        bool handleViewportEvent(ViewportEvent& event);

        template<class MouseEvent>
        bool handleMousePressEvent(MouseEvent& event);

        template<class MouseEvent>
        bool handleMouseReleaseEvent(MouseEvent& event);

        template<class MouseScrollEvent>
        bool handleMouseScrollEvent(MouseScrollEvent& event);

        template<class MouseMoveEvent>
        bool handleMouseMoveEvent(MouseMoveEvent& event);

        template<class KeyEvent>
        bool handleKeyPressEvent(KeyEvent& event);

        template<class KeyEvent>
        bool handleKeyReleaseEvent(KeyEvent& event);

        template<class TextInputEvent> bool handleTextInputEvent(TextInputEvent& event);

        Application& application() { return *_app; }

        auto& colorStacks() { return _colorStacks; }

        void addStyle(Ui::Style& style);

        void pushStyleSettings(const std::string& styleName);

        void pushStyleSettings(Data::LinkedList<Ui::StyleSetting>& sets);

        void popStyleSettings(const std::string& styleName);

        void popStyleSettings(Data::LinkedList<Ui::StyleSetting>& sets);

        void* processFontData(const Containers::ArrayView<unsigned char>& data, float pixelSize);

        void relayout(Vector2f, Vector2i, Vector2i);

        void newFrame();

        void drawUi(Ui::SceneInstance&);

    private:
        std::unordered_map<StringHash, Data::LinkedList<Ui::StyleSetting>*> _loadedStyles;
        Containers::StaticArray<ImGuiCol_COUNT, std::stack<ImVec4>> _colorStacks;
        ImGuiIntegration::Context _imgui;
        Application* _app;
        bool _rebuildFonts = false;
        bool _showConsole = false;
};

}

#endif
