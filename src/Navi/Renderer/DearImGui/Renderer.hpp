#ifndef Navi_Renderer_DearImGui_Renderer_hpp
#define Navi_Renderer_DearImGui_Renderer_hpp

#include "Navi/Renderer/DearImGui/Renderer.h"

namespace Navi::Renderer::DearImGui {

template<class ViewportEvent>
bool Renderer::handleViewportEvent(ViewportEvent& event) {
    return true;
}

template<class MouseEvent>
bool Renderer::handleMousePressEvent(MouseEvent& event) {
    return _imgui.handleMousePressEvent(event);
}

template<class MouseEvent>
bool Renderer::handleMouseReleaseEvent(MouseEvent& event) {
    return _imgui.handleMouseReleaseEvent(event);
}

template<class MouseScrollEvent>
bool Renderer::handleMouseScrollEvent(MouseScrollEvent& event) {
    return _imgui.handleMouseScrollEvent(event);
}

template<class MouseMoveEvent>
bool Renderer::handleMouseMoveEvent(MouseMoveEvent& event) {
    return _imgui.handleMouseMoveEvent(event);
}

template<class KeyEvent>
bool Renderer::handleKeyPressEvent(KeyEvent& event) {
    bool result = _imgui.handleKeyPressEvent(event);
    if (ImGui::GetIO().KeyAlt && event.keyName()[0] == '`') {
        _showConsole = !_showConsole;
    }
    return result;
}

template<class KeyEvent>
bool Renderer::handleKeyReleaseEvent(KeyEvent& event) {
    return _imgui.handleKeyReleaseEvent(event);
}

template<class TextInputEvent>
bool Renderer::handleTextInputEvent(TextInputEvent& event) {
    return _imgui.handleTextInputEvent(event);
}

}

#endif
