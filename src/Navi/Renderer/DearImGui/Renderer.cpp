#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Renderer.h>
#include <imgui.h>
#include <imfilebrowser.h>
#include "Navi/Renderer/DearImGui/Renderer.h"
#include "Navi/Ui/Image.h"
#include "Navi/Ui/Font.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Style.h"
#include "Navi/Navi.h"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

using namespace Navi::Ui;

namespace Navi::Renderer::DearImGui {

namespace {
static ImGuiStyleVar_ imGuiStyleVarsFloat[] = {
    ImGuiStyleVar_Alpha,
    ImGuiStyleVar_WindowRounding,
    ImGuiStyleVar_WindowBorderSize,
    ImGuiStyleVar_ChildRounding,
    ImGuiStyleVar_ChildBorderSize,
    ImGuiStyleVar_PopupRounding,
    ImGuiStyleVar_PopupBorderSize,
    ImGuiStyleVar_FrameRounding,
    ImGuiStyleVar_FrameBorderSize,
    ImGuiStyleVar_IndentSpacing,
    ImGuiStyleVar_ScrollbarSize,
    ImGuiStyleVar_ScrollbarRounding,
    ImGuiStyleVar_GrabMinSize,
    ImGuiStyleVar_GrabRounding,
    ImGuiStyleVar_TabRounding,
};

static ImGuiStyleVar_ imGuiStyleVarsVec2[] = {
    ImGuiStyleVar_WindowPadding,       // ImVec2    WindowPadding
    ImGuiStyleVar_WindowMinSize,       // ImVec2    WindowMinSize
    ImGuiStyleVar_WindowTitleAlign,    // ImVec2    WindowTitleAlign
    ImGuiStyleVar_FramePadding,        // ImVec2    FramePadding
    ImGuiStyleVar_ItemSpacing,         // ImVec2    ItemSpacing
    ImGuiStyleVar_ItemInnerSpacing,    // ImVec2    ItemInnerSpacing
    ImGuiStyleVar_ButtonTextAlign,     // ImVec2    ButtonTextAlign
    ImGuiStyleVar_SelectableTextAlign, // ImVec2    SelectableTextAlign
};

static ImGuiCol imGuiStyleColors[] = {
    ImGuiCol_Text
};

void drawWidget(Renderer& r, Widget& w);

/// Build a zero-terminated char array from a byte array of size `sizeof(std::size_t)` to use as ImGui id
const char* bytesToId(const char* bytes) {
    constexpr std::size_t idSize = sizeof(std::size_t);
    static char id[idSize + 1];
    for (std::size_t i = 0; i < idSize; i++) {
        id[i] = bytes[i];
    }
    id[idSize] = 0;
    return id;
}

void drawChildren(Renderer& r, Widget& w) {
    for (const auto& child : w._children) {
        //w.beforeDrawChild();
        drawWidget(r, *child);
        //w.afterDrawChild();
    }
}

#if 0

void drawCollapsingHeader(Renderer& r, CollapsingHeader& ch) {
    ImGui::PushID(ch.wid());
    bool popen = ch.open();
    ImGui::SetNextItemOpen(popen);
    ImGui::CollapsingHeader(ch.label().c_str(), ImGuiTreeNodeFlags_NoTreePushOnOpen);
    bool activated = ImGui::IsItemActivated();
    if (ch.open()) {
        drawChildren(r, ch);
    }
    if (activated) {
        ch.properties().set<CollapsingHeader::Open>(!popen);
    }
    ImGui::PopID();
}

void drawCombo(Renderer& r, Combo& cb) {
    static std::vector<int> selectedState;
    static std::string previewValue;

    // Determine preview value
    std::size_t numItems = cb.items().size();
    selectedState.resize(numItems);
    for (std::size_t i = 0; i < numItems; i++) {
        const auto& item = cb.items()[i];
        bool selected = (cb.value() == item.first);
        if (selected)
            previewValue.assign(item.second);

        selectedState[i] = (int)(selected);
    }

    ImGui::PushID(cb.wid());
    if (ImGui::BeginCombo("", previewValue.c_str())) {
        ImGuiSelectableFlags sflags = 0;
        
        for (std::size_t i = 0; i < numItems; i++) {
            const auto& item = cb.items()[i];
            if (ImGui::Selectable(item.second.c_str(), selectedState[i], sflags)) {
                //cb.value().modify([](auto&& val, const auto& newval) { val = newval; }, item.first);
            }
        }
        ImGui::EndCombo();
    }
    ImGui::PopID();
}

void drawImage(Renderer& r, Ui::Image& img) {
    auto res = img.image();
    if (res && res.loaded()) {
        auto& texture = img.image().resource().texture();
        auto size = img.image().resource().imageData().size();
        ImGui::Image(reinterpret_cast<ImTextureID>(&texture), { (float)size.x(), (float)size.y() });
    }
}

void drawInputText(Renderer& r, InputText& it) {
    auto& buf = it.inputBuffer();
    ImGui::PushID(it.wid());
    if (ImGui::InputText("", buf.data(), InputText::InputBufferSize)) {
        //it.value().modify([](auto&& str, char* data) { str.assign(data); }, buf.data());
        auto& value = it.value();
        value.assign(buf.data());
        it.changed().emit(!value.empty());
    }
    ImGui::PopID();
}

void drawSliderFloat(Renderer& r, SliderFloat& sf) {
    ImGui::PushID(sf.wid());
    auto format = sf.format();
    if (format.empty()) {
        format = "%.2f";
    }

    auto& vs = sf.values();
    bool changed = false;
    switch (vs.size()) {
    case 1:
        changed = ImGui::SliderFloat("", vs.data(), sf.min(), sf.max(), format.c_str());
        break;
    case 2:
        changed = ImGui::SliderFloat2("", vs.data(), sf.min(), sf.max(), format.c_str());
        break;
    case 3:
        changed = ImGui::SliderFloat3("", vs.data(), sf.min(), sf.max(), format.c_str());
        break;
    case 4:
        changed = ImGui::SliderFloat4("", vs.data(), sf.min(), sf.max(), format.c_str());
        break;
    }
    if (changed) {
        //sf.values().modify([](auto&& vs) {});
    }
    ImGui::PopID();
}

void drawStyleLoader(Renderer& r, StyleLoader& sl) {
    if (sl.style() && sl.style().loaded()) {
        r.addStyle(sl.style().resource().uiStyle());
    }

    drawChildren(r, sl);
}

/// Draws a TreeView node and returns the key of the node that was clicked, or @cpp nullptr if no one got clicked
const StringHash* drawTreeViewNode(Renderer& r, TreeView& tv, const TreeView::TreeType::Node& node) {
    ImGuiTreeNodeFlags flags = 0;
    if (node.begin() == node.end())
        flags |= ImGuiTreeNodeFlags_Leaf;
    if (tv.selectable()) {
        flags |= ImGuiTreeNodeFlags_OpenOnArrow;
        flags |= ImGuiTreeNodeFlags_OpenOnDoubleClick;

        auto& selection = tv.selection();
        if ((selection.find((*node).key) != selection.end())) {
            flags |= ImGuiTreeNodeFlags_Selected;
        }
    }

    const StringHash* result = nullptr;
    bool open = ImGui::TreeNodeEx(bytesToId((*node).key.byteArray()), flags, (*node).text.c_str());
    if (ImGui::IsItemClicked())
        result = &(*node).key;
    if (ImGui::BeginPopupContextItem() && tv.contextMenuWidget()) {
        Menu& menu = *(tv.contextMenuWidget());
        drawChildren(r, menu);
        ImGui::EndPopup();

        result = &(*node).key;
    }
    if (open) {
        for (const auto& child : node) {
            const StringHash* clickedKey = drawTreeViewNode(r, tv, child);
            if (clickedKey != nullptr)
                result = clickedKey;
        }
        ImGui::TreePop();
    }

    return result;
}

void drawTreeView(Renderer& r, TreeView& tv) {
    const StringHash* clickedKey = drawTreeViewNode(r, tv, tv.items().root());
    if (tv.selectable() && clickedKey != nullptr) {
        // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
        auto modify = [](std::unordered_set<StringHash>& v, const StringHash& key) {
            if (ImGui::GetIO().KeyCtrl) {
                auto it = v.find(key);
                if (it == v.end())
                    v.insert(key);
                else
                    v.erase(key);
            }
            else {
                v.clear();
                v.insert(key);
            }
        };
        //tv.selection().modify(modify, *clickedKey);
    }
}


void drawUserWidget(Renderer& r, UserWidget& b) {
    b.doRender();
}

#endif

float calcContentWidth(Widget& w) {
    float width = 0;
    switch (w._type) {
    case WidgetType::Base:
    case WidgetType::Window:
    case WidgetType::VerticalLayout:
        for (const auto& child : w._children) {
            width = std::max(width, calcContentWidth(*child));
        }
        break;

    case WidgetType::HorizontalLayout: {
        float pad = w[HorizontalLayout::Padding].cast<float>();
        if (pad == -1.0f)
            pad = 4.0f; //default padding

        for (const auto& child : w._children) {
            width += calcContentWidth(*child) + (pad*1.0f);
        }
        break;
    }

    case WidgetType::Label:
        width = ImGui::CalcTextSize(w[Label::Text].cast<std::string>().c_str()).x;
        break;
    }
    return width;
}

void drawButton(Renderer& r, Widget& w) {
    ImGui::PushID(w._wid);
    if (ImGui::Button(w[Button::Text].cast<std::string>().c_str())) {
        w.emitSignal(Button::Click, {});
    }
    ImGui::PopID();
}

void drawCheckbox(Renderer& r, Widget& w) {
    ImGui::PushID(w._wid);
    auto& checked = w[Checkbox::Checked].cast<bool>();
    if (ImGui::Checkbox(w[Checkbox::Text].cast<std::string>().c_str(), &checked)) {
        w.emitSignal(Checkbox::Change, Data::Variant{ checked });

        if (checked) {
            w.emitSignal(Checkbox::Yes, Data::Variant{});
        }
        else {
            w.emitSignal(Checkbox::No, Data::Variant{});
        }
    }
    ImGui::PopID();
}

void drawConsoleView(Renderer& r, Widget& w) {
    static std::size_t numEntries;
    static bool* console_autoScroll = &Console::var("console_autoScroll").cast<bool>();
    static bool* console_lockScroll = &Console::var("console_lockScroll").cast<bool>();

    auto ent = Console::entries();
    for (auto& entry : *ent.entries) {
        Vector3f color = entry->color();
        std::string output = entry->output();
        ImGui::TextColored(ImVec4{ color.r(), color.g(), color.b(), 1.0f }, output.c_str());
    }
    if (numEntries != ent.entries->size() && *console_autoScroll) {
        ImGui::SetScrollHereY();
    }
    if (*console_lockScroll) {
        ImGui::SetScrollHereY();
    }
    numEntries = ent.entries->size();
}

struct FileBrowserState : public Data::VariantObject {
    explicit FileBrowserState(const std::string& title) {
        _browser.SetTitle(title);
    }

    explicit FileBrowserState(const std::string& title, const std::string& pwd) {
        _browser.SetTitle(title);
        _browser.SetPwd(pwd);
    }

    ImGui::FileBrowser _browser;
};
void drawFileBrowser(Renderer& r, Widget& w) {
    ImGui::PushID(w._wid);
    auto& vstate = w[FileBrowser::RendererState];
    if (vstate.type() == Data::VariantType::Nil) {

        auto& pwd = w[FileBrowser::Pwd];
        if (pwd.type() != Data::VariantType::Nil) {
            vstate = Data::Variant::makeUniquePointer(new FileBrowserState{
                w[FileBrowser::Title].cast<std::string>(),
                pwd.cast<std::string>()
            });
        }
        else {
            vstate = Data::Variant::makeUniquePointer(new FileBrowserState{
                w[FileBrowser::Title].cast<std::string>()
            });
        }
    }

    auto state = vstate.castUniquePointer<FileBrowserState>();
    if (w[FileBrowser::Open].cast<bool>() && !state->_browser.IsOpened()) {
        state->_browser.Open();
    }
    else if (!w[FileBrowser::Open].cast<bool>() && state->_browser.IsOpened()) {
        state->_browser.Close();
    }

    state->_browser.SetTitle(w[FileBrowser::Title].cast<std::string>());
    state->_browser.SetFlags(w[FileBrowser::Flags].cast<int>());
    state->_browser.Display();

    if (state->_browser.HasSelected()) {
        w.emitSignal(FileBrowser::Select, state->_browser.GetSelected().string());
        state->_browser.ClearSelected();
    }
    else if (state->_browser.HasRequestedCancel()) {
        w.emitSignal(FileBrowser::Cancel, {});
        state->_browser.ClearSelected();
    }

    ImGui::PopID();
}

void drawHorizontalLayoutChildren(Renderer& r, Widget& w) {
    std::size_t size = w._children.size();
    float padding = w[HorizontalLayout::Padding].cast<float>();
    for (std::size_t i = 0; i < size; i++) {
        drawWidget(r, *w._children[i]);
        if (i < size-1) {
            ImGui::SameLine(0, padding*2.0f);
        }
    }
}

void drawHorizontalLayout(Renderer& r, Widget& w) {
    bool& dirty = w[HorizontalLayout::Dirty].cast<bool>();
    if (dirty) {
        //dirty = false;
        w[HorizontalLayout::ContentWidth] = calcContentWidth(w);
    }

    auto& align = w[HorizontalLayout::Align].cast<std::string>();
    if (std::strcmp("right", align.data()) == 0) {
        float oldx = ImGui::GetCursorPosX();
        float offs = ImGui::GetContentRegionAvail().x - w[HorizontalLayout::ContentWidth].cast<float>();
        ImGui::SetCursorPosX(oldx + offs);
        drawHorizontalLayoutChildren(r, w);
        ImGui::SetCursorPosX(oldx);
    }
    else if (std::strcmp("center", align.data()) == 0) {
        // TODO:
    }
    else {
        drawHorizontalLayoutChildren(r, w);
    }
}

void drawInputText(Renderer& r, Widget& w) {
    auto& buf = w[InputText::Buffer].cast<std::string>();
    auto& hint = w[InputText::Hint].cast<std::string>();
    ImGuiInputTextFlags flags = w[InputText::Flags].cast<int>();

    if (w[InputText::Value].isString()) {
        auto& str = w[InputText::Value].cast<std::string>();
        std::memcpy(w[InputText::Buffer].cast<std::string>().data(), str.data(), str.size());
        w[InputText::Buffer].cast<std::string>()[str.size()] = '\0';
    }

    ImGui::PushID(w._wid);
    if (ImGui::InputTextWithHint("", hint.c_str(), buf.data(), InputText::InputBufferSize, flags)) {
        w[InputText::Value] = std::string{ buf.data(), strlen(buf.data()) };
        w.emitSignal(InputText::Change, Data::Variant{});
    }
    ImGui::PopID();
}

void drawLabel(Renderer& r, Widget& w) {
    auto& text = w[Label::Text].cast<std::string>();
    const char* ctext = text.c_str();

    //ImVec2 textSize = ImGui::CalcTextSize(ctext);
    //ImVec2 avail = ImGui::GetContentRegionAvail();
    //ImGui::SetCursorPosX(ImGui::GetCursorPosX() + avail.x - textSize.x);

    ImGui::Text(ctext);
}

void listViewEnsureScenes(Widget& w, int count) {
    auto& scenes = w[ListView::SceneInstances].list();
    if (scenes.size() < count) {
        for (int i = scenes.size(); i < count; i++) {
            scenes.push_back(Data::Variant::makePointer(&Ui::createSubScene(w[ListView::Scene], w, true)));
        }
    }
}

void drawListView(Renderer& r, Widget& w) {
    auto& instances = w[ListView::Instances].list();

    int itemCount = w[ListView::Items].cast<int>();
    if (itemCount < 0) {
        w[ListView::Items] = 0;
        itemCount = w[ListView::Items].cast<int>();
    }

    // if the key changed, rebuild the whole list
    if (w[ListView::Key] != w[ListView::PrevKey]) {
        instances.clear();
        w[ListView::PrevKey] = w[ListView::Key];
    }

    // make sure we have enough scenes
    listViewEnsureScenes(w, itemCount);

    // check for insertion/deletion of elements
    {
        int instanceCount = instances.size();
        if (itemCount > instanceCount) {
            instances.resize(itemCount);

            // create new elements
            for (int i = instanceCount; i < itemCount; i++) {
                ListView::Item* item = new ListView::Item{};
                item->index = i;
                item->scene = w[ListView::SceneInstances].list()[i].castPointer<Ui::SceneInstance>();

                instances[i] = Data::Variant::makeUniquePointer(item);
                w.emitSignal(
                    ListView::Bind,
                    Data::Variant::makePointer(instances[i].castUniquePointer<ListView::Item>()),
                    true
                );
            }
        }
        else if (itemCount < instanceCount) {
            instances.resize(itemCount);
        }
    }

    // draw the scenes
    for (int i = 0; i < itemCount; i++) {
        auto instance = instances[i].castUniquePointer<ListView::Item>();
        drawWidget(r, *instance->scene->owner());
    }
}

void drawMenuBar(Renderer& r, Widget& mb) {
    bool open = false;
    bool isMain = mb._parent->_type == WidgetType::Base;
    if (isMain) {
        open = ImGui::BeginMainMenuBar();
    }
    else {
        open = ImGui::BeginMenuBar();
    }

    if (open) {
        drawChildren(r, mb);
        if (isMain)
            ImGui::EndMainMenuBar();
        else
            ImGui::EndMenuBar();
    }
}

void drawMenu(Renderer& r, Widget& m) {
    auto& title = m[Menu::Title].cast<std::string>();
    if (ImGui::BeginMenu(title.c_str())) {
        drawChildren(r, m);
        ImGui::EndMenu();
    }
}

void drawMenuItem(Renderer& r, Widget& mi) {
    bool* pSelected = nullptr;
    /*
    if (!mi.selectable())
        pSelected = nullptr;
    */

    auto& title = mi[MenuItem::Title].cast<std::string>();
    auto& shortcut = mi[MenuItem::Shortcut].cast<std::string>();
    if (ImGui::MenuItem(title.c_str(), shortcut.c_str(), pSelected)) {
        mi.emitSignal(MenuItem::Click, {});
    }
}

void drawSeparator(Renderer& r, Widget& w) {
    ImVec2 size = { 0.0f, w[Separator::Margin] };
    ImGui::Dummy(size);
    ImGui::Separator();
    ImGui::Dummy(size);
}

void drawSpacing(Renderer& r, Widget& w) {
    float width = w[Spacing::Width];
    float height = w[Spacing::Height];
    ImGui::Dummy(ImVec2{ width, height });
}

void drawSubScene(Renderer& r, Widget& w) {
    auto scene = w[SubScene::SceneInstance].castUniquePointer<Ui::SceneInstance>();
    if (scene->_scene.resource().state() == Resource::ResourceState::Loaded) {
        if (scene->inlineStyle()) {
            r.addStyle(*scene->inlineStyle());
        }

        ImGui::PushID(w._wid);
        drawWidget(r, scene->rootWidget());
        ImGui::PopID();
    }
}

void drawVerticalLayoutChildren(Renderer& r, Widget& w) {
    std::size_t size = w._children.size();
    float padding = w[HorizontalLayout::Padding].cast<float>();
    for (std::size_t i = 0; i < size; i++) {
        drawWidget(r, *w._children[i]);
        if (i < size - 1) {
            ImGui::Dummy(ImVec2{ 0, padding * 2.0f });
        }
    }
}

void drawWindow(Renderer& r, Widget& w) {
    static bool drawingWindow;

    // TODO: implement more window flags

    auto& title = w[Window::Title].cast<std::string>();
    ImGuiWindowFlags windowFlags = w[Window::Flags];
    ImVec2 position = ImVec2(w[Window::Position].floatPtr()[0], w[Window::Position].floatPtr()[1]);
    ImVec2 size = ImVec2(w[Window::Size].floatPtr()[0], w[Window::Size].floatPtr()[1]);
    ImVec2 pivot = ImVec2(w[Window::Pivot].floatPtr()[0], w[Window::Pivot].floatPtr()[1]);
    if (drawingWindow) {
        if (size.x != -1.0f && size.y != -1.0f) {
            //ImGui::SetNextWindowSize(size, ImGuiCond_Always);
        }
        if (position.x != 1.0f && position.y != 1.0f) {
            //ImGui::SetNextWindowPos(position, ImGuiCond_Always, pivot);
        }
        if (ImGui::BeginChild(title.c_str(), size, w[Window::Border].cast<bool>(), windowFlags)) {
            drawChildren(r, w);
        }
        ImGui::EndChild();
    }
    else {
        drawingWindow = true;
        
        // TODO: change ImGuiCond_ if the window is animating
        if (size.x != -1.0f && size.y != -1.0f) {
            ImGui::SetNextWindowSize(size, ImGuiCond_Appearing);
        }
        if (position.x != 1.0f && position.y != 1.0f) {
            ImGui::SetNextWindowPos(position, ImGuiCond_Appearing, pivot);
        }
        if (ImGui::Begin(title.c_str(), nullptr, windowFlags)) {
            drawChildren(r, w);
        }

        //ImVec2 pos = ImGui::GetWindowPos();
        //w.position[0] = pos.x;
        //w.position[1] = pos.y;

        ImGui::End();
        drawingWindow = false;
    }
}


void drawWidget(Renderer& r, Widget& w) {
    if (w._flags & WidgetFlag::Invisible) return;

    // Apply widget styles
    std::vector<std::string> styles = w._styles;
    for (const auto& style : styles) {
        r.pushStyleSettings(style);
    }

    w.updateBindings();

    // Draw specific widget
    switch (w._type) {
    case WidgetType::Base:
        drawChildren(r, w);
        break;

    case WidgetType::Button:
        drawButton(r, w);
        break;

    case WidgetType::Checkbox:
        drawCheckbox(r, w);
        break;

    case WidgetType::ConsoleView:
        drawConsoleView(r, w);
        break;

    case WidgetType::FileBrowser:
        drawFileBrowser(r, w);
        break;

    case WidgetType::HorizontalLayout:
        drawHorizontalLayout(r, w);
        break;

    case WidgetType::InputText:
        drawInputText(r, w);
        break;

    case WidgetType::Label:
        drawLabel(r, w);
        break;

    case WidgetType::ListView:
        drawListView(r, w);
        break;

    case WidgetType::Menu:
        drawMenu(r, w);
        break;

    case WidgetType::MenuBar:
        drawMenuBar(r, w);
        break;

    case WidgetType::MenuItem:
        drawMenuItem(r, w);
        break;

    case WidgetType::Separator:
        drawSeparator(r, w);
        break;

    case WidgetType::Spacing:
        drawSpacing(r, w);
        break;

    case WidgetType::SubScene:
        drawSubScene(r, w);
        break;

    case WidgetType::VerticalLayout:
        drawVerticalLayoutChildren(r, w);
        break;

    case WidgetType::Window:
        drawWindow(r, w);
        break;

    default:
        drawChildren(r, w);
        break;
    }

    w.updateReverseBindings();

    for (auto it = styles.rbegin(); it != styles.rend(); ++it) {
        r.popStyleSettings(*it);
    }
}



// Functions for style application

static int pushedFonts = 0;

void pushStyleSetting(Renderer& r, const Ui::StyleSetting& set) {
    if (set.var == Ui::StyleVar::Font) {
        auto fontid = set.value.cast<std::string>();
        auto font = set.style->fonts()[fontid];
        if (font && font.loaded()) {
            ImGui::PushFont(static_cast<ImFont*>(font.resource().rendererFont()));
            pushedFonts++;
        }
    }
    else if (Ui::isFloat(set.var)) {
        ImGui::PushStyleVar(imGuiStyleVarsFloat[(std::size_t)set.var - ((std::size_t)Ui::StyleVar::BeginFloat) - 1], set.value.cast<float>());
    }
    else if (Ui::isVec2(set.var)) {
        Vector2f v = set.value.cast<Vector2f>();
        ImVec2 imv = { v.x(), v.y() };
        ImGui::PushStyleVar(imGuiStyleVarsVec2[(std::size_t)set.var - ((std::size_t)Ui::StyleVar::BeginVec2) - 1], imv);
    }
    else if (Ui::isColor(set.var)) {
        Vector4f v = set.value.cast<Vector4f>();
        ImVec4 imv = { v.x(), v.y(), v.z(), v.w() };
        ImGuiStyle& style = ImGui::GetStyle();
        ImGuiCol col = imGuiStyleColors[(std::size_t)set.var - ((std::size_t)Ui::StyleVar::BeginColor) - 1];
        r.colorStacks()[col].push(style.Colors[col]);
        style.Colors[col] = imv;
    }
}

void popStyleSetting(Renderer& r, const Ui::StyleSetting& set) {
    switch (set.var) {
    case Ui::StyleVar::Font: {
        auto fontid = set.value.cast<std::string>();
        auto font = set.style->fonts()[fontid];
        if (font && font.loaded() && pushedFonts > 0) {
            ImGui::PopFont();
            pushedFonts--;
        }
        break;
    }
    default: {
        if (Ui::isColor(set.var)) {
            ImGuiStyle& style = ImGui::GetStyle();
            ImGuiCol col = imGuiStyleColors[(std::size_t)set.var - ((std::size_t)Ui::StyleVar::BeginColor) - 1];
            style.Colors[col] = r.colorStacks()[col].top();
            r.colorStacks()[col].pop();
        }
        else {
            ImGui::PopStyleVar(1);
        }
        break;
    }
    }
}
}

Renderer::Renderer(Application* app, Vector2f sw, Vector2i windowSize, Vector2i fbSize)
    : _app{ app }, _imgui{ sw, windowSize, fbSize } {

    /* Set up proper blending to be used by ImGui. There's a great chance
       you'll need this exact behavior for the rest of your scene. If not, set
       this only for the drawFrame() call. */
    GL::Renderer::setBlendEquation(GL::Renderer::BlendEquation::Add,
        GL::Renderer::BlendEquation::Add);
    GL::Renderer::setBlendFunction(GL::Renderer::BlendFunction::SourceAlpha,
        GL::Renderer::BlendFunction::OneMinusSourceAlpha);

    ImGui::GetIO().Fonts->AddFontDefault();
}

void Renderer::addStyle(Ui::Style& style) {
    for (auto& pair : style.settings()) {
        _loadedStyles[pair.first] = &pair.second;
    }
}

void Renderer::pushStyleSettings(const std::string& name) {
    auto it = _loadedStyles.find(name);
    if (it == _loadedStyles.end()) return;

    pushStyleSettings(*it->second);
}

void Renderer::pushStyleSettings(Data::LinkedList<Ui::StyleSetting>& sets) {
    for (auto& set : sets) {
        if (set.animationId != Animation::AnimationIdInvalid)
            set.value = Application::instance().animationManager().animationValue(set.animationId);
        pushStyleSetting(*this, set);
    }
}

void Renderer::popStyleSettings(const std::string& name) {
    auto it = _loadedStyles.find(name);
    if (it == _loadedStyles.end()) return;

    popStyleSettings(*it->second);
}

void Renderer::popStyleSettings(Data::LinkedList<Ui::StyleSetting>& sets) {
    for (auto it = sets.rbegin(); it != sets.rend(); ++it) {
        popStyleSetting(*this, *it);
    }
}

void* Renderer::processFontData(const Containers::ArrayView<unsigned char>& data, float pixelSize) {
    ImGuiIO& io = ImGui::GetIO();

    ImFontConfig cfg;
    cfg.FontDataOwnedByAtlas = false;
	ImFont* imfont = io.Fonts->AddFontFromMemoryTTF((void*)(data.data()), data.size(), pixelSize, &cfg);
    _imgui.rebuildFonts();
    return (void*)imfont;
}

namespace {
static int historyIndex = -1;
int consoleInputCallback(ImGuiInputTextCallbackData* data) {
    if (data->EventFlag == ImGuiInputTextFlags_CallbackHistory) {
        int hsize = int(Console::history().size());
        if (data->EventKey == ImGuiKey_UpArrow && historyIndex < hsize) {
            historyIndex += 1;
        }
        else if (data->EventKey == ImGuiKey_DownArrow && historyIndex >= 0) {
            historyIndex -= 1;
        }

        int actualIndex = hsize - historyIndex - 1;
        if (actualIndex < 0 || historyIndex < 0) {
            std::memset(data->Buf, 0, data->BufSize);
            data->BufTextLen = 0;
            data->BufDirty = true;
            return 0;
        }

        const std::string& cmd = Console::history()[actualIndex];
        std::memcpy(data->Buf, cmd.data(), cmd.size());
        data->Buf[cmd.size()] = '\0';
        data->BufTextLen = cmd.size();
        data->BufDirty = true;
    }

    return 0;
}
}

void Renderer::drawUi(SceneInstance& scene) {
    /* Enable text input, if needed */
    if(ImGui::GetIO().WantTextInput && !_app->isTextInputActive())
        _app->startTextInput();
    else if(!ImGui::GetIO().WantTextInput && _app->isTextInputActive())
        _app->stopTextInput();

    /* 3. Show the ImGui test window. Most of the sample code is in
       ImGui::ShowTestWindow() */

    //ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
    //ImGui::ShowTestWindow();

    /* Set appropriate states. If you only draw imgui UI, it is sufficient to
       do this once in the constructor. */
    GL::Renderer::enable(GL::Renderer::Feature::Blending);
    GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);
    GL::Renderer::disable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::enable(GL::Renderer::Feature::ScissorTest);

    // Clear all styles
    _loadedStyles.clear();

    if (scene.inlineStyle()) {
        addStyle(*scene.inlineStyle());
    }

    drawWidget(*this, scene.rootWidget());

    if (Console::wantToShow()) {
        _showConsole = true;
    }

    if (_showConsole) {
        Console::update();
        ImGui::SetNextWindowSize(ImVec2(520, 600), ImGuiCond_FirstUseEver);
        if (ImGui::Begin("Console")) {
            // Render fps average
            static std::deque<unsigned long> frames;
            frames.push_back(_app->deltaTime() * 1000.0f);
            if (frames.size() > 30) {
                frames.pop_front();
            }
            unsigned long framesSum = 0;
            for (const auto& f : frames)
                framesSum += f;

            unsigned long framesAvg = framesSum / frames.size();
            ImGui::Text("Frame: %dms / %.1f fps", framesAvg, (1.0f / (framesAvg/1000.0f)));

            ImGui::BeginChild("Output", ImVec2(0, -32), true);
            {
                auto ent = Console::entries();
                static std::size_t numEntries;
                for (auto& entry : *ent.entries) {
                    Vector3f color = entry->color();
                    std::string output = entry->output();
                    ImGui::TextColored(ImVec4{ color.r(), color.g(), color.b(), 1.0f }, output.c_str());
                }
                if (numEntries != ent.entries->size() && Console::var("console_autoScroll")) {
                    ImGui::SetScrollHereY();
                }
                if (Console::var("console_lockScroll")) {
                    ImGui::SetScrollHereY();
                }
                numEntries = ent.entries->size();
            }
            ImGui::EndChild();

            static Containers::StaticArray<512, char> inputBuf;
            ImGui::Separator();
            if (ImGui::IsRootWindowOrAnyChildFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked(0))
                ImGui::SetKeyboardFocusHere();
            bool submitInput = ImGui::InputText(
                "###Input", inputBuf.data(), inputBuf.size(),
                ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackHistory,
                consoleInputCallback
            ); ImGui::SameLine();
            bool submitButton = ImGui::Button("Submit");
            if ((submitInput || submitButton) && inputBuf[0] != '\0') {
                historyIndex = -1;
                std::size_t cmdSize = 0;
                for (std::size_t i = 0; i < inputBuf.size(); i++) {
                    if (inputBuf[i] == '\0') break;
                    cmdSize++;
                }
                Console::exec(std::string{ inputBuf.data(), cmdSize });
                std::memset(inputBuf.data(), 0, 512);
            }
        }
        ImGui::End();
    }

    _imgui.drawFrame();

    /* Reset state. Only needed if you want to draw something else with
       different state next frame. */
    GL::Renderer::disable(GL::Renderer::Feature::ScissorTest);
    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);
    GL::Renderer::disable(GL::Renderer::Feature::Blending);
}

void Renderer::newFrame() {
    _imgui.newFrame();
}

void Renderer::relayout(Vector2f scaledWindowSize, Vector2i windowSize, Vector2i fbSize) {
    _imgui.relayout(scaledWindowSize, windowSize, fbSize);
}

}
