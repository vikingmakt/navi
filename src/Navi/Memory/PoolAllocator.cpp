#include <cmath>
#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi::Memory {

PoolAllocator::PoolAllocator(size_t elemSize, size_t bucketElemCount, float bucketGrowFactor) {
    _elemSize = elemSize;
    _bucketBaseElemCount = bucketElemCount;
    _bucketGrowFactor = bucketGrowFactor;
}

PoolAllocator::~PoolAllocator() {
    for (auto& bucket : _buckets) {
        delete[] bucket->data;
        delete bucket;
    }
}

void* PoolAllocator::alloc(std::size_t) {
    // ignore size parameter since the objects are all the same size

    for (Bucket* b : _buckets) {
        if (!isBucketFull(*b)) {
            return bucketAlloc(*b);
        }
    }

    // if all the buckets are full, create a new one
    std::size_t elemCount = _bucketBaseElemCount;
    if (!_buckets.empty()) {
        elemCount = std::size_t(std::round(_buckets.back()->elemCount * _bucketGrowFactor));
    }
    createBucket(elemCount);
    Bucket& b = *_buckets.back();
    return bucketAlloc(b);
}

void PoolAllocator::dealloc(void* it) {
    for (Bucket* b : _buckets) {
        unsigned char* bit = static_cast<unsigned char*>(it);
        if (bit >= b->data && bit < b->end) {
            size_t idx = static_cast<size_t>(uintptr_t((uintptr_t(bit) - uintptr_t(b->data)) / _elemSize));
            b->indices[--b->nextIndex] = idx;
        }
    }
}

void PoolAllocator::createBucket(size_t elemCount) {
    auto b = new Bucket{};
    b->elemCount = elemCount;
    b->indices = new unsigned short[elemCount];
    b->data = new unsigned char[_elemSize * elemCount];
    b->end = b->data + (_elemSize * elemCount);
    b->nextIndex = 0;

    for (std::size_t i = 0; i < elemCount; i++) {
        b->indices[i] = i;
    }

    _buckets.push_back(b);
}

bool PoolAllocator::isBucketFull(const Bucket& b) const {
    return b.nextIndex >= b.elemCount;
}

void* PoolAllocator::bucketAlloc(Bucket& b) {
    void* result = static_cast<void*>(b.data + (b.indices[b.nextIndex] * _elemSize));
    b.nextIndex++;
    return result;
}

}
