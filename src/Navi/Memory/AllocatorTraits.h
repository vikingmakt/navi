#ifndef Navi_Memory_AllocatorTraits_h
#define Navi_Memory_AllocatorTraits_h

namespace Navi::Memory {

template<class T>
struct AllocatorTraits {
    static constexpr bool deallocates = true;
};

}

#endif
