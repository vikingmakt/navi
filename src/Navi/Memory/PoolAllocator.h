#ifndef Navi_Memory_PoolAllocator_h
#define Navi_Memory_PoolAllocator_h

#include "Navi/Navi.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Memory/Pointer.h"

namespace Navi::Memory {

/**
A memory pool of uniformly sized objects that grows as needed
*/
class PoolAllocator {
    public:
        struct Bucket {
            unsigned char* data;
            unsigned char* end;
            unsigned short* indices;
            std::size_t nextIndex;
            std::size_t elemCount;
        };

        explicit PoolAllocator(NoCreateT)
            : _elemSize{0}, _bucketGrowFactor{0} {}

        explicit PoolAllocator(std::size_t elemSize, std::size_t bucketElemCount = (1 << 8), float bucketGrowFactor = 1.5f);

        PoolAllocator(const PoolAllocator&) = delete;
        PoolAllocator(PoolAllocator&& other) {
            *this = std::move(other);
        }

        ~PoolAllocator();

        PoolAllocator& operator =(const PoolAllocator&) = delete;
        PoolAllocator& operator =(PoolAllocator&& other) {
            _buckets = std::move(other._buckets);
            _elemSize = other._elemSize;
            _bucketGrowFactor = other._bucketGrowFactor;
            _bucketBaseElemCount = other._bucketBaseElemCount;
            return *this;
        }

        void* alloc(std::size_t size);
        void  dealloc(void* it);

        std::size_t elemSize() const { return _elemSize; }
        std::size_t bucketElemCount() const { return _bucketBaseElemCount; }
        const Data::LinkedList<Bucket*>& buckets() const { return _buckets; }

    private:
        void createBucket(size_t elemCount);
        bool isBucketFull(const Bucket& b) const;
        void* bucketAlloc(Bucket& b);

        Data::LinkedList<Bucket*> _buckets;
        std::size_t _elemSize;
        std::size_t _bucketBaseElemCount;
        float _bucketGrowFactor;
};

template<class T>
using PoolPointer = Pointer<T, PoolAllocator>;

}

#endif
