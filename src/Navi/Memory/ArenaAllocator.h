#ifndef Navi_Memory_ArenaAllocator_h
#define Navi_Memory_ArenaAllocator_h

#include "Navi/Navi.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Memory/Pointer.h"

namespace Navi::Memory {

namespace Implementation {
    struct ArenaBucket {
        std::uint8_t* data;
        std::uint8_t* end;
        std::size_t capacity;
        std::size_t size;
        std::size_t alignment;
    };
}

class ArenaAllocator {
    public:
        explicit ArenaAllocator(NoCreateT)
            : _bucketSize{0}, _alignment{0} {}

        explicit ArenaAllocator(std::size_t bucketSize, std::size_t alignment = sizeof(std::size_t));

        ArenaAllocator(const ArenaAllocator&) = delete;
        ArenaAllocator(ArenaAllocator&& other) {
            *this = std::move(other);
        }

        ~ArenaAllocator();

        ArenaAllocator& operator =(const ArenaAllocator&) = delete;
        ArenaAllocator& operator =(ArenaAllocator&& other) {
            _buckets = std::move(other._buckets);
            _bucketSize = other._bucketSize;
            _alignment = other._alignment;
            return *this;
        }

        void* alloc(std::size_t size);
        void clear();
        void dealloc(void* it) {}

    private:
        Data::LinkedList<Implementation::ArenaBucket*> _buckets;
        std::size_t _bucketSize;
        std::size_t _alignment;
};

template<>
struct AllocatorTraits<ArenaAllocator> {
    static constexpr bool deallocates = false;
};

template<class T>
using ArenaPointer = Pointer<T, ArenaAllocator>;

}

#endif
