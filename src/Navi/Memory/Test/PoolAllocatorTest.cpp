/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <string>
#include <Corrade/TestSuite/Tester.h>

#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi { namespace Memory { namespace Test { namespace {

using namespace Corrade;

struct PoolAllocatorTest: TestSuite::Tester {
    explicit PoolAllocatorTest();

    void constructTest();
    void allocateTest();

    void allocateAndFree1kUsingNew();
    void allocateAndFree1kUsingPool();
};

PoolAllocatorTest::PoolAllocatorTest() {
    addTests({
        &PoolAllocatorTest::constructTest,
        &PoolAllocatorTest::allocateTest});

    addBenchmarks({
        &PoolAllocatorTest::allocateAndFree1kUsingPool,
        &PoolAllocatorTest::allocateAndFree1kUsingNew}, 100);
}

void PoolAllocatorTest::constructTest() {
    {
        PoolAllocator alloc{ NoCreate };
        CORRADE_VERIFY(alloc.elemSize() == 0);
    }
}

void PoolAllocatorTest::allocateTest() {
    {
        struct MyData {
            std::string str;
            int i;

            MyData() : str{}, i{} {}
        };
        PoolAllocator alloc{ sizeof(MyData) };

        {
            Data::LinkedList<PoolPointer<MyData>> list;
            for (int i = 0; i < alloc.bucketElemCount(); i++) {
                list.push_back(makePointer<MyData>(alloc));
                CORRADE_COMPARE(list.back()->i, 0);
                CORRADE_VERIFY(list.back()->str.size() == 0);

                list.back()->i = i;
                list.back()->str = "My Data";
            }

            CORRADE_COMPARE(alloc.buckets().size(), 1);
            CORRADE_COMPARE(alloc.buckets().front()->nextIndex, alloc.buckets().front()->elemCount);
        }

        CORRADE_COMPARE(alloc.buckets().size(), 1);
        CORRADE_COMPARE(alloc.buckets().begin()->nextIndex, 0);

        {
            Data::LinkedList<PoolPointer<MyData>> list;
            for (int i = 0; i < int(std::round(alloc.bucketElemCount()*2.5f)); i++) {
                list.push_back(makePointer<MyData>(alloc));
                CORRADE_COMPARE(list.back()->i, 0);
                CORRADE_VERIFY(list.back()->str.size() == 0);
            }

            CORRADE_COMPARE(alloc.buckets().size(), 2);
            CORRADE_COMPARE(alloc.buckets().front()->nextIndex, alloc.buckets().front()->elemCount);
            CORRADE_COMPARE(alloc.buckets().back()->nextIndex, alloc.buckets().back()->elemCount);
        }

        CORRADE_COMPARE(alloc.buckets().size(), 2);
        CORRADE_COMPARE(alloc.buckets().begin()->nextIndex, 0);
        CORRADE_COMPARE(alloc.buckets().back()->nextIndex, 0);
    }
}

void PoolAllocatorTest::allocateAndFree1kUsingNew() {
    struct MyData {
        std::string str;
        int i;
    };
    MyData data{};
    CORRADE_BENCHMARK(100) {
        {
            Data::LinkedList<Containers::Pointer<MyData>> list;
            for (int i = 0; i < 1000; i++) {
                auto ptr = Containers::pointer<MyData>();
                ptr->i = i;
                list.push_back(std::move(ptr));
            }
            data.i += list.back()->i;
        }
        {
            Data::LinkedList<Containers::Pointer<MyData>> list;
            for (int i = 0; i < 1000; i++) {
                auto ptr = Containers::pointer<MyData>();
                ptr->i = i;
                list.push_back(std::move(ptr));
            }
            data.i += list.back()->i;
        }
    }
    CORRADE_VERIFY(data.i);
}

void PoolAllocatorTest::allocateAndFree1kUsingPool() {
    struct MyData {
        std::string str;
        int i;
    };
    MyData data{};
    PoolAllocator alloc{ sizeof(MyData) };
    CORRADE_BENCHMARK(100) {
        {
            Data::LinkedList<PoolPointer<MyData>> list;
            for (int i = 0; i < 1000; i++) {
                auto ptr = makePointer<MyData>(alloc);
                ptr->i = i;
                list.push_back(std::move(ptr));
            }
            data.i += list.back()->i;
        }
        {
            Data::LinkedList<PoolPointer<MyData>> list;
            for (int i = 0; i < 1000; i++) {
                auto ptr = makePointer<MyData>(alloc);
                ptr->i = i;
                list.push_back(std::move(ptr));
            }
            data.i += list.back()->i;
        }
    }
    CORRADE_VERIFY(data.i);
}

}}}}

CORRADE_TEST_MAIN(Navi::Memory::Test::PoolAllocatorTest)
