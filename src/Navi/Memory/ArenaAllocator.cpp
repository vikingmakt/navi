#include <cmath>
#include "Navi/Memory/ArenaAllocator.h"

namespace Navi::Memory {

namespace Implementation {

std::size_t nextPow2(std::size_t n) {
    std::size_t p = 1;
    if (n && !(n & (n - 1)))
        return n;

    while (p < n)
        p <<= 1;

    return p;
}

std::size_t alignmentOffset(const ArenaBucket& b) {
    const std::uintptr_t resultPtr = reinterpret_cast<std::uintptr_t>(b.data) + b.size;
    const std::size_t mask = b.alignment - 1;
    if (resultPtr & mask) {
        return b.alignment - (resultPtr & mask);
    }
    return 0;
}

ArenaBucket* createBucket(std::size_t cap, std::size_t alignment) {
    auto b = new ArenaBucket{};
    b->data = new std::uint8_t[cap];
    b->end = b->data + cap;
    b->capacity = cap;
    b->size = 0;
    b->alignment = alignment;
    return b;
}

bool fitsInBucket(const ArenaBucket& b, std::size_t size) {
    const std::size_t offset = alignmentOffset(b);
    return (b.size + size + offset) <= b.capacity;
}

void* bucketAlloc(ArenaBucket& b, std::size_t size) {
    const std::size_t offset = alignmentOffset(b);
    void* addr = static_cast<void*>(b.data + b.size + offset);
    b.size += size + offset;
    return addr;
}

}

ArenaAllocator::ArenaAllocator(std::size_t bucketSize, std::size_t alignment) {
    _bucketSize = Implementation::nextPow2(bucketSize);
    _alignment = alignment;
}

ArenaAllocator::~ArenaAllocator() {
    for (auto& bucket : _buckets) {
        delete[] bucket->data;
        delete bucket;
    }
}

void* ArenaAllocator::alloc(std::size_t size) {
    for (const auto& bucket : _buckets) {
        if (Implementation::fitsInBucket(*bucket, size)) {
            return Implementation::bucketAlloc(*bucket, size);
        }
    }

    // all buckets are full, create a new one
    _buckets.push_back(Implementation::createBucket(_bucketSize, _alignment));
    return Implementation::bucketAlloc(*_buckets.back(), size);
}

void ArenaAllocator::clear() {
    for (const auto& bucket : _buckets) {
        bucket->size = 0;
    }
}

}
