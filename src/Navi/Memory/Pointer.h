#ifndef Navi_Memory_Pointer_h
#define Navi_Memory_Pointer_h

#include "Navi/Navi.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Memory/AllocatorTraits.h"

namespace Navi::Memory {

template<class T, class A>
class Pointer {
    public:
        Pointer()
            : _ptr{ nullptr }, _allocator{ nullptr } {}
        Pointer(T* ptr, A* allocator)
            : _ptr{ ptr }, _allocator{ allocator } {}

        Pointer(const Pointer&) = delete;
        Pointer(Pointer&& other) {
            *this = std::move(other);
        }

        ~Pointer() {
            if (_ptr != nullptr) {
                _ptr->~T();
                _allocator->dealloc(static_cast<void*>(_ptr));
                _ptr = nullptr;
            }
        }

        Pointer& operator =(const Pointer&) = delete;
        Pointer& operator =(Pointer&& other) {
            _allocator = other._allocator;
            _ptr = other._ptr;
            other._ptr = nullptr;
            return *this;
        }

        T* operator ->() const {
            return _ptr;
        }

        T& operator *() const {
            return *_ptr;
        }

        operator bool() const {
            return _ptr != nullptr;
        }

        bool operator ==(const Pointer<T, A>& other) const {
            return _ptr == other._ptr;
        }

        T* get() const {
            return _ptr;
        }

        A* allocator() const {
            return _allocator;
        }

        void release() {
            _ptr = nullptr;
        }

    private:
        A* _allocator;
        T* _ptr;
};

/// Allocates an object of type T
template<class T, class A, class... Args>
Pointer<T, A> makePointer(A& allocator, Args&&... args) {
    void* addr = allocator.alloc(sizeof(T));
    T* obj = new (addr) T(std::forward<Args>(args)...);
    return Pointer<T, A>{ obj, &allocator };
}

template<class To, class From, class A>
Pointer<To, A> staticPointerCast(Pointer<From, A>&& from) {
    auto result = Pointer<To, A>{ static_cast<To*>(from.get()), from.allocator() };
    from.release();
    return result;
}

}

#endif
