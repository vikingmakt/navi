#ifndef Navi_Data_LinkedList_h
#define Navi_Data_LinkedList_h

#include <initializer_list>
#include <utility>

namespace Navi::Data {

/**
LinkedList<> is a std::list<>-style lightweight linked list which is meant to store only built-in or pointer types
*/
template<class T>
class LinkedList {
    public:
        struct Item {
            T content;
            Item* previous;
            Item* next;
        };

        struct Iterator {
            T& operator *() {
                return item->content;
            }

            T& operator ->() {
                return item->content;
            }

            Iterator& operator ++() {
                item = item->next;
                return *this;
            }

            bool operator ==(const Iterator& other) {
                return item == other.item;
            }

            bool operator !=(const Iterator& other) {
                return item != other.item;
            }

            Item* item;
        };

        struct ReverseIterator {
            T& operator *() {
                return item->content;
            }

            T& operator ->() {
                return item->content;
            }

            ReverseIterator& operator ++() {
                item = item->previous;
                return *this;
            }

            bool operator ==(const ReverseIterator& other) {
                return item == other.item;
            }

            bool operator !=(const ReverseIterator& other) {
                return item != other.item;
            }

            Item* item;
        };

        LinkedList()
            : _size{ 0 }, _first { nullptr }, _last{ nullptr } {}
        LinkedList(const LinkedList& other) {
            *this = other;
        }
        LinkedList(LinkedList&& other) {
            *this = std::move(other);
        }
        LinkedList(std::initializer_list<T> list) {
            *this = list;
        }

        ~LinkedList() {
            Item* item = _first;
            while (item) {
                Item* next = item->next;
                delete item;
                item = next;
            }
        }

        LinkedList& operator =(const LinkedList& other) {
            clear();
            for (auto& item : other) {
                push_back(item);
            }
            return *this;
        }
        LinkedList& operator =(LinkedList&& other) {
            _first = other._first;
            _last = other._last;
            other._first = other._last = nullptr;
            return *this;
        }
        LinkedList& operator =(std::initializer_list<T> list) {
            clear();
            for (auto& item : list) {
                push_back(item);
            }
            return *this;
        }

        void push_back(const T& elem);
        void push_back(T&& elem);

        void pop_front();
        void pop_back();

        void remove(const T& elem);

        void erase(const Iterator& it);

        void clear();

        Iterator find(const T& elem);

        std::size_t size() const { return _size; }

        T& front() { return _first->content; }
        T& back()  { return _last->content; }

        const T& front() const { return _first->content; }
        const T& back() const { return _last->content; }

        Iterator begin() const { return Iterator{ _first }; }
        Iterator end()   const { return Iterator{ nullptr }; }

        ReverseIterator rbegin() const { return ReverseIterator{ _last }; }
        ReverseIterator rend() const { return ReverseIterator{ nullptr }; }

        bool empty() const { return begin() == end(); }

    private:
        std::size_t _size = 0;
        Item* _first = nullptr;
        Item* _last = nullptr;
};

}

#endif
