#ifndef Navi_Data_Tokenizer_h
#define Navi_Data_Tokenizer_h

#include <string>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Navi.h"

namespace Navi::Data {

enum class ReservedType {
    Animation,
    Console,
    Cvar,
    Else,
    If,
    Signal,
    Then,
    Var,
};

struct Lexer;

class Tokenizer {
    public:
        explicit Tokenizer(std::string str);

        const std::string& source();

        int current();

        int next();

        Containers::Optional<ReservedType> parseReserved();

        Containers::Optional<std::string> parseIdentifier();

        Containers::Optional<std::string> parseString(bool allowNonQuoted = false);

        Containers::Optional<bool> parseBool();

        Containers::Optional<int> parseInt();

        Containers::Optional<float> parseFloat();

        Containers::Optional<Data::Variant> parseVariantNumber();

        Containers::Optional<Vector2f> parseVector2();

        Containers::Optional<Vector4f> parseHexColor();

    private:
        Containers::Pointer<Lexer> _lex;
        std::string _str;
        int _tok;
};

}

#endif