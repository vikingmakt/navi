#ifndef Navi_Data_PoolArray_h
#define Navi_Data_PoolArray_h

#include <queue>
#include <limits>
#include "Navi/Data/UnorderedArray.h"

namespace Navi::Data {

template<typename T>
struct PoolArray {
    enum Flag {
        Alive = 1,
        Enabled = 1 << 1,
    };

    typedef std::size_t Id;

	struct Entry {
		unsigned int flags;
		T data;
	};

    static constexpr Id IdInvalid = std::numeric_limits<Id>::max();

	// TODO: don't use STL stuff here
	UnorderedArray<Entry> entries;
	std::queue<Id> freeList;
	UnorderedArray<Id> aliveList;

	Id insert(const T& data)
	{
		Id id;
		if (freeList.size() > 0)
		{
			id = freeList.front();
			freeList.pop();
			entries.at(id).data = data;
			entries.at(id).flags |= Flag::Alive | Flag::Enabled;
		}
		else
		{
			id = entries.size();
			Entry entry = { Flag::Alive | Flag::Enabled, data };
			entries.push_back(entry);
		}
		aliveList.push_back(id);
		return id;
	}

	T& emplace(Id& id)
	{
		if (freeList.size() > 0)
		{
			id = freeList.front();
			freeList.pop();
			aliveList.push_back(id);
			entries.at(id).flags |= Flag::Alive | Flag::Enabled;

			auto& data = entries.at(id).data;
			new (&data) T();
			return data;
		}
		else
		{
			id = entries.size();
			aliveList.push_back(id);
			auto& entry = entries.emplace_back();
			entry.flags |= Flag::Alive | Flag::Enabled;
			return entry.data;
		}
	}

	Id remove(Id id)
	{
		if (id == IdInvalid) return IdInvalid;

		if (entries.at(id).flags & Flag::Alive)
		{
            aliveList.remove(id);
            freeList.push(id);
			entries.at(id).flags &= ~(Flag::Alive | Flag::Enabled);
		}
		return IdInvalid;
	}

	void disable(Id id)
	{
		if (id == IdInvalid) return;

		if (entries.at(id).flags & Flag::Enabled)
		{
			aliveList.remove(id);
			entries.at(id).flags &= ~Flag::Enabled;
		}
	}

	void enable(Id id)
	{
		if (id == IdInvalid) return;
		if (entries.at(id).flags & Flag::Enabled) return;

		aliveList.push_back(id);
		entries.at(id).flags |= Flag::Enabled;
	}

	T& get(Id id)
	{
		return entries.at(id).data;
	}

	bool isAlive(Id id)
	{
		return entries.at(id).flags & Flag::Alive;
	}

    bool isEnabled(Id id)
    {
        return entries.at(id).flags & Flag::Enabled;
    }
};

}

#endif