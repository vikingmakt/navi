#ifndef Navi_Data_Tree_h
#define Navi_Data_Tree_h

#include "Navi/Data/FixedArray.h"

namespace Navi::Data {

/// A tree optimized for immutability
template<class T>
class Tree {
    public:
        class Node;

        class NodeIterator {
            public:
                NodeIterator(Tree& tree, std::size_t nodeIndex) : _tree{ &tree }, _nodeIndex{ nodeIndex } {}

                Node& operator *() const {
                    return _tree->_nodes.at(_nodeIndex);
                }

                Node& operator ->() const {
                    return _tree->_nodes.at(_nodeIndex);
                }

                NodeIterator& operator ++() {
                    Node& self = _tree->_nodes.at(_nodeIndex);
                    _nodeIndex = self._nextSiblingIndex;
                    return *this;
                }

                bool operator ==(const NodeIterator& other) {
                    return _nodeIndex == other._nodeIndex;
                }

                bool operator !=(const NodeIterator& other) {
                    return _nodeIndex != other._nodeIndex;
                }

            private:
                Tree* _tree;
                std::size_t _nodeIndex;
        };

        class Node {
            public:
                Node() {}

                Node(Tree& tree, const T& data, std::size_t selfIndex) : _tree{ &tree }, _data{ data }, _selfIndex{ selfIndex } {}

                Node& addChild(const T& v) {
                    if (_firstChildIndex == 0) {
                        _firstChildIndex = _tree->_nodes.size();
                        _tree->_nodes.push_back(Node{ *_tree, v, _firstChildIndex });
                    }
                    else {
                        Node* prevSibling = &_tree->_nodes.at(_firstChildIndex);
                        while (prevSibling->_nextSiblingIndex != 0) {
                            prevSibling = &_tree->_nodes.at(prevSibling->_nextSiblingIndex);
                        }

                        std::size_t idx = prevSibling->_nextSiblingIndex = _tree->_nodes.size();
                        _tree->_nodes.push_back(Node{ *_tree, v, idx });
                    }
                    return _tree->_nodes.back();
                }

                NodeIterator begin() const {
                    return NodeIterator{ *_tree, _firstChildIndex };
                }

                NodeIterator end() const {
                    return NodeIterator{ *_tree, 0 };
                }

                Node& front() {
                    return _tree->_nodes.at(_firstChildIndex);
                }

                T& operator *() {
                    return _data;
                }

                T& operator ->() {
                    return _data;
                }

                const T& operator *() const {
                    return _data;
                }

                const T& operator ->() const {
                    return _data;
                }

                operator T() {
                    return _data;
                }

            private:
                friend class NodeIterator;
                friend class Tree;

                T _data;
                std::size_t _firstChildIndex = 0;
                std::size_t _nextSiblingIndex = 0;
                std::size_t _selfIndex = 0;
                Tree* _tree = nullptr;
        };

        Tree() : _nodes{ } {}

        Tree(std::size_t capacity) : _nodes{ capacity } {
            _nodes.push_back(Node{ *this, T{}, 0 });
        }

        Tree(const T& rootValue, std::size_t capacity) : _nodes{ capacity } {
            _nodes.push_back(Node{ *this, rootValue, 0 });
        }

        Tree(const Tree& other) {
            *this = other;
        }

        Tree& operator =(const Tree& other) {
            _nodes = other._nodes;
            for (auto& node : _nodes) {
                node._tree = this;
            }
            return *this;
        }

        Node& root() { return _nodes.at(0); }

        const FixedArray<Node>& nodes() const { return _nodes; }

        void clear() {
            T rootValue = *_nodes.front();
            _nodes.clear();

            // Re-build root node
            _nodes.push_back(Node{ *this, rootValue, 0 });
        }

    private:
        FixedArray<Node> _nodes;
};

}

#endif
