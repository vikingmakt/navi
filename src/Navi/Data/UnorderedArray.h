#ifndef Navi_Data_UnorderedArray_h
#define Navi_Data_UnorderedArray_h

#include <vector>

namespace Navi::Data {

template<class T>
class UnorderedArray {
    public:
        struct Element {
            T data;
            bool alive;
        };

        struct Iterator {
            T& operator *() {
                return _arr->at(_i);
            }

            T& operator ->() {
                return _arr->at(_i);
            }

            Iterator& operator ++() {
                ++_i;
                while (!_arr->element(_i).alive && _i < _arr->_size) {
                    ++_i;
                }
                return *this;
            }

            bool operator ==(const Iterator& other) {
                return _i == other._i;
            }

            bool operator !=(const Iterator& other) {
                return _i != other._i;
            }

            UnorderedArray<T>* _arr;
            std::size_t _i;
        };

        void push_back(const T& elem) {
            ensureCapacity();
            _data[_size++] = { elem, true };
        }

        void push_back(T&& elem) {
            ensureCapacity();
            _data[_size++] = { std::move(elem), true };
        }

        T& emplace_back() {
            ensureCapacity();
            Element& result = _data[_size++];
            result.alive = true;
            return result.data;
        }

        void erase(size_t i) {
            _data[i] = _data[_size - 1];
            _size -= 1;
        }

        void remove(const T& val) {
            for (size_t i = 0; i < _size; i++) {
                if (_data[i].data == val) {
                    erase(i);
                    return;
                }
            }
        }

        Element& element(size_t i) {
            return _data[i];
        }

        T& at(size_t i) {
            if (!(i < _size))
                throw std::runtime_error("unordered array index out of bounds");
            return _data.at(i).data;
        }

        T& front() {
            return _data[0].data;
        }

        T& back() {
            return _data[_size - 1].data;
        }

        void clear() {
            _size = 0;
            _capacity = 0;
            _data.clear();
        }

        size_t size() const { return _size; }

        Iterator begin(std::size_t i) {
            return Iterator{ this, i };
        }

        Iterator begin() {
            return Iterator{ this, 0 };
        }

        Iterator end() {
            return Iterator{ this, _size };
        }

    private:
        void ensureCapacity() {
            if (_size >= _capacity) {
                if (_capacity == 0) {
                    _capacity = 8;
                } else {
                    _capacity *= 2;
                }
                _data.resize(_capacity);
            }
        }

        std::vector<Element> _data;
        std::size_t _capacity = 0;
        std::size_t _size = 0;
};

}

#endif
