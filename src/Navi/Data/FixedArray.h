#ifndef Navi_Data_FixedArray_h
#define Navi_Data_FixedArray_h

#include <Corrade/Containers/Pointer.h>

namespace Navi::Data {

template<class T>
class FixedArray {
    public:
        FixedArray() : _capacity{ 0 }, _size{ 0 }, _data{ nullptr } {}
        explicit FixedArray(std::size_t capacity) : _capacity{ capacity }, _size{0}, _data{ capacity ? reinterpret_cast<T*>(new unsigned char[capacity * sizeof(T)]) : nullptr } {}

        FixedArray(const FixedArray& other) { *this = other; }
        FixedArray(FixedArray&& other) { *this = std::move(other); }

        ~FixedArray() {
            freeData();
        }

        FixedArray& operator =(const FixedArray& other) {
            if (_data) freeData();

            _size = other._size;
            _capacity = other._capacity;
            _data = reinterpret_cast<T*>(new unsigned char[_capacity * sizeof(T)]);

            for (std::size_t i = 0; i < _size; i++) {
                new (_data + i) T(*(other._data + i));
            }
            return *this;
        }
        FixedArray& operator =(FixedArray&& other) {
            std::swap(_size, other._size);
            std::swap(_capacity, other._capacity);
            std::swap(_data, other._data);
            return *this;
        }

        void push_back(const T& elem) {
            if (!_data) throw std::runtime_error("zero-size fixed array");

            new (_data + _size) T(elem);
            _size++;
        }

        void push_back(T&& elem) {
            if (!_data) throw std::runtime_error("zero-size fixed array");

            new (_data + _size) T(std::move(elem));
            _size++;
        }

        T& at(size_t i) {
            if (!(i < _size)) {
                throw std::runtime_error("fixed array index out of bounds");
            }
            return _data[i];
        }

        const T& at(size_t i) const {
            if (!(i < _size)) {
                throw std::runtime_error("fixed array index out of bounds");
            }
            return _data[i];
        }

        T& front() {
            return _data[0];
        }

        T& back() {
            return _data[_size - 1];
        }

        void clear() {
            _size = 0;
        }

        size_t size() const { return _size; }

        T* begin() {
            return _data;
        }

        T* end() {
            return _data+_size;
        }

        const T* cbegin() const {
            return _data;
        }

        const T* cend() const {
            return _data+_size;
        }

    private:
        void freeData() {
            for (std::size_t i = 0; i < _size; i++) {
                _data[i].~T();
            }
            if (_data) delete[](reinterpret_cast<unsigned char*>(_data));
        }

        T* _data = nullptr;
        std::size_t _capacity = 0;
        std::size_t _size = 0;
};

}

#endif
