/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <sstream>
#include <Corrade/TestSuite/Tester.h>

#include "Navi/Data/Tuple.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Data/Observable.hpp"
#include "Navi/Data/Tree.h"
#include "Navi/Data/UnorderedArray.h"
#include "Navi/Data/Variant.h"

namespace Navi::Data::Test {
namespace {

using namespace Corrade;

struct DataTest: TestSuite::Tester {
    explicit DataTest();

    void tupleConstructTest();
    void tupleSetGetTest();
    void tupleFillTest();
    void tupleSizeStringTest();
    void tupleSizeIntTest();
    void tupleSizeFloatTest();
    void tupleSizeDoubleTest();
    void tupleSizeTest();

    void listConstructTest();
    void listAppendTest();
    void listRemoveTest();
    void listReferenceTest();
    void listCopyTest();
    void listMoveTest();

    void unorderedArrayConstructTest();
    void unorderedArrayAppendTest();
    void unorderedArrayRemoveTest();
    void unorderedArrayCopyTest();

    void treeConstructTest();
    void treeClearTest();

    void observableConstructTest();
    void observableSetGetTest();
    void observableSubscriptionTest();
    void observableBindingTest();
    void observableReferenceTest();

    void variantConstructTest();
    void variantCopyTest();
    void variantMoveTest();
};

DataTest::DataTest() {
    addTests({
        &DataTest::tupleConstructTest,
        &DataTest::tupleSetGetTest,
        &DataTest::tupleFillTest,
        &DataTest::tupleSizeStringTest,
        &DataTest::tupleSizeIntTest,
        &DataTest::tupleSizeFloatTest,
        &DataTest::tupleSizeDoubleTest,
        &DataTest::tupleSizeTest,

        &DataTest::listConstructTest,
        &DataTest::listAppendTest,
        &DataTest::listRemoveTest,
        &DataTest::listReferenceTest,
        &DataTest::listCopyTest,
        &DataTest::listMoveTest,

        &DataTest::unorderedArrayConstructTest,
        &DataTest::unorderedArrayAppendTest,
        &DataTest::unorderedArrayRemoveTest,
        &DataTest::unorderedArrayCopyTest,

        &DataTest::treeConstructTest,
        &DataTest::treeClearTest,

        &DataTest::observableConstructTest,
        &DataTest::observableSetGetTest,
        &DataTest::observableSubscriptionTest,
        &DataTest::observableBindingTest,
        &DataTest::observableReferenceTest,

        &DataTest::variantConstructTest,
        &DataTest::variantCopyTest,
        &DataTest::variantMoveTest});
}

void DataTest::tupleConstructTest() {
    {
        Tuple<std::string, int, float, double, bool> tuple;
        CORRADE_COMPARE(tuple.template get<0>().size(), 0);
        CORRADE_COMPARE(tuple.template get<1>(), 0);
        CORRADE_VERIFY(!tuple.template get<4>());
    }
}

void DataTest::tupleSetGetTest() {
    Tuple<std::string, int, float, double, bool> tuple;
    tuple.set<0>("Hello World");
    CORRADE_VERIFY(strcmp(tuple.template get<0>().c_str(), "Hello World") == 0);

    tuple.set<1>(42);
    CORRADE_COMPARE(tuple.template get<1>(), 42);

    tuple.set<2>(3.1415f);
    CORRADE_COMPARE(tuple.template get<2>(), 3.1415f);

    tuple.set<3>(3.1415);
    CORRADE_COMPARE(tuple.template get<3>(), 3.1415);

    tuple.set<4>(false);
    CORRADE_COMPARE(tuple.template get<4>(), false);
}

void DataTest::tupleFillTest() {
    Tuple<float, float, float, float, float, float> tuple;
    tuple.template fillRange<2, 4>(5.55f);

    CORRADE_COMPARE(tuple.template get<1>(), 0.0f);
    CORRADE_COMPARE(tuple.template get<2>(), 5.55f);
    CORRADE_COMPARE(tuple.template get<3>(), 5.55f);
    CORRADE_COMPARE(tuple.template get<4>(), 5.55f);
    CORRADE_COMPARE(tuple.template get<5>(), 0.0f);
}

void DataTest::tupleSizeStringTest() {
    Tuple<std::string> tuple;
    constexpr size_t szsum = sizeof(std::string);
    constexpr size_t alsum = alignof(std::string);
    CORRADE_COMPARE(sizeof(tuple), szsum+alsum);
}

void DataTest::tupleSizeIntTest() {
    Tuple<int> tuple;
    constexpr size_t szsum = sizeof(int);
    constexpr size_t alsum = alignof(int);
    CORRADE_COMPARE(sizeof(tuple), szsum+alsum);
}

void DataTest::tupleSizeFloatTest() {
    Tuple<float> tuple;
    constexpr size_t szsum = sizeof(float);
    constexpr size_t alsum = alignof(float);
    CORRADE_COMPARE(sizeof(tuple), szsum+alsum);
}

void DataTest::tupleSizeDoubleTest() {
    Tuple<double> tuple;
    constexpr size_t szsum = sizeof(double);
    constexpr size_t alsum = alignof(double);
    CORRADE_COMPARE(sizeof(tuple), szsum+alsum);
}

void DataTest::tupleSizeTest() {
    Tuple<std::string, int, float, double> tuple;
    constexpr size_t szsum = sizeof(std::string) + sizeof(int) + sizeof(float) + sizeof(double);
    constexpr size_t alsum = alignof(std::string) + alignof(int) + alignof(float) + alignof(double);
    CORRADE_VERIFY(sizeof(tuple) <= szsum + alsum);
}

// List tests

void DataTest::listConstructTest() {
    LinkedList<int> list;
    CORRADE_VERIFY(list.begin() == list.end());
}

void DataTest::listAppendTest() {
    LinkedList<int> list;

    list.push_back(42);
    CORRADE_COMPARE(list.back(), 42);
}

void DataTest::listRemoveTest() {
    LinkedList<std::string> list;
    list.push_back("My List");
    list.remove("My List");
    CORRADE_VERIFY(list.empty());

    list.push_back("My List");
    list.remove("Not My List");
    CORRADE_VERIFY(!list.empty());
}

void DataTest::listReferenceTest() {
    LinkedList<int> list;
    { int i = 0; while (i < 50) { list.push_back(0); i++; } }

    {
        int i = 0;
        for (auto& num : list) {
            num = i++;
        }
    }

    {
        int i = 0;
        for (const auto& num : list) {
            CORRADE_COMPARE(num, i++);
        }
    }
}

void DataTest::listCopyTest() {
    LinkedList<int> list;
    { int i = 0; while (i < 50) { list.push_back(0); i++; } }

    // Copy the list
    LinkedList<int> list2 = list;
    { int i = 0;
        for (auto& num : list2) {
            num = i++;
        }
    }

    // Assert that the original list is intact
    {
        int i = 0;
        for (const auto& num : list) {
            CORRADE_COMPARE(num, 0);
        }
    }
}

void DataTest::listMoveTest() {
    LinkedList<std::string> list;
    { int i = 0; while (i < 50) { list.push_back("String " + std::to_string(i)); i++; } }

    LinkedList<std::string> list2 = std::move(list);

    // Assert that the contents were moved
    {
        int i = 0;
        for (const auto& str : list2) {
            CORRADE_VERIFY(str == ("String " + std::to_string(i)));
            i++;
        }
    }

    // Assert that the original list is empty
    CORRADE_VERIFY(list.empty());
}

// UnorderedArray tests

void DataTest::unorderedArrayConstructTest() {
    UnorderedArray<std::string> arr;
    CORRADE_VERIFY(arr.size() == 0);
}

void DataTest::unorderedArrayAppendTest() {
    UnorderedArray<std::string> arr;
    arr.push_back("Hello World");
    arr.push_back("Second String");
    arr.push_back("Third String");

    CORRADE_VERIFY(arr.size() == 3);
    CORRADE_VERIFY(arr.at(0) == "Hello World");
    CORRADE_VERIFY(arr.at(1) == "Second String");
    CORRADE_VERIFY(arr.at(2) == "Third String");
}

void DataTest::unorderedArrayRemoveTest() {
    UnorderedArray<std::string> arr;
    arr.push_back("Hello World");
    arr.push_back("Second String");
    arr.push_back("Third String");
    arr.push_back("Fourth String");

    arr.remove("Second String");
    std::size_t i = 0;
    for (const auto& str : arr) {
        if (i == 0) {
            CORRADE_VERIFY(str == "Hello World");
        }
        else if (i == 1) {
            CORRADE_VERIFY(str == "Fourth String");
        }
        else if (i == 2) {
            CORRADE_VERIFY(str == "Third String");
        }
        i++;
    }

    arr.push_back("Second Modified String");
    CORRADE_VERIFY(arr.back() == "Second Modified String");
    CORRADE_VERIFY(arr.size() == 4);
}

void DataTest::unorderedArrayCopyTest() {
    UnorderedArray<std::string> arr;
    arr.push_back("Foo");
    arr.push_back("Bar");
    arr.push_back("Baz");

    UnorderedArray<std::string> arrCopy = arr;
    arrCopy.remove("Bar");

    CORRADE_VERIFY(arr.size() == 3);
    CORRADE_VERIFY(arrCopy.size() == 2);
    CORRADE_VERIFY(arr.at(1) == "Bar");
    CORRADE_VERIFY(arrCopy.at(1) == "Baz");
}

// Tree tests

void DataTest::treeConstructTest() {
    Tree<std::string> tree{ "Root String", 3 };
    tree.root().addChild("Hello World");
    tree.root().front().addChild("Second Child");

    for (auto& child : tree.root()) {
        CORRADE_VERIFY(*child == "Hello World");
        for (auto& cchild : child) {
            CORRADE_VERIFY(*cchild == "Second Child");
        }
    }
}

void DataTest::treeClearTest() {
    Tree<std::string> tree{ "Root String", 3 };
    tree.root().addChild("Hello World");
    tree.root().front().addChild("Second Child");

    CORRADE_VERIFY(tree.nodes().size() == 3);
    tree.clear();
    CORRADE_VERIFY(tree.nodes().size() == 1); // Root node still remains
}

// Observable tests

void DataTest::observableConstructTest() {
    {
        Observable<std::string> obs;
        CORRADE_COMPARE(obs.value().size(), 0);
    }
    {
        Observable<int> obs;
        CORRADE_COMPARE(obs.value(), 0);
    }
    {
        Observable<bool> obs;
        CORRADE_COMPARE(obs.value(), false);
    }
}

void DataTest::observableSetGetTest() {
    Observable<std::string> obs;
    obs.setValue("Hello World");
    CORRADE_VERIFY(obs.value() == "Hello World");
}

void DataTest::observableSubscriptionTest() {
    Observable<std::string> obs;
    int changes = 0;

    obs.valueChanged().connect([this, &changes](const std::string& value) {
        if (value == "New Value") {
            changes = 1;
        }
        else if (value == "Newest Value") {
            changes = 2;
        }
    });

    obs.setValue("New Value");
    CORRADE_COMPARE(changes, 1);

    obs.setValue("Newest Value");
    CORRADE_COMPARE(changes, 2);
}

void DataTest::observableBindingTest() {
    Observable<std::string> obs{ "5" };

    // Create a binding that parses the string to an integer
    Observable<int>& obsToInt = obs.bind([](const std::string& v) {
        std::istringstream str{ v };
        int num;

        str >> num;
        return num;
    });
    CORRADE_COMPARE(obsToInt.value(), 5);

    obs.setValue("142");
    CORRADE_COMPARE(obsToInt.value(), 142);

    // Create a binding that returns the square value
    Observable<int>& obsSqr = obsToInt.bind([](int v) {
        return v * v;
    });
    CORRADE_COMPARE(obsSqr.value(), 142*142);

    obs.setValue("10");
    CORRADE_COMPARE(obsSqr.value(), 10 * 10);
    CORRADE_COMPARE(obsToInt.value(), 10);
}

void DataTest::observableReferenceTest() {
    Observable<std::string> obs{ "5" };
    Observable<std::string> third{ "Third" };

    Observable<std::string> owner{ "Hello World" };

    {
        owner.bind(ObservableReference<std::string>{ obs });

        obs.setValue("From First");
        CORRADE_VERIFY(owner.value() == "From First");

        owner.setValue("From Second");
        CORRADE_VERIFY(obs.value() == "From Second");
    }

    {
        owner.bind(ObservableReference<std::string>{ third });

        third.setValue("From Third");
        CORRADE_VERIFY(owner.value() == "From Third");
        CORRADE_VERIFY(obs.value() == "From Second"); // The first should not change
    }
}

// Variant tests

void DataTest::variantConstructTest() {
    Variant v;
    CORRADE_VERIFY(v.type() == VariantType::Nil);

    Variant vi{ 2 };
    CORRADE_VERIFY(vi.type() == VariantType::Int);
    CORRADE_VERIFY(vi.cast<int>() == 2);

    Variant vf{ 5.5f };
    CORRADE_VERIFY(vf.type() == VariantType::Float);
    CORRADE_VERIFY(vf.cast<float>() == 5.5f);

    Variant vb{ true };
    CORRADE_VERIFY(vb.type() == VariantType::Bool);
    CORRADE_VERIFY(vb.cast<bool>() == true);

    Variant vs{ "Variant String" };
    CORRADE_VERIFY(vs.type() == VariantType::String);
    CORRADE_VERIFY(vs.cast<std::string>() == "Variant String");

    Variant vl{ LinkedList<Variant>{} };
    CORRADE_VERIFY(vl.type() == VariantType::List);
    CORRADE_VERIFY(vl.list().empty());

    Variant vp{ LinkedList<Tuple<Variant, Variant>>{} };
    CORRADE_VERIFY(vp.type() == VariantType::Pairs);
    CORRADE_VERIFY(vp.pairs().empty());
}

void DataTest::variantCopyTest() {
    Variant vi{ 42 };
    Variant vi2 = vi;
    CORRADE_VERIFY(vi2 == vi);

    Variant vs{ "Hello World" };
    Variant vs2 = vs;
    CORRADE_VERIFY(vs != vi);
    CORRADE_VERIFY(vs2 == vs);

    Variant vl{ Variant::ListType{} };
    Variant vl2 = vl;
    CORRADE_VERIFY(vl != vl2);

    Variant vp = Variant::makePairs();
    Variant vp2 = vp;
    CORRADE_COMPARE(vp2.cast<Variant::PairsType>().size(), vp.cast<Variant::PairsType>().size());
}

void DataTest::variantMoveTest() {
    Variant vs{ "Hello World" };
    Variant vs2 = std::move(vs);
    CORRADE_VERIFY(vs.cast<std::string>().empty());
    CORRADE_VERIFY(vs2.cast<std::string>() == "Hello World");

    Variant vl{ Variant::ListType{ {"String In List", 42} } };
    Variant vl2 = std::move(vl);
    CORRADE_VERIFY(vl.list().empty());
    CORRADE_VERIFY(vl2.list().front().cast<std::string>() == "String In List");
    CORRADE_VERIFY(vl2.list().back() == Variant{ 42 });
}

}}

CORRADE_TEST_MAIN(Navi::Data::Test::DataTest)
