#ifndef Navi_Data_Serializer_h
#define Navi_Data_Serializer_h

#include <Corrade/Containers/Array.h>

namespace Navi::Data {

enum class Endianess: unsigned int {
    Little,
    Big
};


template<class T, Endianess E = Endianess::Little> class Serializer {
    public:
        explicit Serializer(T& stream);

        void seek(std::size_t n);

        template<class Data> std::size_t read(std::size_t n, Data* data);

        unsigned char readUByte();

        unsigned short readUShort();

        unsigned int readUInt();

        template<class Data> std::size_t write(std::size_t n, Data* data);

        std::size_t writeUByte(unsigned char);

        std::size_t writeUShort(unsigned short);

        std::size_t writeUInt(unsigned int);

    private:
        T* _stream;
};


template<class T, Endianess E = Endianess::Little> Serializer<T, E> serializer(T& stream);

}

#endif