#ifndef Navi_Data_Tuple_h
#define Navi_Data_Tuple_h

#include <cstddef>
#include <type_traits>
#include <utility>
#include <Corrade/Containers/Tags.h>

namespace Navi::Data {

struct EndT { };

template<class... Types>
struct TupleDescription { };

template<std::size_t N, typename T, typename... Ts>
struct Select {
    using Type = typename Select<N - 1, Ts...>::Type;
};

template<typename T, typename... Ts>
struct Select<0, T, Ts...> {
    using Type = T;
};

template<typename T, typename... Ts>
struct TupleSize {
    static constexpr int Value = TupleSize<Ts...>::Value + 1;
};

template<typename T>
struct TupleSize<T> {
    static constexpr int Value = 1;
};

template<class T, class... Types>
class TupleT {
    public:
        TupleT() : _value{}, _next{} {}

        template<std::size_t Index, std::enable_if_t<Index == 0, T>* = nullptr>
        typename Select<Index, T, Types...>::Type & get() {
            return _value;
        }
        template<std::size_t Index, std::enable_if_t<Index != 0, T>* = nullptr>
        typename Select<Index, T, Types...>::Type & get() {
            return _next.template get<Index - 1>();
        }

        template<std::size_t Index, std::enable_if_t<Index == 0, T>* = nullptr>
        const typename Select<Index, T, Types...>::Type & get() const {
            return _value;
        }
        template<std::size_t Index, std::enable_if_t<Index != 0, T>* = nullptr>
        const typename Select<Index, T, Types...>::Type & get() const {
            return _next.template get<Index-1>();
        }

        template<std::size_t Index, std::enable_if_t<Index == 0, T>* = nullptr>
        TupleT& set(const typename Select<Index, T, Types...>::Type & v) {
            _value = v;
            return *this;
        }
        template<std::size_t Index, std::enable_if_t<Index != 0, T>* = nullptr>
        TupleT& set(const typename Select<Index, T, Types...>::Type & v) {
            _next.template set<Index - 1>(v);
            return *this;
        }
        template<std::size_t Index, std::enable_if_t<Index == 0, T> * = nullptr>
        TupleT& set(typename Select<Index, T, Types...>::Type&& v) {
            _value = std::move(v);
            return *this;
        }
        template<std::size_t Index, std::enable_if_t<Index != 0, T> * = nullptr>
        TupleT& set(typename Select<Index, T, Types...>::Type&& v) {
            _next.template set<Index - 1>(v);
            return *this;
        }

        template<std::size_t First, std::size_t Last, std::enable_if_t<First == 0 && Last != 0, T>* = nullptr>
        void fillRange(const typename Select<First, T, Types...>::Type & v) {
            _value = v;
            _next.template fillRange<First, Last - 1>(v);
        }
        template<std::size_t First, std::size_t Last, std::enable_if_t<First != 0, T>* = nullptr>
        void fillRange(const typename Select<First, T, Types...>::Type & v) {
            _next.template fillRange<First - 1, Last - 1>(v);
        }
        template<std::size_t First, std::size_t Last, std::enable_if_t<First == 0 && Last == 0, T>* = nullptr>
        void fillRange(const typename Select<First, T, Types...>::Type & v) {
            _value = v;
        }

        template<int Index = 0>
        constexpr std::size_t size() const {
            return _next.template size<Index+1>();
        }

    protected:
        T _value;
        TupleT<Types...> _next;
};

template<class... Types>
class TupleT<EndT, TupleDescription<Types...>> {
    public:
        TupleT() {}

        template<std::size_t Index>
        EndT get() {
            //static_assert(false, "Type not in properties");
            return {};
        }

        template<std::size_t Index>
        void set(const EndT& v) {
            //static_assert(false, "Type not in properties");
        }

        template<std::size_t First, std::size_t Last>
        void fillRange(const EndT& v) {
        }

        template<int Index>
        constexpr std::size_t size() {
            return Index;
        }
};

template<class... Types>
class Tuple : public TupleT<Types..., EndT, TupleDescription<Types...>> {
    typedef TupleT<Types..., EndT, TupleDescription<Types...>> BaseType;

    public:
        Tuple() : BaseType{} {}
};

template<std::size_t N, typename TupleType>
struct TupleElement { };
template<std::size_t N, typename T, typename... Ts>
struct TupleElement<N, Tuple<T, Ts...>> {
    using Type = typename Select<N, T, Ts...>::Type;
};

template<std::size_t Index, class Arg, class... Args>
inline void makeTuple_(Tuple<Arg, Args...>& tuple, Arg&& arg, Args&&... args) {
    tuple.template set<Index>(arg);
    makeTuple_<Index+1>(tuple, std::forward<Args>(args)...);
}
template<std::size_t Index, class Arg, class... Args>
inline void makeTuple_(Tuple<Args...>& tuple, Arg&& arg) {
    tuple.template set<Index>(arg);
}

template<class... Args>
inline Tuple<Args...> makeTuple(Args&&... args) {
    Tuple<Args...> t;
    makeTuple_<0, Args...>(t, std::forward<Args>(args)...);
    return t;
}

}

#endif
