#ifndef Navi_Data_Serializer_hpp
#define Navi_Data_Serializer_hpp

#include "Navi/Data/Serializer.h"

namespace Navi::Data {

namespace {
template<Endianess E> struct UShortReader {};
template<Endianess E> struct UIntReader {};

template<> struct UShortReader<Endianess::Little> {
    template<class T> static unsigned short read(const T& data) {
        return static_cast<unsigned short>(data[0]) | static_cast<unsigned short>((data[1] << 8) & 0xFF00);
    }
};

template<> struct UIntReader<Endianess::Little> {
    template<class T> static unsigned int read(const T& data) {
        return (
            static_cast<unsigned int>(data[0])
            | static_cast<unsigned int>((data[1] << 8) & 0xFF00)
            | static_cast<unsigned int>((data[2] << 16) & 0xFF0000)
            | static_cast<unsigned int>((data[3] << 24) & 0xFF000000)
        );
    }
};


template<Endianess E> struct UShortWriter {};
template<Endianess E> struct UIntWriter {};

template<> struct UShortWriter<Endianess::Little> {
    template<class T> static void write(unsigned short n, T& data) {
        data[0] = n & 0xFF;
        data[1] = (n >> 8) & 0xFF;
    }
};

template<> struct UIntWriter<Endianess::Little> {
    template<class T> static void write(unsigned int n, T& data) {
        data[0] = n & 0xFF;
        data[1] = (n >> 8) & 0xFF;
        data[2] = (n >> 16) & 0xFF;
        data[3] = (n >> 24) & 0xFF;
    }
};
}


template<class T, Endianess E> Serializer<T, E>::Serializer(T& stream)
    : _stream{ &stream } {
}

template<class T, Endianess E> void Serializer<T, E>::seek(std::size_t n) {
    _stream->seek(n);
}

template<class T, Endianess E> template<class Data> std::size_t Serializer<T, E>::read(std::size_t n, Data* data) {
    return _stream->read(n, static_cast<void*>(data));
}

template<class T, Endianess E> unsigned char Serializer<T, E>::readUByte() {
    unsigned char c;
    _stream->read(sizeof(unsigned char), &c);
    return c;
}

template<class T, Endianess E> unsigned short Serializer<T, E>::readUShort() {
    unsigned char data[sizeof(unsigned short)];
    _stream->read(sizeof(unsigned short), data);
    return UShortReader<E>::read(data);
}

template<class T, Endianess E> unsigned int Serializer<T, E>::readUInt() {
    unsigned char data[sizeof(unsigned int)];
    _stream->read(sizeof(unsigned int), &data);
    return UIntReader<E>::read(data);
}

template<class T, Endianess E> template<class Data> std::size_t Serializer<T, E>::write(std::size_t n, Data* data) {
    return _stream->write(n, static_cast<const void*>(data));
}

template<class T, Endianess E> std::size_t Serializer<T, E>::writeUByte(unsigned char b) {
    return _stream->write(1, &b);
}

template<class T, Endianess E> std::size_t Serializer<T, E>::writeUShort(unsigned short s) {
    unsigned char data[sizeof(unsigned short)];
    UShortWriter<E>::write(s, data);
    return _stream->write(sizeof(data), data);
}

template<class T, Endianess E> std::size_t Serializer<T, E>::writeUInt(unsigned int i) {
    unsigned char data[sizeof(unsigned int)];
    UIntWriter<E>::write(i, data);
    return _stream->write(sizeof(data), data);
}


template<class T, Endianess E = Endianess::Little> Serializer<T, E> serializer(T& stream) {
    return Serializer<T, E>{ stream };
}
}

#endif