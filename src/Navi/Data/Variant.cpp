#include <sstream>
#include "Navi/Data/Variant.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi::Data {

namespace Implementation {
    template<class T>
    void destruct(T& v) {
        v.~T();
    }
}

Variant::~Variant() {
    _destruct();
}

void Variant::_destruct() {
    switch (_type) {
    case VariantType::String:
        Implementation::destruct(_string);
        break;
    case VariantType::List:
        Implementation::destruct(_list);
        break;
    case VariantType::Map:
        Implementation::destruct(_map);
        break;
    case VariantType::ResourceHandle:
        Implementation::destruct(_res);
        break;
    case VariantType::UniquePointer:
        if (_varptr) Implementation::destruct(*_varptr);
        break;
    }
}

Variant& Variant::operator =(bool b) {
    _destruct();
    _type = VariantType::Bool;
    _bool = b;
    return *this;
}

Variant& Variant::operator =(int i) {
    _destruct();
    _type = VariantType::Int;
    _int = i;
    return *this;
}

Variant& Variant::operator =(float f) {
    _destruct();
    _type = VariantType::Float;
    _float[0] = f;
    return *this;
}

Variant& Variant::operator =(const Vector2f& v) {
    _destruct();
    _type = VariantType::Float2;
    _float[0] = v.x();
    _float[1] = v.y();
    return *this;
}

Variant& Variant::operator =(const Vector4f& v) {
    _destruct();
    _type = VariantType::Float4;
    _float[0] = v.x();
    _float[1] = v.y();
    _float[2] = v.z();
    _float[3] = v.w();
    return *this;
}

Variant& Variant::operator =(const std::string& str) {
    _destruct();
    _type = VariantType::String;
    new (&_string) std::string{ str };
    return *this;
}

Variant& Variant::operator =(const ListType& list) {
    _destruct();
    _type = VariantType::List;
    new (&_list) ListType{ list };
    return *this;
}

Variant& Variant::operator =(const MapType& map) {
    _destruct();
    _type = VariantType::Map;
    new (&_map) MapType{ map };
    return *this;
}

Variant& Variant::operator =(const Variant& v) {
    _destruct();

    _type = v._type;
    switch (v._type) {
    case VariantType::String:
        new (&_string) std::string{ v._string };
        break;
    case VariantType::List:
        new (&_list) ListType{ v._list };
        break;
    case VariantType::Map:
        new (&_map) MapType{ v._map };
        break;
    case VariantType::ResourceHandle:
        new (&_res) ResourceHandleType{ v._res };
        break;
    case VariantType::UniquePointer:
        throw std::runtime_error("cannot copy unique pointer");
        break;
    default:
        std::memcpy(this, &v, sizeof(Variant));
        break;
    }
    return *this;
}

Variant& Variant::operator =(Variant&& v) {
    _destruct();
    _type = v._type;
    switch (v._type) {
    case VariantType::String:
        new (&_string) std::string{ std::move(v._string) };
        break;
    case VariantType::List:
        new (&_list) ListType{ std::move(v._list) };
        break;
    case VariantType::Map:
        new (&_map) MapType{ std::move(v._map) };
        break;
    case VariantType::ResourceHandle:
        new (&_res) ResourceHandleType{ std::move(v._res) };
        break;
    case VariantType::UniquePointer:
        _varptr = v._varptr;
        v._varptr = nullptr;
        break;
    default:
        std::memcpy(this, &v, sizeof(Variant));
        break;
    }
    return *this;
}

bool Variant::operator ==(const Variant& other) const {
    if (_type != other._type) return false;

    switch (_type) {
    case VariantType::Bool:
    case VariantType::Int:
    case VariantType::Float:
        return _int == other._int;

    case VariantType::Float2:
        return (
           _float[0] == other._float[0] &&
           _float[1] == other._float[1]
        );

    case VariantType::Float3:
        return (
                _float[0] == other._float[0] &&
                _float[1] == other._float[1] &&
                _float[2] == other._float[2]
        );

    case VariantType::Float4:
        return (
                _float[0] == other._float[0] &&
                _float[1] == other._float[1] &&
                _float[2] == other._float[2] &&
                _float[3] == other._float[3]
         );

    case VariantType::String:
        return _string == other._string;

    case VariantType::Pointer:
        return _ptr == other._ptr;

    case VariantType::UniquePointer:
        return _varptr == other._varptr;

    case VariantType::List:
    case VariantType::Map:
        return false;
    }
}

Containers::Optional<Variant> Variant::find(const std::string& key) const {
    auto it = _map.find(key);
    if (it != _map.end())
        return Data::Variant{ it->second };
    return {};
}

std::string floatsToString(const float* fs, int count) {
    std::ostringstream str;
    str << "[";
    for (int i = 0; i < count; i++) {
        str << fs[i];
        if (i < count - 1) {
            str << " ";
        }
    }
    str << "]";
    return str.str();
}

std::string Variant::toString() const {
    switch (_type) {
    case VariantType::Nil:
        return "nil";
    case VariantType::String:
        return "\"" + _string + "\"";
    case VariantType::Bool:
        return _bool ? "true" : "false";
    case VariantType::Int:
        return std::to_string(_int);
    case VariantType::Float:
        return std::to_string(_float[0]);
    case VariantType::Float2:
        return floatsToString(_float, 2);
    case VariantType::Float3:
        return floatsToString(_float, 3);
    case VariantType::Float4:
        return floatsToString(_float, 4);
    case VariantType::ResourceHandle:
        return _res._resource->id();
    default:
        // TODO
        return "TODO";
    }
}

}
