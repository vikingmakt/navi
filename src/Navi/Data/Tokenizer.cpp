#define STB_C_LEXER_IMPLEMENTATION
#define STB_C_LEX_C_SQ_STRINGS Y
#include <stb/stb_c_lexer.h>
#include <Corrade/Containers/StaticArray.h>
#include "Navi/Data/Tokenizer.h"
#include "Navi/Data/Variant.h"

namespace Navi::Data {

struct Lexer {
    stb_lexer l;
    Containers::StaticArray<1024, char> store;
};

Tokenizer::Tokenizer(std::string str) {
    _str = str;
    _lex = Containers::pointer<Lexer>();
    stb_c_lexer_init(&_lex->l, str.data(), str.data() + str.size(), _lex->store.data(), _lex->store.size());
    _tok = next();
}

const std::string& Tokenizer::source() {
    return _str;
}

int Tokenizer::current() {
    return _tok;
}

int Tokenizer::next() {
    return stb_c_lexer_get_token(&_lex->l);
}

Containers::Optional<ReservedType> Tokenizer::parseReserved() {
    if (current() == CLEX_id) {
        switch (_lex->l.string_len) {
        case 2:
            if (!std::strcmp("if", _lex->l.string)) {
                next();
                return { ReservedType::If };
            }
            break;

        case 3:
            if (!std::strcmp("var", _lex->l.string)) {
                next();
                return { ReservedType::Var };
            }
            break;

        case 4:
            if (!std::strcmp("cvar", _lex->l.string)) {
                next();
                return { ReservedType::Cvar };
            }
            if (!std::strcmp("then", _lex->l.string)) {
                next();
                return { ReservedType::Then };
            }
            if (!std::strcmp("else", _lex->l.string)) {
                next();
                return { ReservedType::Else };
            }
            break;

        case 6:
            if (!std::strcmp("signal", _lex->l.string)) {
                next();
                return { ReservedType::Signal };
            }
            break;

        case 7:
            if (!std::strcmp("console", _lex->l.string)) {
                next();
                return { ReservedType::Console };
            }
            break;

        case 9:
            if (!std::strcmp("animation", _lex->l.string)) {
                next();
                return { ReservedType::Animation };
            }
            break;
        }
    }

    return {};
}

Containers::Optional<std::string> Tokenizer::parseIdentifier() {
    if (current() == CLEX_id) {
        next();
        return { std::string{ _lex->l.string, _lex->l.string_len } };
    }

    return {};
}

Containers::Optional<std::string> Tokenizer::parseString(bool allowNonQuoted) {
    int tok = current();
    if (tok == CLEX_dqstring || tok == CLEX_sqstring) {
        next();
        return { std::string{ _lex->l.string, _lex->l.string_len } };
    }
    if (allowNonQuoted && tok == CLEX_id) {
        next();
        return { std::string{ _lex->l.string, _lex->l.string_len } };
    }

    return {};
}

Containers::Optional<bool> Tokenizer::parseBool() {
    if (current() == CLEX_id) {
        if (!std::strcmp("true", _lex->l.string)) {
            next();
            return { true };
        }
        if (!std::strcmp("false", _lex->l.string)) {
            next();
            return { false };
        }
    }

    return {};
}

Containers::Optional<int> Tokenizer::parseInt() {
    if (current() == CLEX_intlit) {
        next();
        return { _lex->l.int_number };
    }

    return {};
}

Containers::Optional<float> Tokenizer::parseFloat() {
    if (current() == CLEX_floatlit) {
        next();
        return { _lex->l.real_number };
    }

    return {};
}

Containers::Optional<Data::Variant> Tokenizer::parseVariantNumber() {
    int tok = current();
    if (tok == CLEX_floatlit) {
        next();
        return { Data::Variant{ float(_lex->l.real_number) } };
    }
    if (tok == CLEX_intlit) {
        next();
        return { Data::Variant{ int(_lex->l.int_number) } };
    }

    return {};
}

Containers::Optional<Vector2f> Tokenizer::parseVector2() {
    if (current() != '[') {
        return {};
    }
    next();

    auto x = parseVariantNumber();
    if (!x) {
        return {};
    }

    auto y = parseVariantNumber();
    if (!y) {
        return {};
    }

    if (current() != ']') {
        return {};
    }
    next();

    return Vector2f{ x->cast<float>(), y->cast<float>() };
}

Containers::Optional<Vector4f> Tokenizer::parseHexColor() {
    if (current() == CLEX_intlit) {
        next();
        unsigned int x = _lex->l.int_number;
        return Data::Variant{ Vector4f{
            float((x >> 24) & 0xFF) / 255.0f,
            float((x >> 16) & 0xFF) / 255.0f,
            float((x >> 8) & 0xFF) / 255.0f,
            float(x & 0xFF) / 255.0f
        } };
    }

    return {};
}

}