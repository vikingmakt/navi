#ifndef Navi_Data_LinkedList_hpp
#define Navi_Data_LinkedList_hpp

#include "Navi/Data/LinkedList.h"

namespace Navi::Data {

template<class T>
void LinkedList<T>::push_back(const T& elem) {
    Item* item = new Item{ elem, _last, nullptr };
    if (_last) _last->next = item;
    _last = item;
    if (!_first) _first = item;
    _size++;
}

template<class T>
void LinkedList<T>::push_back(T&& elem) {
    Item* item = new Item{ std::move(elem), _last, nullptr };
    if (_last) _last->next = item;
    _last = item;
    if (!_first) _first = item;
    _size++;
}

template<class T>
void LinkedList<T>::pop_front() {
    erase(begin());
}

template<class T>
void LinkedList<T>::pop_back() {
    erase(Iterator{ _last });
}

template<class T>
void LinkedList<T>::erase(const Iterator& it) {
    Item* item = it.item;
    if (item != nullptr) {
        if (item->previous) {
            item->previous->next = item->next;
        } else {
            _first = item->next;
        }

        if (item->next) {
            item->next->previous = item->previous;
        } else {
            _last = item->previous;
        }

        delete item;
        _size--;
    }
}

template<class T>
void LinkedList<T>::remove(const T& elem) {
    erase(find(elem));
}

template<class T>
void LinkedList<T>::clear() {
    while (!empty()) {
        erase(begin());
    }
}

template<class T>
typename LinkedList<T>::Iterator LinkedList<T>::find(const T& elem) {
    auto item = _first;
    while (item != nullptr) {
        if (item->content == elem) {
            return Iterator{ item };
        }
        item = item->next;
    }
    return end();
}

}

#endif
