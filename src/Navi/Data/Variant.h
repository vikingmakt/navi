#ifndef Navi_Data_Variant_h
#define Navi_Data_Variant_h

#include <cstring>
#include <string>
#include <stdexcept>
#include <unordered_map>
#include <Corrade/Containers/Optional.h>
#include "Navi/Data/LinkedList.h"
#include "Navi/Data/Tuple.h"
#include "Navi/Resource/ResourceHandle.h"
#include "Navi/StringHash.h"
#include "Navi/Navi.h"

namespace Navi::Data {

enum class VariantType : std::uint8_t {
    Nil,
    Bool,
    Int,
    Float,
    Float2,
    Float3,
    Float4,
    String,
    List,
    Map,
    ResourceHandle,
    Pointer,
    UniquePointer,
};

class Variant;

template<class T>
struct VariantCaster {
    T& operator()(Variant& v);
    const T& operator()(const Variant& v);
};

class VariantObject {
    public:
        virtual ~VariantObject() {}
};

class Variant {
    public:
        using ListType = std::vector<Variant>;
        using MapType = std::unordered_map<std::string, Data::Variant>;
        using ResourceHandleType = Resource::ResourceHandle<Resource::AbstractResource>;

        static Variant makeList() {
            ListType list;
            return { list };
        }

        static Variant makeMap() {
            MapType map;
            return { map };
        }

        template<class T>
        static Variant makePointer(T* ptr) {
            Data::Variant v;
            v._type = VariantType::Pointer;
            v._ptr = static_cast<void*>(ptr);
            return v;
        }

        static Variant makeUniquePointer(VariantObject* ptr) {
            Data::Variant v;
            v._type = VariantType::UniquePointer;
            v._varptr = ptr;
            return v;
        }

        Variant()
            : _type{ VariantType::Nil } {}
        Variant(bool b) {
            *this = b;
        }
        Variant(int i) {
            *this = i;
        }
        Variant(float f) {
            *this = f;
        }
        Variant(const Vector2f& v) {
            *this = v;
        }
        Variant(const Vector4f& v) {
            *this = v;
        }
        template<std::size_t size> Variant(const char (&str)[size]) {
            *this = str;
        }
        Variant(const std::string& str) {
            *this = str;
        }
        Variant(const ListType& list) {
            *this = list;
        }
        Variant(const MapType& map) {
            *this = map;
        }
        template<class T> Variant(const Resource::ResourceHandle<T>& res) {
            *this = res;
        }
        Variant(const Variant& other) {
            *this = other;
        }
        Variant(Variant&& other) {
            *this = std::move(other);
        }

        ~Variant();

        Variant& operator =(bool b);
        Variant& operator =(int i);
        Variant& operator =(float f);
        Variant& operator =(const Vector2f& v);
        Variant& operator =(const Vector4f& v);
        Variant& operator =(const std::string& str);
        Variant& operator =(const ListType& list);
        Variant& operator =(const MapType& map);
        Variant& operator =(const Variant& v);
        Variant& operator =(Variant&& v);
        template<std::size_t size> Variant& operator =(const char (&str)[size]) {
            _destruct();
            _type = VariantType::String;
            new (&_string) std::string{ (const char*)str, size - 1 };
            return *this;
        }
        template<class T> Variant& operator =(const Resource::ResourceHandle<T>& res) {
            _destruct();
            _type = VariantType::ResourceHandle;
            new (&_res) ResourceHandleType{ *static_cast<Resource::AbstractResource*>(res._resource), *res._mgr };
            return *this;
        }

        /// Variant equality operator, performs standard == comparison between string and built-in types.
        /// It will not perform comparisons against list and map and always return false, if needed it's better to iterate them and check each item.
        bool operator ==(const Variant& other) const;

        bool operator !=(const Variant& other) const {
            return !(*this == other);
        }

        Data::Variant operator !() const {
            return { !_bool };
        }

        VariantType type() const { return _type; }

        template<class T>
        operator T() const { return cast<T>(); }

        template<class T>
        T& cast() { return VariantCaster<T>{}(*this); }

        template<class T>
        const T& cast() const { return VariantCaster<T>{}(*this); }

        template<class T> T& castResource() {
            return *static_cast<T*>(&_res.resource());
        }

        template<class T> T* castPointer() {
            return static_cast<T*>(_ptr);
        }

        template<class T> T* castUniquePointer() {
            return static_cast<T*>(_varptr);
        }

        ListType& list() { return _list; }

        const ListType& list() const { return _list; }

        MapType& map() { return _map; }

        const MapType& map() const { return _map; }

        std::size_t size() const {
            if (_type == VariantType::List) {
                return _list.size();
            }
            else if (_type == VariantType::Map) {
                return _map.size();
            }

            throw std::runtime_error("Variant::size() only available in List or Map");
        }

        Variant& operator[](const std::string& key) { return _map[key]; }

        Containers::Optional<Variant> find(const std::string& key) const;

        std::string toString() const;

        float* floatPtr() { return _float; }

        const float* floatPtr() const { return _float; }

        bool isNil() const { return _type == VariantType::Nil; }

        bool isString() const { return _type == VariantType::String; }

    private:
        template<class T>
        friend struct VariantCaster;

        void _destruct();

        VariantType _type = VariantType::Nil;
        union {
            bool _bool;
            int _int;
            float _float[4];
            void* _ptr;
            VariantObject* _varptr;
            std::string _string;
            ListType _list;
            MapType _map;
            ResourceHandleType _res;
        };
};

template<>
struct VariantCaster<bool> {
    bool& operator()(Variant& v) { return v._bool; }
    const bool& operator()(const Variant& v) { return v._bool; }
};
template<>
struct VariantCaster<int> {
    int& operator()(Variant& v) { return v._int; }
    const int& operator()(const Variant& v) { return v._int; }
};
template<>
struct VariantCaster<float> {
    float& operator()(Variant& v) { return v._float[0]; }
    const float& operator()(const Variant& v) { return v._float[0]; }
};
template<>
struct VariantCaster<Vector2f> {
    const Vector2f& operator()(Variant& v) { return Vector2f{ v._float[0], v._float[1] }; }
    const Vector2f& operator()(const Variant& v) { return Vector2f{ v._float[0], v._float[1] }; }
};
template<>
struct VariantCaster<Vector3f> {
    const Vector3f& operator()(Variant& v) { return Vector3f{ v._float[0], v._float[1], v._float[2] }; }
    const Vector3f& operator()(const Variant& v) { return Vector3f{ v._float[0], v._float[1], v._float[2] }; }
};
template<>
struct VariantCaster<Vector4f> {
    const Vector4f& operator()(Variant& v) { return Vector4f{ v._float[0], v._float[1], v._float[2], v._float[3] }; }
    const Vector4f& operator()(const Variant& v) { return Vector4f{ v._float[0], v._float[1], v._float[2], v._float[3] }; }
};
template<>
struct VariantCaster<std::string> {
    std::string& operator()(Variant& v) { return v._string; }
    const std::string& operator()(const Variant& v) { return v._string; }
};
template<>
struct VariantCaster<const std::string&> {
    const std::string& operator()(const Variant& v) { return v._string; }
};
template<>
struct VariantCaster<Variant::ListType> {
    Variant::ListType& operator()(Variant& v) { return v._list; }
    const Variant::ListType& operator()(const Variant& v) { return v._list; }
};
template<>
struct VariantCaster<Variant::MapType> {
    Variant::MapType& operator()(Variant& v) { return v._map; }
    const Variant::MapType& operator()(const Variant& v) { return v._map; }
};
template<>
struct VariantCaster<const Variant&> {
    const Variant& operator()(const Variant& v) { return v; }
};
template<class T>
struct VariantCaster<T*> {
    T* operator()(Variant& v) { return static_cast<T*>(v._ptr); }
    const T* operator()(const Variant& v) { return static_cast<const T*>(v._ptr); }
};

}

#endif
