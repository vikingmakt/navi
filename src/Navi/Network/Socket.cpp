#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#endif

#include <iostream>
#include "Navi/Network/Socket.h"
#include "Navi/Application.h"
#include "Navi/WorkQueue.h"
#include "Navi/Console.hpp"

namespace Navi::Network {

enum class SocketConnectResult {
    Ok,
    AddressError,
    SocketError,
    ConnectError,
};

enum class SocketRecvResult {
    Ok,
    Timeout,
    Error,
};

#ifdef _WIN32
#pragma comment(lib, "Ws2_32.lib")

struct Context {
    WSADATA wsaData;
    int result;

    Context() {
        // Initialize Winsock
        result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    }

    ~Context() {
        WSACleanup();
    }
};

struct SocketImpl {
    SOCKET _socket;

    SocketConnectResult connect(const std::string& host, int port) {
        int ret = 0;
        int conn_fd;
        struct sockaddr_in server_addr = { 0 };

        // Specifiy the communication domain in which the address lives:
        // AF_INET      - IPV4;
        // AF_INET6     - IPV6;
        // AF_UNIX      - Local communication (used w/ unix sockets).
        server_addr.sin_family = AF_INET;

        // Assign the port that we supplied in host byte order in the
        // form of network byte order (`htons` performs the conversion).
        //
        // On amd64 (x86-64), the byte ordering of the host is little endian
        // (o.e., the least significant byte comes first).
        //
        // However, when it comes to networking, the byte ordering is big
        // endian - the most significant byte comes first.
        //
        // This means that a value (like 8000) defined in our machine needs
        // to be properly translated to the network byte order before being
        // sent.
        server_addr.sin_port = htons(port);

        // Fill the destination address with a 4-byte (32bit) unsigned integer
        // in network byte order after converting the address from the string
        // representation (e.g., "127.0.0.1").
        //
        // Given that we might see a failure in the conversion (e.g., if we
        // supply an invalid server_addr pointer or an invalid string), check
        // for these errors.
        //
        // Note that differently from the majority of syscalls and glibc
        // methods, success is defined with `1`, and not `0`. The information of
        // what represents success can often be found in man pages - e.g., `man
        // inet_pton`.
        ret = inet_pton(AF_INET, host.c_str(), &server_addr.sin_addr);

        if (ret != 1) {
            if (ret == -1) { 
                perror("inet_pton");
            }
            fprintf(stderr,
                    "failed to convert address %s "
                    "to binary net address\n",
                    host.c_str());
            
            return SocketConnectResult::AddressError;
        }

        // Create an endpoint for communication in our machine (our side) that
        // is meant to communicate over the AF_INET (IPv4) domain, using
        // SOCK_STREAM semantics (sequenced, reliable, two-way, connection-base
        // byte streams - TCP, for instance).
        //
        // Note that at this point, no communication has been made to an
        // external server yet - this operation is entirely local.
        _socket = socket(AF_INET, SOCK_STREAM, 0);
        if (_socket == -1) {
            printf("error in socket");
            return SocketConnectResult::SocketError;
        }

        fprintf(stdout, "CONNECTING: address=%s port=%d\n", host.c_str(), port);

        // Connect our local endpoint (represented by the socket file
        // descriptor) to the address specified by `server_addr`.
        //
        // On a TCP connection, `connect` passes to the kernel the
        // responsability of perming the TCP handshake, blocking the call while
        // the kernel goes forward (when nonblocking flag - SOCK_NONBLOCK - is
        // not set in the socket).
        //
        // Once the handshake has been succesful, on the server side the
        // connection is then put into a queue so that the server can
        // `accept(2)` on a passive socket to make use of such established
        // connection.
        ret = ::connect(_socket, (struct sockaddr*)&server_addr, sizeof(server_addr));
        if (ret == -1) {
            printf("error in connect\n");
            return SocketConnectResult::ConnectError;
        }

        // Set the socket recv timeout
        const int kSocketTimeoutSeconds = 10*1000;
        struct timeval tv;
        tv.tv_sec = kSocketTimeoutSeconds;
        setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));

        return SocketConnectResult::Ok;
    }

    int send(const std::vector<unsigned char>& data) {
        //std::cout << str << std::endl;

        int nwrite = ::send(_socket, (const char*)data.data(), data.size(), 0);

        //std::cout << nwrite << std::endl;
        return nwrite;
    }

    std::pair<SocketRecvResult, int> recv(std::vector<unsigned char>& dest) {
        //std::cout << dest.size() << std::endl;
        int nread = ::recv(_socket, (char*)&dest[0], dest.size(), 0);
        int err = WSAGetLastError();

        if (errno == EWOULDBLOCK || errno == EAGAIN || err == WSAETIMEDOUT) {
            return std::make_pair(SocketRecvResult::Timeout, nread);
        }
        if (nread < 0) {
            wprintf(L"recv failed with error: %d\n", err);
            return std::make_pair(SocketRecvResult::Error, nread);
        }
        //std::cout << nread << std::endl;
        return std::make_pair(SocketRecvResult::Ok, nread);
    }

    void disconnect() {
        int ret;
        // After the connection has been properly established, we could go
        // forward with `read(2)` and `write(2)` calls.
        //
        // As in this example we don't want to read or write from it, we can
        // proceed with a `shutdown(2)`, which takes care of terminating our
        // side of the channel.
        //
        // By specifying `SHUT_RDWR`, not only furtes receptions are
        // dissallowed, but transmissions to.
        ret = shutdown(_socket, SD_BOTH);
        if (ret == -1) {
            printf("socket shutdown error\n");
            //return -1;
        }

        // Once the connection got properly terminated, now we can proceed with
        // actually closing the file descriptor
        ret = closesocket(_socket);
        if (ret == -1) {
            printf("socket close error\n");
            //return -1;
        }

        //return 0;
    }
};
#else
struct Context {

    Context() { }

    ~Context() { }
};

struct SocketImpl {
    int _socket;

    void connect(const std::string& host, int port) {
        int ret = 0;
        int conn_fd;
        struct sockaddr_in server_addr = { 0 };

        // Specifiy the communication domain in which the address lives:
        // AF_INET      - IPV4;
        // AF_INET6     - IPV6;
        // AF_UNIX      - Local communication (used w/ unix sockets).
        server_addr.sin_family = AF_INET;

        // Assign the port that we supplied in host byte order in the
        // form of network byte order (`htons` performs the conversion).
        //
        // On amd64 (x86-64), the byte ordering of the host is little endian
        // (o.e., the least significant byte comes first).
        //
        // However, when it comes to networking, the byte ordering is big
        // endian - the most significant byte comes first.
        //
        // This means that a value (like 8000) defined in our machine needs
        // to be properly translated to the network byte order before being
        // sent.
        server_addr.sin_port = htons(port);

        // Fill the destination address with a 4-byte (32bit) unsigned integer
        // in network byte order after converting the address from the string
        // representation (e.g., "127.0.0.1").
        //
        // Given that we might see a failure in the conversion (e.g., if we
        // supply an invalid server_addr pointer or an invalid string), check
        // for these errors.
        //
        // Note that differently from the majority of syscalls and glibc
        // methods, success is defined with `1`, and not `0`. The information of
        // what represents success can often be found in man pages - e.g., `man
        // inet_pton`.
        ret = inet_pton(AF_INET, host.c_str(), &server_addr.sin_addr);

        if (ret != 1) {
            if (ret == -1) { 
                perror("inet_pton");
            }
            fprintf(stderr,
                    "failed to convert address %s "
                    "to binary net address\n",
                    host.c_str());
            //return -1;
        }

        // Create an endpoint for communication in our machine (our side) that
        // is meant to communicate over the AF_INET (IPv4) domain, using
        // SOCK_STREAM semantics (sequenced, reliable, two-way, connection-base
        // byte streams - TCP, for instance).
        //
        // Note that at this point, no communication has been made to an
        // external server yet - this operation is entirely local.
        _socket = socket(AF_INET, SOCK_STREAM, 0);
        if (_socket == -1) {
            printf("error in socket");
            //return -1;
        }

        fprintf(stdout, "CONNECTING: address=%s port=%d\n", host.c_str(), port);

        // Connect our local endpoint (represented by the socket file
        // descriptor) to the address specified by `server_addr`.
        //
        // On a TCP connection, `connect` passes to the kernel the
        // responsability of perming the TCP handshake, blocking the call while
        // the kernel goes forward (when nonblocking flag - SOCK_NONBLOCK - is
        // not set in the socket).
        //
        // Once the handshake has been succesful, on the server side the
        // connection is then put into a queue so that the server can
        // `accept(2)` on a passive socket to make use of such established
        // connection.
        ret = ::connect(_socket, (struct sockaddr*)&server_addr, sizeof(server_addr));
        if (ret == -1) {
            printf("error in connect\n");
            //return -1;
        }

        // Set the socket recv timeout
        const int kSocketTimeoutSeconds = 10;
        struct timeval tv;
        tv.tv_sec = kSocketTimeoutSeconds;
        tv.tv_usec = 0;
        setsockopt(_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
    }

    int send(const std::vector<char>& data) {
        //std::cout << str << std::endl;

        ssize_t nwrite = write(_socket, data.data(), data.size());

        //std::cout << nwrite << std::endl;
        return nwrite;
    }

    std::pair<SocketRecvResult, int> recv(std::vector<char>& dest) {
        //std::cout << dest.size() << std::endl;
        ssize_t nread = ::recv(_socket, &dest[0], dest.size(), 0);

        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return std::make_pair(SocketRecvResult::Timeout, nread);
        }

        //std::cout << nread << std::endl;
        return std::make_pair(SocketRecvResult::Ok, nread);
    }

    void disconnect() {
        int ret;
        // After the connection has been properly established, we could go
        // forward with `read(2)` and `write(2)` calls.
        //
        // As in this example we don't want to read or write from it, we can
        // proceed with a `shutdown(2)`, which takes care of terminating our
        // side of the channel.
        //
        // By specifying `SHUT_RDWR`, not only furtes receptions are
        // dissallowed, but transmissions to.
        ret = shutdown(_socket, SHUT_RDWR);
        if (ret == -1) {
            printf("socket shutdown error\n");
            //return -1;
        }

        // Once the connection got properly terminated, now we can proceed with
        // actually closing the file descriptor
        ret = close(_socket);
        if (ret == -1) {
            printf("socket close error\n");
            //return -1;
        }

        //return 0;
    }
};
#endif

struct SocketConnect {
    Socket* _socket;
    std::string host;
    int port;

    void operator()() {
        auto result = _socket->_impl->connect(host, port);
        switch (result) {
        case SocketConnectResult::AddressError:
            Console::error() << "Socket address error:" << host << port;
            break;
        case SocketConnectResult::SocketError:
            Console::error() << "Socket error:" << host << port;
            break;
        case SocketConnectResult::ConnectError:
            Console::error() << "Socket connect error:" << host << port;
            break;
        default:
            Console::debug() << "Socket connected: " << host << port;
            break;
        }

        _socket->_connected.emit(result == SocketConnectResult::Ok);
    }
};

struct SocketRecv {
    Socket* _socket;
    std::size_t _recvLength;

    void operator()() {
        std::vector<unsigned char> buf;
        buf.resize(_recvLength);

        SocketRecvResult result;
        int bytesRead;
        std::tie(result, bytesRead) = _socket->_impl->recv(buf);

        if (result == SocketRecvResult::Ok) {
            buf.resize(bytesRead);
            _socket->_dataReceived.emit(buf);
        }
        else if (result == SocketRecvResult::Timeout) {
            Console::debug() << "SocketRecv timeout";
            _socket->_recvTimeout.emit(0);
        }
        else if (result == SocketRecvResult::Error) {
            Console::error() << "SocketRecv unknown error";
        }
    }
};

Context& mainContext() {
    static Context ctx;
    return ctx;
}

Socket::Socket(const std::string& host, int port, WorkQueue& queue) : _queue{ &queue } {
    _context = &mainContext();

    queue.app().addDeferredSignal(queue.app().tick(), this->_connected);
    queue.app().addDeferredSignal(queue.app().tick(), this->_dataReceived);
    queue.app().addDeferredSignal(queue.app().tick(), this->_recvTimeout);

    _impl = Containers::pointer<SocketImpl>();
    _queue->addWork(SocketConnect{ this, host, port });
}

Socket::~Socket() {
    _impl->disconnect();
}

void Socket::send(const std::vector<unsigned char>& data, std::size_t recvlen) {
    _impl->send(data);
    if (recvlen > 0) {
        recv(recvlen);
    }
}

void Socket::recv(std::size_t len) {
    _queue->addWork(SocketRecv{ this, len });
}

}
