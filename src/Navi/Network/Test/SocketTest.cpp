/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <sstream>
#include <Corrade/TestSuite/Tester.h>

#include "Navi/Async/WorkQueue.h"
#include "Navi/Async/Future.hpp"
#include "Navi/Network/UtosClient.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi { namespace Network { namespace Test { namespace {

using namespace Corrade;

struct SocketTest: TestSuite::Tester {
    explicit SocketTest();

    void echoTest();
    void utosEchoTest();
    void utosRequestTest();
};

SocketTest::SocketTest() {
  addTests({
          //&SocketTest::echoTest,
          &SocketTest::utosEchoTest,
          &SocketTest::utosRequestTest});
}

void SocketTest::echoTest() {
    Async::WorkQueue queue{ 1, 10 };

    Socket sock{ "10.0.10.11", 6668, queue };
    bool running = true;
    std::string outstr = "";

    sock.connected().connect([](bool success) {
        std::cout << "Socket Connected: " << success << std::endl;
    });

    sock.send("Hello World!\n", 64);
    sock.dataReceived().connect([&outstr, &running](std::string v) {
            outstr = v;
            running = false;
            std::cout << "Socket Data Received" << std::endl;
    });
    sock.recvTimeout().connect([&running](int t) {
            running = false;
            std::cout << "Socket Timeout" << std::endl;
    });

    std::size_t i = 0;
    while (running && i < 5) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        sock.connected().dispatch();
        sock.dataReceived().dispatch();
        sock.recvTimeout().dispatch();
        i += 1;
    }

    CORRADE_VERIFY(outstr == "Hello World!\n");

    queue.wait();
}

void SocketTest::utosEchoTest() {
    Async::WorkQueue queue{ 1, 10 };

    UtosClient sock{ "10.0.10.11", 6668, queue };
    bool running = true;
    std::string outstr = "";

    auto body = Data::Variant::makeList();
    body.list().push_back(Data::Variant{ 42 });
    body.list().push_back(Data::Variant{ "Hello World" });
    body.list().push_back(Data::Variant{ true });
    body.list().push_back(Data::Variant{});

    sock.send(Encode::Payload{ "test_action", body });

    sock.socket().connected().connect([](bool b) {
        std::cout << "Socket connected" << std::endl;
    });

    sock.msgReceived().connect([&running, &outstr](Encode::Payload payload) {
        std::cout << "Payload received: " << payload.data() << std::endl;

        auto it = payload.body().list().begin();
        ++it;

        outstr = (*it).cast<std::string>();
        running = false;
    });

    std::size_t i = 0;
    while (running && i < 5) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        sock.socket().connected().dispatch();
        sock.socket().dataReceived().dispatch();
        sock.socket().recvTimeout().dispatch();
        i += 1;
    }

    CORRADE_VERIFY(outstr == "Hello World");

    queue.wait();
}

void SocketTest::utosRequestTest() {
    Async::WorkQueue queue{ 1, 10 };

    UtosClient sock{ "10.0.10.11", 6668, queue };
    int running = 6;
    int numItems = 0;

    auto body = Data::Variant::makeList();
    body.list().push_back(Data::Variant{ 42 });
    body.list().push_back(Data::Variant{ "Hello World" });
    body.list().push_back(Data::Variant{ true });
    body.list().push_back(Data::Variant{});

    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });
    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });
    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });
    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });
    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });
    sock.request(Encode::Payload{ "test_action", body }, [&running, &numItems](const Encode::Payload& response) {
            running--;
            numItems += response.body().list().size();
    });

    sock.socket().connected().connect([](bool b) {
        std::cout << "Socket connected" << std::endl;
    });

    std::size_t i = 0;
    while (running > 0 && i < 123) {
        std::this_thread::sleep_for(std::chrono::milliseconds(16));
        sock.socket().connected().dispatch();
        sock.socket().dataReceived().dispatch();
        sock.socket().recvTimeout().dispatch();
        i += 1;
    }

    CORRADE_VERIFY(numItems == 4*6);

    queue.wait();
}

}}}}

CORRADE_TEST_MAIN(Navi::Network::Test::SocketTest)
