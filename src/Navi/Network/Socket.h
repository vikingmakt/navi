#ifndef Navi_Network_Socket_h
#define Navi_Network_Socket_h

#include <string>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Signal.h"

namespace Navi { class WorkQueue; }

namespace Navi::Network {

struct SocketImpl;

struct Context;

/// Socket implements a tcp client socket.
class Socket {
    public:
        /// Construct a Socket that will connect to the given host and port number.
        /// The Socket's async tasks will be dispatched to the given queue.
        explicit Socket(const std::string& host, int port, WorkQueue& queue);
        ~Socket();

        /// Send bytes to the server, optionally set a length to automatically call `Socket::recv`.
        void send(const std::vector<unsigned char>& str, std::size_t recvlen = 0);

        /// Receive len bytes from server.
        void recv(std::size_t len);

        /// Signal emitted when the socket receives data from the server.
        DeferredSignal<std::vector<unsigned char>>& dataReceived() { return _dataReceived; }

        /// Signal emitted when the socket has been listening for longer than the set timeout.
        DeferredSignal<int>& recvTimeout() { return _recvTimeout; }

        /// Signal emitted when the socket acquires a connection to the server.
        DeferredSignal<bool>& connected() { return _connected; }

    private:
        friend struct SocketConnect;
        friend struct SocketDisconnect;
        friend struct SocketRecv;

        Containers::Pointer<SocketImpl> _impl;
        DeferredSignal<std::vector<unsigned char>> _dataReceived;
        DeferredSignal<int> _recvTimeout;
        DeferredSignal<bool> _connected;
        WorkQueue* _queue;
        Context* _context;
};

}

#endif
