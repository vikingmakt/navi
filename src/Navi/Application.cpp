/*
    This file is part of Navi.

    Original authors — credit is appreciated but not required:

        2019 — Guilherme Nemeth <guilherme.nemeth@gmail.com>

    This is free and unencumbered software released into the public domain.

    Anyone is free to copy, modify, publish, use, compile, sell, or distribute
    this software, either in source code form or as a compiled binary, for any
    purpose, commercial or non-commercial, and by any means.

    In jurisdictions that recognize copyright laws, the author or authors of
    this software dedicate any and all copyright interest in the software to
    the public domain. We make this dedication for the benefit of the public
    at large and to the detriment of our heirs and successors. We intend this
    dedication to be an overt act of relinquishment in perpetuity of all
    present and future rights to this software under copyright law.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <Corrade/Utility/Unicode.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Renderer.h>
#include "Navi/Application.h"
#include "Navi/Console.hpp"
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/Image.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Controller.h"
#include "Navi/Ui/FileBrowser.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Animation/Animation.h"
#include "Navi/Ui/Editors/PakEditor.h"

#ifdef NAVI_LUA
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Runtime.h"
#endif

#ifdef NAVI_DEFAULT
#include "Navi/Runtime/Default/Runtime.h"
#endif

#ifdef NAVI_IMGUI
#include "Navi/Renderer/DearImGui/Renderer.hpp"
#endif

namespace Navi {

static Application* g_instance = nullptr;

Application& Application::instance() {
    return *g_instance;
}

Application::Application(
    const Arguments& arguments,
    const PlatformConfiguration& appConfig,
    const Configuration& config)
    : Platform::Application{arguments, appConfig}
    , _config{config}
{
    //CORRADE_ASSERT(g_instance == nullptr, "Invalid application instance");
    g_instance = this;

    _renderer = Containers::Pointer<RendererType>(
        new RendererType(this, Vector2f{windowSize()}/dpiScaling(), windowSize(), framebufferSize())
    );

    #if !defined(MAGNUM_TARGET_WEBGL) && !defined(CORRADE_TARGET_ANDROID)
    /* Have some sane speed, please */
    setMinimalLoopPeriod(33);
    #endif

    _widgetManager = Containers::pointer<Ui::WidgetManager>(this);
    _resourceManager = Containers::pointer<Resource::ResourceManager>(*this);
    _animationManager = Containers::pointer<Animation::AnimationManager>(*this);
    _runtime = Containers::pointer<RuntimeType>(*this);
    _runtime->init();

    // Initialize console and application-related console commands
    addDeferredSignal(tick(), Console::_state.deferredCall);

    Console::varChanged().connect([this](std::pair<Containers::Reference<const std::string>, Containers::Reference<const Data::Variant>> pair) {
        if (*pair.first == "app_name") {
            setWindowTitle(pair.second->cast<std::string>());
        }
        else if (*pair.first == "app_scene") {
            std::string path = pair.second->cast<std::string>();
            _scene = Containers::pointer<Ui::SceneInstance>(_resourceManager->loadAsyncWithFile<Ui::Scene>(path, path));
            _scene->_scene.resource().finishedLoading().connect(*this, &Application::handleSceneLoaded);
        }
        else if (*pair.first == "app_fps") {
            unsigned int mil = std::floorf(1.0f / pair.second->cast<float>() * 1000.0f);
            setMinimalLoopPeriod(mil);
        }
    });

    auto bind = [this](const ConsoleCommandCall& call) {
        std::string cmd = call.args[1].cast<std::string>();
        bindKey(call.args[0].cast<std::string>(), cmd, [cmd=std::move(cmd)]() {
            Console::execDeferred(cmd, false);
        });
    };
    auto unbind = [this](const ConsoleCommandCall& call) {
        unbindKey(call.args[0].cast<std::string>());
    };
    auto unbindAll = [this](const ConsoleCommandCall& call) {
        unbindAllKeys();
    };
    auto showBindings = [this](const ConsoleCommandCall& call) {
        for (const auto& it : _keyBindings) {
            Console::info() << it.first << "-" << it.second.action;
        }
    };
    auto quit = [this](const ConsoleCommandCall& call) {
        exit();
    };
    auto subScene = [this](const ConsoleCommandCall& call) {
        Ui::createSubScene(call.args[0].cast<std::string>(), _scene->rootWidget());
    };
    auto destroyThisScene = [this](const ConsoleCommandCall& call) {
        if (!call.scene) {
            Console::error() << "this command can only run in the context of a scene";
            return;
        }

        call.scene->controller<Ui::Controller>().destroyScene();
    };
    auto selectFile = [this](const ConsoleCommandCall& call, bool cvar) {
        std::string varname = call.args[0].cast<std::string>();
        int varIndex = -1;
        if (!cvar) {
            varIndex = call.scene->addVar(varname, {});
        }

        std::string title;
        if (call.args.size() >= 2) title = call.args[1].cast<std::string>();

        std::string basepath;
        if (call.args.size() >= 3) basepath = call.args[2].cast<std::string>();

        Ui::FileBrowser::selectFile(title, basepath, *_scene).connect([this, varname = std::move(varname), varIndex, &call, cvar](Containers::Optional<std::string> filename) {
            if (filename) {
                if (cvar) Console::setVar(varname, *filename);
                else      call.scene->setVar(varIndex, *filename);
            }
        });
    };
    auto selectDir = [this](const ConsoleCommandCall& call, bool cvar) {
        std::string varname = call.args[0].cast<std::string>();
        int varIndex = -1;
        if (!cvar) {
            varIndex = call.scene->addVar(varname, {});
        }

        std::string title;
        if (call.args.size() >= 2) title = call.args[1].cast<std::string>();

        std::string basepath;
        if (call.args.size() >= 3) basepath = call.args[2].cast<std::string>();

        Ui::FileBrowser::selectDirectory(title, basepath, *_scene).connect([this, varname=std::move(varname), varIndex, &call, cvar](Containers::Optional<std::string> filename) {
            if (filename) {
                if (cvar) Console::setVar(varname, *filename);
                else      call.scene->setVar(varIndex, *filename);
            }
        });
    };
    auto selectFileWrapper = [this, selectFile](bool cvar) {
        return [this, selectFile, cvar](const ConsoleCommandCall& call) { selectFile(call, cvar); };
    };
    auto selectDirWrapper = [this, selectDir](bool cvar) {
        return [this, selectDir, cvar](const ConsoleCommandCall& call) { selectDir(call, cvar); };
    };
    Console::registerCommands(Containers::Array<ConsoleCommand>{Containers::InPlaceInit, {
        {"bind", "<keys> <cmd>", "Runs a command whenever the keys are pressed", 2, bind},
        {"unbind", "<keys>", "Unbind specific keys", 1, unbind},
        {"unbindAll", "", "Clears all key bindings", 0, unbindAll},
        {"showBindings", "", "Show key bindings", 0, showBindings},
        {"quit", "", "Quit the application", 0, quit},
        {"subScene", "<scene_path>", "Creates a sub scene in the main scene", 1, subScene},
        {"destroyThisScene", "", "Destroy the scene where this command was called from", 0, destroyThisScene},
        {"selectFile", "<scene_var_name> <title> <base_path>", "Select a file and store it in a scene var", 1, selectFileWrapper(false)},
        {"selectDir", "<scene_var_name> <title> <base_path>", "Select a directory and store it in a scene var", 1, selectDirWrapper(false)},
        {"selectFileCvar", "<cvar_name> <title> <base_path>", "Select a file and store it in a cvar", 1, selectFileWrapper(true)},
        {"selectDirCvar", "<cvar_name> <title> <base_path>", "Select a directory and store it in a cvar", 1, selectDirWrapper(true)}
    }});

    Vector2i ws = windowSize();
    Vector2i fs = framebufferSize();
    Console::registerVars(Containers::Array<ConsoleVar>{Containers::InPlaceInit, {
        {"app_name", "The application name (also the window title)", Data::Variant{"Player Application"}},
        {"app_scene", "The main scene of the application", Data::Variant{""}},
        {"app_fps", "Target FPS of the application", Data::Variant{ 30.0f }},
        {"window_size", "The window size", Vector2f(ws.x(), ws.y())},
        {"window_framebufferSize", "The window's default framebuffer size", Vector2f(fs.x(), fs.y())}
    }});

    if (!_config.isBundle) {
        _resourceManager->addDirectoryOrPak("vm1");
    }
    _resourceManager->addDirectoryOrPak(_config.mainResourcePath, true);
    _timeline.start();

    registerController<Ui::NullController>();
    Ui::FileBrowser::registerController(*this);
    Ui::Editors::registerPakEditorControllers(*this);
}

void Application::handleSceneLoaded(Resource::AbstractResource* res) {
    //auto it = _controllerFactories.find(res->id());
    //if (it == _controllerFactories.end()) return;

    //_scene->setController(it->second(*_scene));
}

void Application::registerControllerFactory(const StringHash& name, Application::ControllerFactoryFunc func) {
    _controllerFactories[name] = func;
}

Containers::Pointer<Ui::Controller> Application::makeController(const StringHash& name, Ui::SceneInstance& scene) {
    auto it = _controllerFactories.find(name);
    if (it == _controllerFactories.end()) {
        return nullptr;
    }

    return it->second(scene);
}

void Application::bindKey(std::string expr, std::string action, std::function<void()> cb) {    
    std::transform(expr.begin(), expr.end(), expr.begin(), [](unsigned char c){ return std::tolower(c); });
    _keyBindings[expr] = { action, cb };
}

void Application::unbindKey(std::string expr) {    
    std::transform(expr.begin(), expr.end(), expr.begin(), [](unsigned char c){ return std::tolower(c); });
    _keyBindings.erase(expr);
}

void Application::unbindAllKeys() {
    _keyBindings.clear();
}

void Application::tickEvent() {
    float dt = _timeline.previousFrameDuration();
    _deltaTime = dt;
    _time += dt;
    _tick.emit(dt);
}

void Application::exitEvent(ExitEvent& event) {
    _scene.release();
    event.setAccepted();
}

void Application::drawEvent() {
    GL::defaultFramebuffer.clear(GL::FramebufferClear::Color);

    _preRender.emit(_timeline.previousFrameDuration());

    if (_scene && _scene->_scene.loaded()) {
        _renderer->newFrame();
        _renderer->drawUi(*_scene);
    }

    _postRender.emit(_timeline.previousFrameDuration());

    swapBuffers();
    redraw();

    _timeline.nextFrame();
}

void Application::viewportEvent(ViewportEvent& event) {
    GL::defaultFramebuffer.setViewport({{}, event.framebufferSize()});

    _renderer->relayout(Vector2{event.windowSize()},
        event.windowSize(), event.framebufferSize());

    Vector2i ws = event.windowSize();
    Vector2i fs = event.framebufferSize();
    Console::setVar("window_size", Vector2f(ws.x(), ws.y()));
    Console::setVar("window_framebufferSize", Vector2f(fs.x(), fs.y()));
}

void Application::keyPressEvent(KeyEvent& event) {
    //redraw();
    std::string bindingName = "";
    if (event.modifiers() & KeyEvent::Modifier::Ctrl) {
        bindingName.append("ctrl+");
    }
    if (event.modifiers() & KeyEvent::Modifier::Alt) {
        bindingName.append("alt+");
    }
    if (event.modifiers() & KeyEvent::Modifier::Shift) {
        bindingName.append("shift+");
    }

    {
        std::string keyName = event.keyName();
        std::transform(keyName.begin(), keyName.end(), keyName.begin(), [](unsigned char c){ return std::tolower(c); });
        bindingName.append(keyName);
    }

    auto it = _keyBindings.find(bindingName);
    if (it != _keyBindings.end()) {
        it->second.callback();
    }

    if(_renderer->handleKeyPressEvent(event)) return;
}

void Application::keyReleaseEvent(KeyEvent& event) {
    //redraw();
    if(_renderer->handleKeyReleaseEvent(event)) return;
}

void Application::mousePressEvent(MouseEvent& event) {
    //redraw();
    if(_renderer->handleMousePressEvent(event)) return;
}

void Application::mouseReleaseEvent(MouseEvent& event) {
    //redraw();
    if(_renderer->handleMouseReleaseEvent(event)) return;
}

void Application::mouseMoveEvent(MouseMoveEvent& event) {
    //redraw();
    if(_renderer->handleMouseMoveEvent(event)) return;
}

void Application::mouseScrollEvent(MouseScrollEvent& event) {
    //redraw();
    if(_renderer->handleMouseScrollEvent(event)) {
        /* Prevent scrolling the page */
        event.setAccepted();
        return;
    }
}

void Application::textInputEvent(TextInputEvent& event) {
    //redraw();
    //_inputRecorder.handleTextInputEvent(event);
    if(_renderer->handleTextInputEvent(event)) return;
}

}