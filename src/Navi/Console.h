#ifndef Navi_Console_h
#define Navi_Console_h

#include <sstream>
#include <vector>
#include <map>
#include <functional>
#include <utility>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/Optional.h>
#include "Navi/Navi.h"
#include "Navi/Signal.h"
#include "Navi/StringHash.h"
#include "Navi/WorkQueue.h"
#include "Navi/Data/Variant.h"
#include "Navi/Resource/FileWatcher.h"

namespace Navi {

namespace Ui { class SceneInstance; }

class Application;

template<class Arg>
void writeCout(Arg&& arg);

/// Tag types for log levels
class LogDebug {
    public:
        static const char* prefix() { return ""; }
        static Vector3f color() { return {0.7f, 0.7f, 0.7f}; }
};

class LogInfo {
    public:
        static const char* prefix() { return ""; }
        static Vector3f color() { return {0.9f, 0.9f, 0.9f}; }
};

class LogError {
    public:
        static const char* prefix() { return "[error] "; }
        static Vector3f color() { return {1.0f, 0.0f, 0.0f}; }
};

class AbstractLogEntry {
    public:
        virtual Vector3f color() = 0;
        virtual std::string output() = 0;
};

typedef std::pair<std::string, std::vector<Data::Variant>> ParsedCommand;

struct ConsoleCommandCall {
    std::string name;
    std::vector<Data::Variant> args;
    Ui::SceneInstance* scene;

    const Data::Variant* findOption(const std::string& optname) const;
};

struct ConsoleCommand {
    typedef std::function<void(const ConsoleCommandCall&)> Func;

    std::string name;
    std::string usage;
    std::string description;
    std::size_t requiredArgs;
    Func func;
};

enum class ConsoleVarFlag: unsigned int {
    None = 0,
    UserConfig = 1,
};
typedef Containers::EnumSet<ConsoleVarFlag> ConsoleVarFlags;
CORRADE_ENUMSET_OPERATORS(ConsoleVarFlags);

struct ConsoleVar {
    std::string name;
    std::string description;
    Data::Variant value;
    ConsoleVarFlags flags = ConsoleVarFlag::None;
};

struct ConsoleState {
    typedef std::pair<Containers::Reference<const std::string>, Containers::Reference<const Data::Variant>> VarChangedArg;

    std::vector<Containers::Pointer<AbstractLogEntry>> entries;
    std::vector<Containers::Pointer<AbstractLogEntry>> capturedEntries;
    std::vector<std::string> history;
    std::map<std::string, ConsoleCommand> commands;
    std::map<std::string, Containers::Pointer<ConsoleVar>> vars;
    std::map<std::string, Containers::Pointer<Resource::FileWatcher>> watchingFiles;
    Signal<VarChangedArg> varChanged;
    DeferredSignal<ConsoleCommandCall> deferredCall;    
    std::mutex entriesMutex;
    float lastUserConfigWriteTime = 0.0f;
    bool wantToShow = false;
    bool hasReadUserConfig = false;
    Containers::Pointer<WorkQueue> _ioQueue;
    Containers::Pointer<std::ostringstream> capture;
    bool capturing = false;

    explicit ConsoleState();

    WorkQueue& ioQueue();
};

struct ConsoleLogEntries {
    std::vector<Containers::Pointer<AbstractLogEntry>>* entries;
    Containers::Pointer<std::lock_guard<std::mutex>> lock;
};


/// Log format flags
enum class LogEntryFlag: unsigned int {
    None = 0,
    Space = 1,
    Newline = 2,
};
typedef Containers::EnumSet<LogEntryFlag> LogEntryFlags;
CORRADE_ENUMSET_OPERATORS(LogEntryFlags);

template<typename Level>
class LogEntry : public AbstractLogEntry {
    public:
        explicit LogEntry(ConsoleState& state)
            : _state{ &state } {}

        template<class Arg>
        inline LogEntry& operator <<(Arg&& arg) {
            if (_state->capturing) {
                (*_state->capture) << arg;
            }
            else {
                writeCout(arg);
                _stream << arg;
            }
            return *this;
        }

        Vector3f color() override { return Level::color(); }

        std::string output() override { return _stream.str(); }

        std::ostringstream _stream;
        ConsoleState* _state;
};

template<typename Level>
class Logger {
    public:
        explicit Logger(LogEntry<Level>& entry, LogEntryFlags flags = LogEntryFlag::Space | LogEntryFlag::Newline)
            : _entry{ &entry }, _flags{ flags } {

            entry << Level::prefix();
        }

        ~Logger() {
            if ((_flags & LogEntryFlag::Newline) && !_entry->_state->capturing)
                *_entry << "\n";
        }

        template<class Arg>
        inline Logger& operator <<(Arg&& arg) {
            *_entry << arg;
            if ((_flags & LogEntryFlag::Space) && !_entry->_state->capturing)
                *_entry << " ";
            return *this;
        }

    private:
        LogEntry<Level>* _entry;
        LogEntryFlags _flags;
};


class Console {
    public:
        static Logger<LogDebug> debug(LogEntryFlags flags = LogEntryFlag::Space | LogEntryFlag::Newline);

        static Logger<LogInfo> info(LogEntryFlags flags = LogEntryFlag::Space | LogEntryFlag::Newline);

        static Logger<LogError> error(LogEntryFlags flags = LogEntryFlag::Space | LogEntryFlag::Newline);

        static auto& varChanged() { return _state.varChanged; }

        static bool wantToShow() { return _state.wantToShow; }

        static const std::vector<std::string>& history() { return _state.history; }

        static WorkQueue& ioQueue() { return _state.ioQueue(); }

        static ConsoleLogEntries entries();

        static void registerCommands(const Containers::Array<ConsoleCommand>& cmds);

        static void registerVars(const Containers::Array<ConsoleVar>& vars);

        static void registerVar(const std::string& name, const std::string& desc, const Data::Variant& defValue, ConsoleVarFlags flags = ConsoleVarFlag::None);

        static void setVar(const std::string& name, const Data::Variant& value);

        static Data::Variant& var(const std::string& name);

        static Data::Variant* findVar(const std::string& name);

        static void exec(const std::string& cmd, bool addToHistory = true, Ui::SceneInstance* scene = nullptr);

        static void execDeferred(const std::string& cmd, bool addToHistory = true, Ui::SceneInstance* scene = nullptr);

        static void execFile(const std::string& filename);

        static void call(const std::string& cmd, const std::vector<Data::Variant>& args, Ui::SceneInstance* scene = nullptr);

        static Containers::Optional<ParsedCommand> parse(const std::string& cmd);

        static void saveUserConfig();

        static void loadUserConfig();

        static void saveUserConfig(const std::string& filename);

        static void loadUserConfig(const std::string& filename);

        static void update();

        static void beginCapture();

        static std::string endCapture();

    private:
        friend class Application;

        static ConsoleState _state;
};

}

#endif
