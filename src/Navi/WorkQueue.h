#ifndef Navi_WorkQueue_h
#define Navi_WorkQueue_h

#include <atomic>
#include <thread>
#include <future>
#include <queue>
#include <condition_variable>
#include <mutex>
#include <functional>
#include "Navi/Data/LinkedList.h"
#include "Navi/Signal.h"

namespace Navi {

class WorkQueue {
    public:
        typedef std::function<void()> WorkFuncType;
        typedef std::function<void()> CallbackFuncType;

        struct WorkData {
            WorkFuncType func;
            CallbackFuncType callback;
        };

        struct ThreadData {
            std::queue<WorkData> jobs;
            std::thread thread;
            std::condition_variable conditionVar;
            std::mutex mutex;
            WorkQueue* workQueue;
            bool stop = false;

            ThreadData() {}

            ThreadData(const ThreadData&) = delete;

            ThreadData(ThreadData&& rhs) {
                *this = std::move(rhs);
            }

            ~ThreadData() {
                stop = true;
                if (thread.joinable()) thread.join();
            }

            ThreadData& operator =(const ThreadData&) = delete;

            ThreadData& operator =(ThreadData&& rhs) {
                jobs = std::move(rhs.jobs);
                thread = std::move(rhs.thread);
                workQueue = rhs.workQueue;
                stop = rhs.stop;
                return *this;
            }
        };

        static void threadLoop(ThreadData*);

        WorkQueue(Application& app, std::size_t maxThreads, std::size_t maxQueueSize);
        ~WorkQueue();

        bool addWorkWithCallback(WorkFuncType work, CallbackFuncType callback);

        bool addWork(WorkFuncType work) { return addWorkWithCallback(work, {}); }

        void tick(float dt);

        Application& app() { return *_app; }

        std::size_t numThreads() const {
            return _threads.size();
        }

        void wait() {
            while (_pendingJobs > 0) {}
        }

    private:
        Data::LinkedList<ThreadData> _threads;
        DeferredSignal<WorkData> _workFinished;
        std::size_t _maxThreads;
        std::size_t _maxQueueSize;
        std::size_t _nextThreadIdx;
        std::atomic_size_t _pendingJobs;
        Application* _app;
};

}

#endif
