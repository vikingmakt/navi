#ifndef Navi_Application_h
#define Navi_Application_h

/*
    This file is part of Navi.

    Original authors — credit is appreciated but not required:

        2019 — Guilherme Nemeth <guilherme.nemeth@gmail.com>

    This is free and unencumbered software released into the public domain.

    Anyone is free to copy, modify, publish, use, compile, sell, or distribute
    this software, either in source code form or as a compiled binary, for any
    purpose, commercial or non-commercial, and by any means.

    In jurisdictions that recognize copyright laws, the author or authors of
    this software dedicate any and all copyright interest in the software to
    the public domain. We make this dedication for the benefit of the public
    at large and to the detriment of our heirs and successors. We intend this
    dedication to be an overt act of relinquishment in perpetuity of all
    present and future rights to this software under copyright law.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <map>
#include <functional>
#include <Corrade/Containers/StaticArray.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Interconnect/Receiver.h>
#include <Magnum/Math/Color.h>

#ifdef CORRADE_TARGET_ANDROID
#include <Magnum/Platform/AndroidApplication.h>
#elif defined(CORRADE_TARGET_EMSCRIPTEN)
#include <Magnum/Platform/EmscriptenApplication.h>
#else
#include <Magnum/Platform/Sdl2Application.h>
#endif

#include <Magnum/Timeline.h>

#include "Navi/Navi.h"
#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Resource/ResourceManager.h"
#include "Navi/Ui/Ui.h"

#define NAVI_DEFAULT
#define NAVI_IMGUI

#ifdef NAVI_LUA
#include "Navi/Runtime/Lua/Lua.h"
#endif

#ifdef NAVI_DEFAULT
#include "Navi/Runtime/Default/Default.h"
#endif

namespace Navi {

#ifdef NAVI_DEFAULT
using RuntimeType = Navi::Runtime::Default::Runtime;
#endif

#ifdef NAVI_IMGUI
namespace Renderer::DearImGui {
class Renderer;
}
using RendererType = Renderer::DearImGui::Renderer;
#endif

namespace Animation {
class AnimationManager;
}

namespace Ui {
class Controller;
}

using namespace Math::Literals;

typedef Platform::Sdl2Application::Configuration PlatformConfiguration;

class Application: public Interconnect::Receiver, public Platform::Application {
    public:
        typedef Containers::Pointer<Ui::Controller>(*ControllerFactoryFunc)(Ui::SceneInstance& scene);

        struct Configuration {
            std::string mainResourcePath;
            bool isBundle = false;

            Configuration& setMainResourcePath(const std::string& path) {
                mainResourcePath = path;
                return *this;
            }

            Configuration& setIsBundle(bool isBundle) {
                this->isBundle = isBundle;
                return *this;
            }
        };

        struct KeyBinding {
            std::string action;
            std::function<void()> callback;
        };

        static Application& instance();

        template<class T> static Containers::Pointer<Ui::Controller> genericControllerFactory(Ui::SceneInstance& scene) {
            return Containers::Pointer<Ui::Controller>{ new T{ scene } };
        }

        explicit Application(
            const Arguments& arguments,
            const Platform::Sdl2Application::Configuration& appConfig,
            const Configuration& config
        );

        void tickEvent() override;
        void drawEvent() override;

        void exitEvent(ExitEvent& event) override;

        void viewportEvent(ViewportEvent& event) override;

        void keyPressEvent(KeyEvent& event) override;
        void keyReleaseEvent(KeyEvent& event) override;

        void mousePressEvent(MouseEvent& event) override;
        void mouseReleaseEvent(MouseEvent& event) override;
        void mouseMoveEvent(MouseMoveEvent& event) override;
        void mouseScrollEvent(MouseScrollEvent& event) override;
        void textInputEvent(TextInputEvent& event) override;

        void handleSceneLoaded(Resource::AbstractResource*);

        void registerControllerFactory(const StringHash& name, ControllerFactoryFunc func);

        Containers::Pointer<Ui::Controller> makeController(const StringHash& name, Ui::SceneInstance& scene);

        void bindKey(std::string expr, std::string action, std::function<void()> callback);

        void unbindKey(std::string expr);

        void unbindAllKeys();

        Ui::SceneInstance& sceneInstance() { return *_scene; }

        Ui::WidgetManager& widgetManager() { return *_widgetManager; }

        RuntimeType& runtime() { return *_runtime; }

        Resource::ResourceManager& resourceManager() { return *_resourceManager; }

        Animation::AnimationManager& animationManager() { return *_animationManager; }

        RendererType& renderer() { return *_renderer; }

        Float deltaTime() const { return _deltaTime; }

        Float time() const { return _time; }

        Signal<float>& tick() { return _tick; }

        Signal<float>& preRender() { return _preRender; }

        Signal<float>& postRender() { return _postRender; }

        template<class T> auto addDeferredSignal(Signal<float>& ltsig, DeferredSignal<T>& sig, const std::string& name = "no name") {
            return ltsig.connect([&sig, name=std::move(name)](float) {
                sig.dispatch();
            });
        }

        template<class T> void registerController() {
            registerControllerFactory(T::controllerName(), &Application::genericControllerFactory<T>);
        }

    private:
        Configuration _config;

        Containers::Pointer<RendererType> _renderer;
        Containers::Pointer<Ui::SceneInstance> _scene;
        Containers::Pointer<Ui::WidgetManager> _widgetManager;
        Containers::Pointer<RuntimeType> _runtime;
        Containers::Pointer<Resource::ResourceManager> _resourceManager;
        Containers::Pointer<Animation::AnimationManager> _animationManager;
        Signal<float> _tick;
        Signal<float> _preRender;
        Signal<float> _postRender;
        std::unordered_map<StringHash, ControllerFactoryFunc> _controllerFactories;
        std::map<std::string, KeyBinding> _keyBindings;
        Timeline _timeline;
        bool _scenePrevLoaded = false;
        bool _showTestWindow = true;
        bool _showAnotherWindow = false;
        Color4 _clearColor = 0x72909aff_rgbaf;
        Float _floatValue = 0.0f;
        Float _deltaTime = 0.0f;
        Float _time = 0.0f;
};

}

#endif
