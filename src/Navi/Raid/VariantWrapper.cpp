#include <iostream>
#include "Navi/Raid/VariantWrapper.h"

namespace Navi::Raid {

void VariantWrapper::msgpack_pack(msgpack::packer<msgpack::sbuffer>& o) const {
    switch(_data->type()) {
    case Data::VariantType::Nil:
        o.pack(msgpack::type::nil_t());
        break;

    case Data::VariantType::Bool:
        o.pack(_data->cast<bool>());
        break;

    case Data::VariantType::Int:
        o.pack(_data->cast<int>());
        break;

    case Data::VariantType::Float:
        o.pack(_data->cast<float>());
        break;

    case Data::VariantType::String:
        o.pack(_data->cast<std::string>());
        break;

    case Data::VariantType::List:
        {
            const auto& value_list = _data->list();
            o.pack_array(value_list.size());

            for (const auto& value : value_list) {
                o.pack(VariantWrapper{value});
            }
            break;
        }

    case Data::VariantType::Map:
        {
            const auto& value_map = _data->map();
            o.pack_map(value_map.size());

            for (const auto& value : value_map) {
                o.pack(value.first);
                o.pack(VariantWrapper{value.second});
            }
            break;
        }
    }
}

}
