#ifndef Navi_Raid_Payload_h
#define Navi_Raid_Payload_h

#include <iostream>
#include <msgpack.hpp>

#include "Navi/Data/Variant.h"
#include "Navi/Raid/VariantDecoder.h"
#include "Navi/Raid/VariantWrapper.h"
#include "Navi/StringHash.h"

namespace Navi::Raid {

class Payload {
    public:
        Payload() {}
        Payload(const std::string& action);
        Payload(const Data::Variant& header, const Data::Variant& body);

        void generateEtag();

        std::string action() const;
        std::string etag() const;
        std::string code() const;

        inline Data::Variant& body()             { return _body; }
        inline const Data::Variant& body() const { return _body; }

        inline Data::Variant& header()             { return _header; }
        inline const Data::Variant& header() const { return _header; }

        void setAction(const std::string& action);
        void setBody(const Data::Variant& body);
        void setHeader(const Data::Variant& header);

        static Payload fromMsg(const std::vector<char>& msg);

        std::vector<char> data() const;
        Data::Variant mapData() const;

    private:
        Data::Variant _header;
        Data::Variant _body;
};

}

#endif
