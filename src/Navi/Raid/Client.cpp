#include <iostream>
#include "Navi/Raid/Client.h"
#include "Navi/Application.h"
#include "Navi/WorkQueue.h"
#include "Navi/Console.hpp"

namespace Navi::Raid {

static constexpr const int kMsgChunkSize = 1024;
static constexpr const int kMsgLengthSize = 4;

Client::Client(const std::string& host, int port, WorkQueue& queue)
    : _socket{ new Network::Socket{host, port, queue} }, _state{ State::WaitingMessage } {
    _waitMsg();

    _socket->dataReceived().connect(*this, &Client::_handleDataReceived);
    _socket->recvTimeout().connect(*this, &Client::_handleRecvTimeout);
}

Client::~Client() {
}

void Client::send(const Payload& msg) {
    std::vector<char> payload = msg.data();

    unsigned int lbuf = payload.size();
    std::vector<unsigned char> buf;

    buf.push_back((lbuf >> 24) & 0xFF);
    buf.push_back((lbuf >> 16) & 0xFF);
    buf.push_back((lbuf >> 8) & 0xFF);
    buf.push_back(lbuf & 0xFF);
    buf.insert(buf.end(), payload.begin(), payload.end());

    //Console::debug() << "Client::send - payload: " << payload;
    //Console::debug() << "Client::send - payload size: " << lbuf;

    std::cout << std::string{ payload.data(), payload.size() } << std::endl;

    _socket->send(buf);
}

void Client::request(const Payload& msg, RequestCallbackFunc cb) {
    std::string etagstr = msg.etag();
    Console::debug() << "Client::request" << etagstr << msg.action();

    StringHash etag = etagstr;
    _requests[etag] = cb;
    send(msg);
    //_waitMsg();
}

void Client::_processMsg(uint32_t len) {
    _state = State::ProcessingMessage;
    _msgTotalSize = len;
    _msgBuf.clear();
    _socket->recv(len >= kMsgChunkSize ? kMsgChunkSize : len);

    Console::debug() << "Client::_processMsg - payload size: " << len;
}

void Client::_waitMsg() {
    _state = State::WaitingMessage;
    _socket->recv(kMsgLengthSize);
}

void Client::_replyRequest(const Payload& payload) {
    StringHash etag = payload.etag();
    if (_requests.find(etag) == _requests.end()) {
        return;
    }

    Console::debug() << "Client::_replyRequest - etag: " << payload.etag() << "\n";

    auto callback = _requests[etag];
    _requests.erase(etag);
    callback(payload);
}

void Client::_handleDataReceived(std::vector<unsigned char> data) {
    //Utility::Debug{} << "data received: " << data;
    switch (_state) {
    case State::WaitingMessage:
        {
            uint32_t len = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3]);
            _processMsg(len);
        }
        break;

    case State::ProcessingMessage:
        _msgBuf.insert(_msgBuf.end(), data.begin(), data.end());

        // If the message is complete, decode it and emit the messageReceived signal
        if (_msgBuf.size() >= _msgTotalSize) {
            //Console::debug() << "Client::_handleDataReceived - message finished";

            auto payload = Payload::fromMsg(_msgBuf);
            _msgReceived.emit(payload);
            _replyRequest(payload);
            _waitMsg();
            return;
        }

        // If the message is not completely received yet, read one more chunk
        std::size_t remSize = _msgTotalSize - _msgBuf.size();
        _socket->recv(remSize >= kMsgChunkSize ? kMsgChunkSize : remSize);
        break;
    }
}

void Client::_handleRecvTimeout(int t) {
    switch (_state) {
    case State::WaitingMessage:
        _waitMsg();
        break;

    case State::ProcessingMessage:
        Console::error() << "Client: timed out while processing message, falling back to wait new message";
        _waitMsg();
        break;
    }
}

}
