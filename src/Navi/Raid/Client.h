#ifndef Navi_Raid_Client_h
#define Navi_Raid_Client_h

#include <string>
#include <unordered_map>
#include <functional>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Raid/Payload.h"
#include "Navi/Network/Socket.h"
#include "Navi/StringHash.h"
#include "Navi/Signal.h"

namespace Navi { class WorkQueue; }

namespace Navi::Raid {

class Client : public Interconnect::Receiver {
    public:
        typedef std::function<void(const Payload&)> RequestCallbackFunc;

        explicit Client(const std::string& host, int port, WorkQueue& queue);
        ~Client();

        void send(const Payload& msg);

        void request(const Payload& msg, RequestCallbackFunc func);

        Network::Socket& socket() { return *_socket; }

        Signal<Payload>& msgReceived() { return _msgReceived; }

    private:
        enum State {
            WaitingMessage,
            ProcessingMessage,
        };

        void _processMsg(uint32_t len);
        void _waitMsg();
        void _replyRequest(const Payload& p);
        void _handleDataReceived(std::vector<unsigned char> data);
        void _handleRecvTimeout(int t);

        std::unordered_map<StringHash, RequestCallbackFunc> _requests;
        Signal<Payload> _msgReceived;
        Containers::Pointer<Network::Socket> _socket;
        uint32_t _msgTotalSize;
        std::vector<char> _msgBuf;
        State _state;
};

}

#endif
