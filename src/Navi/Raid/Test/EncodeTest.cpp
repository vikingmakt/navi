/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <sstream>
#include <Corrade/TestSuite/Tester.h>

#include "Navi/Encode/Payload.h"

namespace Navi::Encode::Test {
namespace {

using namespace Corrade;

struct EncodeTest: TestSuite::Tester {
    explicit EncodeTest();

    void payloadVariantGetMsgTest();
    void payloadVariantWithMsgTest();

};

EncodeTest::EncodeTest() {
    addTests({
        &EncodeTest::payloadVariantGetMsgTest,
        &EncodeTest::payloadVariantWithMsgTest
    });
}

void EncodeTest::payloadVariantGetMsgTest() {
    const std::string body = "32131231312";
    Payload p("msg_send", Data::Variant{body});

    msgpack::sbuffer sbuf;
    msgpack::packer<msgpack::sbuffer> pk(&sbuf);

    pk.pack_map(2);
    pk.pack("header");
    pk.pack_map(2);
    pk.pack("action");
    pk.pack("msg_send");
    pk.pack("etag");
    pk.pack(std::string(p.etag()));
    pk.pack("body");
    pk.pack("32131231312");

    std::string payload = sbuf.data();
    unsigned int lbuf = payload.size();
    std::string data;

    data += (lbuf >> 24) & 0xFF;
    data += (lbuf >> 16) & 0xFF;
    data += (lbuf >> 8) & 0xFF;
    data += lbuf & 0xFF;

    data.append(payload);

    CORRADE_COMPARE(p.msg(), data);
}

void EncodeTest::payloadVariantWithMsgTest() {
    const std::string body = "32131231312";
    const std::string etag = "12345678";
    msgpack::sbuffer sbuf;
    msgpack::packer<msgpack::sbuffer> pk(&sbuf);

    pk.pack_map(2);
    pk.pack("header");
    pk.pack_map(2);
    pk.pack("action");
    pk.pack("msg_send");
    pk.pack("etag");
    pk.pack(etag);
    pk.pack("body");
    pk.pack("32131231312");

    std::string payload = sbuf.data();

    Payload p = Payload::withMsg(payload);

    CORRADE_COMPARE(p.etag(), etag);
}

}}

CORRADE_TEST_MAIN(Navi::Encode::Test::EncodeTest)
