#include <iostream>
#include "Navi/Raid/VariantDecoder.h"

namespace Navi::Raid {

void VariantDecoder::msgpack_unpack(msgpack::object const& o) {
    msgpack_unpack(*_data, o);
}

void VariantDecoder::msgpack_unpack(Data::Variant& data, msgpack::object const& o) {
    switch(o.type) {
    case msgpack::type::NIL:
        data = Data::Variant{};
        return;

    case msgpack::type::BOOLEAN:
        data = Data::Variant{o.via.boolean};
        return;

    case msgpack::type::POSITIVE_INTEGER:
        data = Data::Variant{static_cast<int>(o.via.u64)};
        return;

    case msgpack::type::NEGATIVE_INTEGER:
        data = Data::Variant{static_cast<int>(o.via.i64)};
        return;

    case msgpack::type::FLOAT32:
        data = Data::Variant{static_cast<float>(o.via.f64)};
        return;

    case msgpack::type::FLOAT64:
        data = Data::Variant{static_cast<float>(o.via.f64)};
        return;

    case msgpack::type::STR:
        {
            char* value = new char[o.via.str.size + 1];
            std::memcpy(value, o.via.str.ptr, o.via.str.size);
            value[o.via.str.size] = '\0';
            data = Data::Variant{std::string(value)};
            return;
        }

    case msgpack::type::BIN:
        {
            char* value = new char[o.via.str.size + 1];
            memcpy(value, o.via.str.ptr, o.via.str.size);
            value[o.via.str.size] = '\0';
            data = Data::Variant{std::string(value)};
            return;
        }

    case msgpack::type::EXT:
        throw msgpack::type_error();

    case msgpack::type::ARRAY:
        data = Data::Variant::makeList();

        for (auto i : o.via.array) {
            auto value = Data::Variant{};
            msgpack_unpack(value, i);
            data.list().push_back(value);
        }
        return;

    case msgpack::type::MAP:
        data = Data::Variant::makeMap();

        for (auto i : o.via.map) {
            char* cstr = new char[i.key.via.str.size + 1];
            std::memcpy(cstr, i.key.via.str.ptr, i.key.via.str.size);
            cstr[i.key.via.str.size] = '\0';
            std::string key{ cstr };

            auto value = Data::Variant{};
            msgpack_unpack(value, i.val);

            data.map()[key] = value;
        }
        return;

    default:
        throw msgpack::type_error();

    }
}

}
