#ifndef Navi_Raid_VariantWrapper_h
#define Navi_Raid_VariantWrapper_h

#include <msgpack.hpp>

#include "Navi/Data/Variant.h"

namespace Navi::Raid {

class VariantWrapper {
    public:
        VariantWrapper(const Data::Variant& var) : _data{ &var } {}
        void msgpack_pack(msgpack::packer<msgpack::sbuffer>& o) const;

    private:
        const Data::Variant* _data;
};

}

#endif
