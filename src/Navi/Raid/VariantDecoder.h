#ifndef Navi_Raid_VariantDecoder_h
#define Navi_Raid_VariantDecoder_h

#include <msgpack.hpp>

#include "Navi/Data/Variant.h"

namespace Navi::Raid {

class VariantDecoder {
    public:
        VariantDecoder(Data::Variant& var) : _data{ &var } {}
        void msgpack_unpack(msgpack::object const& o);
        void msgpack_unpack(Data::Variant& data, msgpack::object const& o);
        inline Data::Variant& getData() { return *_data; }

    private:
        Data::Variant* _data;
};

}

#endif
