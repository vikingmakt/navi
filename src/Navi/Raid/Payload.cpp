#include "Navi/Raid/Payload.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi::Raid {

Payload::Payload(const std::string& action) {
    if (action == "") {
        throw std::runtime_error("Action must not be empty");
    }
    _header = Data::Variant::makeMap();
    _header.map()["action"] = action;
    generateEtag();
}

Payload::Payload(const Data::Variant& header, const Data::Variant& body) {
    setHeader(header);
    setBody(body);
}

void Payload::generateEtag() {
    if (_header.find("etag")) return;

    auto etag = StringHash::generateHex(4);
    _header.map()["etag"] = etag;
}

std::string Payload::action() const {
    auto action = _header.find("action");
    return action->cast<std::string>();
}

std::string Payload::code() const {
    auto code = _header.find("code");
    return code->cast<std::string>();
}

std::string Payload::etag() const {
    auto etag = _header.find("etag");
    return etag->cast<std::string>();
}

std::vector<char> Payload::data() const {
    msgpack::sbuffer sbuf;
    msgpack::packer<msgpack::sbuffer> pk(&sbuf);
    pk.pack(VariantWrapper{mapData()});

    std::size_t size = sbuf.size();
    char* data = sbuf.release();

    std::vector<char> buf;
    buf.insert(buf.begin(), data, data + size);
    return std::move(buf);
}

Data::Variant Payload::mapData() const {
    auto data = Data::Variant::makeMap();
    data.map()["header"] = _header;
    data.map()["body"] = _body;
    return data;
}

void Payload::setAction(const std::string& action) {
    _header.map()["action"] = action;
}

void Payload::setBody(const Data::Variant& body) {
    _body = body;
}

void Payload::setHeader(const Data::Variant& header) {
    _header = header;
}

Payload Payload::fromMsg(const std::vector<char>& msg) {
    msgpack::object_handle oh = msgpack::unpack(msg.data(), msg.size());
    msgpack::object obj = oh.get();

    if (obj.type == msgpack::type::MAP) {
        Data::Variant response;
        VariantDecoder decoder{ response };
        obj.convert(decoder);

        return Payload{ response.map()["header"], response.map()["body"] };
    }

    Console::error() << "invalid bytes for payload:" << std::string{ msg.data(), msg.size() };

    auto nheader = Data::Variant::makeMap();
    nheader.map()["etag"] = "";

    auto nbody = Data::Variant::makeMap();
    return Payload{ nheader, nbody };
}

}
