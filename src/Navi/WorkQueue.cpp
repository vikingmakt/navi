#include "Navi/WorkQueue.h"
#include "Navi/Data/LinkedList.hpp"

namespace Navi {

void WorkQueue::threadLoop(typename WorkQueue::ThreadData* data) {
    auto wq = data->workQueue;

    for (;;) {
        typename WorkQueue::WorkData work;
        {
            std::unique_lock<std::mutex> lock(data->mutex);
            data->conditionVar.wait(lock, [data] { return !data->jobs.empty() || data->stop; });
            if (data->stop) return;

            work = data->jobs.front();
            data->jobs.pop();
        }
        work.func();
        if (data->stop) return;

        wq->_pendingJobs--;
        wq->_workFinished.emit(work);
    }
}

WorkQueue::WorkQueue(Application& app, std::size_t maxThreads, std::size_t maxQueueSize)
    : _app(&app), _maxThreads(maxThreads), _maxQueueSize(maxQueueSize), _nextThreadIdx(0), _pendingJobs(0) {

    _workFinished.connect([](WorkData work) {
        if (work.callback)
            work.callback();
    });
    app.addDeferredSignal(app.tick(), _workFinished);
}

WorkQueue::~WorkQueue() {
    for (auto& data : _threads) {
        std::unique_lock<std::mutex> lock(data.mutex);
        data.stop = true;
        data.conditionVar.notify_one();
    }
}

bool WorkQueue::addWorkWithCallback(typename WorkQueue::WorkFuncType func, typename WorkQueue::CallbackFuncType callback) {
    typename WorkQueue::WorkData work = { func, callback };

    std::size_t i = 0;
    for (auto& data : _threads) {
        if (i++ < _nextThreadIdx) continue;

        std::unique_lock<std::mutex> lock(data.mutex, std::defer_lock);
        lock.lock();
        if (data.jobs.size() < _maxQueueSize) {
            data.jobs.push(work);
            _pendingJobs++;

            lock.unlock();
            data.conditionVar.notify_one();
            _nextThreadIdx = i % _threads.size();
            return true;
        } else {
            lock.unlock();
        }
    }

    if (_threads.size() < _maxThreads) {
        _threads.push_back(ThreadData{});

        ThreadData& data = _threads.back();
        data.jobs.push(work);
        data.workQueue = this;
        data.thread = std::thread(threadLoop, &data);
        data.conditionVar.notify_one();

        _pendingJobs++;
        return true;
    }

    return false;
}

void WorkQueue::tick(float dt) {
}

}
