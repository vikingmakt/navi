#ifndef Navi_Console_hpp
#define Navi_Console_hpp

#include <iostream>
#include "Navi/Console.h"

namespace Navi {

template<typename Arg>
void writeCout(Arg&& arg) {
    std::cout << arg;
}

}

#endif