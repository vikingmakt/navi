#ifndef Navi_StringHash_h
#define Navi_StringHash_h

#include <Corrade/Utility/MurmurHash2.h>

namespace Navi {

class StringHash : public Corrade::Utility::MurmurHash2::Digest {
    public:
        /**
         * @brief Default constructor
         *
         * Creates zero key. Note that it is not the same as calling other
         * constructors with empty string.
         */
        constexpr /*implicit*/ StringHash() {}

        /** @brief Construct resource key directly from hashed value */
        explicit StringHash(std::size_t key): Corrade::Utility::MurmurHash2::Digest{Corrade::Utility::MurmurHash2::Digest::fromByteArray(reinterpret_cast<const char*>(&key))} {}

        /** @brief Constructor */
        StringHash(const std::string& key): Corrade::Utility::MurmurHash2::Digest(Corrade::Utility::MurmurHash2()(key)) {}

        /**
         * @brief Constructor
         * @todo constexpr
         */
        template<std::size_t size> constexpr StringHash(const char(&key)[size]): Corrade::Utility::MurmurHash2::Digest(Corrade::Utility::MurmurHash2()(key)) {}

		std::size_t integral() const { return *reinterpret_cast<const std::size_t*>(byteArray()); }

        static int randomChar();

        static const std::string generateHex(const unsigned int len);
};

template<class T>
struct Hash {
    StringHash operator()(const T& v) const {
        return v.hash();
    }
};

}

template<>
struct std::hash<Navi::StringHash> {
    std::size_t operator()(const Navi::StringHash& h) const {
        return h.integral();
    }
};

#endif
