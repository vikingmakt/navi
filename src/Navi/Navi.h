#ifndef Navi_Navi_h
#define Navi_Navi_h

#include <Corrade/Utility/Utility.h>
#include <Magnum/Tags.h>
#include <Magnum/Math/Vector2.h>
#include <Magnum/Math/Vector3.h>
#include <Magnum/Math/Vector4.h>

namespace Navi {

class Application;

typedef Magnum::Math::Vector2<float> Vector2f;
typedef Magnum::Math::Vector2<int> Vector2i;

typedef Magnum::Math::Vector3<float> Vector3f;
typedef Magnum::Math::Vector3<int> Vector3i;

typedef Magnum::Math::Vector4<float> Vector4f;
typedef Magnum::Math::Vector4<int> Vector4i;

/* Bring whole Corrade namespace */
using namespace Corrade;

using namespace Magnum;

/* Bring debugging facility from Corrade::Utility namespace */
using Corrade::Utility::Debug;
using Corrade::Utility::Warning;
using Corrade::Utility::Error;
using Corrade::Utility::Fatal;

using Magnum::NoCreateT;
using Magnum::NoCreate;

}

#endif
