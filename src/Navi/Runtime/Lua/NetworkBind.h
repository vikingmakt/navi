#ifndef Navi_Lua_NetworkBind_h
#define Navi_Lua_NetworkBind_h

#include <memory>
#include "Navi/Network/Socket.h"
#include "Navi/Network/UtosClient.h"
#include "Navi/Lua/AsyncBind.h"
#include "Navi/Lua/SignalBind.h"

namespace Navi::Lua {

class SocketBind {
    public:
        SocketBind() : _ctx{ nullptr }, _socket{ nullptr } {}
        SocketBind(const SocketBind& other) { *this = other; }
        SocketBind(SocketBind&& other) { *this = std::move(other); }
        SocketBind(Context& ctx, std::string addr, int port, Async::WorkQueue&);
        SocketBind(Context& ctx, Network::Socket& sock);

        SocketBind& operator =(const SocketBind& other) {
            _ctx = other._ctx;
            _socket = other._socket;
            _ptr = other._ptr;
            return *this;
        }

        SocketBind& operator =(SocketBind&& other) {
            std::swap(_ctx, other._ctx);
            std::swap(_ptr, other._ptr);
            _socket = std::move(other._socket);
            return *this;
        }

        Network::Socket& socket() { return *_ptr; }

        SignalBind<std::vector<char>> dataReceived() const;

        SignalBind<int> recvTimeout() const;

        SignalBind<bool> connected() const;

        void send(std::string str, int recvlen);

        void recv(int len);

    private:
        // TODO: find a better solution for ownership of the socket than std::shared_ptr
        std::shared_ptr<Network::Socket> _socket;
        Network::Socket* _ptr;
        Context* _ctx;
};

class UtosClientBind {
    public:
        UtosClientBind(): _ctx{ nullptr }, _client{ nullptr } { }
        UtosClientBind(const UtosClientBind& other) { *this = other; }
        UtosClientBind(UtosClientBind&& other) { *this = std::move(other); }
        explicit UtosClientBind(Context& ctx, std::string addr, int port, Async::WorkQueue& queue);

        UtosClientBind& operator =(const UtosClientBind& other) {
            _ctx = other._ctx;
            _client = other._client;
            return *this;
        }

        UtosClientBind& operator =(UtosClientBind&& other) {
            std::swap(_ctx, other._ctx);
            _client = std::move(other._client);
            return *this;
        }

        Network::UtosClient& client() { return *_client; }
        SignalBind<Encode::Payload, false> msgReceived() const;
        void request(const LuaRef msg, const LuaRef func);
        void send(const LuaRef msg);
        SocketBind socket() const;

    private:
        std::shared_ptr<Network::UtosClient> _client;
        Context* _ctx;
};

// Specializations of lua conversions to Encode::Payload
template<>
Encode::Payload fromLua(LuaRef lr);

template<>
LuaRef toLua(Context& ctx, Encode::Payload& v);

// Specializations of lua conversions to std::vector<char>
template<>
std::vector<char> fromLua(LuaRef lr);

template<>
LuaRef toLua(Context& ctx, std::vector<char>& v);


void registerNetworkTypes(Context& ctx);

}

#endif
