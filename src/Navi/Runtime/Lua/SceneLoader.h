#ifndef Navi_Lua_SceneLoader_h
#define Navi_Lua_SceneLoader_h

#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Resource/Scene.h"

namespace Navi::Lua {

class SceneLoader : public Resource::AbstractResourceLoader<Resource::Scene> {
    public:
        explicit SceneLoader(Context& ctx) : _ctx{ &ctx } {}

    protected:
        void doBegin(Resource::Scene& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Resource::Scene& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Resource::Scene& res, Resource::ResourceManager& mgr) override;

    private:
        Context* _ctx;
};

}

#endif
