#ifndef Navi_Lua_SignalBind_hpp
#define Navi_Lua_SignalBind_hpp

#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/SignalBind.h"

namespace Navi::Lua {

template<class T, bool Deferred>
void SignalBind<T, Deferred>::connect(LuaRef handler) {
    _self->connect([this, handler](T arg) {
        _ctx->call(Console::error(), handler, toLua(*_ctx, arg));
    });
}

template<class T, bool Deferred>
void registerSignalType(Context& ctx) {
    std::string name = "SignalBind_" + std::string(typeid(T).name());

    luabridge::getGlobalNamespace(ctx.state())
        .beginClass<SignalBind<T, Deferred>>(name.c_str())
        .template addConstructor<void(*)()>()
        .addFunction("__shr", &SignalBind<T, Deferred>::connect)
        .endClass();
}

}

#endif
