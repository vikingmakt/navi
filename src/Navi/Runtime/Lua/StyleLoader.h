#ifndef Navi_Lua_StyleLoader_h
#define Navi_Lua_StyleLoader_h

#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Resource/Style.h"

namespace Navi::Lua {

class StyleLoader : public Resource::AbstractResourceLoader<Resource::Style> {
    public:
        explicit StyleLoader(Context& ctx) : _ctx{ &ctx } {}

    protected:
        void doBegin(Resource::Style& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Resource::Style& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Resource::Style& res, Resource::ResourceManager& mgr) override;

    private:
        Context* _ctx;
};

Data::LinkedList<Ui::StyleSetting> evalStyleSettings(Ui::Style& style, Resource::ResourceManager& mgr, LuaRef luaSets);

}

#endif
