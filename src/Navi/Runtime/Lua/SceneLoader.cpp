#include "Navi/Lua/SceneLoader.h"
#include "Navi/Lua/SceneBind.h"

using namespace luabridge;

namespace Navi::Lua {

void SceneLoader::doBegin(Resource::Scene& scene, Resource::ResourceManager& mgr) {
    auto param = mgr.param(scene.id());
    auto filename = param->find("filename");
    scene.addFile(mgr.findFile(filename->cast<std::string>()));
}

void SceneLoader::doLoadAsyncPart(Resource::Scene& scene, Resource::ResourceManager& mgr) {
    scene.file(0).cache();
}

void SceneLoader::doLoadSyncPart(Resource::Scene& scene, Resource::ResourceManager& mgr) {
    if (!_ctx->execFile(scene.file(0))) {
        Ui::Scene* uiScene = new Ui::Scene();
        uiScene->sceneMounted();
        scene.setUiScene(Containers::Pointer<Ui::Scene>{ uiScene });
        return;
    }

    LuaRef tree = _ctx->popFromStack();
    LuaRef sceneMod = _ctx->global("scene");
    LuaRef evalSceneTree = sceneMod["eval_scene_tree"];
    SceneBind* sceneBind = evalSceneTree(tree);
    Ui::Scene* uiScene = sceneBind->release();
    uiScene->sceneMounted();
    scene.setUiScene(Containers::Pointer<Ui::Scene>{ uiScene });
}

}
