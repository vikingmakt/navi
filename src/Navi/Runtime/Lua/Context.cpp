#include <lua.hpp>
#include <Corrade/Utility/Debug.h>
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/ConsoleBind.h"
#include "Navi/Lua/AnimationBind.h"
#include "Navi/Lua/AsyncBind.hpp"
#include "Navi/Lua/NetworkBind.h"
#include "Navi/Lua/SceneBind.h"
#include "Navi/Lua/ScriptBind.h"
#include "Navi/Lua/SignalBind.hpp"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

using namespace luabridge;
using namespace Navi::Ui;

namespace Navi::Lua {

namespace Implementation {
    StringHash makeStringHash(const std::string& str) {
        return LuaRef{ Application::instance().runtime().context().state(), StringHash{ str } };
    }

    void registerBaseLib(Context& c) {
        registerNetworkTypes(c);
        registerAsyncNamespace(c);
        registerSceneTypes(c);
        registerConsoleNamespace(c);
        registerAnimationNamespace(c);

        getGlobalNamespace(c.state())

            .beginClass<ClickedData>("ClickedData")
                .addConstructor<void(*)()>()
            .endClass()

            .beginClass<LuaListView::ItemSceneInstantiatedData>("LuaListView_ItemSceneInstantiatedData")
                .addProperty("index", &LuaListView::ItemSceneInstantiatedData::index, false)
                .addProperty("item", &LuaListView::ItemSceneInstantiatedData::item, false)
                .addProperty("scene", (std::function<LuaRef(const LuaListView::ItemSceneInstantiatedData * data)>)[&c](const auto* data) {
                    return LuaRef{ c.state(), SceneBind{ c, *data->scene } };
                })
            .endClass()

            .beginClass<SceneBind>("SceneBind")
                .addConstructor<void(*)()>()
                .addProperty("root", &SceneBind::root)
                .addFunction("find_widget_by_id", &SceneBind::findWidgetById)
            .endClass()

            .beginClass<ScriptBind>("ScriptBind")
                .addConstructor<void(*)()>()
                .addFunction("__index", &ScriptBind::index)
            .endClass()

            .beginClass<WidgetGetter>("WidgetGetter")
                .addConstructor<void(*)()>()
                .addFunction("__index", &WidgetGetter::index)
            .endClass()

            .beginClass<Vector2f>("Vector2f")
                .addConstructor<void(*)(float, float)>()
                .addProperty<float>("x", &Vector2f::x)
                .addProperty<float>("y", &Vector2f::y)
            .endClass()

            .beginClass<StringHash>("StringHash")
                .addConstructor<void(*)(const std::string&)>()
            .addProperty("integral", &StringHash::integral)
            .endClass()

            .addFunction("hash", makeStringHash);
    }
}

Context::Context() {
    _state = luaL_newstate();
    luaL_openlibs(_state);

    Implementation::registerBaseLib(*this);
}

Context::~Context() {
    lua_close(_state);
}

LuaRef Context::makeTable() {
    return luabridge::newTable(_state);
}

bool Context::execString(const std::string& code, const std::string& name) {
    return execBuffer(code.c_str(), code.size(), name);
}

bool Context::execBuffer(const char* code, std::size_t size, const std::string& name) {
    int result = luaL_loadbuffer(_state, code, size, name.c_str()) || lua_pcall(_state, 0, 1, 0);
    if (result != 0) {
        auto exc = luabridge::LuaException(_state, result);
        Console::error() << "lua error: " << exc.what();
        return false;
    }
    return true;
}

bool Context::execFile(const std::string& filename) {
    int result = luaL_dofile(_state, filename.c_str());
    if (result != 0) {
        auto exc = luabridge::LuaException(_state, result);
        Console::error() << "lua error: " << exc.what();
        return false;
    }
    return true;
}

bool Context::execFile(Resource::AbstractFile& file) {
    auto buf = file.readBytes();
    return execBuffer(buf.data(), buf.size(), file.path());
}

LuaRef Context::global(const char* name) {
    return luabridge::getGlobal(_state, name);
}

void Context::setGlobal(const char* name, const LuaRef& v) {
    luabridge::setGlobal(_state, v, name);
}

LuaRef Context::popFromStack() {
    LuaRef result = LuaRef::fromStack(_state, -1);
    lua_pop(_state, -1);
    return result;
}

template<>
LuaRef fromLua(LuaRef lr) {
    return lr;
}

template<>
LuaRef toLua(Context& ctx, const LuaRef& v) {
    return v;
}

}
