#include "Navi/Lua/NetworkBind.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/SignalBind.hpp"
#include "Navi/Async/Future.hpp"
#include "Navi/Application.h"

namespace Navi::Lua {

namespace {
SocketBind makeSocket(std::string host, int port) {
    auto& ctx = Application::instance().runtime().context();
    auto& queue = Application::instance().backgroundWorkQueue();
    SocketBind sock{ ctx, host, port, queue };
    Application::instance().addDeferredSignal(sock.socket().dataReceived());
    Application::instance().addDeferredSignal(sock.socket().recvTimeout());
    Application::instance().addDeferredSignal(sock.socket().connected());
    return sock;
}

UtosClientBind makeUtosClient(std::string host, int port) {
    auto& ctx = Application::instance().runtime().context();
    auto& queue = Application::instance().backgroundWorkQueue();
    UtosClientBind sock{ ctx, host, port, queue };
    Application::instance().addDeferredSignal(sock.client().socket().dataReceived());
    Application::instance().addDeferredSignal(sock.client().socket().recvTimeout());
    Application::instance().addDeferredSignal(sock.client().socket().connected());
    return sock;
}
}

SocketBind::SocketBind(Context& ctx, std::string host, int port, Async::WorkQueue& queue) {
    _ctx = &ctx;
    _socket = std::make_shared<Network::Socket>(host, port, queue);
    _ptr = _socket.get();
}

SocketBind::SocketBind(Context& ctx, Network::Socket& socket) {
    _ctx = &ctx;
    _ptr = &socket;
}

SignalBind<std::vector<char>> SocketBind::dataReceived() const {
    return SignalBind<std::vector<char>>{ *_ctx, _ptr->dataReceived() };
}

SignalBind<int> SocketBind::recvTimeout() const {
    return SignalBind<int>{ *_ctx, _ptr->recvTimeout() };
}

SignalBind<bool> SocketBind::connected() const {
    return SignalBind<bool>{ *_ctx, _ptr->connected() };
}

void SocketBind::send(std::string str, int recvlen) {
    std::vector<char> data;
    data.insert(data.end(), str.data(), str.data() + str.size());
    _ptr->send(data, recvlen);
}

void SocketBind::recv(int len) {
    _ptr->recv(len);
}

UtosClientBind::UtosClientBind(Context& ctx, std::string addr, int port, Async::WorkQueue& queue) {
    _ctx = &ctx;
    _client = std::make_shared<Network::UtosClient>(addr, port, queue);
}

SignalBind<Encode::Payload, false> UtosClientBind::msgReceived() const {
    return SignalBind<Encode::Payload, false>{ *_ctx, _client->msgReceived() };
}

void UtosClientBind::request(const LuaRef msg, const LuaRef func) {
    auto payload = fromLua<Encode::Payload>(msg);
    auto cb = [this, func](const Encode::Payload& response) {
        func(toLua(*_ctx, response));
    };

    _client->request(payload, cb);
}

void UtosClientBind::send(const LuaRef msg) {
    auto payload = fromLua<Encode::Payload>(msg);

    _client->send(payload);
}

SocketBind UtosClientBind::socket() const {
    return SocketBind(*_ctx, _client->socket());
}

void registerNetworkTypes(Context& ctx) {
    registerSignalType<std::vector<char>, true>(ctx);
    registerSignalType<int, true>(ctx);
    registerSignalType<bool, true>(ctx);
    registerSignalType<Encode::Payload, false>(ctx);

    luabridge::getGlobalNamespace(ctx.state())
        .beginClass<SocketBind>("Socket")
        .template addConstructor<void(*)()>()
        .addFunction("send", &SocketBind::send)
        .addFunction("recv", &SocketBind::recv)
        .addProperty("data_received", &SocketBind::dataReceived)
        .addProperty("recv_timeout", &SocketBind::recvTimeout)
        .addProperty("connected", &SocketBind::connected)
        .endClass();

    luabridge::getGlobalNamespace(ctx.state())
        .beginNamespace("network")
        .addFunction("socket", &makeSocket)
        .addFunction("utos_client", &makeUtosClient)
        .endNamespace();

    luabridge::getGlobalNamespace(ctx.state())
        .beginClass<UtosClientBind>("UtosClient")
        .template addConstructor<void(*)()>()
        .addFunction("send", &UtosClientBind::send)
        .addFunction("request", &UtosClientBind::request)
        .addProperty("msg_received", &UtosClientBind::msgReceived)
        .addProperty("socket", &UtosClientBind::socket)
        .endClass();
}

template<>
Encode::Payload fromLua(LuaRef lr) {
    auto payload = Encode::Payload();
    payload.setHeader(luaRefToVariant(lr["header"]));
    payload.setBody(luaRefToVariant(lr["body"]));
    payload.generateEtag();

    return payload;
}

template<>
LuaRef toLua(Context& ctx, Encode::Payload& v) {
    auto table = ctx.makeTable();
    table["header"] = variantToLuaRef(ctx, v.header());
    table["body"] = variantToLuaRef(ctx, v.body());

    return table;
}

template<>
std::vector<char> fromLua(LuaRef lr) {
    std::string str = lr.cast<std::string>();
    std::vector<char> data;
    data.insert(data.end(), str.data(), str.data() + str.size());
    return data;
}

template<>
LuaRef toLua(Context& ctx, std::vector<char>& v) {
    std::string str;
    str.append(v.data(), v.size());
    return LuaRef{ ctx.state(), str };
}

}
