/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <Corrade/TestSuite/Tester.h>

#include "Navi/Ui/Widgets.h"
#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/ObservableBind.hpp"
#include "Navi/Lua/Implementation/Userdata.hpp"
#include "Navi/Lua/Implementation/Widgets.hpp"

namespace Navi::Lua::Test {
namespace {

using namespace Corrade;
using namespace luabridge;
using namespace Navi::Ui;

void registerAbstractWidgetBindClass(Context& ctx) {
    getGlobalNamespace(ctx.state()).beginClass<AbstractWidgetBind>("AbstractWidgetBind")
        .addProperty("id", &AbstractWidgetBind::getBaseProperty<AbstractWidget::Id>, &AbstractWidgetBind::setBaseProperty<AbstractWidget::Id>)
        .addProperty("visible", &AbstractWidgetBind::getBaseProperty<AbstractWidget::Visible>, &AbstractWidgetBind::setBaseProperty<AbstractWidget::Visible>)
        .addProperty("script", &AbstractWidgetBind::getScript, &AbstractWidgetBind::setScript)
        .endClass();
}

struct LuaTest: TestSuite::Tester {
    explicit LuaTest();

    void userdataCheckTest();
    void widgetPropertiesTest();
    void widgetBindingTest();
};

LuaTest::LuaTest() {
    addTests({
            &LuaTest::userdataCheckTest
    });
}

void LuaTest::userdataCheckTest() {
    struct UserdataFoo {
        std::string str;
    };

    struct UserdataBar {
        int i;
    };

    lua_State* L = luaL_newstate();
    luaL_openlibs(L);

    getGlobalNamespace(L)
        .beginClass<UserdataFoo>("UserdataFoo")
            .addConstructor<void(*)()>()
        .endClass()
        .beginClass<UserdataBar>("UserdataBar")
            .addConstructor<void(*)()>()
        .endClass();

    LuaRef foo = LuaRef{ L, UserdataFoo{} };
    LuaRef bar = LuaRef{ L, UserdataBar{} };

    CORRADE_VERIFY(Implementation::isUserdataOf<UserdataFoo>(L, foo));
    CORRADE_VERIFY(Implementation::isUserdataOf<UserdataBar>(L, bar));
    CORRADE_VERIFY(!Implementation::isUserdataOf<UserdataBar>(L, foo));
    CORRADE_VERIFY(!Implementation::isUserdataOf<UserdataFoo>(L, bar));
}

void LuaTest::widgetPropertiesTest() {
    Context ctx;

    registerAbstractWidgetBindClass(ctx);

    Implementation::registerWidgetClass<Label, Label::Text>(ctx, "Label", { "text" });

    Label label{ *Label::def() };
    WidgetBind<Label> labelRef{ ctx, label };
    label.properties().set<Label::Text>("Hello World");

    ctx.setGlobal("label", LuaRef{ ctx.state(), labelRef });
    ctx.execString("return label.text", "label_test");
    std::string result = ctx.popFromStack().cast<std::string>();

    CORRADE_VERIFY(result == label.properties().get<Label::Text>());
}

void LuaTest::widgetBindingTest() {
    Context ctx;

    registerAbstractWidgetBindClass(ctx);
    registerObservableTypes(ctx);
    Implementation::registerWidgetClass<Label, Label::Text>(ctx, "Label", { "text" });

    Label label{ *Label::def() };
    WidgetBind<Label> labelRef{ ctx, label };
    label.properties().set<Label::Text>("Hello World");

    Observable<LuaRef> obs{ LuaRef{ctx.state(), "Lua String"} };
    ObservableBind<LuaRef> obsRef{ ctx, obs };

    ctx.setGlobal("label", LuaRef{ ctx.state(), labelRef });
    ctx.setGlobal("obs", LuaRef{ ctx.state(), obsRef });

    ctx.execString("label.text = obs", "label_test");
    CORRADE_VERIFY(label.text() == "Lua String");

    obs.setValue(LuaRef{ ctx.state(), "CPP String" });
    CORRADE_VERIFY(label.text() == "CPP String");
}

}}

CORRADE_TEST_MAIN(Navi::Lua::Test::LuaTest)
