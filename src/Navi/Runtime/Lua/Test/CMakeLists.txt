if (UNIX)
    corrade_add_test(LuaTest LuaTest.cpp LIBRARIES Navi m)
endif (UNIX)

if (WIN32)
    corrade_add_test(LuaTest LuaTest.cpp LIBRARIES Navi)
endif (WIN32)

set_target_properties(LuaTest PROPERTIES FOLDER "Navi/Lua/Test")
target_compile_options(LuaTest PRIVATE -w -std=c++14)

target_link_options(LuaTest PRIVATE -Wl,--no-as-needed)
