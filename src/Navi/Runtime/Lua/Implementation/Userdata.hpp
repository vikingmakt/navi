#ifndef Navi_Lua_Implementation_Userdata_hpp
#define Navi_Lua_Implementation_Userdata_hpp

#include "Navi/Lua/Lua.h"

namespace Navi::Lua::Implementation {

template<class T>
bool isUserdataOf(lua_State* L, LuaRef val) {
    if (!val.isUserdata()) return false;

    val.push(); // Stack: val
    lua_getmetatable(L, -1); // Stack: val, mt
    lua_rawgetp(L, LUA_REGISTRYINDEX, luabridge::ClassInfo<T>::getClassKey()); // Stack, val, mt, checkct

    if (!lua_rawequal(L, -1, -2)) {
        lua_pop(L, 3);
        return false;
    }

    lua_pop(L, 3);
    return true;
}

}

#endif
