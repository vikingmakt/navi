#ifndef Navi_Lua_Implementation_Widgets_hpp
#define Navi_Lua_Implementation_Widgets_hpp

#include "Navi/Lua/Lua.h"
#include "Navi/Lua/SceneBind.h"
#include "Navi/Lua/SignalBind.hpp"

namespace Navi::Lua::Implementation {

template<class Widget, std::size_t Prop, class PropType>
struct AddWidgetProperty {
    void operator()(Context& ctx, luabridge::Namespace::Class<WidgetBind<Widget>>& cls, const char* name) {
        cls.addProperty(name, &WidgetBind<Widget>::template getProperty<Prop>, &WidgetBind<Widget>::template setProperty<Prop>);
    };
};

template<class Widget, std::size_t Prop, class PropType>
struct AddWidgetProperty<Widget, Prop, Signal<PropType>> {
    void operator()(Context& ctx, luabridge::Namespace::Class<WidgetBind<Widget>>& cls, const char* name) {
        cls.addProperty(name, &WidgetBind<Widget>::template getSignalProperty<Prop, PropType>);
    };
};

template<class Widget, std::size_t Prop, class PropType>
struct RegisterPropertyType {
    void operator()(Context& ctx) {}
};

template<class Widget, std::size_t Prop, class PropType>
struct RegisterPropertyType<Widget, Prop, Signal<PropType>> {
    void operator()(Context& ctx) {
        registerSignalType<PropType, true>(ctx);
    }
};

template<class Widget, std::size_t Prop>
void addWidgetProperty(Context& ctx, luabridge::Namespace::Class<WidgetBind<Widget>>& cls, const char* name) {
    using PropType = typename Data::TupleElement<Prop, typename Widget::PropertiesType::TupleType>::Type;
    AddWidgetProperty<Widget, Prop, PropType>{}(ctx, cls, name);
}

template<class Widget, std::size_t Prop>
void addWidgetProperties(Context& ctx, luabridge::Namespace::Class<WidgetBind<Widget>>& cls, const char* const* nameptr) {
    addWidgetProperty<Widget, Prop>(ctx, cls, *nameptr);
}
template<class Widget, std::size_t Prop, std::size_t Next, std::size_t... Props>
void addWidgetProperties(Context& ctx, luabridge::Namespace::Class<WidgetBind<Widget>>& cls, const char* const* nameptr) {
    addWidgetProperty<Widget, Prop>(ctx, cls, *nameptr);
    addWidgetProperties<Widget, Next, Props...>(ctx, cls, nameptr + 1);
}

template<class Widget, std::size_t Prop>
void registerPropertyType(Context& ctx) {
    using PropType = typename Data::TupleElement<Prop, typename Widget::PropertiesType::TupleType>::Type;
    RegisterPropertyType<Widget, Prop, PropType>{}(ctx);
}

template<class Widget, std::size_t Prop>
void registerPropertiesTypes(Context& ctx) {
    registerPropertyType<Widget, Prop>(ctx);
}
template<class Widget, std::size_t Prop, std::size_t Next, std::size_t... Props>
void registerPropertiesTypes(Context& ctx) {
    registerPropertyType<Widget, Prop>(ctx);
    registerPropertiesTypes<Widget, Next, Props...>(ctx);
}

template<class Widget, std::size_t... Props>
void registerWidgetClass(Context& ctx, const char* className, std::initializer_list<const char*> props) {
    luabridge::Namespace::Class<WidgetBind<Widget>> cls = luabridge::getGlobalNamespace(ctx.state()).deriveClass<WidgetBind<Widget>, AbstractWidgetBind>(className);
    auto it = props.begin();
    addWidgetProperties<Widget, Props...>(ctx, cls, it);
    cls.endClass();

    registerPropertiesTypes<Widget, Props...>(ctx);
}

}

#endif // Navi_Lua_Implementation_Widgets_h
