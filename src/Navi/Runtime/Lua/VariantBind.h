#ifndef Navi_Lua_VariantBind_h
#define Navi_Lua_VariantBind_h

#include "Navi/Lua/Lua.h"
#include "Navi/Data/Variant.h"
#include "Navi/Navi.h"

namespace Navi::Lua {

LuaRef variantToLuaRef(Context& ctx, const Data::Variant& v);

Data::Variant luaRefToVariant(const LuaRef& v);

}

#endif
