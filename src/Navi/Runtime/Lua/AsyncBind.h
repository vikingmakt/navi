#ifndef Navi_Lua_AsyncBind_h
#define Navi_Lua_AsyncBind_h

#include "Navi/Lua/Lua.h"
#include "Navi/Async/Future.h"

namespace Navi::Lua {

void registerAsyncNamespace(Context& ctx);

}

#endif
