#ifndef Navi_Lua_Lua_h
#define Navi_Lua_Lua_h

#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>

namespace Navi::Lua {

using luabridge::LuaRef;

class Runtime;
class Context;
class SceneLoader;
class StyleLoader;
class ScriptLoader;

}

#endif
