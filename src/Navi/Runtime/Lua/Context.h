#ifndef Navi_Lua_Context_h
#define Navi_Lua_Context_h

#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Navi.h"
#include "Navi/Lua/Lua.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Resource/AbstractFile.h"

namespace Navi::Lua {

class Context : public Interconnect::Receiver {
    public:
        Context();
        ~Context();

        /// Creates an empty table
        LuaRef makeTable();

        /// Execute a string of lua code, accepts filename for better error reporting
        bool execString(const std::string& code, const std::string& filename);

        /// Execute a buffer of lua code, accepts filename for better error reporting
        bool execBuffer(const char* code, std::size_t size, const std::string& filename);

        /// Execute a lua file using lua's IO
        bool execFile(const std::string& filename);

        /// Execute a lua file using navi's virtual IO
        bool execFile(Resource::AbstractFile& file);

        /// Gets and sets global variables
        LuaRef global(const char* name);
        void setGlobal(const char* name, const LuaRef& v);

        /// Pops a value from the lua stack and returns it
        LuaRef popFromStack();

        /// Calls a lua function in protected mode and outputs any error to @param str, returns
        /// a pair with the actual return value of the call and whether or not an error ocurred
        template<class S, class... Args>
        std::pair<LuaRef, bool> call(S&& str, const LuaRef& fn, Args&&...);

        /// Lua VM state
        lua_State* state() const { return _state; }

    private:
        lua_State* _state;
};

template<class S, class... Args>
std::pair<LuaRef, bool> Context::call(S&& str, const LuaRef& fn, Args&&... args) {
    try {
        LuaRef result = fn(std::forward<Args>(args)...);
        return std::make_pair(result, true);
    }
    catch (luabridge::LuaException& err) {
        str << "lua error: " << err.what();
        return std::make_pair(LuaRef{ _state }, false);
    }
}

template<class T>
T fromLua(LuaRef lr) {
	return lr.cast<T>();
}

template<>
LuaRef fromLua(LuaRef lr);

template<class T>
LuaRef toLua(Context& ctx, T&& v) {
	return LuaRef{ ctx.state(), T{v} };
}

template<>
LuaRef toLua(Context& ctx, const LuaRef& v);

}

#endif
