#include "Navi/Lua/ScriptBind.h"
#include "Navi/Lua/SceneBind.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Data/LinkedList.hpp"

using namespace luabridge;

namespace Navi::Lua {

LuaRef WidgetGetter::index(const std::string& k) const {
    return widgetToWidgetBind(*_ctx, *_scene->findWidgetById(k));
}

void ScriptBind::setup() {
    // Sets the scene of the script
    (*_scriptTable)["scene"] = LuaRef{ _ctx->state(), SceneBind{ *_ctx, _self->scene() } };

    (*_scriptTable)["widget"] = widgetToWidgetBind(*_ctx, _self->widget());

    (*_scriptTable)["w"] = LuaRef{ _ctx->state(), WidgetGetter{ *_ctx, _self->scene() } };

    // Call the setup function
    _ctx->call(Console::error(), (*_scriptTable)["setup"], LuaRef{ _ctx->state(), this });
}

void ScriptBind::setScriptTable(const LuaRef& t) {
    _scriptTable = Containers::Pointer<LuaRef>{ new LuaRef{_ctx->state(), t} };
}

LuaRef ScriptBind::index(const std::string& k) const {
    return (*_scriptTable)[k];
}

}
