#ifndef Navi_Lua_Runtime_h
#define Navi_Lua_Runtime_h

#include "Navi/Signal.h"
#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Resource/Resource.h"

namespace Navi::Lua {

class Runtime : public Interconnect::Receiver {
    public:
        explicit Runtime(Resource::ResourceManager& mgr);

        Context& context() { return _ctx; }

        void init();

        Containers::Pointer<SceneLoader> sceneLoader();
        Containers::Pointer<StyleLoader> styleLoader();
        Containers::Pointer<ScriptLoader> scriptLoader();

        void runNaviFile(Resource::AbstractFile& file);
        void runResourcesFile(Resource::AbstractFile& file);

        void handleResourceDirectoryAdded(std::string path);
        void handleResourcePakAdded(Resource::PakReader* pak);

    private:
        Context _ctx;
        Resource::ResourceManager* _resMgr;
};

}

#endif
