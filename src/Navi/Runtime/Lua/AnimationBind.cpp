#include "Navi/Lua/AnimationBind.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Lua/Implementation/Userdata.hpp"
#include "Navi/Application.h"

namespace Navi::Lua {

namespace {
static std::unordered_map<std::string, Animation::Ease> animationEasing = {
    {"step", Animation::Ease::Step},
    {"linear", Animation::Ease::Linear},
};
}

Animation::AnimationId createAnimationFromTable(LuaRef animation) {
    Animation::AnimationManager& anim = Application::instance().animationManager();
    auto animid = anim.createAnimation();
    LuaRef keys = animation["keys"];
    int keyindex = 0;
    Data::Variant initialValue;
    for (const auto& pair : luabridge::pairs(keys)) {
        float key = pair.first.cast<float>();

        Data::Variant value = luaRefToVariant(pair.second);
        anim.setKeyFrame(animid, key, value);
        if (keyindex == 0)
            initialValue = value;
        keyindex++;
    }
    if (!animation["loop"].isNil()) {
        anim.setLoop(animid, animation["loop"]);
    }
    if (!animation["ease"].isNil()) {
        anim.setEase(animid, animationEasing[animation["ease"]]);
    }
    if (!animation["time"].isNil()) {
        anim.setTotalTime(animid, animation["time"]);
    }
    if (!animation["autoplay"].isNil()) {
        anim.play(animid);
    }
    return animid;
}

AnimationBind animationNew(LuaRef animation) {
    return AnimationBind{ createAnimationFromTable(animation) };
}

void registerAnimationNamespace(Context& ctx) {
    luabridge::getGlobalNamespace(ctx.state())
        .addFunction("_animation_new", animationNew)
        .beginClass<AnimationBind>("Animation")
        .addConstructor<void(*)()>()
        .endClass();
}

}