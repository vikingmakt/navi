#ifndef Navi_Lua_SignalBind_h
#define Navi_Lua_SignalBind_h

#include <Corrade/Containers/Pointer.h>
#include "Navi/Lua/Context.h"
#include "Navi/Lua/ScriptBind.h"
#include "Navi/Signal.h"
#include "Navi/Navi.h"

namespace Navi::Lua {

/// Lua representation of a signal emitter
template<class Arg, bool Deferred = true>
class SignalBind {
public:
    SignalBind()
        : _ctx{ nullptr }, _self{ nullptr } {}
    SignalBind(const SignalBind& other)
        : _ctx{ other._ctx }, _self{ other._self } {}
    explicit SignalBind(Context& ctx, Signal<Arg, Deferred>& self)
        : _ctx{ &ctx }, _self{ &self } {}

    void connect(LuaRef handler);

private:
    Context* _ctx;
    Signal<Arg, Deferred>* _self;
};

template<class T, bool Deferred>
void registerSignalType(Context& ctx);

}

#endif
