#include "Navi/Lua/Script.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Application.h"

namespace Navi::Lua {

LuaRef Script::returnValue() {
    return *_returnValue;
}

void Script::setReturnValue(const LuaRef& val) {
    _returnValue = Containers::Pointer<LuaRef>{ new LuaRef{ val } };
}

void ScriptLoader::doBegin(Script& scr, Resource::ResourceManager& mgr) {
    auto param = mgr.param(scr.id());
    auto filename = param->find("filename");
    scr.addFile(mgr.findFile(*filename));
}

void ScriptLoader::doLoadAsyncPart(Script& scr, Resource::ResourceManager& mgr) {
    scr.file(0).cache();
}

void ScriptLoader::doLoadSyncPart(Script& scr, Resource::ResourceManager& mgr) {
    _ctx->execFile(scr.file(0));
    scr.setReturnValue(_ctx->popFromStack());
}

void ScriptComponent::doWidgetMounted(Ui::Scene& scene) {
    auto& ctx = Application::instance().runtime().context();
    if (_propValue.isString()) {
        auto scriptPathOrId = _propValue.cast<std::string>();
        auto& mgr = Application::instance().resourceManager();
        _script = mgr.loadSyncWithFile<Script>(scriptPathOrId, scriptPathOrId);
        _bind = Containers::Pointer<ScriptBind>{ new ScriptBind{ ctx, *this, _script.resource().returnValue() } };
        _script.resource().finishedLoading().connect(*this, &ScriptComponent::scriptReloaded);
    } else if (_propValue.isTable()) {
        _bind = Containers::Pointer<ScriptBind>{ new ScriptBind{ ctx, *this, _propValue } };
    }

    _bind->setup();
}

void ScriptComponent::scriptReloaded(std::string id) {
    _bind->setScriptTable(_script.resource().returnValue());
    _bind->setup();
}

}
