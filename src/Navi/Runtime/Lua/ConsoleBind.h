#ifndef Navi_Lua_ConsoleBind_h
#define Navi_Lua_ConsoleBind_h

#include "Navi/Lua/Lua.h"
#include "Navi/Console.h"

namespace Navi::Lua {

void registerConsoleNamespace(Context& ctx);

}

#endif
