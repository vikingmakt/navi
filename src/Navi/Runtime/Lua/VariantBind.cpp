#include "Navi/Data/LinkedList.hpp"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Implementation/Userdata.hpp"

namespace Navi::Lua {

LuaRef variantToLuaRef(Context& ctx, const Data::Variant& v) {
    switch (v.type()) {
    case Data::VariantType::Nil:
        return LuaRef{ ctx.state() };
    case Data::VariantType::Bool:
        return LuaRef{ ctx.state(), v.cast<bool>() };
    case Data::VariantType::Int:
        return LuaRef{ ctx.state(), v.cast<int>() };
    case Data::VariantType::Float:
        return LuaRef{ ctx.state(), v.cast<float>() };
    case Data::VariantType::Float2:
        return LuaRef{ ctx.state(), v.cast<Vector2f>() };
    case Data::VariantType::String:
        return LuaRef{ ctx.state(), v.cast<std::string>() };
    case Data::VariantType::List: {
        LuaRef table = ctx.makeTable();
        for (const auto& item : v.list()) {
            table.append(variantToLuaRef(ctx, item));
        }
        return table;
    }
    case Data::VariantType::Pairs: {
        LuaRef table = ctx.makeTable();
        for (const auto& pair : v.pairs()) {
            table[variantToLuaRef(ctx, pair.get<0>())] = variantToLuaRef(ctx, pair.get<1>());
        }
        return table;
    }
    }
}

Data::Variant luaRefToVariant(const LuaRef& lr) {
    switch (lr.type()) {
    case LUA_TNIL:
        return {};
    case LUA_TBOOLEAN:
        return { lr.cast<bool>() };
    case LUA_TNUMBER:
        return { lr.cast<float>() };
    case LUA_TSTRING:
        return { lr.cast<std::string>() };
    case LUA_TTABLE: {
        if (!lr[1].isNil()) {
            auto v = Data::Variant::makeList();
            for (const auto& pair : luabridge::pairs(lr)) {
                v.list().push_back(luaRefToVariant(pair.second));
            }
            return v;
        } else {
            auto v = Data::Variant::makePairs();
            for (const auto& pair : luabridge::pairs(lr)) {
                v.pairs().push_back(Data::makeTuple(luaRefToVariant(pair.first), luaRefToVariant(pair.second)));
            }
            return v;
        }
    }
    case LUA_TUSERDATA: {
        if (Implementation::isUserdataOf<Vector2f>(lr.state(), lr)) {
            return { lr.cast<Vector2f>() };
        }
        break;
    }
    }
}

}
