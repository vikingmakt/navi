#ifndef Navi_Lua_AnimatedPropertyBind_h
#define Navi_Lua_AnimatedPropertyBind_h

#include "Navi/Animation/Animation.h"
#include "Navi/Lua/Lua.h"

namespace Navi::Lua {

class AnimationBind {
    public:
        AnimationBind()
            : _animId(Animation::AnimationIdInvalid) {}

        AnimationBind(Animation::AnimationId animId)
            : _animId(animId) {}

        AnimationBind(const AnimationBind& other) { *this = other; }

        AnimationBind& operator =(const AnimationBind& other) {
            _animId = other._animId;
            return *this;
        }

        Animation::AnimationId animationId() { return _animId; }

    private:
        Animation::AnimationId _animId;
};

Animation::AnimationId createAnimationFromTable(LuaRef animation);

void registerAnimationNamespace(Context& ctx);

}

#endif