#include "Navi/Lua/StyleLoader.h"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Resource/Font.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Application.h"
#include "Navi/Animation/Animation.h"
#include "Navi/Lua/AnimationBind.h"

using namespace luabridge;

namespace Navi::Lua {

namespace {
static std::unordered_map<std::string, Ui::StyleVar> styleVars = {
    {"alpha", Ui::StyleVar::Alpha},
    {"font", Ui::StyleVar::Font},
    {"text_color", Ui::StyleVar::TextColor},
    {"window_padding", Ui::StyleVar::WindowPadding},
    {"window_rounding", Ui::StyleVar::WindowRounding},
    {"window_border_size", Ui::StyleVar::WindowBorderSize},
    {"window_min_size", Ui::StyleVar::WindowMinSize},
    {"window_title_align", Ui::StyleVar::WindowTitleAlign},
};

std::pair<Data::Variant, Animation::AnimationId> evalStyleValue(Ui::Style& style, Resource::ResourceManager& mgr, Ui::StyleVar var, LuaRef lval) {
    // Check if it's an animation
    if (lval.isTable() && !lval["animation"].isNil()) {
        auto animid = createAnimationFromTable(lval["animation"]);
        return std::make_pair(Application::instance().animationManager().animationValue(animid), animid);
    }

    // Check var type
    if (var == Ui::StyleVar::Font) {
        Data::Variant fontPath = luaRefToVariant(lval);
        std::string id = fontPath.cast<std::string>();
        style.addFont(id, mgr.loadAsyncWithFile<Resource::Font>(id, id));
        return std::make_pair(Data::Variant{ id }, Animation::AnimationIdInvalid);
    }
    if (Ui::isFloat(var)) {
        return std::make_pair(Data::Variant{ lval.cast<float>() }, Animation::AnimationIdInvalid);
    }
    if (Ui::isVec2(var)) {
        return std::make_pair(luaRefToVariant(lval), Animation::AnimationIdInvalid);
    }
    if (Ui::isColor(var)) {
        std::string hex = lval.cast<std::string>();
        unsigned int x = std::stoul(hex, nullptr, 16);
        return std::make_pair(Data::Variant{ Vector4f{
            float((x >> 24) & 0xFF) / 255.0f,
            float((x >> 16) & 0xFF) / 255.0f,
            float((x >> 8) & 0xFF) / 255.0f,
            float(x & 0xFF) / 255.0f
        } }, Animation::AnimationIdInvalid);
    }
    return std::make_pair(Data::Variant{}, Animation::AnimationIdInvalid);
}

Ui::Style* evalStyleTree(Resource::ResourceManager& mgr, LuaRef tree) {
    Ui::Style* style = new Ui::Style{};

    for (const auto& pair : luabridge::pairs(tree)) {
        std::string key = pair.first.cast<std::string>();
        Data::LinkedList<Ui::StyleSetting> opts = evalStyleSettings(*style, mgr, pair.second);
        style->addSettings(key, std::move(opts));
    }

    return style;
}
}

Data::LinkedList<Ui::StyleSetting> evalStyleSettings(Ui::Style& style, Resource::ResourceManager& mgr, LuaRef luaSets) {
    Data::LinkedList<Ui::StyleSetting> sets;
    for (const auto& optkv : luabridge::pairs(luaSets)) {
        // TODO: validate style var exists
        std::string varName = optkv.first.cast<std::string>();
        Ui::StyleVar var = styleVars[varName];
        Data::Variant value;
        Animation::AnimationId animId;
        std::tie(value, animId) = evalStyleValue(style, mgr, var, optkv.second);
        sets.push_back({ var, value, animId, &style });
    }
    return sets;
}

void StyleLoader::doBegin(Resource::Style& style, Resource::ResourceManager& mgr) {
    auto param = mgr.param(style.id());
    auto filename = param->find("filename");
    style.addFile(mgr.findFile(filename->cast<std::string>()));
}

void StyleLoader::doLoadAsyncPart(Resource::Style& style, Resource::ResourceManager& mgr) {
    style.file(0).cache();
}

void StyleLoader::doLoadSyncPart(Resource::Style& style, Resource::ResourceManager& mgr) {
    if (!_ctx->execFile(style.file(0))) {
        style.setUiStyle(Containers::Pointer<Ui::Style>{ new Ui::Style{} });
        return;
    }

    if (style.hasUiStyle()) {
        for (const auto& pair : style.uiStyle().settings()) {
            for (const auto& set : pair.second) {
                if (set.animationId != Animation::AnimationIdInvalid)
                    Application::instance().animationManager().destroyAnimation(set.animationId);
            }
        }
    }

    LuaRef tree = _ctx->popFromStack();
    Ui::Style* uiStyle = evalStyleTree(mgr, tree);
    style.setUiStyle(Containers::Pointer<Ui::Style>{ uiStyle });
}

}
