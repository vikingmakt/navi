#include <functional>
#include "Navi/Lua/SceneBind.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/StyleLoader.h"
#include "Navi/Lua/Implementation/Widgets.hpp"
#include "Navi/Ui/Widgets.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

using namespace Navi::Ui;
using namespace luabridge;

namespace Navi::Lua {

LuaRef widgetToWidgetBind(Context& ctx, AbstractWidget& w) {
    switch (w.type()) {
    case WidgetType::Window:
        return LuaRef{ ctx.state(), WidgetBind<Window>{ ctx, *static_cast<Window*>(&w) } };

    case WidgetType::Label:
        return LuaRef{ ctx.state(), WidgetBind<Label>{ ctx, *static_cast<Label*>(&w) } };

    case WidgetType::Button:
        return LuaRef{ ctx.state(), WidgetBind<Button>{ ctx, *static_cast<Button*>(&w) } };

    case WidgetType::CollapsingHeader:
        return LuaRef{ ctx.state(), WidgetBind<CollapsingHeader>{ ctx, *static_cast<CollapsingHeader*>(&w) } };

    case WidgetType::Combo:
        return LuaRef{ ctx.state(), WidgetBind<Combo>{ ctx, *static_cast<Combo*>(&w) } };

    case WidgetType::MenuBar:
        return LuaRef{ ctx.state(), WidgetBind<MenuBar>{ ctx, *static_cast<MenuBar*>(&w) } };

    case WidgetType::Menu:
        return LuaRef{ ctx.state(), WidgetBind<Menu>{ ctx, *static_cast<Menu*>(&w) } };

    case WidgetType::MenuItem:
        return LuaRef{ ctx.state(), WidgetBind<MenuItem>{ ctx, *static_cast<MenuItem*>(&w) } };

    case WidgetType::HorizontalLayout:
        return LuaRef{ ctx.state(), WidgetBind<HorizontalLayout>{ ctx, *static_cast<HorizontalLayout*>(&w) } };

    case WidgetType::VerticalLayout:
        return LuaRef{ ctx.state(), WidgetBind<VerticalLayout>{ ctx, *static_cast<VerticalLayout*>(&w) } };

    case WidgetType::SliderFloat:
        return LuaRef{ ctx.state(), WidgetBind<SliderFloat>{ ctx, *static_cast<SliderFloat*>(&w) } };

    case WidgetType::Separator:
        return LuaRef{ ctx.state(), WidgetBind<Separator>{ ctx, *static_cast<Separator*>(&w) } };

    case WidgetType::StyleLoader:
        return LuaRef{ ctx.state(), WidgetBind<Ui::StyleLoader>{ ctx, *static_cast<Ui::StyleLoader*>(&w) } };

    case WidgetType::TreeView:
        return LuaRef{ ctx.state(), WidgetBind<TreeView>{ ctx,* static_cast<TreeView*>(&w) } };

#if LUA_LIST_VIEW
    case WidgetType::ListView:
        return LuaRef{ ctx.state(), WidgetBind<LuaListView>{ ctx,* static_cast<LuaListView*>(&w) } };
#endif

    case WidgetType::SceneInstance:
        return LuaRef{ ctx.state(), WidgetBind<SceneInstance>{ ctx,* static_cast<SceneInstance*>(&w) } };

    case WidgetType::SceneFactory:
        return LuaRef{ ctx.state(), WidgetBind<SceneFactory>{ ctx,* static_cast<SceneFactory*>(&w) } };

    case WidgetType::Image:
        return LuaRef{ ctx.state(), WidgetBind<Ui::Image>{ ctx,* static_cast<Ui::Image*>(&w) } };

    case WidgetType::InputText:
        return LuaRef{ ctx.state(), WidgetBind<InputText>{ ctx,* static_cast<InputText*>(&w) } };

    case WidgetType::Widget:
        return LuaRef{ ctx.state(), WidgetBind<Widget>{ ctx,* static_cast<Widget*>(&w) } };

    default:
        return LuaRef{ ctx.state() };
    }
}

#if LUA_LIST_VIEW
WidgetDef* LuaListViewImpl::def() {
    static WidgetDef def;
    def.makeWidgetFunc = [](WidgetManager& mgr, WidgetDef&) {
        return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<LuaListView>(*LuaListView::pool()));
    };
    return &def;
}

Memory::PoolAllocator* LuaListViewImpl::pool() {
    static Memory::PoolAllocator pool{ sizeof(LuaListView) };
    return &pool;
}
#endif


void AbstractWidgetBind::setStylePatch(const LuaRef& lr) {
    auto comp = _self->component<Ui::StylePatchComponent>();
    if (!comp) {
        auto& component = _self->addComponent<Ui::StylePatchComponent>();
        setBaseProperty<AbstractWidget::StylePatch>(lr, [&component](const LuaRef& val) {
            return evalStyleSettings(component.style(), Application::instance().resourceManager(), val);
        });
    }
    else {
        auto& component = *(*comp);
        setBaseProperty<AbstractWidget::StylePatch>(lr, [&component](const LuaRef& val) {
            return evalStyleSettings(component.style(), Application::instance().resourceManager(), val);
        });
    }
}

LuaRef AbstractWidgetBind::makeChild(StringHash* type) {
    return widgetToWidgetBind(*_ctx, _self->makeChild(*type));
}


SceneBind::SceneBind() {
    _ctx = &Application::instance().runtime().context();
    _self = new Ui::Scene{};
    _owns = true;
}

LuaRef SceneBind::root() const {
    return LuaRef{ _ctx->state(), AbstractWidgetBind{ *_ctx, _self->rootWidget() } };
}

LuaRef SceneBind::findWidgetById(const std::string& id) const {
    auto widget = _self->findWidgetById(id);
    if (!widget) {
        return LuaRef{ _ctx->state() };
    }
    return widgetToWidgetBind(*_ctx, *widget);
}

Scene* SceneBind::release() {
    _owns = false;
    return _self;
}

void registerSceneTypes(Context& ctx) {

    widget_set_prop(widget, )

    getGlobalNamespace(ctx.state())
        .beginClass<WidgetBind>("Widget")
        .addProperty("")
        .endClass();

#if 0
    getGlobalNamespace(ctx.state())
        .beginClass<AbstractWidgetBind>("AbstractWidgetBind")
        .addProperty("id", &AbstractWidgetBind::getBaseProperty<AbstractWidget::Id>, &AbstractWidgetBind::setBaseProperty<AbstractWidget::Id>)
        .addProperty("visible", &AbstractWidgetBind::getBaseProperty<AbstractWidget::Visible>, &AbstractWidgetBind::setBaseProperty<AbstractWidget::Visible>)
        .addProperty("styles", &AbstractWidgetBind::getBaseProperty<AbstractWidget::Styles>, &AbstractWidgetBind::setBaseProperty<AbstractWidget::Styles>)
        .addProperty("style_patch", &AbstractWidgetBind::getStylePatch, &AbstractWidgetBind::setStylePatch)
        .addProperty("script", &AbstractWidgetBind::getScript, &AbstractWidgetBind::setScript)
        .addProperty("props", &AbstractWidgetBind::getProps)
        .addFunction("make_child", &AbstractWidgetBind::makeChild)
        .endClass();

    Implementation::registerWidgetClass<Window,
        Window::Position, Window::Size, Window::Title, Window::Border, Window::NoTitleBar,
        Window::NoMenuBar, Window::NoBackground, Window::NoBringToFrontOnFocus, Window::NoClose,
        Window::NoCollapse, Window::NoMove, Window::NoNav, Window::NoResize, Window::NoScrollBar
    >(ctx, "Window", {
        "position", "size", "title", "border", "no_title_bar",
        "no_menu_bar", "no_background", "no_bring_to_front_on_focus", "no_close",
        "no_collapse", "no_move", "no_nav", "no_resize", "no_scroll_bar"
    });

    Implementation::registerWidgetClass<Label,
        Label::Text
    >(ctx, "Label", {
        "text",
    });

    Implementation::registerWidgetClass<Button,
        Button::Text, Button::Clicked
    >(ctx, "Button", {
        "text", "clicked"
    });

    Implementation::registerWidgetClass<CollapsingHeader,
        CollapsingHeader::Label, CollapsingHeader::Open
    >(ctx, "CollapsingHeader", { "label", "open" });

    Implementation::registerWidgetClass<Combo,
        Combo::Items, Combo::Value
    >(ctx, "Combo", { "items", "value" });

    Implementation::registerWidgetClass<MenuBar,
        MenuBar::ForceMain
    >(ctx, "MenuBar", {
        "force_main"
    });

    Implementation::registerWidgetClass<Menu,
        Menu::Title
    >(ctx, "Menu", {
        "title"
    });

    Implementation::registerWidgetClass<MenuItem,
        MenuItem::Title, MenuItem::Shortcut, MenuItem::Clicked
    >(ctx, "MenuItem", {
        "title", "shortcut", "clicked"
    });

    Implementation::registerWidgetClass<HorizontalLayout,
        HorizontalLayout::Padding
    >(ctx, "HorizontalLayout", {
        "padding"
    });

    Implementation::registerWidgetClass<VerticalLayout,
        VerticalLayout::Padding
    >(ctx, "VerticalLayout", { "padding" });

    Implementation::registerWidgetClass<SliderFloat,
        SliderFloat::Values, SliderFloat::Min, SliderFloat::Max, SliderFloat::Format
    >(ctx, "SliderFloat", { "values", "min", "max", "format" });

    Implementation::registerWidgetClass<Separator,
        Separator::Dummy
    >(ctx, "Separator", { "dummy" });

	Implementation::registerWidgetClass<TreeView,
		TreeView::Items, TreeView::ContextMenu, TreeView::Selectable, TreeView::Selection
	>(ctx, "TreeView", { "items", "context_menu", "selectable", "selection" });

    Implementation::registerWidgetClass<SceneInstance,
        SceneInstance::Scene, SceneInstance::Instantiated
    >(ctx, "SceneInstance", { "scene", "instantiated" });

    Implementation::registerWidgetClass<SceneFactory,
        SceneFactory::Scene, SceneFactory::Instantiated
    >(ctx, "SceneFactory", { "scene", "instantiated" });

    Implementation::registerWidgetClass<Ui::StyleLoader,
        Ui::StyleLoader::Src
    >(ctx, "StyleLoader", { "src" });

    Implementation::registerWidgetClass<Ui::Image,
        Ui::Image::Src
    >(ctx, "Image", { "src" });

	Implementation::registerWidgetClass<InputText,
		InputText::Value
	>(ctx, "InputText", { "value" });

    Implementation::registerWidgetClass<Widget,
        Widget::Dummy
    >(ctx, "Widget", { "dummy" });

#if LUA_LIST_VIEW
    // Register the lua ListView
    Application::instance().widgetManager().registerWidgetDef("LuaListView", LuaListViewImpl::def());
    Implementation::registerWidgetClass<LuaListView,
        LuaListView::Items, LuaListView::ItemSceneFactory, LuaListView::ItemSceneInstantiated
    >(ctx, "ListView", { "items", "item_scene_factory", "item_scene_instantiated" });
#endif

#endif
}

}
