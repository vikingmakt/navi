#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Lua/SceneLoader.h"
#include "Navi/Lua/StyleLoader.h"
#include "Navi/Lua/Script.h"
#include "Navi/Resource/AbstractFile.h"
#include "Navi/Resource/PakReader.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Console.hpp"

namespace Navi::Lua {

namespace {
void runInitFiles(Runtime& r, Resource::ResourceManager& mgr) {
    auto naviFile = mgr.findFile("res://navi.lua");
    auto resourcesFile = mgr.findFile("res://res.lua");
    if (naviFile) {
        r.runNaviFile(*naviFile);
    }
    if (resourcesFile) {
        r.runResourcesFile(*resourcesFile);
    }
}
}

Runtime::Runtime(Resource::ResourceManager& mgr) : _resMgr{ &mgr } {
}

void Runtime::init() {
    _resMgr->setLoader<Resource::Scene>(sceneLoader());
    _resMgr->setLoader<Resource::Style>(styleLoader());
    _resMgr->setLoader<Script>(scriptLoader());
    _resMgr->resourceDirectoryAdded().connect(*this, &Runtime::handleResourceDirectoryAdded);
    _resMgr->resourcePakAdded().connect(*this, &Runtime::handleResourcePakAdded);
    _resMgr->addDirectoryOrPak("vm1");

    auto baseFile = _resMgr->findFile("res://lua/base.lua");
    _ctx.execFile(*baseFile);
}

Containers::Pointer<SceneLoader> Runtime::sceneLoader() {
    return Containers::pointer<SceneLoader>(_ctx);
}

Containers::Pointer<StyleLoader> Runtime::styleLoader() {
    return Containers::pointer<StyleLoader>(_ctx);
}

Containers::Pointer<ScriptLoader> Runtime::scriptLoader() {
    return Containers::pointer<ScriptLoader>(_ctx);
}

void Runtime::runNaviFile(Resource::AbstractFile& file) {
    _ctx.execFile(file);
    LuaRef table = _ctx.popFromStack();
    for (const auto& pair : luabridge::pairs(table)) {
        Console::setVar(pair.first.cast<std::string>(), luaRefToVariant(pair.second));
    }
}

void Runtime::runResourcesFile(Resource::AbstractFile& file) {
    _ctx.execFile(file);
    LuaRef table = _ctx.popFromStack();
    for (const auto& pair : luabridge::pairs(table)) {
        std::string id = pair.first;
        Data::Variant param = luaRefToVariant(pair.second);
        _resMgr->setParam(id, param);
    }
}

void Runtime::handleResourceDirectoryAdded(std::string path) {
    runInitFiles(*this, *_resMgr);
}

void Runtime::handleResourcePakAdded(Resource::PakReader* pak) {
    runInitFiles(*this, *_resMgr);
}

}