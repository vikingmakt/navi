#ifndef Navi_Lua_SceneBind_h
#define Navi_Lua_SceneBind_h

#include "Navi/Data/LinkedList.h"
#include "Navi/Data/Tree.h"
#include "Navi/Lua/Lua.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/ScriptBind.h"
#include "Navi/Lua/SignalBind.h"
#include "Navi/Lua/Script.h"
#include "Navi/Lua/AnimationBind.h"
#include "Navi/Lua/Implementation/Userdata.hpp"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Widgets.hpp"
#include "Navi/Signal.h"

namespace Navi::Lua {

#define LUA_LIST_VIEW 1

#if LUA_LIST_VIEW
class LuaListViewImpl {
    public:
        static Memory::PoolAllocator* pool();
        static Ui::WidgetDef* def();
};

typedef Ui::ListView<LuaRef, LuaListViewImpl> LuaListView;
#endif

namespace Implementation {
template<class Widget, std::size_t Prop, class PropType>
struct PropertySetter {
    static void set(Context& ctx, Widget& w, LuaRef v, std::function<PropType(LuaRef)> trans) {
        w.properties().template set<Prop>(trans(v));
    }
};

template<class Widget, std::size_t Prop, class PropType>
struct PropertyGetter {
    static LuaRef get(Context& ctx, Widget& w) {
        return toLua(ctx, w.properties().template get<Prop>());
    }
};

template<class PropType>
struct ActualProperty {
    using Type = PropType;
};

/// Returns the property of a widget
template<class Widget, std::size_t Prop>
LuaRef getProperty(Context& ctx, Widget& w) {
    using PropType = typename Data::TupleElement<Prop, typename Widget::PropertiesType::TupleType>::Type;
    return PropertyGetter<Widget, Prop, PropType>::get(ctx, w);
}

/// Returns the signal property of a widget
template<class Widget, std::size_t Prop, class... Args>
LuaRef getSignalProperty(Context& ctx, Widget& w) {
    return LuaRef{ ctx.state(), SignalBind<Args...>{ ctx, w.properties().template get<Prop>() } };
}

/// Sets the property of a widget
template<class Widget, std::size_t Prop, class Trans>
void setProperty(Context& ctx, Widget& w, LuaRef v, Trans&& trans) {
    using PropType = typename Data::TupleElement<Prop, typename Widget::PropertiesType::TupleType>::Type;
    PropertySetter<Widget, Prop, PropType>::set(ctx, w, v, trans);
}

template<class Widget, std::size_t Prop>
void setProperty(Context& ctx, Widget& w, LuaRef v) {
    using PropType = typename Data::TupleElement<Prop, typename Widget::PropertiesType::TupleType>::Type;
    PropertySetter<Widget, Prop, PropType>::set(ctx, w, v, (std::function<typename ActualProperty<PropType>::Type(LuaRef)>)[](LuaRef lr) -> typename ActualProperty<PropType>::Type {
        return fromLua<typename ActualProperty<PropType>::Type>(lr);
    });
}

}

class AbstractWidgetBind {
    public:
        AbstractWidgetBind()
            : _ctx{ nullptr }, _self{ nullptr } {}
        AbstractWidgetBind(const AbstractWidgetBind& other)
            : _ctx{ other._ctx }, _self{ other._self } {}
        explicit AbstractWidgetBind(Context& ctx, Ui::AbstractWidget& self)
            : _ctx{ &ctx }, _self{ &self } {}

        /// Generic function to get some property of a widget
        template<std::size_t Prop>
        LuaRef getBaseProperty() const {
            return Implementation::getProperty<Ui::AbstractWidget, Prop>(*_ctx, *_self);
        }

        /// Generic function to set some property of a widget
        template<std::size_t Prop>
        void setBaseProperty(const LuaRef& v) {
            Implementation::setProperty<Ui::AbstractWidget, Prop>(*_ctx, *_self, v);
        }

        template<std::size_t Prop, class Trans>
        void setBaseProperty(const LuaRef& v, Trans&& trans) {
            Implementation::setProperty<Ui::AbstractWidget, Prop>(*_ctx, *_self, v, trans);
        }

        /// Returns the lua script associated with this widget
        LuaRef getScript() const {
            return LuaRef{ _ctx->state() };
        }

        /// Loads and sets the lua script associated with this widget
        void setScript(const LuaRef& lr) {
            _self->addComponent<ScriptComponent>(lr);
        }

        /// Returns the style patch associated with this widget
        LuaRef getStylePatch() const {
            return LuaRef{ _ctx->state() };
        }

        /// Sets the style patch associated with this widget
        void setStylePatch(const LuaRef& lr);

        /// Returns the script's props
        LuaRef getProps() const {
            auto script = _self->component<ScriptComponent>();
            if (script) {
                return (*script)->bind().index("props");
            }

            // Returns nil if widget doesn't have script
            return LuaRef{ _ctx->state() };
        }

        LuaRef makeChild(StringHash* type);

    protected:
        Context* _ctx;
        Ui::AbstractWidget* _self;
};

/// Lua representation of a widget
template<class Widget>
class WidgetBind : public AbstractWidgetBind {
    public:
        WidgetBind() : AbstractWidgetBind{} {}
        WidgetBind(const WidgetBind& other) : AbstractWidgetBind{ *static_cast<const AbstractWidgetBind*>(&other) } {}
        explicit WidgetBind(Context& ctx, Widget& self) : AbstractWidgetBind{ ctx, *static_cast<Ui::AbstractWidget*>(&self) } {}

        /// Generic function to get some property of a node
        template<std::size_t Prop>
        LuaRef getProperty() const {
            return Implementation::getProperty<Widget, Prop>(*_ctx, *static_cast<Widget*>(_self));
        }

        /// Generic function to get some signal emitter of a node
        template<std::size_t Prop, class... Args>
        LuaRef getSignalProperty() const {
            return Implementation::getSignalProperty<Widget, Prop, Args...>(*_ctx, *static_cast<Widget*>(_self));
        }

        /// Generic function to set some property of a node
        template<std::size_t Prop>
        void setProperty(const LuaRef& v) {
            if (Implementation::isUserdataOf<AnimationBind>(_ctx->state(), v)) {
                auto animid = v.cast<AnimationBind*>()->animationId();
                static_cast<Widget*>(_self)->properties().template set<Prop>(AnimatedProperty{ animid });
                return;
            }
            Implementation::setProperty<Widget, Prop>(*_ctx, *static_cast<Widget*>(_self), v);
        }
};

/// Lua representation of a Scene
class SceneBind {
    public:
        /// This constructor should only be called from lua when creating a new scene
        SceneBind();

        /// Copy is allowed but we don't own the scene
        SceneBind(const SceneBind& other)
            : _ctx{ other._ctx }, _self{ other._self }, _owns{ false } {}

        /// Construct a ref from an existing scene, not owning it
        explicit SceneBind(Context& ctx, Ui::Scene& self)
            : _ctx{ &ctx }, _self{ &self }, _owns{ false } {}

        LuaRef root() const;

        LuaRef findWidgetById(const std::string& id) const;

        Ui::Scene* release();

    private:
        Context* _ctx;

        /// If _owns is false this may become dangling at some point
        Ui::Scene* _self;

        /// Whether this ref owns the scene or not
        bool _owns;
};

/// Specialization for AbstractWidget styles
template<>
std::vector<std::string> fromLua(LuaRef lr);
template<>
LuaRef toLua(Context& ctx, const std::vector<std::string>&);

/// Specialization for LuaListView items
template<>
Data::FixedArray<LuaRef> fromLua(LuaRef lr);

/// Specialization for TreeView tree
template<>
Ui::TreeView::TreeType fromLua(LuaRef lr);
template<>
LuaRef toLua(Context& ctx, const Ui::TreeView::TreeType& tree);

/// Specialization for SceneInstance scenes
template<>
LuaRef toLua(Context& ctx, Ui::Scene& scene);

/// Specialization for SliderFloat values
template<>
std::vector<float> fromLua(LuaRef lr);
template<>
LuaRef toLua(Context& ctx, std::vector<float>&);

/// Specialization for TreeView selection
template<>
LuaRef toLua(Context& ctx, const std::unordered_set<StringHash>&);

/// Specialization for Combo items
template<>
std::vector<std::pair<Data::Variant, std::string>> fromLua(LuaRef lr);
template<>
LuaRef toLua(Context& ctx, const std::vector<std::pair<Data::Variant, std::string>>&);

/// Specialization for Variant
template<>
Data::Variant fromLua(LuaRef lr);
template<>
LuaRef toLua(Context& ctx, Data::Variant& v);


LuaRef widgetToWidgetBind(Context& ctx, Ui::AbstractWidget& w);

void registerSceneTypes(Context& ctx);

}

#endif
