#ifndef Navi_Lua_ScriptBind_h
#define Navi_Lua_ScriptBind_h

#include <Corrade/Interconnect/Receiver.h>
#include <Corrade/Containers/Pointer.h>
#include "Navi/Data/LinkedList.h"
#include "Navi/Data/Variant.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Navi.h"
#include "Navi/Console.hpp"

namespace Navi::Lua {

class ScriptBind;
class ScriptComponent;

class WidgetGetter {
    public:
        WidgetGetter() : _ctx{ nullptr }, _scene{ nullptr } {}
        WidgetGetter(Context& ctx, Ui::Scene& scene) : _ctx{ &ctx }, _scene{ &scene } {}

        LuaRef index(const std::string& k) const;

    private:
        Context* _ctx;
        Ui::Scene* _scene;
};

class ScriptBind {
    public:
        ScriptBind()
            : _ctx{ nullptr }, _scriptTable{ nullptr } {}
        ScriptBind(const ScriptBind& other)
            : _ctx{ other._ctx }, _self{ other._self }, _scriptTable{ new LuaRef{*other._scriptTable} } {}
        ScriptBind(Context& ctx, ScriptComponent& self, const LuaRef& st)
            : _ctx{ &ctx }, _self{ &self }, _scriptTable{ new LuaRef{st} } {}

        /// Calls the lua script function "setup"
        void setup();

        void setScriptTable(const LuaRef& t);

        LuaRef index(const std::string& k) const;

    private:
        Context* _ctx;
        ScriptComponent* _self;
        Containers::Pointer<LuaRef> _scriptTable;
};

}

#endif
