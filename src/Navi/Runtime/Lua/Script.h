#ifndef Navi_Lua_Script_h
#define Navi_Lua_Script_h

#include <Corrade/Containers/EnumSet.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Ui/Widgets.h"
#include "Navi/Lua/Lua.h"
#include "Navi/Lua/ScriptBind.h"
#include "Navi/Memory/ArenaAllocator.h"
#include "Navi/Resource/AbstractResource.h"

namespace Navi::Lua {

class Script : public Resource::AbstractResource {
    public:
        static StringHash resourceTypeHash() { return "Script"; }

        explicit Script(const std::string& id) : AbstractResource{ id } {}

        LuaRef returnValue();

        void setReturnValue(const LuaRef& val);

    protected:
        virtual StringHash doTypeHash() { return resourceTypeHash(); }

    private:
        Containers::Pointer<LuaRef> _returnValue;
};

class ScriptLoader : public Resource::AbstractResourceLoader<Script> {
    public:
        explicit ScriptLoader(Context& ctx) : _ctx{ &ctx } {}

    protected:
        void doBegin(Script& scr, Resource::ResourceManager& mgr) override;

        void doLoadAsyncPart(Script& scr, Resource::ResourceManager& mgr) override;

        void doLoadSyncPart(Script& scr, Resource::ResourceManager& mgr) override;

    private:
        Context* _ctx;
};

class ScriptComponent : public Ui::AbstractComponent, public Interconnect::Receiver {
    public:
        static StringHash componentTypeHash() { return "ScriptComponent"; }

        explicit ScriptComponent(Ui::AbstractWidget& widget, LuaRef propValue)
            : Ui::AbstractComponent{ widget }, _propValue{ propValue } {}

        void doWidgetMounted(Ui::Scene& scene) override;

        ScriptBind& bind() { return *_bind; }

        Ui::Scene& scene() const { return _widget->scene(); }

        void scriptReloaded(std::string);

    protected:
        LuaRef _propValue;
        Resource::ResourceHandle<Script> _script;
        Containers::Pointer<ScriptBind> _bind;
};

}

#endif
