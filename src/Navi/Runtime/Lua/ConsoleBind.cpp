#include "Navi/Lua/ConsoleBind.h"
#include "Navi/Lua/Context.h"
#include "Navi/Lua/Runtime.h"
#include "Navi/Lua/VariantBind.h"
#include "Navi/Lua/Implementation/Userdata.hpp"
#include "Navi/Console.hpp"
#include "Navi/Application.h"

namespace Navi::Lua {

namespace {

void consoleSetVar(const std::string& name, const LuaRef& value) {
    Console::setVar(name, luaRefToVariant(value));
}

LuaRef consoleVar(const std::string& name) {
    auto& ctx = Application::instance().runtime().context();
    auto value = Console::var(name);
    if (value)
        return variantToLuaRef(ctx, *value);
    return LuaRef{ ctx.state() };
}

void consoleLogDebug(LuaRef args) {
    auto logger = Console::debug();
    for (const auto& pair : luabridge::pairs(args)) {
        if (pair.first.isNumber()) {
            logger << pair.second.tostring();
        }
    }
}

void consoleLogInfo(LuaRef args) {
    auto logger = Console::info();
    for (const auto& pair : luabridge::pairs(args)) {
        if (pair.first.isNumber()) {
            logger << pair.second.tostring();
        }
    }
}

void consoleLogError(LuaRef args) {
    auto logger = Console::error();
    for (const auto& pair : luabridge::pairs(args)) {
        if (pair.first.isNumber()) {
            logger << pair.second.tostring();
        }
    }
}

void consoleExec(std::string cmd) {
    Console::exec(cmd);
}
}

void registerConsoleNamespace(Context& ctx) {
    luabridge::getGlobalNamespace(ctx.state())
        .addFunction("_console_set_var", consoleSetVar)
        .addFunction("_console_var", consoleVar)
        .addFunction("_console_debug", consoleLogDebug)
        .addFunction("_console_info", consoleLogInfo)
        .addFunction("_console_error", consoleLogError)
        .addFunction("_console_exec", consoleExec);
}

}
