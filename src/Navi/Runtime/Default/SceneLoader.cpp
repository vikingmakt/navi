#include <sstream>
#include <tinyxml2.h>
#include "Navi/Runtime/Default/SceneLoader.h"
#include "Navi/Runtime/Default/StyleLoader.h"
#include "Navi/Runtime/Default/Runtime.h"
#include "Navi/Application.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Console.hpp"

using namespace tinyxml2;

namespace Navi::Runtime::Default {

namespace {
Memory::PoolPointer<Ui::Widget> parseWidgetElement(XMLElement* elem, Ui::Widget* parent, Ui::SceneInstance& scene) {
    auto props = Data::Variant::makeMap();
    auto name = std::string{ elem->Name() };
    auto attr = elem->FirstAttribute();
    while (attr) {
        std::string key{ attr->Name() };
        Data::Variant value = parseVariantAttribute(attr);
        props.map()[key] = value;
        attr = attr->Next();
    }

    auto widget = scene.app().widgetManager().makeWidget(name, props, parent, scene);
    auto child = elem->FirstChildElement();
    while (child) {
        widget->_children.push_back(parseWidgetElement(child, widget.get(), scene));
        child = child->NextSiblingElement();
    }

    return std::move(widget);
}
}

void SceneLoader::doBegin(Ui::Scene& scene, Resource::ResourceManager& mgr) {
    auto param = mgr.param(scene.id());
    auto filename = param->find("filename");
    scene.addFile(mgr.findFile(filename->cast<std::string>()));
}

void SceneLoader::doLoadAsyncPart(Ui::Scene& scene, Resource::ResourceManager& mgr) {
    scene.file(0).cache();
}

void SceneLoader::doLoadSyncPart(Ui::Scene& scene, Resource::ResourceManager& mgr) {
    //scene.setApp(_rt->app());
    auto bytes = scene.file(0).readBytes();

    auto doc = Containers::pointer<XMLDocument>();
    doc->Parse((const char*)bytes.data(), bytes.size());

    scene._instanceFactory = Containers::pointer<XmlSceneInstanceFactory>(std::move(doc), _rt->app());
}

void XmlSceneInstanceFactory::createSceneInstance(Ui::SceneInstance& scene) {
    scene.setApp(*_app);
    auto& mgr = scene.app().resourceManager();

    auto rootProps = Data::Variant::makeMap();
    scene.setRootWidget(scene.app().widgetManager().makeWidget("Base", rootProps, nullptr, scene));

    auto sceneElement = _doc->FirstChildElement("scene");
    if (!sceneElement) {
        Console::error() << "invalid scene file:" << scene._scene.resource().file(0).path();
        return;
    }

    {
        auto varsElement = sceneElement->FirstChildElement("vars");
        if (varsElement) {
            auto var = varsElement->FirstChildElement("var");
            while (var) {
                std::string str(var->FindAttribute("name")->Value());
                Data::Variant value = parseVariantAttribute(var->FindAttribute("value"));
                scene.addVar(str, value);
                var = var->NextSiblingElement();
            }
        }
    }

    {
        auto signalsElement = sceneElement->FirstChildElement("signals");
        if (signalsElement) {
            auto signal = signalsElement->FirstChildElement("signal");
            while (signal) {
                std::string str(signal->FindAttribute("name")->Value());
                scene.signal(str);
                signal = signal->NextSiblingElement();
            }
        }
    }

    {
        auto stylesElement = sceneElement->FirstChildElement("styles");
        if (stylesElement) {
            auto styleElement = stylesElement->FirstChildElement("style");
            while (styleElement) {
                if (styleElement->FindAttribute("href")) {
                    std::string path = styleElement->FindAttribute("href")->Value();
                    scene.styles().push_back(mgr.loadAsyncWithFile<Ui::Style>(path, path));
                }
                else {
                    auto style = new Ui::Style{ scene._scene.resource().id() + " style" };
                    parseStyleElement(styleElement, *style, mgr);
                    scene.setInlineStyle(Containers::Pointer<Ui::Style>{ style });
                }
                styleElement = styleElement->NextSiblingElement("style");
            }
        }
    }

    {
        auto widgetsElement = sceneElement->FirstChildElement("widgets");
        if (widgetsElement) {
            auto widgetElem = widgetsElement->FirstChildElement();
            while (widgetElem) {
                auto widget = parseWidgetElement(widgetElem, &scene.rootWidget(), scene);
                scene.rootWidget()._children.push_back(std::move(widget));
                widgetElem = widgetElem->NextSiblingElement();
            }
        }
    }

    // Instantiate controller after scene is loaded
    auto controllerAttr = sceneElement->FindAttribute("controller");
    if (controllerAttr) {
        std::string name = controllerAttr->Value();
        auto ctrl = _app->makeController(name, scene);
        if (!ctrl) {
            Console::error() << "Controller not found:" << name;
            return;
        }

        scene.setController(std::move(ctrl));
    }
}

}
