#ifndef Navi_Runtime_Default_Default_h
#define Navi_Runtime_Default_Default_h

namespace Navi::Runtime::Default {

class Runtime;
class SceneLoader;
class StyleLoader;

}

#endif
