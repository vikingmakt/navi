#ifndef Navi_Runtime_Default_Runtime_h
#define Navi_Runtime_Default_Runtime_h

#include "Navi/Signal.h"
#include "Navi/Data/Variant.h"
#include "Navi/Resource/Resource.h"

namespace tinyxml2 { class XMLAttribute; }

namespace Navi {
class Application; 
}

namespace Navi::Runtime::Default {

class SceneLoader;
class StyleLoader;

class Runtime : public Interconnect::Receiver {
    public:
        explicit Runtime(Application& app);

        Application& app() { return *_app; }

        void init();

        Containers::Pointer<SceneLoader> sceneLoader();
        Containers::Pointer<StyleLoader> styleLoader();

        void runAppFile(Resource::AbstractFile& file);
        void runResourcesFile(Resource::AbstractFile& file);

        void handleResourceDirectoryAdded(std::pair<std::string, bool> path);
        void handleResourcePakAdded(std::pair<Resource::PakFile*, bool> pak);

    private:
        Application* _app;
};

Data::Variant parseVector2(const std::string& str);

Data::Variant parseVariantAttribute(const tinyxml2::XMLAttribute* attr);

}

#endif
