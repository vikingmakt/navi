#ifndef Navi_Runtime_Default_SceneLoader_h
#define Navi_Runtime_Default_SceneLoader_h

#include "Navi/Ui/Scene.h"
#include "Navi/Resource/ResourceManager.h"

namespace tinyxml2 {
    class XMLDocument;
}

namespace Navi::Runtime::Default {

class Runtime;

class SceneLoader : public Resource::AbstractResourceLoader<Ui::Scene> {
    public:
        explicit SceneLoader(Runtime& rt) : _rt{ &rt } {}

    protected:
        void doBegin(Ui::Scene& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Ui::Scene& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Ui::Scene& res, Resource::ResourceManager& mgr) override;

    private:
        Runtime* _rt;
};

class XmlSceneInstanceFactory : public Ui::SceneInstanceFactory {
    public:
        explicit XmlSceneInstanceFactory(Containers::Pointer<tinyxml2::XMLDocument>&& doc, Application& app)
            : _doc{ std::move(doc) }, _app{ &app } {}

        void createSceneInstance(Ui::SceneInstance& inst) override;

        Containers::Pointer<tinyxml2::XMLDocument> _doc;
        Application* _app;
};

}

#endif
