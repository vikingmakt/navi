#include <sstream>
#include <tinyxml2.h>
#include "Navi/Runtime/Default/StyleLoader.h"
#include "Navi/Runtime/Default/Runtime.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Console.hpp"

using namespace tinyxml2;

namespace Navi::Runtime::Default {
namespace {
static std::unordered_map<std::string, Ui::StyleVar> styleVars = {
    {"alpha", Ui::StyleVar::Alpha},
    {"font", Ui::StyleVar::Font},
    {"textColor", Ui::StyleVar::TextColor},
    {"windowPadding", Ui::StyleVar::WindowPadding},
    {"windowRounding", Ui::StyleVar::WindowRounding},
    {"windowBorderSize", Ui::StyleVar::WindowBorderSize},
    {"windowMinSize", Ui::StyleVar::WindowMinSize},
    {"windowTitleAlign", Ui::StyleVar::WindowTitleAlign},
};

static std::unordered_map<std::string, Animation::Ease> animationEasing = {
    {"step", Animation::Ease::Step},
    {"linear", Animation::Ease::Linear},
};

void parseStyleSettings(XMLElement* setsElem, Data::LinkedList<Ui::StyleSetting>& sets, Ui::Style& style, Resource::ResourceManager& mgr) {
    auto elem = setsElem->FirstChildElement("var");
    while (elem) {
        Ui::StyleSetting set;
        std::string name = elem->FindAttribute("name")->Value();
        auto value = parseVariantAttribute(elem->FindAttribute("value"));
        set.var = styleVars[name];
        set.value = value;
        if (set.var == Ui::StyleVar::Font && set.value.type() == Data::VariantType::String) {
            auto& id = set.value.cast<std::string>();
            style.addFont(id, mgr.loadAsyncWithFile<Ui::Font>(id, id));
        }
        sets.push_back(set);

        elem = elem->NextSiblingElement();
    }
}

void parseAnimation(XMLElement* animElem, Animation::Animation& anim, Ui::Style& style) {
    auto timeAttr = animElem->FindAttribute("time");
    if (timeAttr) {
        anim.totalTime = timeAttr->FloatValue();
    }

    auto easeAttr = animElem->FindAttribute("ease");
    if (easeAttr) {
        anim.ease = animationEasing[std::string{easeAttr->Value()}];
    }

    auto loopAttr = animElem->FindAttribute("loop");
    if (loopAttr) {
        anim.loop = loopAttr->BoolValue();
    }

    auto autoAttr = animElem->FindAttribute("autoplay");
    if (autoAttr) {
        anim.autoplay = autoAttr->BoolValue();
    }

    auto frameElem = animElem->FirstChildElement("frame");
    while (frameElem) {
        Animation::KeyFrame frame;
        auto keyAttr = frameElem->FindAttribute("key");
        if (!keyAttr) {
            Console::error() << "animation frame missing key attribute";
        }
        else {
            frame.time = keyAttr->FloatValue();
            frame.value = parseVariantAttribute(frameElem->FindAttribute("value"));
            anim.frames.push_back(frame);
        }

        frameElem = frameElem->NextSiblingElement("frame");
    }
}
}

void StyleLoader::doBegin(Ui::Style& style, Resource::ResourceManager& mgr) {
    auto param = mgr.param(style.id());
    auto filename = param->find("filename");
    style.addFile(mgr.findFile(filename->cast<std::string>()));
}

void StyleLoader::doLoadAsyncPart(Ui::Style& style, Resource::ResourceManager& mgr) {
    style.file(0).cache();
}

void StyleLoader::doLoadSyncPart(Ui::Style& style, Resource::ResourceManager& mgr) {
    auto bytes = style.file(0).readBytes();

    XMLDocument doc;
    doc.Parse((const char*)bytes.data(), bytes.size());

    auto styleElement = doc.FirstChildElement("style");
    if (!styleElement) {
        Console::error() << "invalid style file:" << style.file(0).path();
        return;
    }

    parseStyleElement(styleElement, style, mgr);
}

void parseStyleElement(XMLElement* styleElem, Ui::Style& style, Resource::ResourceManager& mgr) {
    auto elem = styleElem->FirstChildElement();
    while (elem) {
        if (std::strcmp(elem->Name(), "settings") == 0) {
            Data::LinkedList<Ui::StyleSetting> settings;
            parseStyleSettings(elem, settings, style, mgr);
            style.addSettings(StringHash{ elem->FindAttribute("name")->Value() }, std::move(settings));
        }
        else if (std::strcmp(elem->Name(), "animation") == 0) {
            Animation::Animation animData;
            parseAnimation(elem, animData, style);
            style.addAnimation(StringHash{ elem->FindAttribute("name")->Value() }, std::move(animData));
        }

        elem = elem->NextSiblingElement();
    }
}

}
