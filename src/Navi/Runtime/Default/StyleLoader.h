#ifndef Navi_Runtime_Default_StyleLoader_h
#define Navi_Runtime_Default_StyleLoader_h

#include "Navi/Ui/Style.h"
#include "Navi/Resource/ResourceManager.h"

namespace tinyxml2 { class XMLElement; }

namespace Navi::Runtime::Default {

class Runtime;

class StyleLoader : public Resource::AbstractResourceLoader<Ui::Style> {
    public:
        explicit StyleLoader(Runtime& rt) : _rt{ &rt } {}

    protected:
        void doBegin(Ui::Style& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Ui::Style& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Ui::Style& res, Resource::ResourceManager& mgr) override;

    private:
        Runtime* _rt;
};

void parseStyleElement(tinyxml2::XMLElement* elem, Ui::Style& style, Resource::ResourceManager& mgr);

}

#endif
