#include <tinyxml2.h>
#include "Navi/Runtime/Default/Runtime.h"
#include "Navi/Runtime/Default/SceneLoader.h"
#include "Navi/Runtime/Default/StyleLoader.h"
#include "Navi/Resource/AbstractFile.h"
#include "Navi/Resource/PakFile.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

using namespace tinyxml2;

namespace Navi::Runtime::Default {

namespace {
void runInitFiles(Runtime& r, Resource::ResourceManager& mgr) {
    auto appFile = mgr.findFile("res://app.cfg");
    if (appFile) {
        r.runAppFile(*appFile);
    }
}
}

Runtime::Runtime(Application& mgr) : _app{ &mgr } {
}

void Runtime::init() {
    _app->resourceManager().setLoader<Ui::Scene>(sceneLoader());
    _app->resourceManager().setLoader<Ui::Style>(styleLoader());
    _app->resourceManager().resourceDirectoryAdded().connect(*this, &Runtime::handleResourceDirectoryAdded);
    _app->resourceManager().resourcePakAdded().connect(*this, &Runtime::handleResourcePakAdded);
}

Containers::Pointer<SceneLoader> Runtime::sceneLoader() {
    return Containers::pointer<SceneLoader>(*this);
}

Containers::Pointer<StyleLoader> Runtime::styleLoader() {
    return Containers::pointer<StyleLoader>(*this);
}

void Runtime::runAppFile(Resource::AbstractFile& file) {
    auto stream = file.istream();
    std::string line;
    while (std::getline(*stream, line)) {
        if (line.empty()) continue;
        if (line[0] == '#') continue;
        
        Console::exec(line, false);
    }
}

void Runtime::runResourcesFile(Resource::AbstractFile& file) {

}

void Runtime::handleResourceDirectoryAdded(std::pair<std::string, bool> data) {
    if (data.second) {
        runInitFiles(*this, _app->resourceManager());
    }
}

void Runtime::handleResourcePakAdded(std::pair<Resource::PakFile*, bool> data) {
    if (data.second) {
        runInitFiles(*this, _app->resourceManager());
    }
}

Data::Variant parseVector2(const std::string& str) {
    std::istringstream stream(str);
    char c;
    float x, y;

    stream >> c;
    stream >> x >> y;
    stream >> c;

    return Data::Variant{ Vector2f(x, y) };
}

Data::Variant parseVariantAttribute(const XMLAttribute* attr) {
    if (std::strncmp(attr->Value(), "0x", 2) == 0) {
        std::string hex = attr->Value();
        unsigned int x = std::stoul(hex, nullptr, 16);
        return Data::Variant{ Vector4f{
            float((x >> 24) & 0xFF) / 255.0f,
            float((x >> 16) & 0xFF) / 255.0f,
            float((x >> 8) & 0xFF) / 255.0f,
            float(x & 0xFF) / 255.0f
        } };
    }

    {
        float value;
        if (attr->QueryFloatValue(&value) == XML_SUCCESS) {
            return Data::Variant{ value };
        }
    }
    {
        int value;
        if (attr->QueryIntValue(&value) == XML_SUCCESS) {
            return Data::Variant{ value };
        }
    }
    {
        bool value;
        if (attr->QueryBoolValue(&value) == XML_SUCCESS) {
            return Data::Variant{ value };
        }
    }

    const char* strvalue = attr->Value();
    if (strvalue[0] == '[') {
        return parseVector2(std::string{ strvalue });
    }
    return Data::Variant{ std::string{ strvalue } };
}

}