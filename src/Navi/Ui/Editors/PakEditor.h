#ifndef Navi_Ui_Editors_PakEditor_h
#define Navi_Ui_Editors_PakEditor_h

#include "Navi/Ui/Widget.h"

namespace Navi { class Application; }

namespace Navi::Ui { class SceneInstance; }

namespace Navi::Ui::Editors {

void registerPakEditorControllers(Application& app);

void openPakEditor(const std::string& pakFile, SceneInstance& parent);

}

#endif