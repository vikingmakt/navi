#include <Corrade/Utility/Directory.h>
#include "Navi/Ui/Editors/PakEditor.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Controller.h"
#include "Navi/Ui/FileBrowser.h"
#include "Navi/Resource/PakFile.h"
#include "Navi/Application.h"

namespace Navi::Ui::Editors {

using namespace Utility;

namespace {

struct PakFileItem {
    std::string name;
    std::string path;
    bool selected;
    int index;
};

class PakFileItemController : public Ui::Controller {
    public:
        struct Vars {
            Ui::Var<std::string> name;
            Ui::Var<std::string> path;
            Ui::Var<bool>        selected;
            Ui::Var<int>         index;
        };
        
        static StringHash controllerName() {
            return "PakFileItemController";
        }

        explicit PakFileItemController(Ui::SceneInstance& scene)
            : Ui::Controller{ scene } {

            bindVar("fileName", _vars.name);
            bindVar("filePath", _vars.path);
            bindVar("fileSelected", _vars.selected);
            bindVar("fileIndex", _vars.index);

            connect("fileChanged", *this, &PakFileItemController::handleFileChanged);
        }

        void setFileItem(const PakFileItem& item) {
            _vars.name = item.name;
            _vars.path = item.path;
            _vars.selected = item.selected;
            _vars.index = item.index;

            _fileChanged.disconnectAll();
        }

        void handleFileChanged(int) {
            _fileChanged.emit({ *_vars.name, *_vars.path, *_vars.selected, *_vars.index });
        }

        Signal<PakFileItem> _fileChanged;
        Vars _vars;
};

class PakEditorController : public Ui::Controller {
    public:
        struct Vars {
            Ui::Var<std::string>                path;
            Ui::Var<std::vector<PakFileItem>>   files;
            Ui::Var<int>                        filesKey;
            Ui::Var<bool>                       dirty;
        };

        static StringHash controllerName() {
            return "PakEditorController";
        }

        explicit PakEditorController(Ui::SceneInstance& scene)
            : Ui::Controller{ scene } {
            
            bindVar("path", _vars.path);
            bindVar("files", _vars.files);
            bindVar("filesKey", _vars.filesKey);
            bindVar("dirty", _vars.dirty);

            connect("bindFileListItem", *this, &PakEditorController::bindFileListItem);
            connect("newFile", *this, &PakEditorController::newFile);
            connect("addFile", *this, &PakEditorController::addFile);
            connect("saveFile", *this, &PakEditorController::saveFile);
        }

        void setPakFile(const std::string& pakFile) {
            auto& mgr = _scene->app().resourceManager();
            if (pakFile.empty())
                _pak = Containers::pointer<Resource::PakFile>();
            else
                _pak = Containers::pointer<Resource::PakFile>(mgr.findFile(pakFile));

            _vars.path = _pak->path();
            rebuildFileList();
        }

        void rebuildFileList() {
            _vars.filesKey = *_vars.filesKey + 1;
            _vars.files->clear();

            for (std::size_t i = 0; i < _pak->files().size(); i++) {
                auto& entry = _pak->files()[i];

                PakFileItem f;
                f.name = std::string{ entry.name };
                f.path = _pak->realFiles()[i]->path();
                f.selected = fileSelected(f.name);
                f.index = i;
                _vars.files->push_back(f);
            }
        }

        void bindFileListItem(ListView::Item* item) {
            auto& ctrl = item->scene->controller<PakFileItemController>();
            ctrl.setFileItem(_vars.files->at(item->index));
            ctrl._fileChanged.connect(*this, &PakEditorController::fileChanged);
        }

        bool fileSelected(const std::string& name) {
            auto it = _filesSelected.find(name);
            if (it == _filesSelected.end()) {
                return _filesSelected[name] = false;
            }
            return it->second;
        }

        void newFile(int) {
            Ui::FileBrowser::selectNewFile("New File", "", *_scene).connect([this](Containers::Optional<std::string> filename) {
                if (filename) {
                    _pak->setPath(*filename);

                    _vars.path = _pak->path();
                    _vars.dirty = true;
                }
            });
        }

        void addFile(int) {
            Ui::FileBrowser::selectFile("Add File", "", *_scene).connect([this](Containers::Optional<std::string> filename) {
                if (filename) {
                    auto fn = Directory::fromNativeSeparators(*filename);
                    _pak->addFile(fn, _scene->app().resourceManager().findFile("fs://" + fn));

                    _vars.dirty = true;
                    rebuildFileList();
                }
            });
        }

        void saveFile(int) {
            Console::ioQueue().addWorkWithCallback([this]() {
                _pak->write();
            }, [this]() {
                _vars.dirty = false;
            });
        }

        void fileChanged(PakFileItem item) {
            std::memcpy(_pak->files()[item.index].name, item.name.c_str(), item.name.size() + 1);
            _pak->realFiles()[item.index]->setPath(item.path);
            _filesSelected[item.name] = item.selected;
            _vars.dirty = true;
        }

        Vars _vars;
        std::unordered_map<std::string, bool> _filesSelected;
        Containers::Pointer<Resource::PakFile> _pak;
};

}


void registerPakEditorControllers(Application& app) {
    app.registerController<PakFileItemController>();
    app.registerController<PakEditorController>();
}

void openPakEditor(const std::string& pakFile, SceneInstance& parent) {
    auto& instance = createSubScene("res://scenes/editors/pak/pak_editor.xml", parent.rootWidget(), true);
    instance.controller<PakEditorController>().setPakFile(pakFile);
}

}