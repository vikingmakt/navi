#include <stdexcept>
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Expr.h"
#include "Navi/Animation/Animation.h"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

namespace Navi { namespace Ui {

using namespace Data;

namespace {
Data::Variant evalWidgetPropertyExpr(const Expr& expr, const PropDef& prop, Widget& widget, SceneInstance& scene) {
    switch (expr.type) {
    case ExprType::Var: {
        int varIndex = scene.addVar(expr.name, Data::Variant{});
        widget._bindings.push_back({ prop._index, varIndex });
        if (prop._flags & WidgetPropertyFlag::TwoWayBind) {
            widget._reverseBindings.push_back({ prop._index, varIndex });
        }
        return scene.var(varIndex);
    }

    case ExprType::Console: {
        return Data::Variant{ expr.name };
    }
    
    case ExprType::ConsoleVar: {
        if (Console::findVar(expr.name)) {
            Data::Variant* varptr = &Console::var(expr.name);
            widget._cvarBindings.push_back({ prop._index, varptr });
            if (prop._flags & WidgetPropertyFlag::TwoWayBind) {
                widget._cvarReverseBindings.push_back({ prop._index, varptr });
            }
            return *varptr;
        }
        else {
            Console::error() << "Trying to bind non-existent console variable:" << expr.name;
        }
        break;
    }

    case ExprType::Signal: {
        // this is a signal
        widget._signals.push_back(&scene.signal(expr.name));

        // set the property to be the index of the _signals vector
        return Data::Variant{ static_cast<int>(widget._signals.size() - 1) };
    }

    case ExprType::Animation: {
        auto anim = scene.findAnimation(expr.name);
        if (anim) {
            auto animid = scene.app().animationManager().createAnimation(*anim);
            widget._animations.push_back({ prop._index, animid });
            return anim->frames[0].value;
        }
        return {};
    }
    }
}

void baseWidget(Widget& w, Data::Variant& props) {
    auto styles = props.find("styles");
    if (styles) {
        auto cmd = Console::parse(styles->cast<std::string>());
        if (cmd) {
            w._styles.push_back(cmd->first);
            for (const auto& styl : cmd->second) {
                w._styles.push_back(styl.cast<std::string>());
            }
        }
    }
}
}

WidgetManager::WidgetManager(Application* app) : _app{ app }, _widgetPool{ sizeof(Widget) } {
    _widGenerator = 0;

    // TODO: move this to an external file
    registerWidgetType(WidgetType::Base, baseWidget, {});
    registerWidgetType(WidgetType::Button, buttonWidget, {
        {"text", PropDef{Button::Text, VariantType::String}},
        {"click", PropDef{Button::Click, VariantType::Int, "", WidgetPropertyFlag::Signal}}
    });
    registerWidgetType(WidgetType::Checkbox, checkboxWidget, {
        {"text", PropDef{Checkbox::Text, VariantType::String}},
        {"checked", PropDef{Checkbox::Checked, VariantType::Bool, "", WidgetPropertyFlag::TwoWayBind}},
        {"changed", PropDef{Checkbox::Change, VariantType::Int, "", WidgetPropertyFlag::Signal}},
        {"yes", PropDef{Checkbox::Yes, VariantType::Int, "", WidgetPropertyFlag::Signal}},
        {"no", PropDef{Checkbox::No, VariantType::Int, "", WidgetPropertyFlag::Signal}}
    });
    registerWidgetType(WidgetType::ConsoleView, consoleViewWidget, {
    });
    registerWidgetType(WidgetType::FileBrowser, fileBrowserWidget, {
        {"open", PropDef{FileBrowser::Open, VariantType::Bool}},
        {"title", PropDef{FileBrowser::Title, VariantType::String}},
        {"filters", PropDef{FileBrowser::Filters, VariantType::List}},
        {"pwd", PropDef{FileBrowser::Pwd, VariantType::String}},
        {"select", PropDef{FileBrowser::Select, VariantType::String, "", WidgetPropertyFlag::Signal}},
        {"cancel", PropDef{FileBrowser::Cancel, VariantType::String, "", WidgetPropertyFlag::Signal}},
        {"flags", PropDef{FileBrowser::Flags, VariantType::Int}}
    });
    registerWidgetType(WidgetType::HorizontalLayout, horizontalLayoutWidget, {
        {"align", PropDef{HorizontalLayout::Align, VariantType::String}},
        {"padding", PropDef{HorizontalLayout::Padding, VariantType::Float}}
    });
    registerWidgetType(WidgetType::Label, labelWidget, {
        {"text", PropDef{Label::Text, VariantType::String, ""}}
    });
    registerWidgetType(WidgetType::ListView, listViewWidget, {
        {"scene", PropDef{ListView::Scene, VariantType::String}},
        {"items", PropDef{ListView::Items, VariantType::Int}},
        {"key", PropDef{ListView::Key, VariantType::Int}},
        {"bind", PropDef{ListView::Bind, VariantType::Int, "", WidgetPropertyFlag::Signal}}
    });
    registerWidgetType(WidgetType::Menu, menuWidget, {
        {"title", PropDef{Menu::Title, VariantType::String}}
    });
    registerWidgetType(WidgetType::MenuBar, menuBarWidget, {});
    registerWidgetType(WidgetType::MenuItem, menuItemWidget, {
        {"title", PropDef{Menu::Title, VariantType::String}},
        {"shortcut", PropDef{Menu::Title, VariantType::String}},
        {"click", PropDef{Menu::Title, VariantType::String, "", WidgetPropertyFlag::Signal}}
    });
    registerWidgetType(WidgetType::InputText, inputTextWidget, {
        {"value", PropDef{InputText::Value, VariantType::String, "", WidgetPropertyFlag::TwoWayBind}},
        {"hint", PropDef{InputText::Hint, VariantType::String, "Hint for input that appears when empty"}},
        {"changed", PropDef{InputText::Change, VariantType::Int, "", WidgetPropertyFlag::Signal}}
    });
    registerWidgetType(WidgetType::Separator, separatorWidget, {
        {"margin", PropDef{Separator::Margin, VariantType::Float}}
    });
    registerWidgetType(WidgetType::Spacing, spacingWidget, {
        {"width", PropDef{Spacing::Width, VariantType::Float}},
        {"height", PropDef{Spacing::Height, VariantType::Float}}
    });
    registerWidgetType(WidgetType::SubScene, subSceneWidget, {
        {"scene", PropDef{SubScene::Scene, VariantType::String}}
    });
    registerWidgetType(WidgetType::VerticalLayout, verticalLayoutWidget, {
        {"padding", PropDef{HorizontalLayout::Padding, VariantType::Float}}
    });
    registerWidgetType(WidgetType::Window, windowWidget, {
        {"position", PropDef{Window::Position, VariantType::Float2}},
        {"size", PropDef{Window::Size, VariantType::Float2}},
        {"flags", PropDef{Window::Flags, VariantType::Int, "Window flags"}},
        {"title", PropDef{Window::Title, VariantType::String}}
    });
}

void WidgetManager::registerWidgetType(WidgetType type, WidgetDef::SetupWidgetFunc func, std::unordered_map<StringHash, PropDef> props) {
    StringHash hash = widgetTypeHash(type);
    _defs[hash] = { func, type, props };
}

Memory::PoolPointer<Widget> WidgetManager::makeWidget(StringHash type, Data::Variant& props, Widget* parent, Ui::SceneInstance& scene) {
	auto it = _defs.find(type);
	if (it == _defs.end()) {
        // TODO: return 'null' widget
		throw std::runtime_error("missing Widget definition");
	}
    auto& def = it->second;
    auto widget = Memory::makePointer<Widget>(_widgetPool);
	widget->_wid = _widGenerator++;
    widget->_type = def._type;
    widget->_parent = parent;
    widget->_scene = &scene;

    // Parse variables
    for (auto& kv : props.map()) {
        auto& prop = def._props[kv.first];

        if (kv.second.type() == VariantType::String) {
            auto expr = parseExpr(kv.second.cast<std::string>());
            if (expr) {
                kv.second = evalWidgetPropertyExpr(*expr, prop, *widget, scene);
            }
        }
    }

    baseWidget(*widget, props);

    // Run the specific setup func
    def._func(*widget, props);

    return widget;
}

}}
