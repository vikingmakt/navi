#include "Navi/Ui/Ui.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Application.h"

namespace Navi::Ui {

SceneInstance& createSubScene(const std::string& sceneId, Widget& parent, bool forceSync) {
    auto& wm = parent._scene->app().widgetManager();
    auto props = Data::Variant::makeMap();
    props.map()["scene"] = sceneId;
    props.map()["sync"] = forceSync;

    auto widget = wm.makeWidget("SubScene", props, &parent, *parent._scene);
    auto wptr = widget.get();
    parent._children.push_back(std::move(widget));
    return *wptr->_props[SubScene::SceneInstance].castUniquePointer<SceneInstance>();
}

void removeFromParent(Widget& w) {
    std::size_t idx = 0;
    for (std::size_t i = 0; i < w._parent->_children.size(); i++) {
        if (w._parent->_children[i].get() == &w) {
            idx = i;
            break;
        }
    }

    w._parent->_children.erase(w._parent->_children.begin() + idx);
}

}