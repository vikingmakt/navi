#ifndef Navi_Ui_Scene_h
#define Navi_Ui_Scene_h

#include <unordered_map>
#include <Corrade/Interconnect/Connection.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/StringHash.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Memory/PoolAllocator.h"

namespace Navi::Animation { struct Animation; }

namespace Navi { namespace Ui {

class Style;
class Controller;
class SceneInstanceFactory;

class Scene : public Resource::AbstractResource {
    public:
        static StringHash resourceTypeHash() { return "Scene"; }

        explicit Scene(const std::string& id) : Resource::AbstractResource{ id } {}

    protected:
        StringHash doTypeHash() override { return resourceTypeHash(); }

    public:
        Containers::Pointer<SceneInstanceFactory> _instanceFactory;
};

class SceneInstance : public Interconnect::Receiver, public Data::VariantObject {
    public:
        explicit SceneInstance(const Resource::ResourceHandle<Scene>& res, Widget* owner = nullptr);

        ~SceneInstance();

        /// Adds a variable named [name], if it doesn't exist already
        int addVar(const std::string& name, const Data::Variant& value);

        /// Returns index of the var named [name]
        int varIndex(const std::string& name);

        /// Sets the value of the var at the index [index]
        void setVar(int index, const Data::Variant& value);

        /// Returns a reference to the var at the index [index]
        Data::Variant& var(int index);

        /// Called after the scene is loaded and the hierarchy is established
        void sceneMounted();

        /// Find a widget in the scene with the given id, returns nullptr if the widget is not present
        Widget* findWidgetById(const StringHash& id) const;

        /// Finds an animation that's contained within the styles of this scene, returns nullptr if not found
        Animation::Animation* findAnimation(const StringHash& name);

        /// Called when the scene finishes loading
        void handleSceneLoaded(Resource::AbstractResource*);

        /// Returns the root widget of the scene
        Widget& rootWidget() { return *_rootWidget; }

        /// Sets the root widget of the scene
        void setRootWidget(Memory::PoolPointer<Widget>&& w) { _rootWidget = std::move(w); }

        /// Returns the signal named [name]
        DeferredSignal<Data::Variant>& signal(const StringHash& name) { return _signals[name]; }

        /// Returns the application
        Application& app() { return *_app; }

        /// Sets the application
        void setApp(Application& app) { _app = &app; }

        /// Returns the inline style (defined in the scene file itself)
        Style* inlineStyle() { return _inlineStyle.get(); }

        /// Sets the inline style
        void setInlineStyle(Containers::Pointer<Style>&& style) { _inlineStyle = std::move(style); }

        /// Returns the referenced style resources
        std::vector<Resource::ResourceHandle<Style>>& styles() { return _styles; }

        /// This instance's owner (might be nullptr)
        Widget* owner() const { return _owner; }

        void setController(Containers::Pointer<Controller>&& ctrl) {
            _controller = std::move(ctrl);
        }

        template<class T>
        T& controller() {
            return *static_cast<T*>(_controller.get());
        }

        void _buildWidgetsMap(Widget&);

        Resource::ResourceHandle<Scene> _scene;
        Memory::PoolPointer<Widget> _rootWidget;
        std::unordered_map<StringHash, Widget*> _nodesById;
        std::unordered_map<StringHash, DeferredSignal<Data::Variant>> _signals;
        std::unordered_map<std::string, int> _varsIndex;
        std::vector<Data::Variant> _vars;
        std::vector<Resource::ResourceHandle<Style>> _styles;
        Containers::Pointer<Style> _inlineStyle;
        Containers::Pointer<Controller> _controller;
        std::vector<Interconnect::Connection> _connections;
        Widget* _owner;
        Application* _app;
};

class SceneInstanceFactory {
    public:
        virtual ~SceneInstanceFactory() {}

        virtual void createSceneInstance(SceneInstance& inst) = 0;
};

}}

#endif
