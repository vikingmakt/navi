#ifndef Navi_Ui_Widget_h
#define Navi_Ui_Widget_h

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/Containers/StaticArray.h>
#include <Corrade/Interconnect/Connection.h>
#ifdef NAVI_IMGUI
#include <imgui.h>
#endif
#include "Navi/StringHash.h"
#include "Navi/Signal.h"
#include "Navi/Data/Variant.h"
#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Data/Tree.h"
#include "Navi/Data/Tuple.h"
#include "Navi/Resource/ResourceManager.h"
#include "Navi/Ui/Ui.h"
#include "Navi/Ui/Style.h"
#include "Navi/Navi.h"

namespace Navi { namespace Ui {

enum class WidgetType {
    Base,
    Button,
    Checkbox,
    ConsoleView,
    FileBrowser,
    HorizontalLayout,
    InputText,
    Label,
    ListView,
    Menu,
    MenuBar,
    MenuItem,
    Separator,
    Spacing,
    SubScene,
    VerticalLayout,
    Window,
};

enum class WidgetFlag: unsigned int {
    None = 0,
    Invisible = 1,
};
typedef Containers::EnumSet<WidgetFlag> WidgetFlags;
CORRADE_ENUMSET_OPERATORS(WidgetFlags);

enum class WidgetPropertyFlag : unsigned int {
    None = 0,
    Signal = 1,
    NoBind = 1 << 1,
    TwoWayBind = 1 << 2,
};
typedef Containers::EnumSet<WidgetPropertyFlag> WidgetPropertyFlags;
CORRADE_ENUMSET_OPERATORS(WidgetPropertyFlags);

struct WidgetPropertyBinding {
    unsigned int _index;
    int _varIndex;
};

struct WidgetPropertyCvarBinding {
    unsigned int _index;
    Data::Variant* _varptr;
};

struct WidgetPropertyAnimation {
    unsigned int _index;
    unsigned int _animId;
};

class Widget {
    public:
        typedef Containers::StaticArray<16, Data::Variant>     PropertiesType;

        Data::Variant& operator[](std::size_t idx) { return _props[idx]; }

        void updateBindings();

        void updateReverseBindings();

        void updateAnimations();

        void emitSignal(unsigned int propIdx, const Data::Variant& arg, bool immediateDispatch = false);

        PropertiesType _props;
        WidgetType _type;
        WidgetFlags _flags;
        std::uint32_t _wid;
        std::string _id;
        std::vector<std::string> _styles;
        std::vector<Memory::PoolPointer<Widget>> _children;
        std::vector<WidgetPropertyBinding> _bindings;
        std::vector<WidgetPropertyCvarBinding> _cvarBindings;
        std::vector<WidgetPropertyBinding> _reverseBindings;
        std::vector<WidgetPropertyCvarBinding> _cvarReverseBindings;
        std::vector<WidgetPropertyAnimation> _animations;
        std::vector<DeferredSignal<Data::Variant>*> _signals;
        Widget* _parent;
        SceneInstance* _scene;
};


enum class WindowFlag: unsigned int {
#ifdef NAVI_IMGUI
    None = 0,
    NoTitleBar = ImGuiWindowFlags_NoTitleBar,
    NoMove = ImGuiWindowFlags_NoMove,
    NoResize = ImGuiWindowFlags_NoResize,
    NoBackground = ImGuiWindowFlags_NoBackground,
    MenuBar = ImGuiWindowFlags_MenuBar,
    NoBringToFrontOnFocus = ImGuiWindowFlags_NoBringToFrontOnFocus,
#endif
};
typedef Containers::EnumSet<WindowFlag> WindowFlags;
CORRADE_ENUMSET_OPERATORS(WindowFlags);

enum class InputTextFlag: unsigned int {
#ifdef NAVI_IMGUI
    None = 0,
    ReadOnly = ImGuiInputTextFlags_ReadOnly,
    AllowTabInput = ImGuiInputTextFlags_AllowTabInput,
#endif
};
typedef Containers::EnumSet<InputTextFlag> InputTextFlags;
CORRADE_ENUMSET_OPERATORS(InputTextFlags);

enum class FileBrowserFlag: unsigned int {
#ifdef NAVI_IMGUI
    None = 0,
    SelectDirectory     = (1),//ImGuiFileBrowserFlags_SelectDirectory,
    EnterNewFilename    = (1 << 1),//ImGuiFileBrowserFlags_EnterNewFilename,
    NoModal             = (1 << 2),//ImGuiFileBrowserFlags_NoModal,
    NoTitleBar          = (1 << 3),//ImGuiFileBrowserFlags_NoTitleBar ,
    NoStatusBar         = (1 << 4),//ImGuiFileBrowserFlags_NoStatusBar,
    CloseOnEsc          = (1 << 5),//ImGuiFileBrowserFlags_CloseOnEsc,
    CreateNewDir        = (1 << 6),//ImGuiFileBrowserFlags_CreateNewDir,
#endif
};
typedef Containers::EnumSet<FileBrowserFlag> FileBrowserFlags;
CORRADE_ENUMSET_OPERATORS(FileBrowserFlags);


namespace Button {
    enum Properties {
        Text,
        Click,
    };
}

namespace Checkbox {
    enum Properties {
        Text,
        Checked,
        Change,
        Yes,
        No
    };
}

namespace ConsoleView {
    enum Properties {
    };
}

namespace FileBrowser {
    enum Properties {
        Open,
        Title,
        Filters,
        Pwd,
        Select,
        Cancel,
        Flags,        
        RendererState,
    };
}

namespace HorizontalLayout {
    enum Properties {
        Align,
        Padding,
        ContentWidth,
        Dirty,
    };
}

namespace InputText {
    enum Properties {
        Value,
        Buffer,
        Hint,
        Change,
        Flags,
    };

    enum {
        InputBufferSize = 512
    };
}

namespace Label {
    enum Properties {
        Text,
    };
}

namespace ListView {
    enum Properties {
        Scene,
        Items,
        Bind,
        Key,
        PrevKey,
        Instances,
        SceneInstances,
    };

    struct Item : public Data::VariantObject {
        unsigned int index;
        Ui::SceneInstance* scene;
        bool dirty;
    };
}

namespace Menu {
    enum Properties {
        Title,
    };
}

namespace MenuBar {
    enum Properties {
    };
}

namespace MenuItem {
    enum Properties {
        Title,
        Shortcut,
        Click,
    };
}

namespace Separator {
    enum Properties {
        Margin,
    };
}

namespace Spacing {
    enum Properties {
        Width,
        Height,
    };
}

namespace SubScene {
    enum Properties {
        Scene,
        SceneInstance,
    };
}

namespace VerticalLayout {
    enum Properties {
        Padding,
    };
}

namespace Window {
    enum Properties {
        Position,
        Size,
        Pivot,
        Flags,
        Title,
        Border,
    };
}

StringHash widgetTypeHash(WidgetType);


/// Widget constructors

void buttonWidget(Widget& w, Data::Variant& props);

void checkboxWidget(Widget& w, Data::Variant& props);

void consoleViewWidget(Widget& w, Data::Variant& props);

void fileBrowserWidget(Widget& w, Data::Variant& props);

void horizontalLayoutWidget(Widget& w, Data::Variant& props);

void inputTextWidget(Widget& w, Data::Variant& props);

void labelWidget(Widget& w, Data::Variant& props);

void listViewWidget(Widget& w, Data::Variant& props);

void menuWidget(Widget& w, Data::Variant& props);

void menuBarWidget(Widget& w, Data::Variant& props);

void menuItemWidget(Widget& w, Data::Variant& props);

void separatorWidget(Widget& w, Data::Variant& props);

void spacingWidget(Widget& w, Data::Variant& props);

void subSceneWidget(Widget& w, Data::Variant& props);

void verticalLayoutWidget(Widget& w, Data::Variant& props);

void windowWidget(Widget& w, Data::Variant& props);

}}

#if 0

class Combo : public AbstractWidget {
    public:
        typedef WidgetProperties
            < std::vector<std::pair<Data::Variant, std::string>>
            , Data::Variant
        > PropertiesType;

        /// Property accessors
        enum Properties : std::size_t {
            Items,
            Value,
        };

        static Memory::PoolAllocator* pool();
        static WidgetDef* def();
        static WidgetType type() { return WidgetType::Combo; }

        Combo() : AbstractWidget{ *def(), type() } {}

        PropertiesType& properties() { return _props; }

        std::vector<std::pair<Data::Variant, std::string>>& items() { return _props.get<Items>(); }

        Data::Variant& value() { return _props.get<Value>(); }

    private:
        PropertiesType _props;
};

/// A simple tree view with text leafs
class TreeView : public AbstractWidget {
    public:
		struct Item {
            StringHash key;
            std::string text;
        };

		typedef Data::Tree<Item> TreeType;

        typedef WidgetProperties
            < TreeType
            , std::string
            , bool
            , std::unordered_set<StringHash>
            > PropertiesType;

        /// Property acessors
        enum Properties : std::size_t {
            Items,
            ContextMenu,
            Selectable,
            Selection,
        };

        static Memory::PoolAllocator* pool();
        static WidgetDef* def();
        static WidgetType type() { return WidgetType::TreeView; }

        TreeView() : AbstractWidget{ *def(), type() } {}

        void doWidgetMounted(Scene& scene) override;

        PropertiesType& properties() { return _props; }

        TreeType& items() { return _props.get<Items>(); }

        const std::string& contextMenu() const { return _props.get<ContextMenu>(); }

        bool selectable() const { return _props.get<Selectable>(); }

        std::unordered_set<StringHash>& selection() { return _props.get<Selection>(); }

        Menu* contextMenuWidget() const { return _contextMenuWidget; }

    private:
        PropertiesType _props;
        Menu* _contextMenuWidget;
};

#endif

// Widget factory functions

#endif
