#include "Navi/Ui/Expr.h"
#include "Navi/Console.hpp"

namespace Navi::Ui {

Containers::Optional<Expr> parseExpr(const std::string& str) {
    const char* cstr = str.data();
    if (std::strncmp(cstr, "var ", 4) == 0) {
        auto cmd = Console::parse(str);
        if (cmd) {
            std::string name = cmd->second[0].cast<std::string>();
            return { Expr{ ExprType::Var, name } };
        }
    }
    else if (std::strncmp(cstr, "cvar ", 5) == 0) {
        auto cmd = Console::parse(str);
        if (cmd) {
            std::string name = cmd->second[0].cast<std::string>();
            return { Expr{ ExprType::ConsoleVar, name } };
        }
    }
    else if (std::strncmp(cstr, "signal ", 7) == 0) {
        auto cmd = Console::parse(str);
        if (cmd) {
            std::string name = cmd->second[0].cast<std::string>();
            return { Expr{ ExprType::Signal, name } };
        }
    }
    else if (std::strncmp(cstr, "console ", 8) == 0) {
        return { Expr{ ExprType::Console, str.substr(8) } };
    }
    else if (std::strncmp(cstr, "animation ", 10) == 0) {
        auto cmd = Console::parse(str);
        if (cmd) {
            std::string name = cmd->second[0].cast<std::string>();
            return { Expr{ ExprType::Animation, name } };
        }
    }
    return {};
}
