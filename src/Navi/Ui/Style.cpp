#include "Navi/Ui/Style.h"
#include "Navi/Ui/Expr.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Application.h"

namespace Navi::Ui {

namespace {
void evalSetting(StyleSetting& set, Style& style) {
    set.style = &style;
    if (set.value.type() == Data::VariantType::String) {
        auto expr = parseExpr(set.value.cast<std::string>());
        if (expr && expr->type == ExprType::Animation) {
            // TODO: add animation
            auto& anim = style.animations()[expr->name];
            set.value = anim.frames[0].value;
            set.animationId = Application::instance().animationManager().createAnimation(anim);
        }
    }
}
}

void Style::addSettings(StringHash key, Data::LinkedList<StyleSetting>&& sets) {
    for (auto& set : sets) {
        evalSetting(set, *this);
    }
    _settings[key] = std::move(sets);
}

void Style::addFont(StringHash key, const Resource::ResourceHandle<Ui::Font>& fh) {
    _fonts[key] = fh;
}

void Style::addAnimation(StringHash key, const Animation::Animation& anim) {
    _animations[key] = anim;
}

#if 0
StylePatchComponent::StylePatchComponent(AbstractWidget& widget)
    : AbstractComponent{ widget } {

    widget.properties().propertyChanged<AbstractWidget::StylePatch>().connect(*this, &StylePatchComponent::stylePatchChanged);
}

void StylePatchComponent::stylePatchChanged(Containers::Reference<Data::LinkedList<Ui::StyleSetting>> sets) {
    _settings = &(*sets);
}
#endif

bool isFloat(Ui::StyleVar var) {
    return (
        (std::size_t)var > (std::size_t)Ui::StyleVar::BeginFloat &&
        (std::size_t)var < (std::size_t)Ui::StyleVar::EndFloat
    );
}

bool isVec2(Ui::StyleVar var) {
    return (
        (std::size_t)var > (std::size_t)Ui::StyleVar::BeginVec2 &&
        (std::size_t)var < (std::size_t)Ui::StyleVar::EndVec2
    );
}

bool isColor(Ui::StyleVar var) {
    return (
        (std::size_t)var > (std::size_t)Ui::StyleVar::BeginColor &&
        (std::size_t)var < (std::size_t)Ui::StyleVar::EndColor
    );
}

bool isPointer(Ui::StyleVar var) {
    return (
        (std::size_t)var > (std::size_t)Ui::StyleVar::BeginPointer &&
        (std::size_t)var < (std::size_t)Ui::StyleVar::EndPointer
    );
}

}
