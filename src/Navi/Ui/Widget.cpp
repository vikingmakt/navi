#include <stdexcept>
#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/WidgetManager.h"
#include "Navi/Data/LinkedList.hpp"
#include "Navi/Ui/Scene.h"
#include "Navi/UI/Image.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Application.h"
#include "Navi/Animation/Animation.h"
#include "Navi/Console.hpp"

namespace Navi { namespace Ui {

namespace {
}

void Widget::updateBindings() {
    for (auto& binding : _bindings) {
        _props[binding._index] = _scene->var(binding._varIndex);
    }
    for (auto& binding : _cvarBindings) {
        _props[binding._index] = *binding._varptr;
    }
}

void Widget::updateReverseBindings() {
    for (auto& binding : _bindings) {
        _scene->setVar(binding._varIndex, _props[binding._index]);
    }
    for (auto& binding : _cvarReverseBindings) {
        *binding._varptr = _props[binding._index];
    }
}

void Widget::updateAnimations() {
    for (auto& anim : _animations) {
        _props[anim._index] = Application::instance().animationManager().animationValue(anim._animId);
    }
}

void Widget::emitSignal(unsigned int propIdx, const Data::Variant& arg, bool immediateDispatch) {
    auto& prop = _props[propIdx];
    if (prop.type() == Data::VariantType::Int) {
        int signalIdx = prop.cast<int>();
        if (signalIdx < _signals.size()) {
            _signals[signalIdx]->emit(arg);
            if (immediateDispatch)
                _signals[signalIdx]->dispatch();
        }
    }
    else if (prop.type() == Data::VariantType::String) {
        // this is a console command
        Console::execDeferred(prop.cast <std::string>(), false, _scene);
    }
}


StringHash widgetTypeHash(WidgetType t) {
    switch (t) {
    case WidgetType::Base:
        return "Base";

    case WidgetType::Button:
        return "Button";

    case WidgetType::Checkbox:
        return "Checkbox";

    case WidgetType::ConsoleView:
        return "ConsoleView";

    case WidgetType::FileBrowser:
        return "FileBrowser";

    case WidgetType::HorizontalLayout:
        return "HorizontalLayout";

    case WidgetType::InputText:
        return "InputText";

    case WidgetType::Label:
        return "Label";

    case WidgetType::ListView:
        return "ListView";

    case WidgetType::Menu:
        return "Menu";

    case WidgetType::MenuBar:
        return "MenuBar";

    case WidgetType::MenuItem:
        return "MenuItem";

    case WidgetType::Separator:
        return "Separator";

    case WidgetType::Spacing:
        return "Spacing";

    case WidgetType::SubScene:
        return "SubScene";

    case WidgetType::VerticalLayout:
        return "VerticalLayout";

    case WidgetType::Window:
        return "Window";
    }
}

void buttonWidget(Widget& w, Data::Variant& props) {
    w[Button::Text] = *props.find("text");

    auto click = props.find("click");
    if (click) w[Button::Click] = *click;
}

void checkboxWidget(Widget& w, Data::Variant& props) {
    auto text = props.find("text");
    if (text) {
        w[Checkbox::Text] = *text;
    }

    auto checked = props.find("checked");
    if (checked) {
        w[Checkbox::Checked] = *checked;
    }

    auto change = props.find("change");
    if (change) {
        w[Checkbox::Change] = *change;
    }

    auto yes = props.find("yes");
    if (yes) {
        w[Checkbox::Yes] = *yes;
    }

    auto no = props.find("no");
    if (no) {
        w[Checkbox::No] = *no;
    }
}

void consoleViewWidget(Widget& w, Data::Variant& props) {
}

void fileBrowserWidget(Widget& w, Data::Variant& props) {
    auto title = props.find("title");
    w[FileBrowser::Title] = title ? *title : "Select File";

    auto filters = props.find("filters");
    if (filters) w[FileBrowser::Filters] = *filters;

    auto pwd = props.find("pwd");
    if (pwd) {
        w[FileBrowser::Pwd] = *pwd;
        Console::info() << pwd->cast<std::string>().c_str();
    }

    auto filesel = props.find("select");
    if (filesel) w[FileBrowser::Select] = *filesel;

    auto cancel = props.find("cancel");
    if (cancel) w[FileBrowser::Cancel] = *cancel;

    auto flags = props.find("flags");
    if (flags) w[FileBrowser::Flags] = *flags;
    else w[FileBrowser::Flags] = 0;

    w[FileBrowser::Open] = *props.find("open");
}

void horizontalLayoutWidget(Widget& w, Data::Variant& props) {
    std::string align{ "left" };
    auto alignProp = props.find("align");
    if (alignProp) align = alignProp->cast<std::string>();

    float padding = -1;
    auto paddingProp = props.find("padding");
    if (paddingProp) padding = paddingProp->cast<float>();

    w[HorizontalLayout::Align] = align;
    w[HorizontalLayout::Padding] = padding;
    w[HorizontalLayout::Dirty] = true;
    w[HorizontalLayout::ContentWidth] = 0.0f;
}

void inputTextWidget(Widget& w, Data::Variant& props) {
    w[InputText::Buffer] = std::string(InputText::InputBufferSize, '\0');

    auto value = props.find("value");
    w[InputText::Value] = value ? *value : std::string("");
    if (w[InputText::Value].isString()) {
        auto& str = w[InputText::Value].cast<std::string>();
        std::memcpy(w[InputText::Buffer].cast<std::string>().data(), str.data(), str.size());
    }

    auto hint = props.find("hint");
    if (hint) {
        w[InputText::Hint] = *hint;
    }
    else {
        w[InputText::Hint] = "";
    }

    auto change = props.find("change");
    if (change) {
        w[InputText::Change] = *change;
    }

    InputTextFlags flags = InputTextFlag::None;
    if (props.find("readOnly"))
        flags |= InputTextFlag::ReadOnly;
    if (props.find("allowTab"))
        flags |= InputTextFlag::AllowTabInput;

    w[InputText::Flags] = (int)static_cast<unsigned int>(flags);
}

void labelWidget(Widget& w, Data::Variant& props) {
    w[Label::Text] = *props.find("text");
}

void listViewWidget(Widget& w, Data::Variant& props) {
    w[ListView::Items] = 0;
    w[ListView::Instances] = Data::Variant::makeList();
    w[ListView::SceneInstances] = Data::Variant::makeList();
    w[ListView::Scene] = *props.find("scene");
    w[ListView::Bind] = *props.find("bind");

    auto key = props.find("key");
    if (key) {
        w[ListView::Key] = *key;
        w[ListView::PrevKey] = *key;
    }
}

void menuWidget(Widget& w, Data::Variant& props) {
    w[Menu::Title] = *props.find("title");
}

void menuBarWidget(Widget& w, Data::Variant& props) {
}

void menuItemWidget(Widget& w, Data::Variant& props) {
    w[MenuItem::Title] = *props.find("title");

    auto click = props.find("click");
    if (click) w[MenuItem::Click] = *click;

    auto shortcut = props.find("shortcut");
    if (shortcut) {
        w[MenuItem::Shortcut] = *shortcut;

        auto nobind = props.find("noBind");
        if (!nobind) {
            w._scene->app().bindKey(*shortcut, *click, [&w]() {
                w.emitSignal(MenuItem::Click, {});
            });
        }
    }
}

void separatorWidget(Widget& w, Data::Variant& props) {
    float margin = 0.0f;
    auto marginprop = props.find("margin");
    if (marginprop) margin = *marginprop;
    w[Separator::Margin] = margin;
}

void spacingWidget(Widget& w, Data::Variant& props) {
    float width = 5.0f;
    float height = 5.0f;

    auto widthprop = props.find("width");
    auto heightprop = props.find("height");

    if (widthprop) width = *widthprop;
    if (heightprop) height = *heightprop;

    w[Spacing::Width] = width;
    w[Spacing::Height] = height;
}

void subSceneWidget(Widget& w, Data::Variant& props) {
    auto scene = *props.find("scene");
    w[SubScene::Scene] = scene;

    auto sync = props.find("sync");
    if (sync && sync->cast<bool>()) {
        auto& res = w._scene->app().resourceManager().loadSyncWithFile<Scene>(scene.cast<std::string>(), scene.cast<std::string>());
        w[SubScene::SceneInstance] = Data::Variant::makeUniquePointer(new SceneInstance{ res, &w });
    }
    else {
        auto& res = w._scene->app().resourceManager().loadAsyncWithFile<Scene>(scene.cast<std::string>(), scene.cast<std::string>());
        w[SubScene::SceneInstance] = Data::Variant::makeUniquePointer(new SceneInstance{ res, &w });
    }
}

void verticalLayoutWidget(Widget& w, Data::Variant& props) {
    float padding = 0;
    auto paddingProp = props.find("padding");
    if (paddingProp) padding = paddingProp->cast<float>();

    w[VerticalLayout::Padding] = padding;
}

void windowWidget(Widget& w, Data::Variant& props) {
    Vector2f pos = Vector2f(-1.0f, -1.0f);
    Vector2f size = Vector2f(-1.0f, -1.0f);
    if (props.find("position")) pos = *props.find("position");
    if (props.find("size")) size = *props.find("size");

    auto title = props.find("title");
    if (title)
        w[Window::Title] = *title;
    else
        w[Window::Title] = " ";

    auto pivot = props.find("pivot");
    if (pivot)
        w[Window::Pivot] = *pivot;
    else
        w[Window::Pivot] = Vector2f(0, 0);

    auto border = props.find("border");
    if (border)
        w[Window::Border] = *border;
    else
        w[Window::Border] = false;

    w[Window::Position] = pos;
    w[Window::Size] = size;

    WindowFlags flags = WindowFlag::MenuBar;
    if (props.find("noTitleBar"))
        flags |= WindowFlag::NoTitleBar;
    if (props.find("noMove"))
        flags |= WindowFlag::NoMove;
    if (props.find("noResize"))
        flags |= WindowFlag::NoResize;
    if (props.find("noBackground"))
        flags |= WindowFlag::NoBackground;
    if (props.find("noBringToFront"))
        flags |= WindowFlag::NoBringToFrontOnFocus;
    if (props.find("noMenuBar"))
        flags = flags &~ WindowFlag::MenuBar;

    w[Window::Flags] = (int)static_cast<unsigned int>(flags);
}

#if 0


AbstractWidget::AbstractWidget(WidgetDef& def, WidgetType type) : _def{ &def }, _type{ type }, _name{ widgetTypeName(type) }, _parent{ nullptr }, _scene{ nullptr } {
    _props.set<Visible>(true);
}

void AbstractWidget::addChild(Memory::PoolPointer<AbstractWidget>&& child) {
    if (child->_parent != nullptr) {
        throw std::runtime_error("Cannot reparent node");
    }
    child->_parent = this;
    _children.push_back(std::move(child));
}

AbstractWidget& AbstractWidget::makeChild(StringHash type) {
    auto child = Application::instance().widgetManager().makeWidget(type);
    auto result = child.get();
    addChild(std::move(child));
    return *result;
}

AbstractWidget* AbstractWidget::findParent(WidgetType type, int maxLevel) {
    auto parent = _parent;
    int level = 1;
    while (parent) {
        if (parent->_type == type) {
            return parent;
        }
        else if (maxLevel != -1 && level >= maxLevel) {
            return nullptr;
        }
        parent = parent->_parent;
        ++level;
    }
    return nullptr;
}

void AbstractWidget::widgetMounted(Scene& scene) {
	for (auto& child : _children) {
		child->widgetMounted(scene);
	}
    doWidgetMounted(scene);
    for (auto& pair : _components) {
        pair.second->widgetMounted(scene);
    }
}


Memory::PoolPointer<AbstractWidget> makeButton(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Button>(*Button::pool()));
}

Memory::PoolAllocator* Button::pool() {
    static Memory::PoolAllocator pool{ sizeof(Button) };
    return &pool;
}

WidgetDef* Button::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeButton;
    return &def;
}

void Button::doWidgetMounted(Scene&) {
	Application::instance().addDeferredSignal(clicked());
}


Memory::PoolPointer<AbstractWidget> makeCollapsingHeader(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<CollapsingHeader>(*CollapsingHeader::pool()));
}

Memory::PoolAllocator* CollapsingHeader::pool() {
    static Memory::PoolAllocator pool{ sizeof(CollapsingHeader) };
    return &pool;
}

WidgetDef* CollapsingHeader::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeCollapsingHeader;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeCombo(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Combo>(*Combo::pool()));
}

Memory::PoolAllocator* Combo::pool() {
    static Memory::PoolAllocator pool{ sizeof(Combo) };
    return &pool;
}

WidgetDef* Combo::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeCombo;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeHorizontalLayout(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<HorizontalLayout>(*HorizontalLayout::pool()));
}

Memory::PoolAllocator* HorizontalLayout::pool() {
    static Memory::PoolAllocator pool{ sizeof(HorizontalLayout) };
    return &pool;
}

WidgetDef* HorizontalLayout::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeHorizontalLayout;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeImage(WidgetManager& mgr, WidgetDef& def) {
	return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Image>(*Image::pool()));
}

Memory::PoolAllocator* Image::pool() {
	static Memory::PoolAllocator pool{ sizeof(Image) };
	return &pool;
}

WidgetDef* Image::def() {
	static WidgetDef def;
	def.makeWidgetFunc = makeImage;
	return &def;
}

void Image::doWidgetMounted(Scene& scene) {
	handleSrcChanged(_props.get<Src>());
    _props.propertyChanged<Src>().connect(*this, &Image::handleSrcChanged);
}

Resource::ResourceHandle<Resource::Image>& Image::image() {
    return _image;
}

void Image::handleSrcChanged(Containers::Reference<std::string> src) {
    if (!src->empty()) {
        _image = Application::instance().resourceManager().loadAsyncWithFile<Resource::Image>(*src, *src);
    }
}


Memory::PoolPointer<AbstractWidget> makeInputText(WidgetManager& mgr, WidgetDef& def) {
	return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<InputText>(*InputText::pool()));
}

Memory::PoolAllocator* InputText::pool() {
	static Memory::PoolAllocator pool{ sizeof(InputText) };
	return &pool;
}

WidgetDef* InputText::def() {
	static WidgetDef def;
	def.makeWidgetFunc = makeInputText;
	return &def;
}

InputText::InputText() : AbstractWidget{ *def(), type() } {
	_inputBuf[0] = '\0';
	//value().valueChanged().connect(*this, &InputText::handleValueChanged);
}

void InputText::handleValueChanged(Containers::Reference<std::string> v) {
	std::memcpy(_inputBuf.data(), v->c_str(), v->size());
	_inputBuf[v->size()] = '\0';
}


Memory::PoolPointer<AbstractWidget> makeLabel(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Label>(*Label::pool()));
}

Memory::PoolAllocator* Label::pool() {
    static Memory::PoolAllocator pool{ sizeof(Label) };
    return &pool;
}

WidgetDef* Label::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeLabel;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeMenu(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Menu>(*Menu::pool()));
}

Memory::PoolAllocator* Menu::pool() {
    static Memory::PoolAllocator pool{ sizeof(Menu) };
    return &pool;
}

WidgetDef* Menu::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeMenu;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeMenuBar(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<MenuBar>(*MenuBar::pool()));
}

Memory::PoolAllocator* MenuBar::pool() {
    static Memory::PoolAllocator pool{ sizeof(MenuBar) };
    return &pool;
}

WidgetDef* MenuBar::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeMenuBar;
    return &def;
}

MenuBar::MenuBar() : AbstractWidget{ *def(), type() } {
    _props.set<ForceMain>(false);
}

void MenuBar::doWidgetMounted(Scene& scene) {
    _isMain = findParent(WidgetType::Window) == nullptr;
}


Memory::PoolPointer<AbstractWidget> makeMenuItem(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<MenuItem>(*MenuItem::pool()));
}

Memory::PoolAllocator* MenuItem::pool() {
    static Memory::PoolAllocator pool{ sizeof(MenuItem) };
    return &pool;
}

WidgetDef* MenuItem::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeMenuItem;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeSceneFactory(WidgetManager& mgr, WidgetDef& def) {
	return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<SceneFactory>(*SceneFactory::pool()));
}

Memory::PoolAllocator* SceneFactory::pool() {
	static Memory::PoolAllocator pool{ sizeof(SceneFactory) };
	return &pool;
}

WidgetDef* SceneFactory::def() {
	static WidgetDef def;
	def.makeWidgetFunc = makeSceneFactory;
	return &def;
}

void SceneFactory::doWidgetMounted(Ui::Scene& scene) {
}

Scene& SceneFactory::instantiate() {
    auto& instance = makeChild<SceneInstance>();
    instance.properties().set<SceneInstance::Scene>(scene());
    instance.widgetMounted(*_scene);
    instantiated().emit(instance.sceneInstance());
    return instance.sceneInstance();
}

void SceneFactory::ensureInstances(std::size_t count) {
	std::size_t csize = _children.size();
	if (csize < count) {
		for (std::size_t i = csize; i < count; i++) {
			instantiate();
		}
	}
	else {
		while (_children.size() > count) {
			_children.pop_back();
		}
	}
}


Memory::PoolPointer<AbstractWidget> makeSceneInstance(WidgetManager& mgr, WidgetDef& def) {
	return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<SceneInstance>(*SceneInstance::pool()));
}

Memory::PoolAllocator* SceneInstance::pool() {
	static Memory::PoolAllocator pool{ sizeof(SceneInstance) };
	return &pool;
}

WidgetDef* SceneInstance::def() {
	static WidgetDef def;
	def.makeWidgetFunc = makeSceneInstance;
	return &def;
}

void SceneInstance::doWidgetMounted(Ui::Scene& scene) {
	Application::instance().addDeferredSignal(instantiated());

	if (!_props.get<Scene>().empty()) {
        // TODO: load this async, first we need to implement specific resource callback when loaded
        _sceneResource = Application::instance().resourceManager().loadSyncWithFile<Resource::Scene>(_props.get<Scene>(), _props.get<Scene>());
		_props.get<Instantiated>().emit(_sceneResource.resource().uiScene());
	}
}

Ui::Scene& SceneInstance::sceneInstance() {
    return _sceneResource.resource().uiScene();
}


Memory::PoolPointer<AbstractWidget> makeSliderFloat(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<SliderFloat>(*SliderFloat::pool()));
}

Memory::PoolAllocator* SliderFloat::pool() {
    static Memory::PoolAllocator pool{ sizeof(SliderFloat) };
    return &pool;
}

WidgetDef* SliderFloat::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeSliderFloat;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeSeparator(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Separator>(*Separator::pool()));
}

Memory::PoolAllocator* Separator::pool() {
    static Memory::PoolAllocator pool{ sizeof(Separator) };
    return &pool;
}

WidgetDef* Separator::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeSeparator;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeStyleLoader(WidgetManager& mgr, WidgetDef& def) {
	return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<StyleLoader>(*StyleLoader::pool()));
}

Memory::PoolAllocator* StyleLoader::pool() {
	static Memory::PoolAllocator pool{ sizeof(StyleLoader) };
	return &pool;
}

WidgetDef* StyleLoader::def() {
	static WidgetDef def;
	def.makeWidgetFunc = makeStyleLoader;
	return &def;
}

void StyleLoader::doWidgetMounted(Scene& scene) {
    handleSrcChanged(Containers::Reference<std::string>{ _props.get<Src>() });
    _props.propertyChanged<Src>().connect(*this, &StyleLoader::handleSrcChanged);
}

void StyleLoader::handleSrcChanged(Containers::Reference<std::string> src) {
    if (!src->empty()) {
        _style = Application::instance().resourceManager().loadAsyncWithFile<Resource::Style>(*src, *src);
    }
}


Memory::PoolPointer<AbstractWidget> makeTreeView(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<TreeView>(*TreeView::pool()));
}

Memory::PoolAllocator* TreeView::pool() {
    static Memory::PoolAllocator pool{ sizeof(TreeView) };
    return &pool;
}

WidgetDef* TreeView::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeTreeView;
    return &def;
}

void TreeView::doWidgetMounted(Scene& scene) {
    _contextMenuWidget = static_cast<Menu*>(scene.findWidgetById(contextMenu()));
}


Memory::PoolPointer<AbstractWidget> makeVerticalLayout(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<VerticalLayout>(*VerticalLayout::pool()));
}

Memory::PoolAllocator* VerticalLayout::pool() {
    static Memory::PoolAllocator pool{ sizeof(VerticalLayout) };
    return &pool;
}

WidgetDef* VerticalLayout::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeVerticalLayout;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeWidget(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Widget>(*Widget::pool()));
}

Memory::PoolAllocator* Widget::pool() {
    static Memory::PoolAllocator pool{ sizeof(Widget) };
    return &pool;
}

WidgetDef* Widget::def() {
    static WidgetDef def;
    def.makeWidgetFunc = makeWidget;
    return &def;
}


Memory::PoolPointer<AbstractWidget> makeWindow(WidgetManager& mgr, WidgetDef& def) {
    return Memory::staticPointerCast<AbstractWidget>(Memory::makePointer<Window>(*Window::pool()));
}

Memory::PoolAllocator* Window::pool() {
    static Memory::PoolAllocator pool{ sizeof(Window) };
    return &pool;
}

WidgetDef* Window::def() {
    auto def = new WidgetDef{};
    def->makeWidgetFunc = makeWindow;
    return def;
}

Window::Window() : AbstractWidget{ *def(), type() } {
    _props.tuple().fillRange<NoTitleBar, NoBringToFrontOnFocus>(false);
}

void Window::doWidgetMounted(Scene& scene) {
    _isChildWindow = findParent(type()) != nullptr;
}

#endif

}}
