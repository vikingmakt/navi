#ifndef Navi_Ui_WidgetManager_h
#define Navi_Ui_WidgetManager_h

#include <unordered_map>
#include <Corrade/Containers/Pointer.h>
#include "Navi/StringHash.h"
#include "Navi/Data/Variant.h"
#include "Navi/Memory/Memory.h"
#include "Navi/Memory/PoolAllocator.h"
#include "Navi/Ui/Widget.h"

namespace Navi { namespace Ui {

struct Widget;
class WidgetManager;

struct PropDef {
    unsigned int _index;
    Data::VariantType _type;
    std::string _desc;
    WidgetPropertyFlags _flags;

    PropDef() {}

    explicit PropDef(unsigned int index, Data::VariantType type, std::string desc = "", WidgetPropertyFlags flags = WidgetPropertyFlag::None)
        : _index{ index }, _type{ type }, _desc{ desc }, _flags{ flags } {}
};

struct WidgetDef {
    typedef void(*SetupWidgetFunc)(Widget&, Data::Variant&);

    SetupWidgetFunc _func;
    WidgetType _type;
    std::unordered_map<StringHash, PropDef> _props;
};

class WidgetManager {
    public:
        WidgetManager(Application* app);

        void registerWidgetType(WidgetType type, WidgetDef::SetupWidgetFunc func, std::unordered_map<StringHash, PropDef> props = {});

        Memory::PoolPointer<Widget> makeWidget(StringHash type, Data::Variant& props, Widget* parent, Ui::SceneInstance& scene);

    private:
        Memory::PoolAllocator _widgetPool;
        std::unordered_map<StringHash, WidgetDef> _defs;
		std::size_t _widGenerator;
        Application* _app;
};

}}

#endif