#ifndef Navi_Ui_Expr_h
#define Navi_Ui_Expr_h

#include <string>
#include <Corrade/Containers/Optional.h>
#include "Navi/Navi.h"
#include "Navi/Data/Variant.h"

namespace Navi::Ui {

enum class ExprType {
    Animation,
    Console,
    ConsoleVar,
    Signal,
    Var,
};

struct Expr {
    ExprType type;
    std::string name;
};

Containers::Optional<Expr> parseExpr(const std::string& str);

}

#endif