#include "Navi/Ui/FileBrowser.h"
#include "Navi/Ui/Controller.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Widget.h"
#include "Navi/Ui/Ui.h"

namespace Navi::Ui::FileBrowser {

class Controller : public Ui::Controller {
    public:
        struct Vars {
            Ui::Var<std::string> title;
            Ui::Var<std::string> basePath;
            Ui::Var<int> flags;
        };

        static StringHash controllerName() { return "FileBrowserController"; }
        
        explicit Controller(SceneInstance& scene) : Ui::Controller{ scene } {
            bindVar("title", _vars.title);
            bindVar("base_path", _vars.basePath);
            bindVar("flags", _vars.flags);
            scene.setVar(scene.varIndex("open"), true);

            connect("select", *this, &Controller::handleSelect);
            connect("cancel", *this, &Controller::handleCancel);
        }

        void setParams(const std::string& title, const std::string& pwd, FileBrowserFlags flags) {
            _vars.title = title;
            _vars.basePath = pwd;
            _vars.flags = (int)(unsigned int)flags;
        }

        void handleSelect(const std::string& filename) {
            _mainSignal.emit({ filename });
            destroyScene();
        }

        void handleCancel(const Data::Variant&) {
            _mainSignal.emit({});
            destroyScene();
        }

        Vars _vars;
        Signal<Containers::Optional<std::string>> _mainSignal;
};

void registerController(Application& app) {
    app.registerController<Controller>();
}

Signal<Containers::Optional<std::string>>& create(const std::string& title, const std::string& basePath, FileBrowserFlags flags, SceneInstance& parent) {
    auto& instance = createSubScene("res://scenes/filebrowser.xml", parent.rootWidget(), true);

    auto& ctrl = instance.controller<Controller>();
    ctrl.setParams(title, basePath, flags);
    return ctrl._mainSignal;
}

Signal<Containers::Optional<std::string>>& selectFile(const std::string& title, const std::string& basePath, SceneInstance& parent) {
    return create(title, basePath, FileBrowserFlag::None, parent);
}

Signal<Containers::Optional<std::string>>& selectNewFile(const std::string& title, const std::string& basePath, SceneInstance& parent) {
    return create(title, basePath, FileBrowserFlag::EnterNewFilename, parent);
}

Signal<Containers::Optional<std::string>>& selectDirectory(const std::string& title, const std::string& basePath, SceneInstance& parent) {
    return create(title, basePath, FileBrowserFlag::SelectDirectory, parent);
}

}