#include <Corrade/Containers/StridedArrayView.h>
#include "Navi/Ui/Font.h"
#include "Navi/Resource/ResourceManager.hpp"
#include "Navi/Application.h"

#ifdef NAVI_IMGUI
#include "Navi/Renderer/DearImGui/Renderer.hpp"
#endif

namespace Navi::Ui {

void FontLoader::doBegin(Font& font, Resource::ResourceManager& mgr) {
    auto param = mgr.param(font.id());
    auto filename = param->find("filename");
    font.addFile(mgr.findFile(filename->cast<std::string>()));
}

void FontLoader::doLoadAsyncPart(Font& font, Resource::ResourceManager& mgr) {
    font.file(0).cache();
}

void FontLoader::doLoadSyncPart(Font& font, Resource::ResourceManager& mgr) {
    auto data = font.file(0).readBytes();
    void* rendererFont = Application::instance().renderer().processFontData(data, 16.0f);
    font.setRendererFont(rendererFont);
}

}
