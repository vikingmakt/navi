#ifndef Navi_Ui_Style_h
#define Navi_Ui_Style_h

#include <unordered_map>
#include <Corrade/Containers/Reference.h>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Ui/Ui.h"
#include "Navi/Data/LinkedList.h"
#include "Navi/Data/Variant.h"
#include "Navi/Resource/ResourceManager.h"
#include "Navi/Ui/Font.h"
#include "Navi/StringHash.h"
#include "Navi/Animation/Animation.h"

namespace Navi::Ui {

enum class StyleVar : std::size_t {
    BeginFloat,
        
    Alpha,               // float     Alpha
    WindowRounding,      // float     WindowRounding
    WindowBorderSize,    // float     WindowBorderSize
    ChildRounding,       // float     ChildRounding
    ChildBorderSize,     // float     ChildBorderSize
    PopupRounding,       // float     PopupRounding
    PopupBorderSize,     // float     PopupBorderSize
    FrameRounding,       // float     FrameRounding
    FrameBorderSize,     // float     FrameBorderSize
    IndentSpacing,       // float     IndentSpacing
    ScrollbarSize,       // float     ScrollbarSize
    ScrollbarRounding,   // float     ScrollbarRounding
    GrabMinSize,         // float     GrabMinSize
    GrabRounding,        // float     GrabRounding
    TabRounding,         // float     TabRounding

    EndFloat,
    BeginVec2,

    WindowPadding,       // ImVec2    WindowPadding
    WindowMinSize,       // ImVec2    WindowMinSize
    WindowTitleAlign,    // ImVec2    WindowTitleAlign
    FramePadding,        // ImVec2    FramePadding
    ItemSpacing,         // ImVec2    ItemSpacing
    ItemInnerSpacing,    // ImVec2    ItemInnerSpacing
    ButtonTextAlign,     // ImVec2    ButtonTextAlign
    SelectableTextAlign, // ImVec2    SelectableTextAlign

    EndVec2,
    BeginColor,

    TextColor,

    EndColor,
    BeginPointer,

    Font,                // Font*

    EndPointer,
    BeginCustom,              // Base for custom vars
};

struct StyleSetting {
    StyleVar var;
    Data::Variant value;
    Animation::AnimationId animationId = Animation::AnimationIdInvalid;
    Style* style;
};

class Style : public Resource::AbstractResource {
    public:
        static StringHash resourceTypeHash() { return "Style"; }

        explicit Style(const std::string& id) : Resource::AbstractResource{ id } {}

        void addSettings(StringHash key, Data::LinkedList<StyleSetting>&& settings);

        void addFont(StringHash key, const Resource::ResourceHandle<Ui::Font>& fh);

        void addAnimation(StringHash key, const Animation::Animation& anim);

        auto& settings() { return _settings; }

        auto& fonts() { return _fonts; }

        auto& animations() { return _animations; }

    protected:
        StringHash doTypeHash() override { return resourceTypeHash(); }

    private:
        std::unordered_map<StringHash, Data::LinkedList<StyleSetting>> _settings;
        std::unordered_map<StringHash, Animation::Animation> _animations;
        std::unordered_map<StringHash, Resource::ResourceHandle<Ui::Font>> _fonts;
};

bool isFloat(StyleVar var);

bool isVec2(StyleVar var);

bool isColor(StyleVar var);

bool isPointer(StyleVar var);

}

#endif
