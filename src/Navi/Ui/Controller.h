#ifndef Navi_Ui_Controller_h
#define Navi_Ui_Controller_h

#include <functional>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Data/Variant.h"
#include "Navi/Ui/Var.h"

namespace Navi { namespace Ui {

namespace Implementation {

template<class T> struct ArgCaster {
    static T cast(Data::Variant& val) {
        return val.cast<T>();
    }
};

template<class T> struct ArgCaster<T*> {
    static T* cast(Data::Variant& val) {
        return val.castPointer<T>();
    }
};

}

class SceneInstance;

struct ControllerVarBinding {
    std::function<void()> _bindingFunc;
};

class Controller : public Interconnect::Receiver {
    public:
        explicit Controller(SceneInstance& scene);

        virtual ~Controller() {}

        /// Destroy this controller's scene instance, only works when we're in a sub scene.
        /// The controller will immediately be destroyed as well, making it invalid
        /// to interact with after calling this.
        void destroyScene();

        void updateBindings();

        void updateReverseBindings();

        void handlePreRender(float dt);

        void handlePostRender(float dt);

        /// Bind a variable to a mutable reference, supporting two-way binding
        template<typename T> void bindVar(const std::string& name, Var<T>& ref) {
            // bind the var via type erasure (for now)
            auto ptr = &ref;
            *ptr = Var<T>{ *_scene, _scene->addVar(name, {}) };
            auto func = [=]() {
                ptr->updateContainerSize();
            };
            func();
            _bindings.push_back({ func });
        }

        /// Connects to a scene signal
        template<class Receiver, class Arg> Interconnect::Connection connect(StringHash name, Receiver& r, void (Receiver::* handler)(Arg)) {
            // TODO: validate arg type
            auto rptr = &r;
            return _scene->signal(name).connect([=](Data::Variant val) {
                (rptr->*handler)(Implementation::ArgCaster<Arg>::cast(val));
            });
        }

        /// Connects to a scene signal, lambda version
        template<class Arg, class Functor> Interconnect::Connection connect(StringHash name, Functor&& slot) {
            return _scene->signal(name).connect([=](Data::Variant val) {
                slot(Implementation::ArgCaster<Arg>::cast(val));
            });
        }

    protected:
        std::vector<ControllerVarBinding> _bindings;
        SceneInstance* _scene;
        bool _markedForDelete;
};


class NullController : public Controller {
    public:
        static StringHash controllerName() { return "NullController"; }

        explicit NullController(Ui::SceneInstance& scene) : Controller{ scene } {}
};

}}

#endif
