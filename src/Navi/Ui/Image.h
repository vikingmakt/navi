#ifndef Navi_Ui_Image_h
#define Navi_Ui_Image_h

#include <Corrade/Containers/Pointer.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/GL/Texture.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Resource/AbstractResource.h"

namespace Navi::Ui {

class Image : public Resource::AbstractResource {
    public:
        static StringHash resourceTypeHash() { return "Image"; }

        explicit Image(const std::string& id) : Resource::AbstractResource{ id } {}

        GL::Texture2D& texture() { return _texture; }

        Trade::ImageData2D& imageData() { return *_imageData; }

        void setImageData(Trade::ImageData2D&& data) { _imageData = Containers::Pointer<Trade::ImageData2D>{ new Trade::ImageData2D{ std::move(data) } }; }

    protected:
        virtual StringHash doTypeHash() { return resourceTypeHash(); }

    private:
        Containers::Pointer<Trade::ImageData2D> _imageData;
        GL::Texture2D _texture;
};

class ImageLoader : public Resource::AbstractResourceLoader<Image> {
    protected:
        void doBegin(Image& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Image& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Image& res, Resource::ResourceManager& mgr) override;
};

}

#endif
