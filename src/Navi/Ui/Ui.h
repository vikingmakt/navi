#ifndef Navi_Ui_Ui_h
#define Navi_Ui_Ui_h

#include "Navi/StringHash.h"

namespace Navi { namespace Ui {

class Widget;
class WidgetManager;
class Controller;
class Scene;
class SceneInstance;
class SceneInstanceFactory;
class Style;
enum class StyleVar : std::size_t;
struct StyleSetting;

SceneInstance& createSubScene(const std::string& sceneId, Widget& parent, bool forceSync = false);

void removeFromParent(Widget& w);

}}

#endif