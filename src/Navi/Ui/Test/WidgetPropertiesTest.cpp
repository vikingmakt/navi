/*
    This file is part of Navi.

    Copyright © 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
              Vladimír Vondruš <mosra@centrum.cz>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/

#include <Corrade/TestSuite/Tester.h>

#include "Navi/Ui/WidgetProperties.h"

namespace Navi { namespace Ui { namespace Test { namespace {

using namespace Corrade;

struct WidgetPropertiesTest: TestSuite::Tester {
    explicit WidgetPropertiesTest();

    void constructTest();
    void bindTest();
};

WidgetPropertiesTest::WidgetPropertiesTest() {
    addTests({
        &WidgetPropertiesTest::constructTest,
        &WidgetPropertiesTest::bindTest});
}

void WidgetPropertiesTest::constructTest() {
    WidgetProperties<std::string, int, float, double, bool> props;
    CORRADE_COMPARE(props.get<0>().size(), 0);
    CORRADE_COMPARE(props.get<1>(), 0);
    CORRADE_VERIFY(!props.get<4>());
}

void WidgetPropertiesTest::bindTest() {
    WidgetProperties<std::string, int> props;
    Data::Observable<std::string> obs{ "Hello World" };

    props.set<0>(obs);
    CORRADE_VERIFY(props.get<0>() == "Hello World");

    obs.setValue("Value Changed");
    CORRADE_VERIFY(props.get<0>() == "Value Changed");
}

}}}}

CORRADE_TEST_MAIN(Navi::Ui::Test::WidgetPropertiesTest)
