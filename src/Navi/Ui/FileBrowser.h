#ifndef Navi_Ui_FileBrowser_h
#define Navi_Ui_FileBrowser_h

#include <Corrade/Containers/Optional.h>
#include "Navi/Signal.h"
#include "Navi/Ui/Widget.h"

namespace Navi { class Application; }

namespace Navi::Ui { class SceneInstance; }

namespace Navi::Ui::FileBrowser {

void registerController(Application& app);

Signal<Containers::Optional<std::string>>& create(const std::string& title, const std::string& basePath, FileBrowserFlags flags, SceneInstance& parent);

Signal<Containers::Optional<std::string>>& selectFile(const std::string& title, const std::string& basePath, SceneInstance& parent);

Signal<Containers::Optional<std::string>>& selectNewFile(const std::string& title, const std::string& basePath, SceneInstance& parent);

Signal<Containers::Optional<std::string>>& selectDirectory(const std::string& title, const std::string& basePath, SceneInstance& parent);

}

#endif