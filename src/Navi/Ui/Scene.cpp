#include "Navi/Ui/Scene.h"
#include "Navi/Application.h"

namespace Navi { namespace Ui {


SceneInstance::SceneInstance(const Resource::ResourceHandle<Scene> & res, Widget* owner)
    : _scene{ res }, _owner{ owner } {

    if (res.loaded()) {
        handleSceneLoaded(nullptr);
    }
    else {
        _scene.resource().finishedLoading().connect(*this, &SceneInstance::handleSceneLoaded);
    }
}

SceneInstance::~SceneInstance() {
    for (auto& conn : _connections) {
        _app->tick().disconnect(conn);
    }
}

int SceneInstance::addVar(const std::string& name, const Data::Variant& value) {
    auto it = _varsIndex.find(name);
    if (it != _varsIndex.end()) {
        // var already exists
        return it->second;
    }

    int index = _vars.size();
    _vars.push_back(value);
    _varsIndex[name] = index;
    return index;
}

/// Returns index of the var named [name]
int SceneInstance::varIndex(const std::string& name) {
    auto it = _varsIndex.find(name);
    if (it != _varsIndex.end()) {
        return it->second;
    }

    return -1;
}

/// Sets the value of the var at the index [index]
void SceneInstance::setVar(int index, const Data::Variant& value) {
    _vars[index] = value;
}

/// Returns a reference to the var at the index [index]
Data::Variant& SceneInstance::var(int index) {
    return _vars[index];
}

void SceneInstance::handleSceneLoaded(Resource::AbstractResource*) {
    _scene.resource()._instanceFactory->createSceneInstance(*this);
    sceneMounted();
}

void SceneInstance::sceneMounted() {
    _buildWidgetsMap(*_rootWidget);
    
    for (auto& pair : _signals) {
        _connections.push_back(_app->addDeferredSignal(_app->tick(), pair.second, "scene signal"));
    }
}

Widget* SceneInstance::findWidgetById(const StringHash& id) const {
    auto it = _nodesById.find(id);
    if (it == _nodesById.end())
        return nullptr;

    return it->second;
}

Animation::Animation* SceneInstance::findAnimation(const StringHash& name) {
    if (_inlineStyle) {
        auto it = _inlineStyle->animations().find(name);
        if (it != _inlineStyle->animations().end()) {
            return &it->second;
        }
    }

    for (auto& style : _styles) {
        auto it = style.resource().animations().find(name);
        if (it != style.resource().animations().end()) {
            return &it->second;
        }
    }

    return nullptr;
}

void SceneInstance::_buildWidgetsMap(Widget& node) {
    node._scene = this;

    if (!node._id.empty()) {
        _nodesById[node._id] = &node;
    }

    for (const auto& child : node._children) {
        _buildWidgetsMap(*child);
    }
}

}}
