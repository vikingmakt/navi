#ifndef Navi_Ui_Var_h
#define Navi_Ui_Var_h

#include "Navi/StringHash.h"
#include "Navi/Ui/Scene.h"

namespace Navi { namespace Ui {

class Controller;

namespace Implementation {
    template<class T> class VarHolder {
    };

    template<class T> class VarHolder<std::vector<T>> {
        public:
            std::vector<T> _vec;
    };

    template<class T> class VarGetter {
        public:
            static T& get(SceneInstance& scene, VarHolder<T>& holder, int index) {
                return scene.var(index).cast<T>();
            }
    };

    template<class T> class VarGetter<std::vector<T>> {
        public:
            static std::vector<T>& get(SceneInstance& scene, VarHolder<std::vector<T>>& holder, int index) {
                return holder._vec;
            }
    };

    template<class T> class VarSetter {
        public:
            static void set(SceneInstance& scene, VarHolder<T>& holder, int index, const T& value) {
                return scene.setVar(index, Data::Variant{ value });
            }
    };

    template<class T> class VarSetter<std::vector<T>> {
        public:
            static T& get(SceneInstance& scene, VarHolder<T>& holder, int index, const std::vector<T>& value) {
                return scene.setVar(index, value.size());
            }
    };

    template<class T> class UpdateContainerSize {
        public:
            static void update(SceneInstance& scene, VarHolder<T>& holder, int index) {
            }
    };

    template<class T> class UpdateContainerSize<std::vector<T>> {
        public:
            static void update(SceneInstance& scene, VarHolder<std::vector<T>>& holder, int index) {
                scene.setVar(index, { (int)holder._vec.size() });
            }
    };
}


/// Var is a wrapper class that points to a scene variable, it allows the variable
/// to be modified from the controller and automatically update in the scene, and vice-versa
template<class T> class Var {
    public:
        Var() : _scene{ nullptr }, _index{ -1 } {}

        Var(SceneInstance& scene, int index) : _scene{ &scene }, _index{ index } {}

        Var(const Var& v) = delete;

        Var(Var&& v) { *this = v; }

        T& operator *() {
            return Implementation::VarGetter<T>::get(*_scene, _holder, _index);
        }

        T* operator ->() {
            return &Implementation::VarGetter<T>::get(*_scene, _holder, _index);
        }

        Var& operator =(const Var& v) = delete;

        Var& operator =(Var&& v) {
            _scene = v._scene;
            _index = v._index;
            v._scene = nullptr;
            v._index = -1;
            return *this;
        }

        Var& operator =(const T& value) {
            set(value);
            return *this;
        }

        void set(const T& value) {
            Implementation::VarSetter<T>::set(*_scene, _holder, _index, value);
        }

        bool valid() {
            return _scene != nullptr && _index >= 0;
        }

    private:
        friend class Controller;

        void updateContainerSize() {
            Implementation::UpdateContainerSize<T>::update(*_scene, _holder, _index);
        }

        Implementation::VarHolder<T> _holder;
        SceneInstance* _scene;
        int _index;
};

}}

#endif