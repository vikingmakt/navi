#include "Navi/Ui/Controller.h"
#include "Navi/Ui/Scene.h"
#include "Navi/Ui/Ui.h"
#include "Navi/Application.h"

namespace Navi::Ui {

Controller::Controller(SceneInstance& scene)
    : _scene{ &scene }, _markedForDelete{ false } {

    scene.app().preRender().connect(*this, &Controller::handlePreRender);
    scene.app().postRender().connect(*this, &Controller::handlePostRender);
}

void Controller::destroyScene() {
    if (_scene->_owner) {
        _markedForDelete = true;
    }
}

void Controller::updateBindings() {
    for (auto& binding : _bindings) {
        binding._bindingFunc();
    }
}

void Controller::updateReverseBindings() {
}

void Controller::handlePreRender(float dt) {
    updateBindings();
}

void Controller::handlePostRender(float dt) {
    if (_markedForDelete) {
        removeFromParent(*_scene->_owner);
        return;
    }

    //updateReverseBindings();
}


}