#include <Corrade/Containers/StridedArrayView.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/TextureFormat.h>
#include "Navi/Ui/Image.h"
#include "Navi/Resource/ResourceManager.hpp"

namespace Navi::Ui {

void ImageLoader::doBegin(Image& img, Resource::ResourceManager& mgr) {
    auto param = mgr.param(img.id());
    auto filename = param->find("filename");
    img.addFile(mgr.findFile(filename->cast<std::string>()));
}

void ImageLoader::doLoadAsyncPart(Image& img, Resource::ResourceManager& mgr) {
    Containers::Pointer<Trade::AbstractImporter> importer = mgr.imageImporterManager().loadAndInstantiate("AnyImageImporter");
    auto data = img.file(0).readBytes();

    if (!importer || !importer->openData({ (const char* )data.data(), data.size() })) {
        Utility::Error{} << "Can't open image";
    }

    Containers::Optional<Trade::ImageData2D> image = importer->image2D(0);
    if(!image) Fatal{} << "Importing the image failed";

    // TODO: for some reason the image is imported with the Y coordinate inverted so we have to uninvert it, investigate this
    auto imgSize = image->size();
    auto imgData = image->release();
    auto finalData = Containers::Array<char>{ Containers::NoInit, imgData.size() };
    for (int y = 0; y < imgSize.y(); y++) {
        for (int x = 0; x < imgSize.x(); x++) {
            int srcIndex = ((y * imgSize.x()) + x) * image->pixelSize();
            int destIndex = (((imgSize.y() - y - 1) * imgSize.x()) + x) * image->pixelSize();
            char* src = imgData.begin() + srcIndex;
            char* dest = finalData.begin() + destIndex;

            for (unsigned int i = 0; i < image->pixelSize(); i++) {
                dest[i] = src[i];
            }
        }
    }

    auto finalImage = Trade::ImageData2D{ image->storage(), image->format(), imgSize, std::move(finalData), image->importerState() };
    img.setImageData(std::move(finalImage));
}

void ImageLoader::doLoadSyncPart(Image& img, Resource::ResourceManager& mgr) {
    img.texture().setWrapping(GL::SamplerWrapping::ClampToEdge)
        .setMagnificationFilter(GL::SamplerFilter::Nearest)
        .setMinificationFilter(GL::SamplerFilter::Nearest)
        .setStorage(1, GL::textureFormat(img.imageData().format()), img.imageData().size())
        .setSubImage(0, {}, img.imageData());
}

}
