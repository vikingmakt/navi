#ifndef Navi_Ui_Font_h
#define Navi_Ui_Font_h

#include <Corrade/Containers/Pointer.h>
#include "Navi/Navi.h"
#include "Navi/StringHash.h"
#include "Navi/Resource/AbstractResource.h"

namespace Navi::Ui {

class Font : public Resource::AbstractResource {
    public:
        static StringHash resourceTypeHash() { return "Font"; }

        explicit Font(const std::string& id) : Resource::AbstractResource{ id } {}

        void* rendererFont() const { return _rendererFont; }

        void setRendererFont(void* font) { _rendererFont = font; }

    protected:
        virtual StringHash doTypeHash() { return resourceTypeHash(); }

    private:
        void* _rendererFont;
};

class FontLoader : public Resource::AbstractResourceLoader<Font> {
    protected:
        void doBegin(Font& res, Resource::ResourceManager& mgr) override;
        void doLoadAsyncPart(Font& res, Resource::ResourceManager& mgr) override;
        void doLoadSyncPart(Font& res, Resource::ResourceManager& mgr) override;
};

}

#endif
