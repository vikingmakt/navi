#include "Navi/Animation/Animation.h"
#include "Navi/Application.h"
#include "Navi/Console.hpp"

namespace Navi::Animation {

namespace {
float lerp(float v0, float v1, float t) {
    return (1 - t) * v0 + t * v1;
}

void lerpn(float* dest, const float* src1, const float* src2, float t, int count) {
    for (int i = 0; i < count; i++) {
        dest[i] = lerp(src1[i], src2[i], t);
    }
}

void animateStep(Animation& anim, float t) {
    const auto& frame1 = anim.frames[anim.frame - 1];
    const auto& frame2 = anim.frames[anim.frame];
    const auto& value1 = frame1.value;
    const auto& value2 = frame2.value;
    if (t < frame2.time)
        anim.value = value1;
    else
        anim.value = value2;
}

void animateLinear(Animation& anim, float t) {
    const auto& frame1 = anim.frames[anim.frame - 1];
    const auto& frame2 = anim.frames[anim.frame];
    const auto& value1 = frame1.value;
    const auto& value2 = frame2.value;
    float x = (t - frame1.time) / (frame2.time - frame1.time);
    switch (anim.value.type()) {
    case Data::VariantType::Int:
        anim.value = int(std::floorf(lerp(
            static_cast<float>(value1.cast<int>()),
            static_cast<float>(value2.cast<int>()),
            x
        )));
        break;
    case Data::VariantType::Float:
        *anim.value.floatPtr() = lerp(*value1.floatPtr(), *value2.floatPtr(), x);
        break;
    case Data::VariantType::Float2:
        lerpn(anim.value.floatPtr(), value1.floatPtr(), value2.floatPtr(), x, 2);
        break;
    case Data::VariantType::Float3:
        lerpn(anim.value.floatPtr(), value1.floatPtr(), value2.floatPtr(), x, 3);
        break;
    case Data::VariantType::Float4:
        lerpn(anim.value.floatPtr(), value1.floatPtr(), value2.floatPtr(), x, 4);
        break;
    default:
        // TODO: error?
        break;
    }
}
}

AnimationManager::AnimationManager(Application& app)
    : _app(&app) {

    app.tick().connect(*this, &AnimationManager::tick);
}

AnimationId AnimationManager::createAnimation() {
    AnimationId id = _animations.insert(Animation{});
    _animations.disable(id);
    return id;
}

AnimationId AnimationManager::createAnimation(const Animation& data) {
    AnimationId id = _animations.insert(data);
    if (data.autoplay) {
        play(id);
    }
    else {
        _animations.disable(id);
    }
    return id;
}

void AnimationManager::destroyAnimation(AnimationId id) {
    _animations.remove(id);
}

void AnimationManager::setKeyFrame(AnimationId id, float time, const Data::Variant& value) {
    auto& anim = _animations.get(id);
    anim.frames.push_back({ time, value });
}

void AnimationManager::setTotalTime(AnimationId id, float time) {
    auto& anim = _animations.get(id);
    anim.totalTime = time;
}

void AnimationManager::setLoop(AnimationId id, bool loop) {
    auto& anim = _animations.get(id);
    anim.loop = loop;
}

void AnimationManager::setEase(AnimationId id, Ease ease) {
    auto& anim = _animations.get(id);
    anim.ease = ease;
}

void AnimationManager::play(AnimationId id, float t) {
    auto& anim = _animations.get(id);
    if (anim.frames.size() < 2) {
        Console::error() << "invalid animation, need at least 2 keyframes\n";
        return;
    }

    anim.time = t * anim.totalTime;
    anim.frame = 1;
    std::sort(anim.frames.begin(), anim.frames.end(), [](const KeyFrame& a, const KeyFrame& b) {
        return a.time < b.time;
    });
    _animations.enable(id);
    anim.value = anim.frames[0].value;
}

void AnimationManager::stop(AnimationId id) {
    _animations.disable(id);
}

const Data::Variant& AnimationManager::animationValue(AnimationId id) {
    return _animations.get(id).value;
}

void AnimationManager::tick(float dt) {
    for (AnimationId id : _animations.aliveList) {
        if (!_animations.isEnabled(id)) continue;

        auto& anim = _animations.get(id);
        float t = anim.time / anim.totalTime;
        if (t >= 1.0f) {
            if (anim.loop) {
                anim.frame = 1;
                anim.time = 0.0f;
                t = 0.0f;
            }
            else {
                _animations.disable(id);
                continue;
            }
        }

        while (anim.frame < anim.frames.size() && t > anim.frames[anim.frame].time)
            anim.frame++;

        switch (anim.ease) {
        case Ease::Step:
            animateStep(anim, t);
        case Ease::Linear:
            animateLinear(anim, t);
        }
        anim.time += dt;
    }
}

}