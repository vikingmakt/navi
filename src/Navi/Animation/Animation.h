#ifndef Navi_Animation_Animation_h
#define Navi_Animation_Animation_h

#include <vector>
#include <Corrade/Interconnect/Receiver.h>
#include "Navi/Data/PoolArray.h"
#include "Navi/Data/Variant.h"

namespace Navi {
    class Application;
}

namespace Navi::Animation {

enum class Ease {
    Step,
    Linear,
};

struct KeyFrame {
    float time;
    Data::Variant value;
};

struct Animation {
    Data::Variant value;
    std::vector<KeyFrame> frames;
    float totalTime = 0;
    Ease ease = Ease::Linear;
    bool loop = false;
    bool autoplay = false;
    float time = 0;
    int frame = 0;
};

typedef Data::PoolArray<Animation>::Id AnimationId;

static constexpr AnimationId AnimationIdInvalid = Data::PoolArray<Animation>::IdInvalid;

class AnimationManager : public Interconnect::Receiver {
    public:
        explicit AnimationManager(Application&);

        AnimationId createAnimation();

        AnimationId createAnimation(const Animation& data);

        void destroyAnimation(AnimationId id);

        void setKeyFrame(AnimationId id, float time, const Data::Variant& value);

        void setTotalTime(AnimationId id, float time);

        void setLoop(AnimationId id, bool loop);

        void setEase(AnimationId id, Ease ease);

        void play(AnimationId id, float time = 0);

        void stop(AnimationId id);

        const Data::Variant& animationValue(AnimationId id);

    private:
        void tick(float dt);

        Application* _app;
        Data::PoolArray<Animation> _animations;
};

}

#endif